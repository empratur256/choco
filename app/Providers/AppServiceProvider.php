<?php

namespace Sedehi\Providers;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        /* $this->app->bind('path.public', function () {
             return base_path().'/public_html';
         });*/

        require_once app_path('Libs/jdate.php');
        require_once app_path('Libs/composers.php');
        require_once app_path('Libs/macros.php');
        require_once app_path('Libs/helpers.php');

        Carbon::setLocale(config('app.locale'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
