<?php

namespace Sedehi\Providers;

use Event;
use Exception;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\ServiceProvider;
use Log;

class SlowQueryLoggerProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen(QueryExecuted::class, function (QueryExecuted $queryExecuted) {

            $sql                     = $queryExecuted->sql;
            $bindings                = $queryExecuted->bindings;
            $time                    = $queryExecuted->time;
            $logSqlQueriesSlowerThan = config('site.query_time_to_log');

            if ($logSqlQueriesSlowerThan < 0 || $time < $logSqlQueriesSlowerThan) {
                return;
            }
            try {
                foreach ($bindings as $val) {
                    $sql = preg_replace('/\?/', "'{$val}'", $sql, 1);
                }
                Log::debug($time.'  '.$sql);
            } catch (Exception $e) {

            }

        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
