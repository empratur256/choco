<?php

namespace Sedehi\Console;

use Carbon\Carbon;
use DB;
use Exception;
use File;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Log;
use Schema;
use Sedehi\Http\Controllers\Coupon\Models\Code;
use Sedehi\Http\Controllers\Coupon\Models\PresentedCouponCode;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Payment\Models\Payment;
use Sedehi\Http\Controllers\Product\Models\Price;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\Payment\Models\Log as PaymentLog;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [//
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        set_time_limit(5000);
        $setting = (Schema::hasTable('setting')) ? Setting::first() : null;

        // hazfe sefaresh haye ekhtesasi ke takmil nashodan
        $schedule->call(function () use ($setting) {
            DB::beginTransaction();

            try {
                $orders = Order::where('valid', 0)
                                ->where('paid', 0)
                                ->where('type', 'custom')
                                ->where('created_at', '<', Carbon::now()->subHours(config('site.time_to_remove_invalid_order')))
                                ->delete();

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                Log::alert('moshkel dar cron job hazfe order haye ekhtesasi takmil nashode '.$e);
            }
        })->everyMinute();



        // hazfe sefaresh hae ke pardakht nashodan
        $schedule->call(function () use ($setting) {
            DB::beginTransaction();

            try {
                $orders = Order::where('paid', 0)
                               ->where('type', 'order')
                               ->with([
                                          'items' => function ($query) {
                                              $query->with([
                                                               'product' => function (
                                                                   $query
                                                               ) {
                                                                   $query->with('prices')
                                                                         ->withTrashed();
                                                               }
                                                           ]);
                                          }
                                      ])
                               ->where('created_at', '<', Carbon::now()->subHours($setting->cancel_order))
                               ->get();


                foreach ($orders as $order) {
                    foreach ($order->items as $item) {
                        $product = $item->product;
                        if (!is_null($product)) {
                            if (is_null($item->size_id)) {
                                $product->quantity += $item->quantity;
                                if ($product->order_count > 0) {
                                    $product->order_count -= 1;
                                } else {
                                    $product->order_count = 0;
                                }
                                $product->save();
                            } else {
                                $product->quantity += $item->quantity;
                                if ($product->order_count > 0) {
                                    $product->order_count -= 1;
                                } else {
                                    $product->order_count = 0;
                                }

                                $price = $product->prices()->where('size_id', $item->size_id)->first();
                                if (!is_null($price)) {
                                    $price->quantity += $item->quantity;
                                    $price->save();
                                }
                                $dbPrice            = Price::where('product_id', $product->id)->get();
                                $product->max_price = $dbPrice->max('price');
                                $product->min_price = $dbPrice->min('price');
                                $product->quantity  = $dbPrice->sum('quantity');
                                $product->price     = $dbPrice->min('price');

                                $product->save();
                            }
                        }
                    }
                }

                PresentedCouponCode::whereIn('order_id', $orders->pluck('id'))->update([
                                                                                           'used_at'      => null,
                                                                                           'user_used_id' => null,
                                                                                           'order_id'     => null
                                                                                       ]);

                Code::whereIn('order_id', $orders->pluck('id'))->update([
                                                                            'used_at'  => null,
                                                                            'user_id'  => null,
                                                                            'order_id' => null
                                                                        ]);
                Order::whereIn('id', $orders->pluck('id'))->delete();

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                Log::alert('moshkel dar cron job hazfe order '.$e);
            }
        })->everyMinute();

        // hazfe sefaresh ha hae hazf shode koli be sorate force delete
        /*$schedule->call(function () {
            Order::where('paid', 0)->onlyTrashed()->where('deleted_at', '<', Carbon::now()->subWeek())->forceDelete();
        })->everyMinute();*/

        // hazfe tmp file hae upload shode
        $schedule->call(function () {
            $tempUploadPath = 'uploads/tmp/';
            if (File::isDirectory(public_path($tempUploadPath))) {

                try {

                    $files = File::allFiles(public_path($tempUploadPath));
                    if (count($files) > 0) {
                        foreach ($files as $file) {
                            $fileName      = $file->getFilename();
                            $splitFileName = explode('_', $fileName);
                            $timestamp     = reset($splitFileName);
                            if (is_numeric($timestamp)) {
                                $date = Carbon::createFromTimestamp($timestamp);
                                if ($date->diffInHours() > config('site.time_to_remove_temp')) {
                                    if (File::exists(public_path($tempUploadPath.$fileName))) {
                                        File::delete(public_path($tempUploadPath.$fileName));
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception $exception) {
                    Log::alert('moshkel dar hazfe file haye temp '.$exception);
                }
            }
        })->everyMinute();

        // hazfe tarakonesh hae na movafagh
        $schedule->call(function () {
            Payment::where('created_at', '<', Carbon::now()->subMonths(2))->where('status', 0)->delete();
            PaymentLog::where('created_at', '<', Carbon::now()->subMonths(2))->delete();
        })->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
