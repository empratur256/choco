<?php
use Sedehi\Http\Controllers\Category\Models\Category;
use Sedehi\Http\Controllers\CustomOrder\Models\Portfolio;
use Sedehi\Http\Controllers\Page\Models\Page;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\Brand\Models\Brand;

view()->share('setting', (Schema::hasTable('setting')) ? (Setting::first()) : null);
view()->composer('views.layouts.admin.partials.head', function ($view) {
    $section     = '';
    $actionTilte = '';
    $actions     = explode('\\', request()->route()->getActionName());
    if (count($actions) < 6) {
        return $view->with('adminTitle', '')->with('adminBreadcrumb', '')->with('section', strtolower($section));
    }
    $section    = $actions[3];
    $controller = $actions[6];
    $methods    = explode('@', $controller);
    $method     = end($methods);
    $controller = reset($methods);
    if (File::exists(app_path('Http/Controllers/'.$section.'/roles.php'))) {
        $data         = include(app_path('Http/Controllers/'.$section.'/roles.php'));
        $sectionTitle = $data[strtolower($section)]['title'];
        foreach ($data[strtolower($section)]['access'][$controller] as $key => $value) {
            if (is_array($value)) {

                if (in_array($method, $value)) {

                    $actionTilte = $key;
                }
            } else {
                if (strtolower($value) == strtolower($method)) {
                    $actionTilte = $key;
                }
            }
        }

        return $view->with('adminTitle', $sectionTitle.' - '.$actionTilte)
                    ->with('adminBreadcrumb', $sectionTitle.' / '.$actionTilte)
                    ->with('section', strtolower($section));
    }

    return $view->with('adminTitle', '')->with('adminBreadcrumb', '');
});
view()->composer('views.layouts.site.partials.header', function ($view) {
    $rootCategories = Category::roots()->pluck('name_'.app()->getLocale(), 'id');
    $categories     = Category::all()->toHierarchy();

    $view->with('rootCategories', $rootCategories)->with('categories', $categories);
});

view()->composer('views.layouts.site.partials.footer', function ($view) {
    $footerPages = Page::select('id','title')->where('show_in_footer',1)->get();
    $aboutUsPage = Page::find(3);
    $photoBrands = Brand:: select('photo')->where('photo','<>',null)->take(6)->get();
    return $view->with('footerPages', $footerPages)->with('aboutUsPage', $aboutUsPage)->with('photoBrands', $photoBrands);
});
view()->composer('views.site.partials.favorite', function ($view) {

    $favorites = auth()->user()->favorite()->limit(3)->get();
    $view->with('favorites', $favorites);
});
view()->composer('CustomOrder.views.site.container', function ($view) {
    $steps      = collect();
    $steps->push([
        'trans'  => trans('site.carts.payment_info'),
        'action' => 'Sedehi\Http\Controllers\CustomOrder\Controllers\Site\OrderController@payment'
                 ]);
    $steps->push([
        'trans'  => trans('site.carts.shipping_info'),
        'action' => 'Sedehi\Http\Controllers\CustomOrder\Controllers\Site\OrderController@shipping'

                 ]);
    $steps->push([
        'trans'  => trans('site.custom_order.preview'),
        'action' => 'Sedehi\Http\Controllers\CustomOrder\Controllers\Site\OrderController@preview'

                 ]);

    if (session()->has('customOrderData.packaging_step')) {
        $steps->push([
            'trans'  => trans('site.custom_order.packaging'),
            'action' => 'Sedehi\Http\Controllers\CustomOrder\Controllers\Site\OrderController@packaging'

        ]);
    }

    $steps->push([
        'trans'  => trans('site.custom_order.order_cells'),
        'action' => 'Sedehi\Http\Controllers\CustomOrder\Controllers\Site\OrderController@cells'
                 ]);
    $steps->push([
        'trans'  => trans('site.custom_order.order_layout'),
        'action' => 'Sedehi\Http\Controllers\CustomOrder\Controllers\Site\OrderController@layout'
                 ]);
    $steps->push([
        'trans'  => trans('site.custom_order.choose_type'),
        'action' => 'Sedehi\Http\Controllers\CustomOrder\Controllers\Site\OrderController@index'
                 ]);
    $view->with('steps', $steps);
});
