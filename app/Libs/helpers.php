<?php
/**
 * Created by PhpStorm.
 * User: navid
 * Date: 5/26/16
 * Time: 3:09 AM
 */

if (!function_exists('permission')) {
    function permission($permission)
    {
        if (auth()->check()) {
            return auth()->user()->hasPermission($permission);
        }

        return false;
    }
}
if (!function_exists('humanFileSize')) {
    function humanFileSize($path, $decimals = 2)
    {
        $bytes = File::size($path);
        if ($bytes == 0) {
            return null;
        }
        $size   = [
            'بایت',
            'کیلوبایت',
            'مگابایت',
            'گیگابایت',
            'ترابایت',
            'PB',
            'EB',
            'ZB',
            'YB'
        ];
        $factor = floor((strlen($bytes) - 1) / 3);

        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)).' '.@$size[$factor];
    }
}

if (!function_exists('utf8_slug')) {
    function utf8_slug($title, $separator = '-')
    {
        $flip = $separator == '-' ? '_' : '-';

        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);

        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));

        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

        return trim($title, $separator);
    }
}

if (!function_exists('adminMenu')) {
    function adminMenu()
    {
        $list = scandir(app_path('Http/Controllers'));
        unset($list[0], $list[1]);
        $menu = [];
        foreach ($list as $dir) {
            if (\File::isDirectory(app_path('Http/Controllers/'.$dir))) {
                if (\File::exists(app_path('Http/Controllers/'.$dir.'/menu.php'))) {
                    $menu += include_once(app_path('Http/Controllers/'.$dir.'/menu.php'));
                }
            }
        }

        $unSort = [];
        foreach ($menu as $key => $row) {
            $unSort[$key] = $row['title'];
        }
        if (config('site.menu_sorting') == 'alphabet') {
            array_multisort($unSort, SORT_ASC, $menu);
        } else {

            return array_sort($menu, function ($value) {
                return (isset($value['order'])) ? $value['order'] : 1000;
            });
        }

        return $menu;
    }
}
if (!function_exists('adminRole')) {
    function adminRole()
    {
        $list = scandir(app_path('Http/Controllers'));
        unset($list[0], $list[1]);
        $role = [];
        foreach ($list as $dir) {
            if (\File::isDirectory(app_path('Http/Controllers/'.$dir))) {
                if (\File::exists(app_path('Http/Controllers/'.$dir.'/roles.php'))) {
                    $role += include_once(app_path('Http/Controllers/'.$dir.'/roles.php'));
                }
            }
        }

        return $role;
    }
}
if (!function_exists('generateUniqueCode')) {
    function generateUniqueCode($string = '', $lenght = 16)
    {
        $long = str_replace(".", "", uniqid(mt_rand()).time().str_random(15).time() * time());

        return $string.strtoupper(substr(str_shuffle(str_repeat($long, $lenght)), 0, $lenght));
    }
}
if (!function_exists('generateFileName')) {
    function generateFileName($filePath, $extension, $string = '', $lenght = 16)
    {
        $fileName = generateUniqueCode($string, $lenght);
        if (\File::exists($filePath.$fileName.$extension)) {
            $fileName = generateFileName($filePath, $extension, $string, $lenght);
        }

        return $fileName.'.'.$extension;
    }
}

if (!function_exists('removeImage')) {
    function removeImage($uploadPath, $originalFileName)
    {
        if (!is_null($originalFileName)) {
            foreach (File::allFiles($uploadPath) as $file) {
                if (preg_match('/'.$originalFileName.'/', $file->getFilename())) {
                    if (File::exists(public_path($uploadPath.$file->getFilename()))) {
                        File::delete(public_path($uploadPath.$file->getFilename()));
                    }
                }
            }
        }
    }
}

if (!function_exists('array_filter_recursive')) {

    function array_filter_recursive(array $array, callable $callback = null)
    {
        $array = is_callable($callback) ? array_filter($array, $callback) : array_filter($array);
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = call_user_func(__FUNCTION__, $value, $callback);
            }
        }

        return $array;
    }
}
if (!function_exists('renderNode')) {
    function renderNode($node)
    {
        $html = '';
        if ($node->isLeaf()) {
            $html .= '<li><a href="'.action('Product\Controllers\Site\ProductController@index',
                    [$node->id, utf8_slug($node->name)]).'">'.$node->name.'</a></li>';
        } else {
            $html .= '<li class="dropdown-submenu" ><a href="'.action('Product\Controllers\Site\ProductController@index',
                    [$node->id, utf8_slug($node->name)]).'"
                     class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false">'.$node->name.'<b class="flaticon-down-arrow"></b></a>';

            $html .= '<ul class="dropdown-menu" >';
            foreach ($node->children as $child) {
                $html .= renderNode($child);
            }

            $html .= '</ul>';
            $html .= '</li>';
        }

        return $html;
    }
}

if (!function_exists('finalPrice')) {
    function finalPrice($product, $size = null)
    {
        $price = 0;
        if (is_null($size)) {
            $price = $product->price;
        } else {
            $price = $size->price;
        }
        if ($product->discount > 0 && !is_null($product->discount)) {
            if (!is_null($product->discount_time_start) && !is_null($product->discount_time_end)) {
                if (time() > $product->discount_time_start->timestamp && time() < $product->discount_time_end->timestamp) {
                    if ($product->discount_type == 'price') {

                        return $price - $product->discount;
                    } elseif ($product->discount_type == 'percent') {
                        return $price - (($product->discount / 100) * $price);
                    }
                } else {
                    return $price;
                }
            } else {
                if ($product->discount_type == 'price') {
                    return $price - $product->discount;
                } elseif ($product->discount_type == 'percent') {
                    return $price - (($product->discount / 100) * $price);
                }
            }
        } else {
            return $price;
        }
    }
}
if (!function_exists('numberToPersianLetters')) {
    function numberToPersianLetters($money)
    {
        return (new \Sedehi\Libs\NumbersToString())->numToStr($money);
    }
}
if (!function_exists('remove_http')) {

    function remove_http($url)
    {
        $disallowed = array('http://', 'https://');
        foreach ($disallowed as $d) {
            if (strpos($url, $d) === 0) {
                return str_replace($d, '', $url);
            }
        }

        return $url;
    }
}

if (!function_exists('get_default_value')) {

    function get_default_value($fieldName,$cellNumber)
    {
        $defaultValue = null;

        $sessionChocolateData = session()->get('customOrderData.chocolate_data.'.$cellNumber);

        if (!is_null(old($fieldName.'.'.$cellNumber))) {
            $defaultValue = old($fieldName.'.'.$cellNumber);
        } elseif (!is_null($sessionChocolateData) && array_key_exists($fieldName,$sessionChocolateData) && !is_null($sessionChocolateData[$fieldName])) {
            $defaultValue = $sessionChocolateData[$fieldName];
        } elseif (!is_null($sessionChocolateData) && array_key_exists($fieldName,$sessionChocolateData) && is_null($sessionChocolateData[$fieldName])) {
            return null;
        } elseif (!is_null(session()->get('customOrderData.'.$fieldName))) {
            $defaultValue = session()->get('customOrderData.'.$fieldName);
        }

        return $defaultValue;
    }
}