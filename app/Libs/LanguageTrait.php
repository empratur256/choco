<?php

namespace Sedehi\Libs;

/**
 * Created by PhpStorm.
 * User: Navid Sedehi
 * Date: 4/25/2017 AD
 * Time: 2:58 PM
 */
trait LanguageTrait
{
    public function lang()
    {
        return app()->getLocale();
    }

    public function scopeLanguage($query)
    {
        return $query->where('language', app()->getLocale());
    }





}