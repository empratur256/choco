<?php
Html::macro('alert', function ($message, $class) {

    if (is_array($message)) {
        $alert = '<div class="alert '.$class.'">';
        $alert .= '<button type="button" class="close pull-left" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        foreach ($message as $msg) {
            $alert .= e($msg).'<br>';
        }
        $alert .= '</div>';

        return $alert;
    }

    $alert = '<div class="alert '.$class.'">';
    $alert .= '<button type="button" class="close pull-left" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    $alert .= e($message);
    $alert .= '</div>';

    return $alert;
});
Html::macro('notification', function ($message, $class) {

    if (is_array($message)) {
        $alert = '<script>';
        foreach ($message as $msg) {
            $alert .= 'UIkit.notify({';
            $alert .= 'message: "'.$msg.'",';
            $alert .= 'status: "'.$class.'",';
            $alert .= 'timeout: 5000,';
            $alert .= 'pos: "top-left"';
            $alert .= '});';
        }
        $alert .= ' </script > ';

        return $alert;
    }

    $alert = '<script>';
    $alert .= 'UIkit.notify({';
    $alert .= 'message: "'.$message.'",';
    $alert .= 'status: "'.$class.'",';
    $alert .= 'timeout: 5000,';
    $alert .= 'pos: "top-left"';
    $alert .= '});';
    $alert .= ' </script > ';


    return $alert;
});

HTML::macro('alerts', function ($errors = null) {

    if (Session::has('error')) {
        echo HTML::alert(Session::get('error'), 'alert-danger');
    }
    if (Session::has('warning')) {
        echo HTML::alert(Session::get('warning'), 'alert-warning');
    }
    if (Session::has('success')) {
        echo HTML::alert(Session::get('success'), 'alert-success');
    }
    if (Session::has('info')) {
        echo HTML::alert(Session::get('info'), 'alert-info');
    }
    if (!is_null($errors)) {
        if ($errors->any()) {
            echo HTML::alert($errors->all(), 'alert-danger');
        }
    }
});
HTML::macro('notifications', function ($errors = null) {

    if (Session::has('error')) {
        echo HTML::notification(Session::get('error'), 'danger');
    }

    if (Session::has('warning')) {
        echo HTML::notification(Session::get('warning'), 'warning');
    }
    if (Session::has('success')) {
        echo HTML::notification(Session::get('success'), 'success');
    }
    if (Session::has('info')) {
        echo HTML::notification(Session::get('info'), 'info');
    }
    if (!is_null($errors)) {
        if ($errors->any()) {
            echo HTML::notification($errors->all(), 'danger');
        }
    }
});

HTML::macro('activeAdmin', function ($url, $section = false) {
    $class = 'act_item';

    if ($section) {
        $class = 'current_section';
    }

    return (request()->url() == $url) ? $class : '';
});
