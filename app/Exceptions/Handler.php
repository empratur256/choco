<?php

namespace Sedehi\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Sedehi\Payment\PaymentException;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        PaymentException::class,

    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof PaymentException) {
            return redirect()
                ->action('User\Controllers\Site\DashboardController@index')
                ->with('error', $exception->getMessage());
        }
        if ($exception instanceof QueryException) {
            $errorCode = $exception->errorInfo[1];
            $error     = 'اطلاعات تکراری می باشد';
            if ($errorCode == 1062) {
                if ($request->has('return')) {
                    return redirect()->to($request->get('return'))->with('error', $error);
                }

                return redirect()->back()->with('error', $error);
            }
        }

        if ($exception instanceof TokenMismatchException) {
            $error = 'مشکل در انجام عملیات لطفا مجدد تلاش کنید';
            if ($request->has('return')) {
                return redirect()->to($request->get('return'))->with('error', $error);
            }

            return redirect()->back()->with('error', $error);
        }

        if ($exception instanceof ModelNotFoundException) {
            return abort(404);
        }
        if ($this->isHttpException($exception)) {
            return $this->customRenderHttpException($request, $exception);
        } else {
            if (config('app.debug')) {
                return parent::render($request, $exception);
            }
            /* Log::notice('error dar handler '.$e);

             return view('views.errors.default');*/
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }


    public function customRenderHttpException($request, $e)
    {
        $status = $e->getStatusCode();
        if (view()->exists("views.errors.{$status}")) {
            return response()->view("views.errors.{$status}", [], $status);
        } else {
            return parent::render($request, $e);
        }
    }
}
