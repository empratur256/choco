<?php

namespace Sedehi\Http\Middleware;

use Closure;

class SetLanguageFromDomain
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        $parse = parse_url($request->root());
        //$domain = config('site.domain_fa');
        /* if (isset($parse['host'])) {
             $domain = $parse['host'];
         }*/
        if ($request->hasCookie('lang')) {
            $lang = $request->cookie('lang');
            app()->setLocale($lang);
        } else {
            app()->setLocale('fa');
        }
        app()->setLocale('fa');

        /* if ($domain == config('site.domain_fa')) {
             app()->setLocale('fa');
         } elseif ($domain == config('site.domain_en')) {
             app()->setLocale('en');
         } else {
             app()->setLocale('fa');
         }*/

        return $next($request);
    }
}
