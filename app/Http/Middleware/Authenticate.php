<?php

namespace Sedehi\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $controllerAll = explode('\\', $request->route()->getActionName());
        $controller    = explode('@', end($controllerAll));


        $isAdmin = false;
        if (isset($controllerAll[5]) && strtolower($controllerAll[5]) == 'admin') {
            $isAdmin = true;
        }


        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                if ($isAdmin || (end($controller) == 'admin' && reset($controller) == 'HomeController') || reset($controller) == 'ElfinderController') {
                    $action = 'Auth\Controllers\Admin\AuthController@showLoginForm';
                } else {
                    $action = 'Auth\Controllers\Site\AuthController@showLoginForm';
                }

                return redirect()->action($action);
            }
        }

        if (Auth::guard($guard)->user()->ban) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                if ($isAdmin || (end($controller) == 'admin' && reset($controller) == 'HomeController')) {
                    $action = 'Auth\Controllers\Admin\AuthController@showLoginForm';
                } else {
                    $action = 'Auth\Controllers\Site\AuthController@showLoginForm';
                }

                return redirect()->action($action)->with('error', 'حساب کاربری شما مسدود شده است');
            }
        }

        return $next($request);
    }
}
