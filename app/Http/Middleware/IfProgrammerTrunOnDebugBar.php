<?php

namespace Sedehi\Http\Middleware;

use Barryvdh\Debugbar\Middleware\Debugbar;
use Closure;

class IfProgrammerTrunOnDebugBar
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check()) {
            if (auth()->user()->programmer) {
                \Config::set('app.debug', true);
                \Debugbar::enable();
            }
        }

        return $next($request);
    }
}
