<?php

namespace Sedehi\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Route;


class FileManager
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()->programmer == 1 && config('app.env') == 'production') {
            return $next($request);
        }

        if (!$this->auth->user()->hasAnyRole()) {

            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm');
            }
        }


        if ($this->auth->user()->hasPermission('filemanager')) {
            return $next($request);
        }

        if ($request->ajax()) {
            return response('Unauthorized.', 403);
        } else {
            return redirect()->action('HomeController@admin')->with('error', 'شمار دسترسی به این قسمت ندارید ');
        }
    }
}
