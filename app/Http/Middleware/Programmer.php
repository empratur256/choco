<?php

namespace Sedehi\Http\Middleware;

use Closure;

class Programmer
{


    public function handle($request, Closure $next)
    {
        if (auth()->check()) {

            if (auth()->user()->ban) {
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm');
                }
            }
            if (auth()->user()->programmer) {
                return $next($request);
            }
        }
        if ($request->ajax()) {
            return response('Unauthorized.', 401);
        } else {
            return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm');
        }
    }
}
