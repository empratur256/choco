<?php

namespace Sedehi\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Route;

class AdminPermission
{

    private $allowed = [
        'Sedehi\Http\Controllers\Auth\Controllers\Admin\AuthController@logout',
        'Sedehi\Http\Controllers\HomeController@admin',
        'Sedehi\Http\Controllers\User\Controllers\Admin\UserController@showProfile',
        'Sedehi\Http\Controllers\User\Controllers\Admin\UserController@profile',
        'Barryvdh\Elfinder\ElfinderController@showIndex',
    ];

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        app()->setLocale('fa');

        if ($this->auth->user()->programmer == 1 && config('app.env') == 'production') {
            return $next($request);
        }

        if (!$this->auth->user()->hasAnyRole()) {

            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm');
            }
        }

        if (in_array(Route::currentRouteAction(), $this->allowed)) {
            return $next($request);
        }

        $currentController = explode('\\', strtolower(Route::currentRouteAction()));

        $sectionName = $currentController[3];
        $method      = explode('@', end($currentController));
        $controller  = reset($method);
        $method      = end($method);

        if ($this->auth->user()->hasPermission($sectionName.'.'.$controller.'.'.$method)) {
            return $next($request);
        }

        if ($request->ajax()) {
            return response('Unauthorized.', 403);
        } else {
            return redirect()->action('HomeController@admin')->with('error', 'شمار دسترسی به این قسمت ندارید ');
        }
    }
}
