<?php

namespace Sedehi\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Sedehi\Http\Middleware\SetLanguageFromDomain;

class Kernel extends HttpKernel
{

    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Sedehi\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Sedehi\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \Sedehi\Http\Middleware\IfProgrammerTrunOnDebugBar::class,
            SetLanguageFromDomain::class,
        ],

        'api'       => [
            'throttle:60,1',
            'bindings',
        ],
        'admin'     => [
            \Sedehi\Http\Middleware\Authenticate::class,
            \Sedehi\Http\Middleware\AdminPermission::class,
        ],
        'dashboard' => [
            \Sedehi\Http\Middleware\Authenticate::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'            => \Sedehi\Http\Middleware\Authenticate::class,
        'auth.basic'      => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings'        => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can'             => \Illuminate\Auth\Middleware\Authorize::class,
        'guest'           => \Sedehi\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle'        => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'AdminPermission' => \Sedehi\Http\Middleware\AdminPermission::class,
        'Programmer'      => \Sedehi\Http\Middleware\Programmer::class,
        'filemanager'     => \Sedehi\Http\Middleware\FileManager::class,
    ];
}
