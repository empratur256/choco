<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Comments\Controllers\Admin',
             ], function(){
    Route::get('comments', 'CommentsController@index');
    Route::post('comments/status', 'CommentsController@status');
});
Route::post('product/{id}/comment', 'Comments\Controllers\Site\CommentsController@create')->middleware('dashboard');