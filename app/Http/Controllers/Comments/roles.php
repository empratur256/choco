<?php
return [
    'comments' => [
        'title'  => 'نظر کاربران',
        'access' => [
            'CommentsController' => [
                'لیست'        => 'index',
                'تغییر وضعیت' => 'status',
            ],
        ],
    ],
];
