<aside id="sidebar_secondary" class="tabbed_sidebar" style="padding-top: 0px;width: 400px">
    <ul class="uk-tab uk-tab-icons uk-tab-grid" data-uk-tab="{connect:'#dashboard_sidebar_tabs', animation:'slide-horizontal'}"></ul>

    <div class="scrollbar-inner">
        <ul id="dashboard_sidebar_tabs" class="uk-switcher">
            <li>
                <div class="timeline timeline_small uk-margin-bottom">
                    {!! Form::open(['method' => 'GET']) !!}
                    <div class="uk-form-row searchPanel">
                        @if(request()->has('confirm'))
                            {!! Form::hidden('confirm',request('confirm')) !!}
                        @endif
                        @if(request()->has('reject'))
                            {!! Form::hidden('reject',request('reject')) !!}
                        @endif
                        @if(request()->has('user_id'))
                            {!! Form::hidden('user_id',request('user_id')) !!}
                        @endif
                        @if(request()->has('product_id'))
                            {!! Form::hidden('product_id',request('product_id')) !!}
                        @endif
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                {!! Form::selectRange('rate',0,5,request('rate'),['class'=>'md-input','placeholder' => 'امتیاز']) !!}
                            </div>
                            <div class="uk-width-medium-1-1">
                                {!! Form::label('email','ایمیل کاربر') !!}
                                {!! Form::text('email',request('email'),['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-1">
                                {!! Form::select('language',['' => 'ارسال شده در سایت'] +config('app.locales'),request('language'),['class'=>'md-input']) !!}
                            </div>
                        </div>
                        
                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-wave-light waves-effect waves-button waves-light" style="margin-top: 10px">جستجو</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </li>
        </ul>
    </div>
</aside>
