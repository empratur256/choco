@extends('views.layouts.admin.master')
@section('content')
    @include('Comments.views.admin.comments.search')
    {!! Form::open(['action'=>['Comments\Controllers\Admin\CommentsController@status']]) !!}

    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('Comments.CommentsController.status'))
                                <button type="submit" class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">ارسال اطلاعات</button>
                            @endif
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Comments\Controllers\Admin\CommentsController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>

                            <th class="uk-width-2-10 uk-text-center">محصول</th>
                            <th class="uk-width-2-10 uk-text-center">کاربر</th>
                            <th class="uk-width-2-10 uk-text-center">ارسال در سایت</th>
                            <th class="uk-width-2-10 uk-text-center">امتیاز</th>
                            <th class="uk-width-2-10 uk-text-center">وضعیت</th>
                            @if(permission('Comments.CommentsController.status'))
                                <th class="uk-width-2-10 uk-text-center">تغییر وضعیت</th>
                            @endif
                            <th class="uk-width-2-10 uk-text-center">تاریخ درج</th>
                            <th class="uk-width-2-10 uk-text-center">.....</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            {!! Form::hidden('product['.$item->product_id.']',1) !!}
                            <tr class="uk-text-center">
                                <td>
                                    <a href="{!! action('Product\Controllers\Admin\ProductController@show',$item->product_id) !!}" target="_blank">{{ $item->product->title }}</a>
                                </td>
                                <td>
                                    <a href="{!! action('User\Controllers\Admin\UserController@showInfo',$item->user_id) !!}" target="_blank">{{ $item->user->name.' '.$item->user->family }}</a>
                                </td>
                                <td>{{config('app.locales.'.$item->language)}}</td>
                                <td>{{$item->rate}}</td>
                                <td>
                                    @if($item->reject)
                                        <span class="uk-badge uk-badge-danger">عدم تایید</span>
                                    @else
                                        @if($item->confirm)
                                            <span class="uk-badge uk-badge-success">تایید شده</span>
                                        @else
                                            <span class="uk-badge uk-badge-warning">نیاز به بررسی</span>
                                        @endif
                                    @endif

                                </td>
                                @php $status = 0 @endphp
                                @if($item->confirm == 1)
                                    @php $status = 1 @endphp
                                @elseif($item->reject == 1)
                                    @php $status = 2 @endphp
                                @endif
                                @if(permission('Comments.CommentsController.status'))
                                    <td>
                                        <div class="uk-width-medium-1-1">
                                            {!! Form::select('status['.$item->id.']',[0 => 'نیاز به بررسی',1 => 'تایید شده',2 => 'عدم تایید',3 => 'حذف'],$status,['class'=>'md-input','placeholder' => '....']) !!}
                                        </div>
                                    </td>
                                @endif
                                <td>
                                    <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                </td>
                                <td class="uk-text-center">
                                    <a href="#" data-uk-modal="{target:'#show_{{$item->id}}'}"><i class="md-icon material-icons">visibility</i></a>
                                    <div class="uk-modal" id="show_{{$item->id}}" aria-hidden="true">
                                        <div class="uk-modal-dialog" style="white-space: normal">
                                            <button type="button" class="uk-modal-close uk-close"></button>
                                            <p>
                                                {{ $item->text }}
                                            </p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection