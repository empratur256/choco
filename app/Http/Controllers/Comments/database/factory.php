<?php
use Faker\Factory as Faker;

$factory->define(\Sedehi\Http\Controllers\Comments\Models\Comments::class, function($faker){
    $faker = Faker::create('fa_IR');

    return [
        'product_id' => \Sedehi\Http\Controllers\Product\Models\Product::inRandomOrder()->first()->id,
        'user_id'    => \Sedehi\Http\Controllers\User\Models\User::inRandomOrder()->first()->id,
        'text'       => $faker->text(),
        'rate'       => $faker->numberBetween(0, 5),
        'confirm'    => $faker->numberBetween(0, 1),
    ];
});
