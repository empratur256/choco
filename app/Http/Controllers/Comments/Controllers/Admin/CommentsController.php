<?php

namespace Sedehi\Http\Controllers\Comments\Controllers\Admin;

use DB;
use Exception;
use Log;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Comments\Models\Comments;
use Sedehi\Http\Controllers\Comments\Requests\Admin\CommentsRequest;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function index()
    {
        $items = Comments::with([
                                    'user' => function($query){
                                        $query->withTrashed();
                                    },
                                    'product' => function($query){
                                        $query->withTrashed();
                                    },
                                ])->filter()->latest()->paginate(20);

        return view('Comments.views.admin.comments.index', compact('items'));
    }

    public function status(CommentsRequest $request)
    {
        $data = collect($request->get('status'));
        DB::beginTransaction();
        try{
            $unConfirm = $data->filter(function($value){
                return $value == 0;
            })->keys();
            if(count($unConfirm)){
                Comments::whereIn('id', $unConfirm)->update([
                                                                'confirm' => 0,
                                                                'reject'  => 0,
                                                            ]);
            }
            $confirmed = $data->filter(function($value){
                return $value == 1;
            })->keys();
            if(count($confirmed)){
                Comments::whereIn('id', $confirmed)->update([
                                                                'confirm' => 1,
                                                                'reject'  => 0,
                                                            ]);
            }
            $rejected = $data->filter(function($value){
                return $value == 2;
            })->keys();
            if(count($rejected)){
                Comments::whereIn('id', $rejected)->update([
                                                               'confirm' => 0,
                                                               'reject'  => 1,
                                                           ]);
            }
            $deleted = $data->filter(function($value){
                return $value == 3;
            })->keys();
            if(count($deleted)){
                Comments::whereIn('id', $deleted)->delete();
            }
            $products = array_keys($request->get('product'));
            if(count($products)){
                foreach($products as $product){
                    $item = Product::withTrashed()->find($product);
                    if(!is_null($item)){
                        $item->comment_count = Comments::where('product_id', $item->id)->confirmed()->count();
                        $item->rate = intval(Comments::where('rate', '>', 0)->where('product_id', $item->id)
                                                     ->confirmed()->avg('rate'));
                        $item->save();
                    }
                }
            }
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            Log::alert('moshkel dar taide comment '.$e);

            return redirect()->back()->with('error', 'مشکلی در ذخیر اطلاعات');
        }

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شده');
    }
}
