<?php

namespace Sedehi\Http\Controllers\Comments\Controllers\Site;

use Illuminate\Http\Request;
use Sedehi\Http\Controllers\Comments\Models\Comments;
use Sedehi\Http\Controllers\Comments\Requests\Site\CommentsRequest;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Product\Models\Product;

class CommentsController extends Controller
{

    public function create(CommentsRequest $request, $id)
    {
        $product          = Product::active()->findOrFail($id);
        $item             = new Comments();
        $item->user_id    = auth()->user()->id;
        $item->product_id = $product->id;
        $item->text       = $request->get('text');
        $item->language   = app()->getLocale();
        if($request->get('rate') > 0 && $request->get('rate') < 6){
            $item->rate = $request->get('rate');
        }else{
            $item->rate = 0;
        }
        $item->save();

        return redirect()->back()->with('success', trans('site.comment_sent'));
    }
}
