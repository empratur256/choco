<?php
return [
    'comments' => [
        'order'   => 200,
        'title'   => 'نظر کاربران',
        'icon'    => 'fa fa-comment',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index'     => [
                'title'      => 'لیست کلی',
                'action'     => 'Comments\Controllers\Admin\CommentsController@index',
                'parameters' => [],
            ],
            'UnConfirm' => [
                'title'      => 'نیاز به بررسی',
                'action'     => 'Comments\Controllers\Admin\CommentsController@index',
                'parameters' => [
                    'confirm' => 0,
                    'reject'  => 0,
                ],
            ],
            'Confirm'   => [
                'title'      => 'تایید شده ',
                'action'     => 'Comments\Controllers\Admin\CommentsController@index',
                'parameters' => [
                    'confirm' => 1,
                    'reject'  => 0,
                ],
            ],
            'Reject'    => [
                'title'      => 'عدم تایید',
                'action'     => 'Comments\Controllers\Admin\CommentsController@index',
                'parameters' => ['reject' => 1],
            ],
        ],
    ],
];
