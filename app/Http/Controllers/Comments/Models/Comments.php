<?php

namespace Sedehi\Http\Controllers\Comments\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Controllers\User\Models\User;
use Sedehi\Libs\LanguageTrait;

class Comments extends Model
{

    use  Filterable,LanguageTrait;

    protected $table      = 'product_comments';
    public    $timestamps = true;

    protected $filterable = [
        'confirm',
        'language',
        'reject',
        'rate',
        'user_id',
        'product_id',
        'email'      => [
            'scope' => 'UserEmail',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function scopeConfirmed($query)
    {
        return $query->where('confirm', 1)->where('reject', 0);
    }

    public function scopeUserEmail($query)
    {
        return $query->whereHas('user', function($query){
            $query->where('email', request()->get('email'));
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
