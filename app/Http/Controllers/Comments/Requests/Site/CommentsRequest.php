<?php

namespace Sedehi\Http\Controllers\Comments\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Comments\Models\Comments;

class CommentsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'create':
                return [
                    'text' => 'required',
                    'rate' => 'numeric',
                ];
            break;
        }

        return [];
    }

}
