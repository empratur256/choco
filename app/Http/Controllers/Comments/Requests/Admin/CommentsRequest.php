<?php

namespace Sedehi\Http\Controllers\Comments\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Comments\Models\Comments;

class CommentsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('comments.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Comments::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Comments::where('author_id',auth()->user()->id)->find($this->route()->parameter('comments'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'status':
                return [
                    'status' => 'required|array',
                ];
            break;
        }

        return [];
    }

}
