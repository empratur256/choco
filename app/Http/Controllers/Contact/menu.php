<?php
return [
    'contact' => [
        'order'   => 190,
        'title'   => 'تماس با ما',
        'icon'    => 'fa fa-comments',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index'   => [
                'title'  => 'پیام ها',
                'action' => 'Contact\Controllers\Admin\ContactController@index',
            ],
            'Setting' => [
                'title'  => 'تنظیمات',
                'action' => 'Contact\Controllers\Admin\ContactController@setting',
            ],
        ],
    ],
];