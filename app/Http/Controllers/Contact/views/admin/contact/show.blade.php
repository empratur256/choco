@extends('views.layouts.admin.master')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom" id="adminBreadcrumb"></h3>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            @include('views.errors.errors')
                            {!! Form::model($item,['action'=> 'Contact\Controllers\Admin\ContactController@answer']) !!}

                            {{ Form::hidden('id',$item->id) }}

                            <div class="uk-form-row searchPanel">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('subject','موضوع') !!}
                                        {!! Form::text('subject',null,['class'=>'md-input','disabled']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        {!! Form::label('name','نام فرستنده') !!}
                                        {!! Form::text('name',null,['class'=>'md-input','disabled']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        {!! Form::label('companyName','نام شرکت') !!}
                                        {!! Form::text('companyName',null,['class'=>'md-input','disabled']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        {!! Form::label('email','ایمیل فرستنده') !!}
                                        {!! Form::text('email',null,['class'=>'md-input','disabled']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('text','متن پیام') !!}
                                        <br>
                                        {!! Form::textarea('text',null,['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('answer','پاسخ') !!}
                                        <br>
                                        {!! Form::textarea('answer',null,['class'=>'md-input']) !!}
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light " style="margin-top: 10px">ارسال پاسخ</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
