@extends('views.layouts.admin.master')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom" id="adminBreadcrumb"></h3>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            @include('views.errors.errors')
                            {!! Form::model($item,['action'=> 'Contact\Controllers\Admin\ContactController@saveSetting','files' => true]) !!}
                            <div class="uk-form-row searchPanel">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('address_fa','آدرس فارسی') !!}
                                        {!! Form::text('address_fa',null,['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('address_en','آدرس انگلیسی') !!}
                                        {!! Form::text('address_en',null,['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('contact_numbers_fa',' شماره های تماس فارسی') !!}
                                        {!! Form::text('contact_numbers_fa',null,['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('contact_numbers_en',' شماره های تماس انگلیسی') !!}
                                        {!! Form::text('contact_numbers_en',null,['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('contact_text_fa','متن صفحه تماس با ما فارسی') !!}
                                        <br>
                                        {!! Form::textarea('contact_text_fa',null,['class'=>'md-input ckeditor']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('contact_text_en','متن صفحه تماس با ما انگلیسی') !!}
                                        <br>
                                        {!! Form::textarea('contact_text_en',null,['class'=>'md-input ckeditor']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h3 class="heading_a uk-margin-small-bottom">تصویر نقشه</h3>
                                                {!! Form::file('map',['class' => 'dropify','remove' => 'file_remove','data-default-file' => (File::exists(public_path('uploads/map.png')))? asset('uploads/map.png') : '']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light " style="margin-top: 10px">ذخیره</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
