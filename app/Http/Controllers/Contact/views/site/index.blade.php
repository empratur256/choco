@extends('views.layouts.site.master')
@section('title',trans('site.contact.home'))

@section('content')

    <div class="red_box">
        <div class="container">
            <h3 class=""> @lang('site.contact.home')   </h3>
            <div class="divider-title"></div>
            <p>
                <br>@lang('site.contact.top_text') </p>
        </div>
    </div>

    <div class="contactus">
        <div class="container">
            <div class="row">
                <div class="form col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-right">
                    <div style="padding-bottom: 2em">
                        <h4 style=""> @lang('site.contact.home') </h4>
                        @if(!is_null($setting->contact_text) && strlen($setting->contact_text) > 3)
                            <p>
                                {!!$setting->contact_text!!}
                            </p>
                        @endif
                    </div>

                    @include('views.errors.errors')

                    {{ Form::open(['action' => 'Contact\Controllers\Site\ContactController@send'], ['class' => 'row col-md-10 pull-right']) }}
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group pull-right ">
                            <label for="name">@lang('site.contact.name')</label>
                            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
                            <span class="glyphicon glyphicon-user"></span>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-group pull-right">
                            <label for="email">@lang('site.contact.email')</label>
                            {{ Form::text('email', null, ['class' => 'form-control']) }}
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 form-group pull-right ">
                            <label for="subject">@lang('site.contact.message')</label>
                            {{ Form::text('subject', null, ['class' => 'form-control']) }}
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 form-group pull-right">
                            <label for="companyName">@lang('site.contact.company')</label>
                            {{ Form::text('companyName', null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right">
                            <label for="text">@lang('site.contact.text')</label>
                            <textarea name="text" class="form-control" id="text" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="captcha" style="padding-bottom: 2.5em">
                        {!! captcha_img() !!}
                        {{ Form::text('captcha', null, ['class' => 'form-control']) }}
                    </div>

                    <div class="row btn-submit">
                        <div class="col-lg-3 pull-right">
                            <button type="submit" class="btn submit">@lang('site.contact.submit')</button>
                        </div>
                    </div>

                    {{ Form::close() }}

                </div>

                <div style="position: relative;" class="img col-lg-5 col-md-5 col-sm-5 col-xs-12 bg-img">

                    @if(File::exists(public_path('uploads/map.png')))
                        <img id="address-img" class="img-responsive col-md-12 col-sm-12 hidden-xs" src="{!! asset('uploads/map.png') !!}">
                    @endif
                    @if((!is_null($setting->contact_numbers) && strlen($setting->contact_numbers) > 3) ||
                        (!is_null($setting->address) && strlen($setting->address) > 3) ||
                        (!is_null($setting->fax) && strlen($setting->fax) > 3)
                    )
                        <div id="address" style="">

                            <h5> @lang('site.contact.call_Info')</h5>

                            <div style="padding-top: 2em">
                                @if(!is_null( $setting->contact_numbers ) && strlen($setting->contact_numbers) > 3)
                                    <p style="padding-bottom: 1em">
                                        <i class="glyphicon glyphicon-phone-alt p-r-10">
                                        </i>@lang('site.contact.phone_no')
                                        : {{$setting->contact_numbers}}
                                    </p>
                                @endif
                                @if(!is_null( $setting->fax ) && strlen($setting->fax) > 3)
                                    <p style="padding-bottom: 1em">
                                        <i class="fa fa-fax fa" aria-hidden="true">
                                        </i>@lang('site.contact.fax_no')
                                        : {{$setting->fax}}
                                    </p>
                                @endif
                                @if(!is_null( $setting->address ) && strlen($setting->address) > 3)
                                    <p>
                                        <span class="glyphicon glyphicon-home p-r-10"></span>{{$setting->address}}
                                    </p>
                                @endif
                            </div>

                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@push('css')
{{ Html::style('assets/site/css/contactus.css') }}
@endpush
