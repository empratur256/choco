<?php

namespace Sedehi\Http\Controllers\Contact\Controllers\Site;

use Sedehi\Http\Controllers\Contact\Models\Contact;
use Sedehi\Http\Controllers\Contact\Requests\Site\ContactRequest;
use Sedehi\Http\Controllers\Controller;

class ContactController extends Controller
{

    public function index()
    {
        return view('Contact.views.site.index');
    }

    public function send(ContactRequest $request)
    {
        $data                   = new Contact();
        $data->name             = $request->get('name');
        $data->companyName      = $request->get('companyName');
        $data->email            = $request->get('email');
        $data->subject          = $request->get('subject');
        $data->text             = $request->get('text');
        $data->save();

        return redirect()->back()->with('success', trans('site.message_sent'));
    }
}
