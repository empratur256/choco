<?php

namespace Sedehi\Http\Controllers\Contact\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Image;
use Sedehi\Http\Controllers\Contact\Mail\AnswerMail;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Contact\Models\Contact;
use Sedehi\Http\Controllers\Contact\Requests\Admin\ContactRequest;

class ContactController extends Controller
{

    public function index()
    {
        $items = Contact::filter()->latest()->paginate(20);

        return view('Contact.views..admin.contact.index', compact('items'));
    }

    public function show($id)
    {
        $item = Contact::findOrFail($id);
        if(is_null($item->readed_at)){
            $item->readed_at = Carbon::now();
            $item->save();
        }

        return view('Contact.views.admin.contact.show', compact('item'));
    }

    public function destroy(ContactRequest $request)
    {
        Contact::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

    public function answer(ContactRequest $request)
    {
        $data = Contact::findOrFail($request->get('id'));
        Mail::to($data->email)->send(new AnswerMail($data, $request->get('answer')));

        return redirect()->back()->with('success', 'ایمیل با موفقیت ارسال شد');
    }

    public function setting()
    {
        $item = Setting::first();

        return view('Contact.views.admin.contact.setting', compact('item'));
    }

    public function saveSetting(ContactRequest $request)
    {
        $setting = Setting::first();
        if(is_null($setting)){
            $setting = new Setting();
        }

        $setting->address_fa         = request('address_fa');
        $setting->address_en         = request('address_en');
        $setting->contact_numbers_fa = request('contact_numbers_fa');
        $setting->contact_numbers_en = request('contact_numbers_en');
        $setting->contact_text_fa    = request('contact_text_fa');
        $setting->contact_text_en    = request('contact_text_en');

        if($request->hasFile('map')){
            $file            = $request->file('map');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path('uploads/');
            $fileName        = 'map.png';
            Image::make($realPath)->resize(353, 648)->save($destinationPath.$fileName, 90);
        }
        $setting->save();

        return back()->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }
}
