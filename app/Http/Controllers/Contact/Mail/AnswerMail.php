<?php

namespace Sedehi\Http\Controllers\Contact\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnswerMail extends Mailable
{
    use Queueable, SerializesModels;

    public $level      = 'info';
    public $introLines = [];
    public $greeting;
    public $subject;

    /**
     * Create a new message instance.
     * @return void
     */
    public function __construct($data, $answer)
    {
        $this->introLines[] = $answer;
        $this->greeting     = $data->subject;
        $this->subject      = $data->subject;
    }

    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        return $this->subject('RE : '.$this->subject)->view('views.emails.reminder');
    }
}
