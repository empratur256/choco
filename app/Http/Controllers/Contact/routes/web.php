<?php
Route::group([
                 'prefix' => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace' => 'Contact\Controllers\Admin',
             ], function(){
    Route::get('contact/setting', 'ContactController@setting');
    Route::post('contact/setting', 'ContactController@saveSetting');
    Route::post('contact/answer', 'ContactController@answer');
    Route::resource('contact', 'ContactController', [
        'only' => [
            'show',
            'index',
            'destroy',
        ],
    ]);
});
Route::group(['namespace' => 'Contact\Controllers\Site'], function(){
    Route::get('contact', 'ContactController@index');
    Route::post('contact', 'ContactController@send');
});