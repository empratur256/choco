<?php

namespace Sedehi\Http\Controllers\Contact\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class Contact extends Model
{

    use  Filterable;

    protected $table      = 'contact';
    public    $timestamps = true;

    protected $filterable = [
        'subject'    => [
            'operator' => 'Like',
        ],
        'name'       => [
            'operator' => 'Like',
        ],
        'email'      => [
            'operator' => 'Like',
        ],
        'unreaded'   => [
            'scope' => 'unreaded',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function scopeUnreaded($query)
    {
        return $query->whereNull('readed_at');
    }

}
