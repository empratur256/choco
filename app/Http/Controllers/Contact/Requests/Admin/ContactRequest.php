<?php

namespace Sedehi\Http\Controllers\Contact\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Contact\Models\Contact;

class ContactRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'answer':
                return [
                    'id'     => 'required',
                    'answer' => 'required',
                ];
            break;
        }

        return [];
    }

}
