<?php

namespace Sedehi\Http\Controllers\Contact\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Contact\Models\Contact;

class ContactRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('contact.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Contact::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Contact::where('author_id',auth()->user()->id)->find($this->route()->parameter('contact'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required',
            'email'   => 'required|email',
            'subject' => 'required',
            'text'    => 'required',
            'captcha' => 'required|captcha',
        ];
    }

}
