<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class ContactAddLangFields extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('setting', function(Blueprint $table){
            $table->text('address_en')->nullable()->default(null);
            $table->text('contact_numbers_en')->nullable()->default(null);
            $table->text('contact_text_en')->nullable()->default(null);
            $table->renameColumn('address', 'address_fa');
            $table->renameColumn('contact_numbers', 'contact_numbers_fa');
            $table->renameColumn('contact_text', 'contact_text_fa');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
