<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactSetting extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('setting')){
            Schema::table('setting', function(Blueprint $table){
                $table->text('contact_text')->nullable()->default(null);
                $table->text('contact_emails')->nullable()->default(null);
                $table->text('contact_numbers')->nullable()->default(null);
                $table->text('address')->nullable()->default(null);
                $table->string('latitude')->nullable()->default(null);
                $table->string('longitude')->nullable()->default(null);
            });
        }else{
            Schema::create('setting', function(Blueprint $table){
                $table->text('contact_text')->nullable()->default(null);
                $table->text('contact_emails')->nullable()->default(null);
                $table->text('contact_numbers')->nullable()->default(null);
                $table->text('address')->nullable()->default(null);
                $table->string('latitude')->nullable()->default(null);
                $table->string('longitude')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('setting', function(Blueprint $table){
            $table->dropColumn('contact_text');
            $table->dropColumn('contact_emails');
            $table->dropColumn('contact_numbers');
        });
    }
}
