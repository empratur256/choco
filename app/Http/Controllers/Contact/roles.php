<?php
return [
    'contact' => [
        'title'  => 'تماس با ما',
        'access' => [
            'ContactController' => [
                'لیست'    => 'index',
                'نمایش'   => [
                    'show',
                    'answer',
                ],
                'حذف'     => 'destroy',
                'تنظیمات' => [
                    'setting',
                    'saveSetting',
                ],
            ],
        ],
    ],
];
