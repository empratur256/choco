<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Role\Controllers\Admin',
             ], function(){
    Route::resource('role', 'RoleController', ['except' => ['show']]);
});
