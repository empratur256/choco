<?php

namespace Sedehi\Http\Controllers\Role\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\User\Models\User;

class Role extends Model
{

    use  Filterable;

    protected $table      = 'roles';
    public    $timestamps = true;

    protected $filterable = [
        'name'       => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
