<?php

namespace Sedehi\Http\Controllers\Role\database\seeds;

use Artisan;
use Illuminate\Database\Seeder;
use Sedehi\Http\Controllers\Role\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $data             = new Role();
        $data->name       = 'مدیریت کل';
        $data->permission = 'a:8:{s:11:"filemanager";s:1:"1";s:7:"contact";a:2:{s:17:"contactcontroller";a:4:{s:5:"index";s:1:"1";s:11:"show,answer";s:1:"1";s:7:"destroy";s:1:"1";s:19:"setting,savesetting";s:1:"1";}s:10:"onlybyuser";s:1:"1";}s:4:"menu";a:2:{s:14:"menucontroller";a:4:{s:5:"index";s:1:"1";s:12:"create,store";s:1:"1";s:11:"edit,update";s:1:"1";s:7:"destroy";s:1:"1";}s:10:"onlybyuser";s:1:"1";}s:4:"page";a:2:{s:14:"pagecontroller";a:4:{s:5:"index";s:1:"1";s:12:"create,store";s:1:"1";s:11:"edit,update";s:1:"1";s:7:"destroy";s:1:"1";}s:10:"onlybyuser";s:1:"1";}s:4:"role";a:2:{s:14:"rolecontroller";a:4:{s:5:"index";s:1:"1";s:12:"create,store";s:1:"1";s:11:"edit,update";s:1:"1";s:7:"destroy";s:1:"1";}s:10:"onlybyuser";s:1:"1";}s:7:"setting";a:2:{s:17:"settingcontroller";a:4:{s:5:"index";s:1:"1";s:12:"create,store";s:1:"1";s:11:"edit,update";s:1:"1";s:7:"destroy";s:1:"1";}s:10:"onlybyuser";s:1:"1";}s:6:"slider";a:1:{s:16:"slidercontroller";a:4:{s:5:"index";s:1:"1";s:12:"create,store";s:1:"1";s:11:"edit,update";s:1:"1";s:7:"destroy";s:1:"1";}}s:4:"user";a:2:{s:14:"usercontroller";a:4:{s:5:"index";s:1:"1";s:12:"create,store";s:1:"1";s:11:"edit,update";s:1:"1";s:7:"destroy";s:1:"1";}s:10:"onlybyuser";s:1:"1";}}';
        $data->save();
        Artisan::call('section:update-roles');
    }

}
