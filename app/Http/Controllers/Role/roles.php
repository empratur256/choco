<?php
return [
    'role' => [
        'title'  => 'گروه کاربری',
        'access' => [
            'RoleController' => [
                'لیست'   => 'index',
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
