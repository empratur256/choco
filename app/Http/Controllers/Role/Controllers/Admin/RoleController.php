<?php

namespace Sedehi\Http\Controllers\Role\Controllers\Admin;

use Sedehi\Http\Controllers\Reports\Models\UserRoleLog;
use Sedehi\Http\Controllers\User\Models\User;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Role\Models\Role;
use Sedehi\Http\Controllers\Role\Requests\Admin\RoleRequest;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    public function index()
    {
        $items = Role::filter()->latest()->paginate(20);
        $list  = Role::pluck('name', 'id');

        return view('Role.views..admin.role.index', compact('items', 'list'));
    }

    public function create()
    {
        return view('Role.views.admin.role.add')->with('item', null);
    }

    public function store(RoleRequest $request)
    {
        $item             = new Role();
        $item->name       = $request->get('name');
        $item->permission = serialize($request->get('access'));
        $item->save();
        $log             = new UserRoleLog();
        $log->user_id    = auth()->user()->id;
        $log->role_id    = $item->id;
        $log->permission = serialize($request->get('access'));
        $log->type       = 'create';
        $log->save();

        return redirect()->action('Role\Controllers\Admin\RoleController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item             = Role::findOrFail($id);
        $item->permission = unserialize($item->permission);

        return view('Role.views.admin.role.edit', compact('item'));
    }

    public function update(RoleRequest $request, $id)
    {
        $item             = Role::findOrFail($id);
        $item->name       = $request->get('name');
        $item->permission = serialize($request->get('access'));
        if($item->isDirty('permission')){
            $log             = new UserRoleLog();
            $log->user_id    = auth()->user()->id;
            $log->role_id    = $id;
            $log->permission = serialize($request->get('access'));
            $log->type       = 'edit';
            $log->save();
        }
        $item->save();

        return redirect()->action('Role\Controllers\Admin\RoleController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(RoleRequest $request)
    {
        switch($request->get('deleteType')){
            case 1:
                Role::whereIn('id', $request->get('deleteId'))->delete();
            break;
            case 2:
                $roles = Role::with('users')->whereIn('id', $request->get('deleteId'))->get();
                $users = [];
                foreach($roles as $role){
                    foreach($role->users as $user){
                        $users[] = $user->id;
                    }
                }
                Role::whereIn('id', $request->get('deleteId'))->delete();
                User::whereIn('id', $users)->delete();
            break;
            case 3:
                $roles = Role::with('users')->whereIn('id', $request->get('deleteId'))->get();
                $users = [];
                foreach($roles as $role){
                    foreach($role->users as $user){
                        $users[] = $user->id;
                    }
                }
                $toRole = Role::find($request->get('to'));
                if(in_array($request->get('to'), $request->get('deleteId'))){
                    return redirect()->back()
                                     ->with('error', 'گروه کاربری مورد نظر شما برای انتقال کاربران در لیست گروه های حذف شده می باشد');
                }
                $toRole->users()->syncWithoutDetaching($users);
                Role::whereIn('id', $request->get('deleteId'))->delete();
            break;
        }

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
