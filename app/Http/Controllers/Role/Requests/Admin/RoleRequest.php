<?php

namespace Sedehi\Http\Controllers\Role\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Role\Models\Role;

class RoleRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('role.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Role::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Role::where('author_id',auth()->user()->id)->find($this->route()->parameter('role'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId'   => 'required|array',
                    'deleteType' => 'required',
                ];
            break;
            case 'store':
                return [
                    'name' => 'required',
                ];
            break;
            case 'update':
                return [
                    'name' => 'required',
                ];
            break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'deleteType.required' => 'لطفا یک روش حذف انتخاب کنید',
        ];
    }
}
