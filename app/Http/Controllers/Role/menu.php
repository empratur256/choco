<?php
return [
    'role' => [
        'order'   => 10,
        'title'   => 'گروه راهبری',
        'icon'    => 'fa fa-users',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'  => 'لیست',
                'action' => 'Role\Controllers\Admin\RoleController@index',
            ],
            'Add'   => [
                'title'  => 'ایجاد',
                'action' => 'Role\Controllers\Admin\RoleController@create',
            ],
        ],
    ],
];
