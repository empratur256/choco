@extends('views.layouts.admin.master')
@section('content')
    @include('Role.views.admin.role.search')
    {!! Form::open(['action'=>['Role\Controllers\Admin\RoleController@destroy',1],'method' =>'DELETE']) !!}
    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('Role.RoleController.destroy'))
                                <button type="button" data-uk-modal="{target:'#modal_delete'}" class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">حذف</button>
                                <div class="uk-modal" id="modal_delete" style="font-size: 14px !important;">
                                    <div class="uk-modal-dialog">
                                        <div class="uk-modal-header">
                                            <h3 class="uk-modal-title">حذف گروه کاربری </h3>
                                        </div>
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-1">
                                                {!! Form::radio('deleteType',1,null,['data-md-icheck','id'=>'deleteType1']) !!}
                                                {!! Form::label('deleteType1','فقط حذف گروه کاربری',['class' => 'inline-label']) !!}
                                            </div>
                                        </div>
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-1">
                                                {!! Form::radio('deleteType',2,null,['data-md-icheck','id'=>'deleteType2']) !!}
                                                {!! Form::label('deleteType2','حذف گروه کاربری همراه با اعضای عضو',['class' => 'inline-label']) !!}
                                            </div>
                                        </div>
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2">
                                                {!! Form::radio('deleteType',3,null,['data-md-icheck','id'=>'deleteType3']) !!}
                                                {!! Form::label('deleteType3','حذف و انتقال اعضا به گروه',['class' => 'inline-label']) !!}
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                {!! Form::select('to',$list,null,['class'=>'md-input']) !!}
                                            </div>
                                        </div>
                                        <div class="uk-modal-footer uk-text-left">
                                            <button type="button" class="md-btn md-btn-flat uk-modal-close">بستن</button>
                                            <button data-uk-modal="{target:'#modal_new'}" type="submit" class="md-btn md-btn-flat md-btn-flat-danger">حذف</button>
                                        </div>
                                    </div>
                                </div>

                            @endif
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            <a class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" href="{!! action('Role\Controllers\Admin\RoleController@create') !!}" style="margin-left: 15px !important;">ایجاد</a>

                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Role\Controllers\Admin\RoleController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>
                            <th class="uk-width-1-10  uk-text-center small_col">
                                {!! Form::checkbox('',1,null,['class' => 'check_all','data-md-icheck']) !!}
                            </th>
                            <th class="uk-width-2-10 uk-text-center">نام</th>
                            <th class="uk-width-2-10 uk-text-center">تاریخ درج</th>
                            <th class="uk-width-2-10 uk-text-center">.....</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            <tr class="uk-text-center">
                                <td class="uk-text-center uk-table-middle small_col">
                                    {!! Form::checkbox('deleteId[]',$item->id,null,['class' => 'check_row','data-md-icheck']) !!}
                                </td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                </td>
                                <td class="uk-text-center">
                                    @if(permission('Role.RoleController.edit'))
                                        <a href="{!! action('Role\Controllers\Admin\RoleController@edit',$item->id) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection