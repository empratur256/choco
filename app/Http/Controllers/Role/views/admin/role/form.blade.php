<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-2">
                {!! Form::label('name','نام') !!}
                {!! Form::text('name',null,['class'=>'md-input']) !!}
            </div>
            <div class="uk-width-medium-1-2">
                {!! Form::label('select_all','انتخاب همه',['class' => 'inline-label']) !!}
                {!! Form::checkbox('',null,null,['data-md-icheck','id' => 'select_all']) !!}
            </div>
        </div>
    </div>
</div>
<div class="md-card">
    <div class="md-card-content">
        <div class="uk-grid" data-uk-grid-margin>
            <h3 class="heading_a">قسمت های کلی</h3>
            <div class="uk-clearfix"></div>
            <br>
            <div class="uk-width-medium-1-1">
                {!! Form::label('filemanager','مدیریت فایل ها',['class' => 'inline-label']) !!}
                {!! Form::checkbox('access[filemanager]',1,isset($item->permission['filemanager'])? 1 : 0,['data-md-icheck','id' => 'filemanager']) !!}
            </div>

        </div>
    </div>
</div>

@foreach(adminRole() as $key=>$value)
    <div class="md-card">
        <div class="md-card-content">
            <h3 class="heading_a">
                <span class="uk-float-left">{{ $value['title'] }}</span>
                <span class="uk-float-right">
                    {!! Form::checkbox('select_part',null,null,['data-md-icheck','class' => 'checkPart', 'id' => 'checkPart'.$key,'data-part' => $key]) !!}
                    {!! Form::label('checkPart'.$key,'انتخاب این قسمت',['class' => 'inline-label','style' => 'font-size:15px !important']) !!}
                </span>
            </h3>
            <div class="uk-clearfix"></div>
            <br>
            <div class="uk-grid {{ $key }}" data-uk-grid-margin>

                @foreach($value['access'] as $keyAccess => $access)
                    @foreach($access as $aKey=>$aValue)
                        @php
                            $check = 0;
                            if (is_array($aValue)) {
                                $aValue = implode(',', $aValue);
                            }
                            if (!is_null($item)) {
                                if (array_has($item->permission, strtolower($key.'.'.$keyAccess.'.'.$aValue))) {
                                    $check = 1;
                                }
                            }
                        @endphp
                        <div class="uk-width-medium-1-3">
                            @if(is_array($aValue))
                                {!! Form::checkbox('access['.strtolower($key).']['.strtolower($keyAccess).']['.strtolower(implode(',',$aValue)).']',1,$check,['data-md-icheck', 'id' => 'check'.strtolower($keyAccess).strtolower(implode(',',$aValue))]) !!}
                                {!! Form::label('check'.strtolower($keyAccess).strtolower(implode(',',$aValue)),$aKey,['class' => 'inline-label']) !!}
                            @else
                                {!! Form::checkbox('access['.strtolower($key).']['.strtolower($keyAccess).']['.strtolower($aValue).']',1,$check,['data-md-icheck', 'id' => 'check'.strtolower($keyAccess).strtolower($aValue)]) !!}
                                {!! Form::label('check'.strtolower($keyAccess).strtolower($aValue),$aKey,['class' => 'inline-label']) !!}
                            @endif
                        </div>
                    @endforeach
                @endforeach
                @if(isset($value['onlyByUser']))
                    <div class="uk-width-medium-1-3">
                        @php
                            $onlyCheck = 0;
                            if (!is_null($item)) {
                                if (isset($item->permission[$key]['onlybyuser'])) {
                                    $onlyCheck = 1;
                                }
                            }
                        @endphp
                        {!! Form::checkbox('access['.strtolower($key).'][onlybyuser]',1,$onlyCheck,['data-md-icheck', 'id' => 'onlybyuser'.$key.strtolower($aValue)]) !!}
                        {!! Form::label('onlybyuser'.$key.strtolower($aValue),'ویرایش و حذف اطلاعات درج شده توسط کاربر',['class' => 'inline-label']) !!}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endforeach
@push('js')
<script>
    $(document).ready(function () {
        $("#select_all").on('ifChecked', function () {
            $.each($("input"), function (index, value) {
                if (value.type == 'checkbox') {
                    $(this).iCheck("check");
                }
            });
        });
        $("#select_all").on('ifUnchecked', function () {
            $.each($("input"), function (index, value) {
                if (value.type == 'checkbox') {
                    $(this).iCheck("uncheck");
                }
            });
        });

        $('.checkPart').on('ifChecked', function () {
            var checkPart = $(this);
            var className = $(this).attr('data-part');
            $.each($('.' + className + " input"), function (index, value) {

                if (value.type == 'checkbox') {
                    $(this).iCheck("check");

                }
            });
        });
        $('.checkPart').on('ifUnchecked', function () {
            var checkPart = $(this);
            var className = $(this).attr('data-part');
            $.each($('.' + className + " input"), function (index, value) {
                if (value.type == 'checkbox') {
                    $(this).iCheck("uncheck");

                }
            });
        });
    });
</script>
@endpush
