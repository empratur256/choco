<div class="uk-tab-center">
    <ul class="uk-tab" data-uk-tab="{connect:'#tabs'}">
        <li class="uk-active" aria-expanded="true"><a href="#">تنظیمات</a></li>
        <li aria-expanded="false"><a href="#">شبکه های اجتماعی</a></li>
        <li aria-expanded="false"><a href="#">صفحه اول</a></li>
    </ul>
</div>
<ul id="tabs" class="uk-switcher uk-margin">
    <li aria-hidden="false" class="uk-active">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
                {!! Form::label('title','عنوان سایت') !!}
                {!! Form::text('title',null,['class'=>'md-input']) !!}
            </div>
            <div class="uk-width-medium-1-1">
                {!! Form::label('description','توضیحات صفحه ورود و ثبت نام') !!}
                <br>
                {!! Form::textarea('description',null,['class'=>'md-input ckeditor']) !!}
            </div>
            <div class="uk-width-medium-1-1">
                <div class="uk-alert uk-alert-warning">برای غیر فعال کردن مالیات عدد 0 را وارد کنید</div>
                {!! Form::label('tax','مالیات') !!}
                {!! Form::text('tax',null,['class'=>'md-input']) !!}
            </div>
            <div class="uk-width-medium-1-1">
                {!! Form::label('cancel_order','مقدار ساعتی که سفارش می تواند پرداخت نشود') !!}
                {!! Form::number('cancel_order',null,['class'=>'md-input']) !!}
            </div>

        </div>
    </li>
    <li aria-hidden="true">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
                {!! Form::label('telegram','تلگرام') !!}
                {!! Form::text('telegram',null,['class'=>'md-input ltr-placeholder','placeholder' => 'مثال : http://telegram.com']) !!}
            </div>
            <div class="uk-width-medium-1-1">
                {!! Form::label('instagram','اینستاگرام') !!}
                {!! Form::text('instagram',null,['class'=>'md-input ltr-placeholder','placeholder' => 'مثال http://instagram.com']) !!}
            </div>
            <div class="uk-width-medium-1-1">
                {!! Form::label('facebook','فیسبوک') !!}
                {!! Form::text('facebook',null,['class'=>'md-input ltr-placeholder','placeholder' => 'مثال http://facebook.com']) !!}
            </div>
            <div class="uk-width-medium-1-1">
                {!! Form::label('twitter','توییتر') !!}
                {!! Form::text('twitter',null,['class'=>'md-input ltr-placeholder','placeholder' => 'مثال http://twitter.com']) !!}
            </div>
        </div>
    </li>
    <li aria-hidden="true">
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-1-1">
                {!! Form::label('trial_title','عنوان') !!}
                {!! Form::text('trial_title',null,['class'=>'md-input']) !!}
            </div>
            <div class="uk-width-medium-1-1">
                {!! Form::label('trial_text','متن توضیحات ') !!}
                <br>
                {!! Form::textarea('trial_text',null,['class'=>'md-input ckeditor   ']) !!}
            </div>
            <div class="uk-width-medium-1-2">
                {!! Form::label('home_picture','عکس صفحه اول کنار بهترین های فنسی') !!}
                {!! Form::file('home_picture',['class' => 'dropify','remove' => 'remove_home_picture','data-default-file' => (File::exists(public_path('uploads/home_picture.png')))? asset('uploads/home_picture.png') : '']) !!}
                {!! Form::hidden('remove_home_picture',0,['id' => 'remove_home_picture']) !!}
            </div>
            <div class="uk-width-medium-1-2">
                {!! Form::label('main_header_picture','عکس هدر صفحه اصلی') !!}
                {!! Form::file('main_header_picture',['class' => 'dropify','remove' => 'remove_main_header_picture','data-default-file' => (!is_null($item->main_header_picture))? asset('uploads/'.$item->main_header_picture) : '']) !!}
                {!! Form::hidden('remove_main_header_picture',0,['id' => 'remove_main_header_picture']) !!}
            </div>
        </div>
    </li>
</ul>


