<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrialSectionFields extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('setting')){
            Schema::table('setting', function(Blueprint $table){
                $table->string('trial_title')->nullable()->default(null);
                $table->string('trial_url')->nullable()->default(null);
                $table->text('trial_text')->nullable()->default(null);
                $table->string('trial_button_title')->nullable()->default(null);
            });
        }else{
            Schema::create('setting', function(Blueprint $table){
                $table->string('trial_title')->nullable()->default(null);
                $table->string('trial_url')->nullable()->default(null);
                $table->text('trial_text')->nullable()->default(null);
                $table->string('trial_button_title')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
