<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('setting')){
            Schema::table('setting', function(Blueprint $table){
                $table->increments('id');
                $table->string('title')->nullable()->default(null);
                $table->string('instagram')->nullable()->default(null);
                $table->string('telegram')->nullable()->default(null);
                $table->string('twitter')->nullable()->default(null);
                $table->string('facebook')->nullable()->default(null);
//                $table->string('fax')->nullable()->default(null);
                $table->string('tax')->nullable()->default(0);
                $table->text('description')->nullable()->default(null);
                $table->text('email')->nullable()->default(null);
                $table->integer('cancel_order')->default(24);
                $table->enum('log_remove_period', [
                    'daily',
                    'weekly',
                    'monthly',
                    'yearly',
                    'never',
                ])->default('yearly');
            });
        }else{
            Schema::create('setting', function(Blueprint $table){
                $table->increments('id');
                $table->string('title')->nullable()->default(null);
                $table->string('instagram')->nullable()->default(null);
                $table->string('telegram')->nullable()->default(null);
                $table->string('twitter')->nullable()->default(null);
                $table->string('facebook')->nullable()->default(null);
//                $table->string('fax')->nullable()->default(null);
                $table->string('tax')->nullable()->default(0);
                $table->text('description')->nullable()->default(null);
                $table->text('email')->nullable()->default(null);
                $table->integer('cancel_order')->default(24);
                $table->enum('log_remove_period', [
                    'daily',
                    'weekly',
                    'monthly',
                    'yearly',
                    'never',
                ])->default('yearly');
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');

    }
}
