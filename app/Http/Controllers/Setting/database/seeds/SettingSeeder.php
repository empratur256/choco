<?php

namespace Sedehi\Http\Controllers\Setting\database\seeds;

use Illuminate\Database\Seeder;
use Sedehi\Http\Controllers\Setting\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $data                    = new Setting();
        $data->log_remove_period = 'yearly';
        $data->save();
    }
}
