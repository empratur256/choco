<?php

namespace Sedehi\Http\Controllers\Setting\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Libs\LanguageTrait;

class Setting extends Model
{
    use LanguageTrait;
    protected $table      = 'setting';
    public    $timestamps = false;

    public function getAddressAttribute()
    {
        return $this['address_'.$this->lang()];
    }

    public function getContactNumbersAttribute()
    {
        return $this['contact_numbers_'.$this->lang()];
    }

    public function getContactTextAttribute()
    {
        return $this['contact_text_'.$this->lang()];
    }
}
