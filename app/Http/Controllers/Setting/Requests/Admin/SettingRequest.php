<?php

namespace Sedehi\Http\Controllers\Setting\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Setting\Models\Setting;

class SettingRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'update':
                return [
                    'email'     => 'email',
                    'telegram'  => 'url',
                    'instagram' => 'url',
                    'twitter'   => 'url',
                    'facebook'  => 'url',
                    'trial_url' => 'url',
                    'home_picture' => 'image',
                    'main_header_picture' => 'image',
                ];
            break;
        }

        return [];
    }

}
