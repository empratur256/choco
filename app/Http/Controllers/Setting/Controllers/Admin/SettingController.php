<?php

namespace Sedehi\Http\Controllers\Setting\Controllers\Admin;

use File;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\Setting\Requests\Admin\SettingRequest;
use Image;
class SettingController extends Controller
{


    public function index()
    {
        $item = Setting::firstOrFail();
        $log_remove_period = [
            'daily'   => 'روزانه',
            'weekly'  => 'هفتگی',
            'monthly' => 'ماهانه',
            'yearly'  => 'سالانه',
            'never'   => 'پاک نشود',
        ];

        return view('Setting.views.admin.setting.index', compact('item', 'log_remove_period'));
    }

    public function update(SettingRequest $request, $id)
    {
        $item                     = Setting::firstOrFail();
        $item->title              = $request->get('title');
        $item->description        = $request->get('description');
        $item->telegram           = $request->get('telegram');
        $item->instagram          = $request->get('instagram');
        $item->twitter            = $request->get('twitter');
        $item->facebook           = $request->get('facebook');
        $item->tax                = $request->get('tax');
        $item->cancel_order       = $request->get('cancel_order');
        $item->trial_title        = $request->get('trial_title');
        $item->trial_text         = $request->get('trial_text');
        $item->trial_url          = $request->get('trial_url');
        $item->trial_button_title = $request->get('trial_button_title');

        if($request->hasFile('home_picture')){

            $file            = $request->file('home_picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path('uploads/');
            $fileName        = 'home_picture.png';
            Image::make($realPath)->resize(165, 253)->save($destinationPath.$fileName);

        } elseif ($request->get('remove_home_picture') == 1) {
            if (File::exists(public_path('uploads/home_picture.png'))) {
                File::delete(public_path('uploads/home_picture.png'));
            }
        }

        if($request->hasFile('main_header_picture')){

            $file            = $request->file('main_header_picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path('uploads/');
            $fileName        = $file->hashName();
            Image::make($realPath)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath.$fileName);

            $item->main_header_picture = $fileName;

        } elseif ($request->get('remove_main_header_picture') == 1) {
            if (File::exists(public_path('uploads/'.$item->main_header_picture))) {
                File::delete(public_path('uploads/'.$item->main_header_picture));
            }
            $item->main_header_picture = null;
        }

        $item->save();

        return redirect()->action('Setting\Controllers\Admin\SettingController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }
}
