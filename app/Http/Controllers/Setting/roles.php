<?php
return [
    'setting' => [
        'title'  => 'تنظیمات',
        'access' => [
            'SettingController' => [
                'لیست'   => 'index',
                'ویرایش' => ['update'],
            ],
        ],
    ],
];
