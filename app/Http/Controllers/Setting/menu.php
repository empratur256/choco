<?php
return [
    'setting' => [
        'order'  => 140,
        'title'  => 'تنظیمات',
        'icon'   => 'fa fa-gears',
        'badge'  => function(){
            //return 1;
        },
        'action' => 'Setting\Controllers\Admin\SettingController@index',
    ],
];
