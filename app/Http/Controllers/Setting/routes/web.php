<?php
Route::group([
                 'prefix' => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace' => 'Setting\Controllers\Admin',
             ], function(){
    Route::resource('setting', 'SettingController', [
        'only' => [
            'index',
            'update',
        ],
    ]);
});
