<?php
return [
    'payment' => [
        'title'  => 'تراکنش مالی',
        'access' => [
            'PaymentController'    => [
                'لیست تراکنش های آنلاین' => 'index',
            ],
            'SettlementController' => [
                'لیست فیش بانکی'                => 'index',
                'حذف فیش بانکی'                 => 'destroy',
                'تغییر وضعیت فیش بانکی'         => 'status',
                'تغییر وضعیت فیش های تایید شده' => 'confirm-status',
            ],
        ],
    ],
];
