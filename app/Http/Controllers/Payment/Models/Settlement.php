<?php

namespace Sedehi\Http\Controllers\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Order\Models\Order;

class Settlement extends Model
{

    use  Filterable;

    protected $table      = 'settlements';
    public    $timestamps = true;

    protected $filterable = [
        'email'      => [
            'scope' => 'Email',
        ],
        'user_id'    => [
            'scope' => 'UserById',
        ],
        'status'     => [
            'scope' => 'status',
        ],
        'order_id',
        'account_id',
        'confirm',
        'reject',
        'amount',
        'reference',
        'from',
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function scopeStatus($query)
    {
        switch(request('status')){
            case 1:
                return $query->where('confirm', 1)->where('reject', 0);
            break;
            case 2:
                return $query->where('confirm', 0)->where('reject', 1);
            break;
            case 0:
                return $query->where('confirm', 0)->where('reject', 0);
            break;
        }

        return $query;
    }

    public function scopeEmail($query)
    {
        return $query->whereHas('order', function($query){
            $query->whereHas('user', function($query){
                $query->where('email', request('email'))->withTrashed();
            })->withTrashed();
        });
    }

    public function scopeUserById($query)
    {
        return $query->whereHas('order', function($query){
            $query->whereHas('user', function($query){
                $query->where('id', request('user_id'))->withTrashed();
            })->withTrashed();
        });
    }

    public function account()
    {
        return $this->belongsTo(BankAccount::class, 'account_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}
