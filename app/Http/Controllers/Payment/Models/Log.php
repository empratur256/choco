<?php

namespace Sedehi\Http\Controllers\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class Log extends Model
{

    use  Filterable;

    protected $table      = 'payment_transaction_log';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

}
