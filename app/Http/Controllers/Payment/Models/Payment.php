<?php

namespace Sedehi\Http\Controllers\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Plan\Models\Plan;
use Sedehi\Http\Controllers\User\Models\User;

class Payment extends Model
{

    use  Filterable;

    protected $table      = 'payment_transaction';
    public    $timestamps = true;

    protected $filterable = [
        'name'       => [
            'operator' => 'Like',
        ],
        'status',
        'user_id',
        'order_id',
        'reference',
        'amount',
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
