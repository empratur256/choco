<?php

namespace Sedehi\Http\Controllers\Payment\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Libs\LanguageTrait;

class BankAccount extends Model
{

    use  Filterable, SoftDeletes, LanguageTrait;

    protected $table      = 'bank_account';
    public    $timestamps = true;

    protected $filterable = [
        'title_fa'   => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

}
