@extends('views.layouts.admin.master')
@section('content')
    @include('Payment.views.admin.settlement.search')
    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">

                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Payment\Controllers\Admin\SettlementController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                            @if (request()->has('start_created') && request()->has('end_created'))
                                <span class="uk-badge uk-badge-success" style="float: left">قیمت کل پرداخت شده : {{ number_format($sumPrice) }} تومان‌</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-nowrap table_check">
                            <thead>
                            <tr>
                                <th class="uk-width-2-10 uk-text-center">شماره سفارش</th>
                                <th class="uk-width-2-10 uk-text-center">نام</th>
                                <th class="uk-width-2-10 uk-text-center">حساب</th>
                                <th class="uk-width-2-10 uk-text-center">شماره پیگیری</th>
                                <th class="uk-width-2-10 uk-text-center">از حساب</th>
                                <th class="uk-width-2-10 uk-text-center">مبلغ (تومان)</th>
                                <th class="uk-width-2-10 uk-text-center">وضعیت</th>
                                <th class="uk-width-2-10 uk-text-center">تغییر وضعیت به</th>
                                <th class="uk-width-2-10 uk-text-center">تاریخ پرداخت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr class="uk-text-center">

                                    <td>
                                        <a href="{!! action('Order\Controllers\Admin\OrderController@show',$item->order->id) !!}" target="_blank">{{ $item->order->id }}</a>
                                    </td>
                                    <td>
                                        <a target="_blank" href="{!! action('User\Controllers\Admin\UserController@showInfo',$item->order->user->id) !!}">{{ $item->order->user->name.' '.$item->order->user->family }}</a>
                                    </td>
                                    <td>{{ $item->account->account_name.' - '.$item->account->bank_name }}</td>
                                    <td>{{$item->reference}}</td>
                                    <td>{{$item->from}}</td>
                                    <td>{{number_format($item->amount)}}</td>
                                    <td>
                                        @if($item->reject == 1)
                                            <span class="uk-badge uk-badge-danger">رد شده</span>
                                        @elseif($item->confirm == 1)
                                            <span class="uk-badge uk-badge-success">تایید شده</span>
                                        @elseif($item->confirm == 0 && $item->reject == 0)
                                            <span class="uk-badge uk-badge-warning">نیاز به بررسی</span>
                                        @endif
                                    </td>
                                    @php $status = 0 @endphp
                                    @if($item->confirm == 1 && $item->reject == 0)
                                        @php $status = 1 @endphp
                                    @elseif($item->confirm == 0 && $item->reject == 1)
                                        @php $status = 2 @endphp
                                    @endif

                                    <td>
                                        @if($status == 0 && permission('Payment.SettlementController.status'))
                                            {!! Form::select('status',[1=>'تایید شده',0=>'نیاز به بررسی',2=>'رد شده'],$status,['class'=>'md-input change-status','data-id' => $item->id]) !!}
                                        @elseif(permission('Payment.SettlementController.confirm-status'))
                                            {!! Form::select('status',[1=>'تایید شده',0=>'نیاز به بررسی',2=>'رد شده'],$status,['class'=>'md-input change-status','data-id' => $item->id]) !!}
                                        @else
                                            <span class="uk-badge uk-badge-warning">امکان تغییر وضعیت نمی باشد</span>
                                        @endif

                                    </td>
                                    <td>
                                        <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>

@endsection
@push('js')

<script>
    $(document).ready(function () {
        $('.change-status').on('change', function () {
            if (confirm("آیا از انجام این عملیات اطمینان دارید؟")) {
                window.location = "{!! action('Payment\Controllers\Admin\SettlementController@status') !!}?id=" + $(this).data('id') + "&status=" + $(this).val();
            }
        });
    });
</script>

@endpush