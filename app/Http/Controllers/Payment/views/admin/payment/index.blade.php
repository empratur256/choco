@extends('views.layouts.admin.master')
@section('content')
    @include('Payment.views.admin.payment.search')
    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">

                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Payment\Controllers\Admin\PaymentController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                            @if (request()->has('start_created') && request()->has('end_created'))
                                <span class="uk-badge uk-badge-success" style="float: left">قیمت کل پرداخت شده : {{ number_format($sumPrice) }} تومان ‌</span>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-nowrap table_check">
                            <thead>
                            <tr>

                                <th class="uk-width-2-10 uk-text-center">شماره سفارش</th>
                                <th class="uk-width-2-10 uk-text-center">شماره کاربر</th>
                                <th class="uk-width-2-10 uk-text-center">نام</th>
                                <th class="uk-width-2-10 uk-text-center">شماره پیگیری</th>
                                <th class="uk-width-2-10 uk-text-center">مبلغ</th>
                                <th class="uk-width-2-10 uk-text-center">وضعیت</th>
                                <th class="uk-width-2-10 uk-text-center">تاریخ پرداخت</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr class="uk-text-center">

                                    <td>
                                        <a target="_blank" href="{!! action('Order\Controllers\Admin\OrderController@show',$item->order_id) !!}">{{ $item->order_id }}</a>
                                    </td>
                                    <td>
                                        <a href="{!! action('Payment\Controllers\Admin\PaymentController@index',['user_id'=>$item->user_id]) !!}">{{ $item->user_id }}</a>
                                    </td>
                                    <td>{{ $item->user->name.' '.$item->user->family }}</td>
                                    <td>{{$item->reference}}</td>
                                    <td>{{number_format($item->amount)}}</td>
                                    <td>@if($item->status) <span style="color: green">پرداخت شده</span> @else
                                            <span style="color: red">پرداخت نشده</span> @endif
                                    </td>
                                    <td>
                                        <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                    </td>

                                </tr>
                            @empty
                                <tr>
                                    <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>

@endsection