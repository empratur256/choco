<aside id="sidebar_secondary" class="tabbed_sidebar" style="padding-top: 0px;width: 400px">
    <ul class="uk-tab uk-tab-icons uk-tab-grid" data-uk-tab="{connect:'#dashboard_sidebar_tabs', animation:'slide-horizontal'}"></ul>

    <div class="scrollbar-inner">
        <ul id="dashboard_sidebar_tabs" class="uk-switcher">
            <li>
                <div class="timeline timeline_small uk-margin-bottom">
                    {!! Form::open(['method' => 'GET']) !!}
                    <div class="uk-form-row searchPanel">
                        @if(request()->has('user_id'))
                            {!! Form::hidden('user_id',request()->get('user_id')) !!}
                        @endif
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                {!! Form::label('user_id','شماره کاربری') !!}
                                {!! Form::text('user_id',request('user_id'),['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-1">
                                {!! Form::label('order_id','شماره سفارش') !!}
                                {!! Form::text('order_id',request('order_id'),['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-1">
                                {!! Form::label('reference','کد پیگیری') !!}
                                {!! Form::text('reference',request('reference'),['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-1">
                                {!! Form::label('amount','مبلغ') !!}
                                {!! Form::text('amount',request('amount'),['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-1">
                                {!! Form::select('status',['' => 'وضعیت',1=>'پرداخت شده',0=>'پرداخت نشده'],request('status'),['class'=>'md-input']) !!}
                            </div>

                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                    {!! Form::label('start_created','از تاریخ') !!}
                                    {!! Form::text('start_created',request()->get('start_created'),['class' => 'md-input date','id' => 'start_date']) !!}
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                    {!! Form::label('end_created','تا تاریخ') !!}
                                    {!! Form::text('end_created',request()->get('end_created'),['class' => 'md-input date','id' => 'end_date']) !!}
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-wave-light waves-effect waves-button waves-light" style="margin-top: 10px">جستجو</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </li>
        </ul>
    </div>
</aside>
