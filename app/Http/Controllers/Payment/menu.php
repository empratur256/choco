<?php
return [
    'payment' => [
        'order'   => 30,
        'title'   => 'تراکنش مالی',
        'icon'    => 'fa fa-dollar',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'تراکنش های آنلاین',
                'action'     => 'Payment\Controllers\Admin\PaymentController@index',
                'parameters' => [],
            ],
            'Settlement' => [
                'title'      => 'فیش بانکی / کارت به کارت',
                'action'     => 'Payment\Controllers\Admin\SettlementController@index',
                'parameters' => [],
            ],
            /* 'SettlementUnConfirm' => [
                 'title'      => 'فیش بانکی / کارت به کارت نیاز به تایید',
                 'action'     => 'Payment\Controllers\Admin\SettlementController@index',
                 'parameters' => ['confirm' => 0, 'reject' => 0]
             ],*/
        ],
    ],
];
