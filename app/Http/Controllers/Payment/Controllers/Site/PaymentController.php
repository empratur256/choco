<?php

namespace Sedehi\Http\Controllers\Payment\Controllers\Site;

use Carbon\Carbon;
use DB;
use Exception;
use Log;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Payment\Models\BankAccount;
use Sedehi\Http\Controllers\Payment\Models\Settlement;
use Sedehi\Http\Controllers\Payment\Requests\Site\PaymentRequest;
use Sedehi\Payment\Facades\Payment as BankPayment;
use Sedehi\Http\Controllers\Controller;

class PaymentController extends Controller
{

    public function verify()
    {
        $transaction = BankPayment::zarinpal()->transaction();

        $order = Order::where('user_id', $transaction->user_id)->find($transaction->order_id);

        if(is_null($order)){
            return redirect()->action('User\Controllers\Site\DashboardController@index')
                             ->with('error', trans('site.carts.payments.invalid_order'));
        }

        if($order->paid == 1){
            return redirect()->action('User\Controllers\Site\DashboardController@index')
                             ->with('error', trans('site.carts.payments.already_paid'));
        }

        $transaction = BankPayment::zarinpal()->verify();

        DB::beginTransaction();

        try {

            if($transaction->status == 1) {
                $order->paid = 1;
                $order->save();
            }

            DB::commit();

            return redirect()->action('User\Controllers\Site\DashboardController@index')->with('success', [
                trans('site.carts.payments.success'),
                trans('site.carts.payments.ref_number').' '.$transaction->reference,
            ]);

        } catch (Exception $e){
            DB::rollback();
            Log::alert('moshkel dar verify pardakht '.$e);

            return redirect()->action('User\Controllers\Site\DashboardController@index')
                             ->with('error', trans('site.carts.payments.error'));
        }
    }

    public function submit(PaymentRequest $request)
    {
        $account = BankAccount::Language()->findOrFail($request->get('account'));
        $dates = explode('/', $request->get('date'));
        if(count($dates) != 3){
            return redirect()->back()->withInput()->with('error', trans('site.carts.payments.invalid_date'));
        }
        if(app()->isLocale('fa')){
            $created_at = jmktime('0', '0', '0', $dates[1], $dates[2], $dates[0]);
        }else{
            $created_at = mktime('0', '0', '0', $dates[1], $dates[2], $dates[0]);
        }
        $item             = new Settlement;
        $item->order_id   = $request->get('order');
        $item->account_id = $account->id;
        $item->amount     = $request->get('amount');
        $item->reference  = $request->get('reference');
        $item->from       = $request->get('from');
        $item->created_at = Carbon::createFromTimestamp($created_at);
        $item->updated_at = Carbon::createFromTimestamp($created_at);
        $item->save();

        return redirect()->action('Order\Controllers\Site\OrderController@index')
                         ->with('success', trans('site.carts.payments.submit'));
    }
}
