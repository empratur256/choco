<?php

namespace Sedehi\Http\Controllers\Payment\Controllers\Admin;

use DB;
use Exception;
use Log;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Order\Models\StatusLog;
use Sedehi\Http\Controllers\Payment\Models\Settlement;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Payment\Models\Payment;
use Sedehi\Http\Controllers\Payment\Requests\Admin\SettlementRequest;
use Illuminate\Http\Request;

class SettlementController extends Controller
{

    public function index()
    {
        $sumPrice = 0;
        $items    = Settlement::with([
                                         'account' => function($query){
                                             $query->withTrashed();
                                         },
                                         'order'   => function($query){
                                             $query->with([
                                                              'user' => function($query){
                                                                  $query->withTrashed();
                                                              },
                                                          ]);
                                             $query->withTrashed();
                                         },
                                     ])->filter()->latest()->paginate(20);
        if(request()->has('start_created') && request()->has('end_created')){
            $sumPrice = Settlement::where('confirm', 1)->where('reject', 0)->filter()->sum('amount');
        }

        return view('Payment.views.admin.settlement.index', compact('items', 'sumPrice'));
    }

    public function status()
    {
        if(request()->has('id') && request()->has('status')){
            $status  = 'paid';
            $checked = 2;
            DB::beginTransaction();
            try{
                $settlement = Settlement::findOrFail(request('id'));
                if($settlement->confirm == 1 || $settlement->reject == 1){
                    if(!permission('Payment.SettlementController.confirm-status')){
                        return redirect()->back()->with('error', 'شما دسترسی تغییر وضعیت این تراکنش را ندارید');
                    }
                }
                $order = Order::withTrashed()->findOrFail($settlement->order_id);
                switch(request()->get('status')){
                    case 0:
                        $settlement->confirm = 0;
                        $settlement->reject  = 0;
                        $order->paid         = 0;
                        $checked             = 2;
                    break;
                    case 1:
                        $settlement->confirm = 1;
                        $settlement->reject  = 0;
                        $order->paid         = 1;
                        $checked             = 1;
                    break;
                    case 2:
                        $settlement->confirm = 0;
                        $settlement->reject  = 1;
                        $order->paid         = 0;
                        $checked             = 0;
                    break;
                }
                $settlement->save();
                $order->save();
                $log           = new StatusLog();
                $log->order_id = $order->id;
                $log->user_id  = auth()->user()->id;
                $log->status   = $status;
                $log->checked  = $checked;
                $log->save();
                DB::commit();
            }catch(Exception $e){
                DB::rollBack();
                Log::alert('moshkel dar taide settlement '.$e);

                return redirect()->back()->with('error', 'مشکلی در ذخیره اطلاعات');
            }
        }

        return redirect()->back()->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(SettlementRequest $request)
    {
        Settlement::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
