<?php

namespace Sedehi\Http\Controllers\Payment\Controllers\Admin;

use Sedehi\Http\Controllers\Payment\Models\Settlement;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Payment\Models\Payment;
use Sedehi\Http\Controllers\Payment\Requests\Admin\PaymentRequest;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public function index()
    {
        $sumPrice = 0;
        $items    = Payment::with('user')->filter()->latest()->paginate(20);
        if(request()->has('start_created') && request()->has('end_created')){
            $sumPrice = Payment::where('status', 1)->filter()->sum('amount');
        }

        return view('Payment.views.admin.payment.index', compact('items', 'sumPrice'));
    }

}
