<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Payment\Controllers\Admin',
             ], function(){
    Route::resource('payment', 'PaymentController', ['except' => ['show']]);
    Route::get('settlement/status', 'SettlementController@status');
    Route::resource('settlement', 'SettlementController', ['except' => ['show']]);
});
Route::group([
                 'prefix'     => config('site.dashboard'),
                 'middleware' => ['dashboard'],
                 'namespace'  => 'Payment\Controllers\Site',
             ], function(){
    Route::post('payment/submit', 'PaymentController@submit');
});
Route::any('payment/return', 'Payment\Controllers\Site\PaymentController@verify');
