<?php

namespace Sedehi\Http\Controllers\Payment\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Payment\Models\Payment;

class PaymentRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'submit':
                return [
                    'from'      => 'required',
                    'reference' => 'required',
                    'amount'    => 'required|numeric',
                    'date'      => 'required',
                    'account'   => 'required',
                ];
            break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'from.required'      => 'شماره حساب یا کارت واریز کننده را وارد کنید',
            'reference.required' => 'شماره پیگری را وارد کنید',
            'amount.required'    => 'مبلغ واریزی را وارد کنید',
            'amount.numeric'     => 'مبلغ واریزی باید عدد باشد',
            'date.required'      => 'تاریخ واریز را وارد کنید',
            'account.required'   => 'یکی از حساب های واریزی را انتخاب کنید  ',
        ];
    }
}
