<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettlementTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('settlements', function(Blueprint $table){
            $table->increments('id');
            $table->integer('order_id')->unsigned()->index();
            $table->integer('account_id')->unsigned()->index();
            $table->tinyInteger('confirm')->default(0)->index();
            $table->tinyInteger('reject')->default(0)->index();
            $table->bigInteger('amount')->default(0)->index();
            $table->string('reference')->index();
            $table->string('from')->index();
            $table->timestamps();
            $table->index('created_at');
            $table->foreign('order_id')->references('id')->on('order')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('account_id')->references('id')->on('bank_account')->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settlements');
    }
}
