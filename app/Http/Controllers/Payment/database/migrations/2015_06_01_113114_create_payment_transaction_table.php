<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create(config('payment.table'), function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('order_id')->unsigned()->nullable()->default(null)->index();
            $table->string('reference', 255)->nullable()->default(null)->index();
            $table->string('authority', 255)->nullable()->default(null)->index();
            $table->bigInteger('amount');
            $table->enum('provider', [
                'zarinpal',
                'payline',
                'jahanpay',
                'mellat',
            ])->index();
            $table->enum('currency', [
                'toman',
                'rial',
            ])->index();
            $table->tinyInteger('status')->index();
            $table->string('card_number', 20)->nullable()->default(null)->index();
            $table->text('description')->nullable();
            $table->string('ip', 20)->nullable()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop(config('payment.table'));
    }

}
