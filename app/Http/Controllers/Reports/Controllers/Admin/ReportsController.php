<?php

namespace Sedehi\Http\Controllers\Reports\Controllers\Admin;

use Sedehi\Http\Controllers\Order\Models\StatusLog;
use Sedehi\Http\Controllers\Reports\Models\UserRoleLog;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportsController extends Controller
{

    public function index()
    {
        $items  = StatusLog::filter()->latest()->with([
                                                          'user' => function($query){
                                                              $query->withTrashed();
                                                          },
                                                      ])->paginate(20);
        $status = [
            'confirm'                 => 'ثبت شده',
            'paid'                    => 'پرداخت شده',
            'processing_stock'        => 'پردازش انبار',
            'ready_to_send'           => 'در حال ارسال',
            'delivered'               => 'تحویل شده',
            'ready_for_production'    => 'اماده براي توليد',
            'in_production_procedure' => 'در حال توليد',
        ];

        return view('Reports.views.admin.reports.index', compact('items', 'status'));
    }

    public function roles()
    {
        $items = UserRoleLog::filter()->latest()->with([
                                                           'user' => function($query){
                                                               $query->withTrashed();
                                                           },
                                                           'role',
                                                       ])->has('role')->paginate(20);
        $roles = adminRole();

        return view('Reports.views.admin.roles.index', compact('items', 'roles'));
    }

}
