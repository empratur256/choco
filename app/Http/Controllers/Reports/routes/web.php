<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Reports\Controllers\Admin',
             ], function(){
    Route::get('reports/roles', 'ReportsController@roles');
    Route::resource('reports', 'ReportsController', ['only' => ['index']]);
});
