@extends('views.layouts.admin.master')
@section('content')
    @include('Reports.views.admin.roles.search')

    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Reports\Controllers\Admin\ReportsController@roles') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>
                            <th class="uk-width-2-10 uk-text-center">گروه کاربری</th>
                            <th class="uk-width-2-10 uk-text-center">شماره مدیر</th>
                            <th class="uk-width-2-10 uk-text-center">نام و نام خانوادگی مدیر</th>
                            <th class="uk-width-2-10 uk-text-center">تغییر</th>
                            <th class="uk-width-2-10 uk-text-center">دسترسی ها</th>
                            <th class="uk-width-2-10 uk-text-center">تاریخ</th>
                        </tr>
                        </thead>
                        <tbody>

                        @forelse($items as $item)
                            <tr class="uk-text-center">
                                <td>{{ $item->role->name }}</td>
                                <td>
                                    <a href="{!! action('Reports\Controllers\Admin\ReportsController@roles',['user_id'=>$item->user_id]) !!}">{{ $item->user_id }}</a>
                                </td>
                                <td>
                                    <a href="{!! action('Reports\Controllers\Admin\ReportsController@roles',['user_id'=>$item->user_id]) !!}">{{ $item->user->name.' '.$item->user->family }}</a>
                                </td>
                                <td>
                                    @if($item->type == 'edit')
                                        <span class="uk-badge uk-badge-info">ویرایش</span>
                                    @else
                                        <span class="uk-badge uk-badge-success">ایجاد</span>
                                    @endif
                                </td>
                                <td>
                                    <button class="md-btn" data-uk-modal="{target:'#modal_{{$item->id}}'}">نمایش</button>
                                    <div class="uk-modal" id="modal_{{$item->id}}">
                                        <div class="uk-modal-dialog">
                                            <button type="button" class="uk-modal-close uk-close"></button>
                                            <span class="uk-badge uk-badge-success">دسترسی دارد</span>
                                            <span class="uk-badge uk-badge-danger">دسترسی ندارد</span>

                                            <table class="uk-table">

                                                @foreach($roles as $key=>$value)
                                                    <tr>
                                                        <th>{{ $value['title'] }}</th>
                                                    </tr>

                                                    @foreach($value['access'] as $keyAccess => $access)

                                                        @foreach($access as $aKey=>$aValue)
                                                            @if($loop->iteration == 1)
                                                                <tr>
                                                                    @endif
                                                                    @php
                                                                        $check = 0;
                                                                        if (is_array($aValue)) {
                                                                            $aValue = implode(',', $aValue);
                                                                        }
                                                                        if (!is_null($item)) {
                                                                            if (array_has(unserialize($item->permission), strtolower($key.'.'.$keyAccess.'.'.$aValue))) {
                                                                                $check = 1;
                                                                            }
                                                                        }

                                                                    @endphp
                                                                    <td>
                                                                        <span class="uk-badge @if($check) uk-badge-success @else uk-badge-danger @endif">{{$aKey}}</span>
                                                                    </td>
                                                                    @if($loop->iteration == 3)
                                                                </tr>
                                                            @endif

                                                        @endforeach

                                                    @endforeach

                                                @endforeach
                                            </table>


                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                </td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>

@endsection