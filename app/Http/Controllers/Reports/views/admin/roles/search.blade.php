<aside id="sidebar_secondary" class="tabbed_sidebar" style="padding-top: 0px;width: 400px">
    <ul class="uk-tab uk-tab-icons uk-tab-grid" data-uk-tab="{connect:'#dashboard_sidebar_tabs', animation:'slide-horizontal'}"></ul>

    <div class="scrollbar-inner">
        <ul id="dashboard_sidebar_tabs" class="uk-switcher">
            <li>
                <div class="timeline timeline_small uk-margin-bottom">
                    {!! Form::open(['method' => 'GET']) !!}
                    <div class="uk-form-row searchPanel">

                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1">
                                {!! Form::label('user_id','شماره مدیر') !!}
                                {!! Form::text('user_id',request('user_id'),['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                    {!! Form::label('start_created','تاریخ شروع') !!}
                                    {!! Form::text('start_created',request('start_created'),['class' => 'md-input date','id' => 'start_created']) !!}
                                </div>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <div class="uk-input-group">
                                    <span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
                                    {!! Form::label('end_created','تاریخ شروع') !!}
                                    {!! Form::text('end_created',request('end_created'),['class' => 'md-input date','id' => 'end_created']) !!}
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-wave-light waves-effect waves-button waves-light" style="margin-top: 10px">جستجو</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </li>
        </ul>
    </div>
</aside>
