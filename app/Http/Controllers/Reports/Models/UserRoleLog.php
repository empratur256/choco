<?php

namespace Sedehi\Http\Controllers\Reports\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Role\Models\Role;
use Sedehi\Http\Controllers\User\Models\User;

class UserRoleLog extends Model
{

    use  Filterable;

    protected $table      = 'user_role_log';
    public    $timestamps = true;

    protected $filterable = [
        'user_id',
        'role_id',
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

}
