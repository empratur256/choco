<?php
return [
    'reports' => [
        'order'   => 31,
        'title'   => 'گزارش ها',
        'icon'    => 'fa fa-database',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'تغییر وضعیت سفارش',
                'action'     => 'Reports\Controllers\Admin\ReportsController@index',
                'parameters' => [],
            ],
            'Roles' => [
                'title'      => 'تغییر وضعیت گروه کاربری',
                'action'     => 'Reports\Controllers\Admin\ReportsController@roles',
                'parameters' => [],
            ],
        ],
    ],
];
