<?php
return [
    'reports' => [
        'title'  => 'گزارش ها',
        'access' => [
            'ReportsController' => [
                'تغییر وضعیت سفارش'       => 'index',
                'تغییر وضعیت گروه کاربری' => 'roles',
            ],
        ],
    ],
];
