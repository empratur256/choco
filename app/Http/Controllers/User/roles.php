<?php
return [
    'user' => [
        'title'  => 'کاربران و راهبران',
        'access' => [
            'UserController'    => [
                'لیست'          => 'index',
                'ایجاد'         => [
                    'create',
                    'store',
                ],
                'ویرایش'        => [
                    'edit',
                    'update',
                ],
                'حذف'           => 'destroy',
                'مشاهده جزئیات' => 'showInfo',
                'ورود با کاربر' => 'loginByUser',
            ],
            'AddressController' => [
                'لیست آدرس ها' => 'index',
                'ایجاد آدرس'   => [
                    'create',
                    'store',
                ],
                'ویرایش آدرس'  => [
                    'edit',
                    'update',
                ],
                'حذف آدرس'     => 'destroy',
            ],
        ],
    ],
];
