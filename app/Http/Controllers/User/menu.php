<?php
return [
    'user' => [
        'order'   => 1,
        'title'   => 'راهبران و کاربران',
        'icon'    => 'fa fa-user',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index'       => [
                'title'  => 'لیست کاربران',
                'action' => 'User\Controllers\Admin\UserController@index',
            ],
            'Add'         => [
                'title'  => 'ایجاد',
                'action' => 'User\Controllers\Admin\UserController@create',
            ],
            'Admin-Index' => [
                'title'      => 'لیست راهبران',
                'action'     => 'User\Controllers\Admin\UserController@index',
                'parameters' => [
                    'admin',
                ],
            ],
        ],
    ],
];
