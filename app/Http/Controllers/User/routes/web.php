<?php
Route::group([
                 'prefix'     => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace'  => 'User\Controllers\Admin',
             ], function(){
    Route::get('user/profile', 'UserController@showProfile');
    Route::post('user/profile', 'UserController@profile');
    Route::get('user/info/{id}', 'UserController@showInfo');
    Route::get('user/login/{token}', 'UserController@loginByUser');
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::resource('user.address', 'AddressController', ['except' => ['show']]);
});
Route::group([
                 'prefix'     => config('site.dashboard'),
                 'middleware' => 'dashboard',
                 'namespace'  => 'User\Controllers\Site',
             ], function(){
    Route::get('/', 'DashboardController@index');
    Route::get('/favorite', 'DashboardController@favorite');
    Route::get('/edit', 'DashboardController@edit');
    Route::post('/edit', 'DashboardController@update');
    Route::resource('address', 'AddressController', [
        'only' => [
            'store',
            'update',
            'destroy',
        ],
    ]);
});
Route::get('/r/{rand}-{id}', 'User\Controllers\Site\UserController@presented');