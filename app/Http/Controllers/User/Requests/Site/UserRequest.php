<?php

namespace Sedehi\Http\Controllers\User\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Sedehi\Http\Controllers\User\Models\User;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('user.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return User::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return User::where('author_id',auth()->user()->id)->find($this->route()->parameter('user'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'update':
                return [
                    'name'         => 'required',
                    'family'       => 'required',
                    'email'        => [
                        'required',
                        'email',
                        Rule::unique('users')->ignore(auth()->user()->id)->whereNull('deleted_at'),
                    ],
                    'mobile'        => [
                        'required',
                        'iran_mobile',
                        Rule::unique('users')->ignore(auth()->user()->id)->whereNull('deleted_at'),
                    ],
                    'old_password' => 'min:6',
                    'password'     => 'required_with:old_password|min:6|confirmed',
                ];
            break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'password.required_with' => trans('site.validation_password'),
        ];
    }

}
