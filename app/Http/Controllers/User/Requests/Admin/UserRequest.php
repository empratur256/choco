<?php

namespace Sedehi\Http\Controllers\User\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\User\Models\User;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('user.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return User::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return User::where('author_id',auth()->user()->id)->find($this->route()->parameter('user'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'name'     => 'required',
                    'family'   => 'required',
                    'email'    => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
                    'password' => 'required|min:6',
                    'ban'      => 'required|in:0,1',
                ];
            break;
            case 'update':
                return [
                    'name'     => 'required',
                    'family'   => 'required',
                    'email'    => 'required|email|unique:users,email,'.$this->route()
                                                                            ->getParameter('user').',id,deleted_at,NULL',
                    'password' => 'min:6',
                    'ban'      => 'required|in:0,1',
                ];
            break;
            case 'profile':
                return [
                    'name'     => 'required',
                    'family'   => 'required',
                    'email'    => 'required|email|unique:users,email,'.auth()->user()->id,
                    'password' => 'min:6',
                ];
            break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'group.required_if' => 'گروه مدیریتی الزامی است',
        ];
    }
}
