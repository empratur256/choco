<?php

namespace Sedehi\Http\Controllers\User\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\User\Models\User;

class AddressRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('user.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return User::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return User::where('author_id',auth()->user()->id)->find($this->route()->parameter('user'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'province_id' => 'required|numeric',
                    'city_id'     => 'numeric',
                    'postal_code' => 'required|numeric',
                    'name_family' => 'required',
                    'mobile'      => 'required|digits:11',
                    'tel'         => 'required|digits_between:3,11',
                    'address'     => 'required',
                ];
            break;
            case 'update':
                return [
                    'province_id' => 'required|numeric',
                    'city_id'     => 'numeric',
                    'postal_code' => 'required|numeric',
                    'name_family' => 'required',
                    'mobile'      => 'required|digits:11',
                    'tel'         => 'required|digits_between:3,11',
                    'address'     => 'required',
                ];
            break;
        }

        return [];
    }

}
