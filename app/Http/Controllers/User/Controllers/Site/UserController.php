<?php

namespace Sedehi\Http\Controllers\User\Controllers\Site;

use Carbon\Carbon;
use Cookie;
use Illuminate\Http\Request;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\User\Models\User;

class UserController extends Controller
{

    public function presented($rand, $id)
    {
        $setting = Setting::first();
        $user    = User::where('coupon_rand', $rand)->find($id);
        if(!$setting->ref_coupon || is_null($user)){
            return redirect()->action('HomeController@index');
        }
        $cookie = cookie()->make('presented_by', $user->id, Carbon::now()->addDays(config('site.day_for_get_coupon'))
                                                                  ->diffInMinutes());

        return redirect()->action('HomeController@index')->withCookie($cookie);
    }
}
