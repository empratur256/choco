<?php

namespace Sedehi\Http\Controllers\User\Controllers\Site;

use Sedehi\Http\Controllers\User\Models\Address;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\User\Models\User;
use Sedehi\Http\Controllers\User\Requests\Site\AddressRequest;
use Illuminate\Http\Request;

class AddressController extends Controller
{

    public function store(AddressRequest $request)
    {
        $item              = new Address();
        $item->province_id = $request->get('province_id');
        $item->user_id     = auth()->user()->id;
        $item->city_id     = $request->get('city_id');
        $item->postal_code = $request->get('postal_code');
        $item->name_family = $request->get('name_family');
        $item->mobile      = $request->get('mobile');
        $item->tel         = $request->get('phone_code').'-'.$request->get('tel');
        $item->address     = $request->get('address');
        $item->save();

        return redirect()->back()->with('success', trans('site.saved'));
    }

    public function update(AddressRequest $request, $id)
    {
        $item              = Address::where('user_id', auth()->user()->id)->findOrFail($id);
        $item->province_id = $request->get('province_id');
        $item->city_id     = $request->get('city_id');
        $item->postal_code = $request->get('postal_code');
        $item->name_family = $request->get('name_family');
        $item->mobile      = $request->get('mobile');
        $item->tel         = $request->get('phone_code').'-'.$request->get('tel');
        $item->address     = $request->get('address');
        $item->save();
        if ($request->has('return')) {
            return redirect()->to($request->get('return'))->with('success', trans('site.saved'));
        }

        return redirect()->back()->with('success', trans('site.saved'));
    }

    public function destroy(AddressRequest $request)
    {
        Address::where('id', $request->get('deleteId'))->where('user_id', auth()->user()->id)->delete();

        if($request->has('return')) {
            return redirect()->to($request->get('return'))->with('success', trans('site.deleted'));
        }

        return redirect()->back()->with('success', trans('site.deleted'));

    }
}
