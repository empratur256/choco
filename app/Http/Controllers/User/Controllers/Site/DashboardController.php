<?php

namespace Sedehi\Http\Controllers\User\Controllers\Site;

use Hash;
use Illuminate\Http\Request;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\User\Requests\Site\UserRequest;
use Sedehi\Http\Controllers\User\Models\Address;

class DashboardController extends Controller
{

    public function index()
    {
        $orders = auth()->user()->orders()->with([
            'address' => function ($query) {
                $query->with([
                    'shipping' => function ($query) {
                        $query->withTrashed();
                    },
                    'city' => function ($query) {
                        $query->withTrashed();
                    },
                    'province' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'items' => function ($query) {
                $query->with([
                    'product' => function ($query) {
                        $query->withTrashed();
                    }
                ]);
            },
        ])->latest()->limit(6)->get();
        $tickets = auth()->user()->tickets()->latest()->limit(6)->get();
        $user = auth()->user();
        $address = Address::where('user_id', $user->id)->first();

        return view('User.views.site.profile',
            compact(
                'orders',
                'tickets',
                'user',
                'address'
            )
        );
    }

    public function favorite()
    {
        $favorites = auth()->user()->favorite()->paginate(10);

        return view('User.views.site.favorites', compact('favorites'));
    }

    public function edit()
    {
        return view('User.views.site.edit');
    }

    public function update(UserRequest $request)
    {
        $user = auth()->user();
        $user->name = $request->get('name');
        $user->family = $request->get('family');
        $user->email = $request->get('email');
        $user->mobile = $request->get('mobile');
        if ($request->has('old_password')) {
            if (Hash::check($request->get('old_password'), $user->password)) {
                $user->password = bcrypt($request->get('password'));
            } else {
                return redirect()->back()->withInput()->with('error', trans('site.user.password_invalid'));
            }
        }
        $user->save();

        return redirect()->action('User\Controllers\Site\DashboardController@index')
            ->with('success', trans('site.saved'));
    }

}
