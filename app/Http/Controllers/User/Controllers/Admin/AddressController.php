<?php

namespace Sedehi\Http\Controllers\User\Controllers\Admin;

use Sedehi\Http\Controllers\City\Models\City;
use Sedehi\Http\Controllers\User\Models\Address;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\User\Models\User;
use Sedehi\Http\Controllers\User\Requests\Admin\AddressRequest;
use Illuminate\Http\Request;

class AddressController extends Controller
{

    public function index($id)
    {
        $user  = User::withTrashed()->findOrFail($id);
        $items = $user->addresses()->latest()->with('city', 'province')->paginate(20);

        return view('User.views.admin.address.index', compact('items', 'id'));
    }

    public function create($id)
    {
        $user      = User::withTrashed()->findOrFail($id);
        $provinces = City::roots()->pluck('name', 'id');

        return view('User.views.admin.address.add', compact('provinces', 'id'));
    }

    public function store(AddressRequest $request, $id)
    {
        $user = User::withTrashed()->findOrFail($id);
        $item              = new Address();
        $item->province_id = $request->get('province_id');
        $item->user_id     = $user->id;
        $item->city_id     = $request->get('city_id');
        $item->postal_code = $request->get('postal_code');
        $item->name_family = $request->get('name_family');
        $item->mobile      = $request->get('mobile');
        $item->tel         = $request->get('tel');
        $item->address     = $request->get('address');
        $item->save();

        return redirect()->action('User\Controllers\Admin\AddressController@index', $user->id)
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id, $addressId)
    {
        $user      = User::withTrashed()->findOrFail($id);
        $item      = $user->addresses()->findOrFail($addressId);
        $provinces = City::roots()->pluck('name', 'id');

        return view('User.views.admin.address.edit', compact('item', 'provinces', 'id'));
    }

    public function update(AddressRequest $request, $id, $addressId)
    {
        $user = User::withTrashed()->findOrFail($id);
        $item              = Address::findOrFail($addressId);
        $item->province_id = $request->get('province_id');
        $item->user_id     = $user->id;
        $item->city_id     = $request->get('city_id');
        $item->postal_code = $request->get('postal_code');
        $item->name_family = $request->get('name_family');
        $item->mobile      = $request->get('mobile');
        $item->tel         = $request->get('tel');
        $item->address     = $request->get('address');
        $item->save();

        return redirect()->action('User\Controllers\Admin\AddressController@index', $user->id)
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(AddressRequest $request, $id)
    {
        $user = User::withTrashed()->findOrFail($id);
        Address::where('user_id', $user->id)->whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

}
