<?php

namespace Sedehi\Http\Controllers\User\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Sedehi\Http\Controllers\Role\Models\Role;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\Ticket\Models\Department;
use Sedehi\Http\Controllers\User\Models\UserLog;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\User\Models\User;
use Sedehi\Http\Controllers\User\Requests\Admin\UserRequest;

class UserController extends Controller
{

    public function index()
    {
        $query = User::query();
        if(request()->exists('admin')){
            $query->has('roles')->with('roles');
        }else{
            $query->has('roles', '<', 1);
        }
        $items = $query->where('programmer', 0)->filter()->latest()->paginate(20);

        return view('User.views..admin.user.index', compact('items'));
    }

    public function create()
    {
        $groups      = Role::all();
        $departments = Department::all();

        return view('User.views.admin.user.add', compact('groups', 'admin', 'departments'));
    }

    public function store(UserRequest $request)
    {
        $item              = new User();
        $item->name        = $request->get('name');
        $item->family      = $request->get('family');
        $item->email       = $request->get('email');
        $item->password    = bcrypt($request->get('password'));
        $item->ban         = $request->get('ban');
        $item->coupon_rand = rand(10, 99);
        $item->save();
        $item->roles()->sync(($request->has('group')) ? $request->get('group') : []);
        $item->departments()->sync(($request->has('department')) ? $request->get('department') : []);
        if(count($request->get('group'))){
            return redirect()->action('User\Controllers\Admin\UserController@index', ['admin'])
                             ->with('success', 'اطلاعات با موفقیت ذخیره شد');
        }

        return redirect()->action('User\Controllers\Admin\UserController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = User::with('roles')->findOrFail($id);
        $groups             = Role::all();
        $relatedGroups      = $item->roles()->getRelatedIds()->toArray();
        $departments        = Department::all();
        $relatedDepartments = $item->departments()->getRelatedIds()->toArray();

        return view('User.views.admin.user.edit', compact('item', 'relatedGroups', 'groups', 'admin', 'departments', 'relatedDepartments'));
    }

    public function update(UserRequest $request, $id)
    {
        $item         = User::findOrFail($id);
        $item->name   = $request->get('name');
        $item->family = $request->get('family');
        $item->email  = $request->get('email');
        $item->ban    = $request->get('ban');
        if($request->has('password')){
            $item->password = bcrypt($request->get('password'));
        }
        $item->save();
        $item->roles()->sync(($request->has('group')) ? $request->get('group') : []);
        $item->departments()->sync(($request->has('department')) ? $request->get('department') : []);
        if(count($request->get('group'))){
            return redirect()->action('User\Controllers\Admin\UserController@index', ['admin'])
                             ->with('success', 'اطلاعات با موفقیت ذخیره شد');
        }

        return redirect()->action('User\Controllers\Admin\UserController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(UserRequest $request)
    {
        User::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

    public function showProfile()
    {
        $user = auth()->user();

        return view('User.views.admin.user.profile', compact('user'));
    }

    public function profile(UserRequest $request)
    {
        $user         = auth()->user();
        $user->name   = $request->get('name');
        $user->family = $request->get('family');
        $user->email  = $request->get('email');
        if($request->has('password')){
            $user->password = bcrypt($request->get('password'));
        }
        $user->save();

        return back()->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function removeLog()
    {
        $setting = Setting::first();
        $date    = null;
        switch($setting->log_remove_period){
            case 'daily';
                $date = Carbon::now()->subDay();
            break;
            case 'weekly';
                $date = Carbon::now()->subWeek();
            break;
            case 'monthly';
                $date = Carbon::now()->subMonth();
            break;
            case 'yearly';
                $date = Carbon::now()->subYear();
            break;
        }
        if(!is_null($date)){
            UserLog::where('created_at', '<=', $date)->delete();
        }
    }

    public function ban(UserRequest $request)
    {
        $user = User::findOrFail($request->get('id'));
        if($request->get('type') == 'on'){
            $user->ban = 1;
        }elseif($request->get('type') == 'off'){
            $user->ban = 0;
        }
        $user->save();
    }

    public function log($id = null)
    {
        $this->removeLog();
        $query = UserLog::query();
        if(!is_null($id)){
            $query->where('user_id', $id);
        }
        if(request()->has('email')){
            $query->whereHas('user', function($query){
                $query->where('email', 'LIKE', request('email'));
            });
        }
        $datas = $query->with('user')->searchable()->latest()->paginate(20);

        return view('User.views.admin.log', compact('datas'));
    }

    public function deleteLog(UserRequest $request)
    {
        UserLog::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

    public function showInfo($id)
    {
        $user = User::findOrFail($id);

        return view('User.views.admin.user.info', compact('user'));
    }

    public function loginByUser($token)
    {
        try{
            $userId = decrypt($token);
            $user   = User::find($userId);
            if(is_null($user)){
                return back()->with('error', 'کاربر پیدا نشد');
            }
            auth()->loginUsingId($user->id);

            return redirect()->to(config('site.dashboard'));
        }catch(DecryptException $e){
            return back()->with('error', 'مشکل در ورود با کاربر');
        }
    }
}
