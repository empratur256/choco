<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->nullable()->default(null)->index();
            $table->string('family')->nullable()->default(null)->index();
            $table->string('email')->nullable()->default(null)->index();
            $table->string('mobile')->nullable()->default(null)->index();
            $table->string('password');
            $table->tinyInteger('ban')->default(0)->index();
            $table->tinyInteger('programmer')->default(0)->index();
            $table->rememberToken();
            $table->timestamp('last_login')->index();
            $table->softDeletes();
            $table->timestamps();
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
        Schema::table('role_user', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
