<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('user_address', function(Blueprint $table){
            $table->increments('id');
            $table->integer('province_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('city_id')->unsigned()->index();
            $table->string('postal_code')->index();
            $table->string('name_family');
            $table->string('mobile');
            $table->string('tel');
            $table->text('address');
            $table->timestamps();
            $table->index('created_at');
            $table->foreign('province_id')->references('id')->on('city')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('city')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_address');
    }
}
