<?php

namespace Sedehi\Http\Controllers\User\database\seeds;

use Illuminate\Database\Seeder;
use Sedehi\Http\Controllers\Role\Models\Role;
use Sedehi\Http\Controllers\User\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $data             = new User();
        $data->name       = 'نوید';
        $data->family     = 'سه دهی';
        $data->email      = 'navid.sedehi@gmail.com';
        $data->password   = bcrypt('123456');
        $data->programmer = 1;
        $data->save();
        $data->roles()->attach(Role::first());
        $data             = new User();
        $data->name       = 'مجید';
        $data->family     = 'قائمی فر';
        $data->email      = 'mgh145@gmail.com';
        $data->password   = bcrypt('123456');
        $data->programmer = 1;
        $data->save();
        $data->roles()->attach(Role::first());
    }

}
