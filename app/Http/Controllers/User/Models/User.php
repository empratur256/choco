<?php

namespace Sedehi\Http\Controllers\User\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Coupon\Models\PresentedCouponCode;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Controllers\Role\Models\Role;
use Sedehi\Http\Controllers\Ticket\Models\Department;
use Sedehi\Http\Controllers\Ticket\Models\Ticket;

class User extends Authenticatable
{

    use Notifiable, Filterable, SoftDeletes;

    protected $dates = [
        'deleted_at',
        'last_login',
    ];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $filterable = [
        'ban',
        'id',
        'name'       => [
            'operator' => 'Like',
        ],
        'family'     => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasAnyRole()
    {
        if($this->roles->count()){
            return true;
        }

        return false;
    }

    public function hasPermission($access)
    {
        if(is_array($access)){
            foreach($access as $acc){
                if($this->checkPermission($acc)){
                    return true;
                }
            }

            return false;
        }

        return $this->checkPermission($access);
    }

    public function checkPermission($access)
    {
        $access      = strtolower($access);
        $accessArray = explode('.', $access);
        $accessCount = count($accessArray);
        foreach($this->roles as $userRole){
            $allPermissions = unserialize($userRole->permission);
            if(is_null($allPermissions)){
                return false;
            }
            switch($accessCount){
                case 1:
                    if(array_key_exists($access, $allPermissions)){
                        return true;
                    }
                break;
                case 2:
                    $sectionName = $accessArray[0];
                    $controller  = strtolower($accessArray[1]);
                    if(isset($allPermissions[$sectionName])){
                        if(isset($allPermissions[$sectionName][$controller])){
                            return true;
                        }
                    }
                break;
                case 3:
                    $sectionName = $accessArray[0];
                    $controller  = $accessArray[1];
                    $method      = $accessArray[2];
                    if(isset($allPermissions[$sectionName])){
                        if(isset($allPermissions[$sectionName][$controller])){
                            foreach($allPermissions[$sectionName][$controller] as $methods => $val){
                                if(in_array($method, explode(',', $methods))){
                                    return true;
                                }
                            }
                        }
                    }
                break;
            }

            return false;
        }

        return false;
    }

    public function logs()
    {
        return $this->hasMany(UserLog::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

    public function favorite()
    {
        return $this->belongsToMany(Product::class, 'product_favorite');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function presentedCouponCode()
    {
        return $this->hasMany(PresentedCouponCode::class);
    }
}
