<?php

namespace Sedehi\Http\Controllers\User\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class UserLog extends Model
{
    use  Filterable;

    protected $table      = 'user_logs';
    public    $timestamps = true;

    protected $filterable = [
        'name'       => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

}
