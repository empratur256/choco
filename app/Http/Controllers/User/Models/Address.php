<?php

namespace Sedehi\Http\Controllers\User\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\City\Models\City;
use Sedehi\Http\Controllers\Transport\Models\Transport;

class Address extends Model
{

    use  Filterable;

    protected $table      = 'user_address';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function province()
    {
        return $this->belongsTo(City::class, 'province_id');
    }

    public function shipping()
    {
        return $this->belongsTo(Transport::class, 'shipping_id');
    }

}
