@extends('views.layouts.site.master')
@section('title',trans('site.header.profile'))
@section('content')
    <div class="red_box">
        <div class="container">
            <h3> @lang('site.header.profile')</h3>
            <div class="divider-title"></div>
            <p> @lang('site.hi') {{auth()->user()->name.' '.auth()->user()->family}}! </p>
        </div>
    </div>

    <div class="bg-color ">
        <div class="container">
            <div style="padding-top: 1em;padding-bottom: 1em">
                <div class="bread-crumbs">
                    <span>@lang('site.home')</span>
                    @if(App::isLocale('en'))
                        <span> <i class="flaticon-right-arrow"></i> </span>
                    @else
                        <span> <i class="flaticon-left-arrow-1"></i> </span>
                    @endif
                    <span> @lang('site.user.edit_profile')</span>
                </div>
            </div>
            <div class="info-order">
                @include('views.errors.errors')
                <div class="col-sm-3 col-sm-push-9 profile text-center" style="margin-bottom: 30px;">
                    @include('views.site.partials.user')
                </div>
                <div class="col-sm-9 col-sm-pull-3" style="margin-bottom:30px;">
                    <div class="product-tabs">
                        <!-- Nav tabs -->
                        <div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                                          data-toggle="tab">@lang('site.orders.latest')</a>
                                </li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                           data-toggle="tab">@lang('site.user.wishlist')</a></li>
                            </ul>

                            <!-- Tab panes -->

                            <div class="tab-content">


                                <div role="tabpanel" class="tab-pane active" id="home" style="background-color: white;">
                                    @if(count($orders) == 6)
                                        <a href="{!! action('Order\Controllers\Site\OrderController@index') !!}"
                                           style="padding: 1.5% 0 0 1.5%;"
                                           class="red_text bolder hover-hand pull-left">@lang('site.products.more')</a>
                                        <br>
                                        <br>
                                        @php $orders->pop() @endphp
                                    @endif
                                    <div class="table-responsive" style="">
                                        <table class="table table-bordred table-striped">
                                            <thead>
                                            <tr>
                                                <th class="text-right defult-text" style="">@lang('site.orders.number')</th>
                                                <th class="text-right defult-text"
                                                    style="padding-right:1.2em">@lang('site.orders.final_price')
                                                    (@lang('site.currency'))
                                                </th>
                                                <th class="text-right defult-text"
                                                    style="padding-right:1.2em">@lang('site.orders.discount')
                                                    (@lang('site.currency'))
                                                </th>
                                                <th class="text-right defult-text"
                                                    style="padding-right:1.2em">@lang('site.date')</th>
                                                <th class="text-right defult-text"
                                                    style="padding-right:1.2em">@lang('site.status')</th>
                                                <th class="text-right defult-text" style="padding-right:1.2em"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @forelse($orders as $order)
                                                <tr>
                                                    <td>{{ $order->id }}</td>
                                                    <td>{{number_format($order->full_price)}}</td>
                                                    <td>@if($order->discount > 0){{number_format($order->discount)}}  @else @lang('site.not') @endif</td>
                                                    <td>{{jdate('Y/m/d',$order->created_at->timestamp,'','','en')}}</td>
                                                    <td>
                                                        @if($order->delivered)
                                                            <i class="fa fa-circle" aria-hidden="true"
                                                               style="padding-left: 0.5em; color:darkgreen">@lang('site.orders.delivered')</i>
                                                        @elseif($order->ready_to_send)
                                                            <i class="fa fa-circle" aria-hidden="true"
                                                               style="padding-left: 0.5em; color:darkgreen">  @lang('site.orders.ready_to_send')</i>
                                                        @elseif($order->processing_stock)
                                                            <i class="fa fa-circle" aria-hidden="true"
                                                               style="padding-left: 0.5em; color:darkgreen">  @lang('site.orders.processing_stock')</i>
                                                        @elseif($order->in_production_procedure)
                                                            <i class="fa fa-circle" aria-hidden="true"
                                                               style="padding-left: 0.5em; color:darkgreen"> @lang('site.orders.in_production_procedure')</i>
                                                        @elseif($order->paid)
                                                            <i class="fa fa-circle" aria-hidden="true"
                                                               style="padding-left: 0.5em; color:darkgreen">  @lang('site.orders.paid')</i>
                                                        @elseif($order->confirm)
                                                            <i class="fa fa-circle" aria-hidden="true"
                                                               style="padding-left: 0.5em; color:darkgreen">   @lang('site.orders.confirm')</i>
                                                        @endif
                                                        @if($order->paid == 0)
                                                            <a class="btn grayish-btn red_box"
                                                               href="{!! action('Order\Controllers\Site\OrderController@payment',[$order->id,$order->payment_type]) !!}">@lang('site.pay')</a>
                                                        @endif
                                                        @if($order->paid == 1 && $order->type == 'custom')
                                                            <a class="btn grayish-btn red_box"
                                                               href="{!! action('CustomOrder\Controllers\Site\OrderController@reorder',[$order->id]) !!}">@lang('site.custom_again')</a>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="" class="open-info">
                                                            <i class="fa fa-angle-up fa-2x" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <tr style="display: none;height: 400px">
                                                    <td style="background-color: white" colspan="5">
                                                        <table class="table table-bordred table-striped">
                                                            <thead>
                                                            <tr>
                                                                <th class="text-right defult-text"
                                                                    style="">@lang('site.orders.product')</th>
                                                                <th class="text-right defult-text"
                                                                    style="padding-right:1.2em">@lang('site.quantity')
                                                                </th>
                                                                @if($order->type == 'order')
                                                                    <th class="text-right defult-text"
                                                                        style="padding-right:1.2em">@lang('site.carts.unit_price')
                                                                        (@lang('site.currency'))</th>
                                                                @endif
                                                                <th class="text-right defult-text"
                                                                    style="padding-right:1.2em">@lang('site.carts.total_price')
                                                                    (@lang('site.currency'))
                                                                </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($order->items as $item)
                                                                <tr style="background-color: white">
                                                                    <td class="bolder">
                                                                        @if($order->type == 'order')
                                                                            {{$item->product->title}}
                                                                        @elseif($order->type == 'custom')
                                                                            @lang('site.custom_order.custom_order')
                                                                        @endif
                                                                    </td>
                                                                    <td class="bolder">
                                                                        @if($order->type == 'order')
                                                                            {{ $item->quantity }}
                                                                        @elseif($order->type == 'custom')
                                                                            {{ $order->custom_order_count }}
                                                                        @endif
                                                                    </td>
                                                                    @if($order->type == 'order')
                                                                        <td class="bolder">{{ number_format($item->price) }}</td>
                                                                    @endif
                                                                    <td class="bolder">
                                                                        @if($order->type == 'order')
                                                                            {{ number_format($item->price * $item->quantity) }}
                                                                        @elseif($order->type == 'custom')
                                                                            {{ number_format($order->total_price) }}
                                                                        @endif
                                                                    </td>

                                                                </tr>

                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                        <div class="col-xs-12 col-centered text-center">
                                                            <div class="bought-progress border-bottom" dir="rtl">
                                                                <div class="progres">
                                                                    <div class="circle @if($order->confirm) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                        <span class="title">@lang('site.orders.confirm')</span>
                                                                    </div>
                                                                    <span class="bar @if($order->paid) done @endif">
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                    <div class="circle @if($order->paid) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                        <span class="title">@lang('site.orders.paid')</span>
                                                                    </div>
                                                                    @if($order->need_to_produce)
                                                                        <span class="bar @if($order->in_production_procedure) done @endif">
                                                                         <i class="fa fa-circle" aria-hidden="true"
                                                                            @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    </span>
                                                                        <div class="circle @if($order->in_production_procedure) done @endif">
                                                                        <span class="label"><span
                                                                                    class="fa fa-check"></span></span>
                                                                            <span class="title">@lang('site.orders.in_production_procedure')</span>
                                                                        </div>
                                                                    @endif
                                                                    <span class="bar @if($order->processing_stock) done @endif">
                                                                     <i class="fa fa-circle" aria-hidden="true"
                                                                        @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                    <div class="circle @if($order->processing_stock) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                        <span class="title">@lang('site.orders.processing_stock')</span>
                                                                    </div>
                                                                    <span class="bar @if($order->ready_to_send) done @endif">
                                                                     <i class="fa fa-circle" aria-hidden="true"
                                                                        @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                    <div class="circle @if($order->ready_to_send) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                        <span class="title">@lang('site.orders.ready_to_send')</span>
                                                                    </div>
                                                                    <span class="bar @if($order->delivered) done @endif">
                                                                     <i class="fa fa-circle" aria-hidden="true"
                                                                        @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                    <div class="circle @if($order->delivered) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                        <span class="title">@lang('site.orders.delivered')</span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="padding-top-2">
                                                            <div style="padding: 1em; border:1px solid #ebebeb ">
                                                                <p class="padding-top-1"><span class="red_text bolder">@lang('site.carts.shipping_type')
                                                                        :</span>
                                                                    {{$order->address->shipping->title}}
                                                                    @if($order->shipping_price > 0) @lang('site.currency') {{number_format($order->shipping_price)}}  @else @lang('site.free') @endif
                                                                </p>
                                                                @if($order->tax_price > 0 && $order->tax > 0)
                                                                    <p class="padding-top-1"><span class="red_text bolder"> @lang('site.carts.tax')
                                                                            :</span>
                                                                        {{ number_format($order->tax_price) }} @lang('site.currency')
                                                                    </p>
                                                                @endif
                                                                <p class="padding-top-1"><span class="red_text bolder">@lang('site.orders.get_address')
                                                                        :</span>
                                                                    {{ $order->address->province->name.' - '.$order->address->city->name.' - '.$order->address->address.' - '.trans('site.postal_code').':'.$order->address->postal_code }}
                                                                </p>
                                                                <p class="padding-top-1"><span class="red_text bolder">@lang('site.orders.transferee')
                                                                        :</span>
                                                                    {{ $order->address->name_family }}</p>
                                                                @if(strlen($order->address->track_id) > 0)
                                                                    <p class="padding-top-1"><span class="red_text bolder">@lang('site.orders.tracking_code')
                                                                            :</span>
                                                                        {{$order->address->track_id}}</p>
                                                                @endif
                                                                <p class="padding-top-1"><span class="red_text bolder">@lang('site.telephone')
                                                                        :</span>
                                                                    {{ $order->address->mobile.' - '.$order->address->tel }}
                                                                </p>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="6" class="text-center">@lang('site.orders.is_empty')</td>
                                                </tr>
                                            @endforelse

                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile" style="background-color: white;">
                                    @include('views.site.partials.favorite')
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 border sent-tickets">
                        <h5 class="pull-right bolder">
                            @lang('site.ticket.list')
                        </h5>

                        @if(count($tickets) == 6)
                            <h5 class="pull-left">
                                <a href="{!! action('Ticket\Controllers\Site\TicketController@index') !!}"
                                   style="padding-right: 10px;"
                                   class="red_text bolder hover-hand"> @lang('site.products.more') </a>
                            </h5>
                            @php $tickets->pop() @endphp
                        @endif
                        <h5 class="pull-left">
                            <a class="red_text bolder hover-hand"
                               href="{!! action('Ticket\Controllers\Site\TicketController@create') !!}"> @lang('site.ticket.send') </a>
                        </h5>
                    </div>
                    <div class="col-xs-12 bg-white">
                        <div class="table-responsive clear_both suport-request padding-bottom-2" style="">
                            <table class="table table-bordred table-striped bg-white">
                                <thead>
                                <tr>
                                    <th class="text-right defult-text" style="">@lang('site.ticket.subject')</th>
                                    <th class="text-right defult-text" style="padding-right:1.2em">@lang('site.status')</th>
                                    <th class="text-right defult-text" style="padding-right:1.2em">@lang('site.date')</th>
                                    <th></th>
                                </tr>

                                </thead>
                                <tbody>
                                @forelse($tickets as $ticket)
                                    <tr @if($ticket->readed == 0) style="font-weight: bold" @endif>
                                        <td class="bolder">{{ str_limit($ticket->subject,30) }}</td>
                                        <td class="bolder"><i class="fa fa-circle" aria-hidden="true"
                                                              style="padding-left: 0.5em; color:@if($ticket->readed == 0) red @else aqua  @endif">
                                            </i> @if($ticket->readed == 0) @lang('site.ticket.ok_answer') @else  @lang('site.ticket.no_answer') @endif
                                        </td>
                                        <td class="bolder">{{ jdate('Y/m/d',$ticket->created_at->timestamp) }}</td>
                                        <td class="bolder red_text text-center">
                                            <a href="{!! action('Ticket\Controllers\Site\TicketController@show',$ticket->id) !!}"
                                               class="dark-blue-text">
                                                @lang('site.ticket.show')
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">@lang('site.ticket.is_empty')</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/profile.css') }}
@endpush

@push('js')
    <script>
        $(document).ready(function () {
            $('.open-info').on('click', openNext);
        });

        function openNext(e) {
            e.preventDefault();
            $(this).find('.fa-angle-up').toggleClass('icon-rotate-180');
            $(e.target).closest('tr').next().slideToggle("slow");
            $(".test").slideToggle("slow");
        }
    </script>
@endpush