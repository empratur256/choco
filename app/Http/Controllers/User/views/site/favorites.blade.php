@extends('views.layouts.site.master')
@section('title', trans('site.favorites'))
@section('content')

<div class="red_box">
    <div class="container">
        <span> @lang('site.favorites') </span>
        <p> @lang('site.favorites_list') </p>
    </div>
</div>

    <div class="profile-page">
        <div class="container">
            
            @include('views.errors.errors')
            <div class="row">
                <div class="col-xs-12 right-panel">
                    <div class="row">
                        <div class="user-title purple-text col-xs-12">
                          <span class="heading green-text">@lang('site.user.wishlist')</span>
                        </div>
                        <div class="col-sm-8 col-xs-12 col-sm-pull-2 favorites with-border">
                            
                            @forelse($favorites as $favorite)
                                <div class="item @if(!$loop->last) border-bottom @endif">
                                    <img src="{{ asset('uploads/product/'.$favorite->id.'/111x111-'.$favorite->picture) }}" alt="item picture">
                                    <div class="info">
                                        <a href="{!! action('Product\Controllers\Site\ProductController@show',[$favorite->id,utf8_slug($favorite->title)]) !!}" class="name">{{str_limit($favorite->title,60)}}</a>
                                        <div class="price grayish-text">@if(finalPrice($favorite) > 0) {{ number_format(finalPrice($favorite)) }} @lang('site.currency') @endif</div>
                                        <div class="available dark-blue-text">@if($favorite->quantity > 0) @lang('site.products.available') @else @lang('site.products.unavailable') @endif</div>
                                        {!! Form::open(['action' => 'Product\Controllers\Site\ProductController@favorite']) !!}
                                        {!! Form::hidden('id',$favorite->id) !!}
                                        <button type="submit" class="remove-favorite">
                                            <span class="fa fa-times"></span>
                                        </button>
                                        {!! Form::close() !!}

                                    </div>
                                </div>

                            @empty
                                <div class="item">@lang('site.user.wishlist_empty')</div>
                            @endforelse
                        </div>

                        <div class="text-center col-xs-12" style="text-align: center">
                            {!! $favorites->appends(Request::except('page'))->links() !!}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('css')

   {{ Html::style('assets/site/css/public.css') }}
   {{ Html::style('assets/site/css/favorits.css') }}
   


@endpush