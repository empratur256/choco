@extends('views.layouts.site.master')
@section('title', trans('site.user.edit_profile'))
@section('content')
    <div class="red_box">
        <div class="container">
            <h3>@lang('site.user.edit_profile')</h3>
            <div class="divider-title"></div>
            <p>@lang('site.user.top_text')</p>
        </div>
    </div>
    <div style="padding-top: 1em;padding-bottom: 3em">
        <div class="container bread-crumbs">
            <span>@lang('site.home')</span>
            @if(App::isLocale('en'))
                <span> <i class="flaticon-right-arrow"></i> </span>
            @else
                <span> <i class="flaticon-left-arrow-1"></i> </span>
            @endif
            <span> @lang('site.user.edit_profile')</span>
        </div>
    </div>
    <div class="profile-page">
        <div class="container">
            <div class="row edit-profile-page">


                {{ Form::model(auth()->user(),['action' => 'User\Controllers\Site\DashboardController@update']) }}

                <div class="col-xs-12 col-sm-6 file-inp col-sm-push-3">
                    @include('views.errors.errors')
                    <div class="form-group">
                        {{ Form::text('name',null,['class' => 'form-control', 'placeholder' => trans('site.user.name')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('family',null,['class' => 'form-control', 'placeholder' => trans('site.user.family')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::email('email',null,['class' => 'form-control', 'placeholder' => trans('site.user.email')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::text('mobile',null,['class' => 'form-control', 'placeholder' => trans('site.user.mobile')]) }}
                    </div>
                    {!! HTML::alert(trans('site.user.change_password_alert'),'alert-warning') !!}
                    <div class="form-group">
                        {{ Form::password('old_password',['class' => 'form-control', 'placeholder' => trans('site.user.old_password')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password',['class' => 'form-control', 'placeholder' => trans('site.password')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::password('password_confirmation',['class' => 'form-control', 'placeholder' => trans('site.password_confirmation')]) }}
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn purple-btn">@lang('site.save')</button>
                    </div>
                </div>


                {{ Form::close() }}
            </div>
        </div>


    </div>


@endsection

@push('css')
    {{ Html::style('assets/site/css/public.css') }}
@endpush
