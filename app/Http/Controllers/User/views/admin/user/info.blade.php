@extends('views.layouts.admin.master')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-7-10">
                    <div class="md-card">
                        <div class="user_heading">
                            @if(permission(['User.UserController.loginByUser']))
                                <div class="user_heading_menu" data-uk-dropdown="{pos:'right-top'}">
                                    <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            @if(permission('User.UserController.loginByUser'))
                                                <li>
                                                    <a href="{{ action('User\Controllers\Admin\UserController@loginByUser',encrypt($user->id)) }}">ورود با این کاربر</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            @endif

                            <div class="user_heading_avatar">
                                <div class="thumbnail">
                                    <img src="@if(is_null($user->picture)) {{ asset('assets/no-avatar.png') }} @else {{ asset('uploads/avatar/'.$user->picture) }} @endif" alt="user avatar"/>
                                </div>
                            </div>

                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom">
                                    <span class="uk-text-truncate">{{ $user->name.' '.$user->family }}</span>
                                    @if($user->hasAnyRole())
                                        <span class="sub-heading">مدیر</span>
                                    @else
                                        <span class="sub-heading">کاربر عادی</span>
                                    @endif
                                </h2>

                            </div>

                            @if(permission('User.UserController.edit'))
                                <a class="md-fab md-fab-small md-fab-accent" href="{!! action('User\Controllers\Admin\UserController@edit',$user->id) !!}">
                                    <i class="material-icons">&#xE150;</i>
                                </a>
                            @endif
                        </div>
                        <div class="user_content">
                            <table class="uk-table uk-table-hover">

                                <tbody>
                                <tr>
                                    <td class="custom-title">وضعیت</td>
                                    <td>
                                        @if($user->ban == 1)
                                            <span class="uk-badge uk-badge-danger">غیرفعال</span>
                                        @else
                                            <span class="uk-badge uk-badge-success">فعال</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="custom-title">نام کاربری</td>
                                    <td>{{ is_null($user->username) ? '-' : $user->username }}</td>
                                </tr>
                                <tr>
                                    <td class="custom-title">ایمیل</td>
                                    <td>{{ is_null($user->email) ? '-' : $user->email }}</td>
                                </tr>
                                <tr>
                                    <td class="custom-title">موبایل</td>
                                    <td>{{ is_null($user->mobile) ? '-' : $user->mobile }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @if(permission(['Comments.CommentsController.index','Ticket.TicketController.index','Payment.PaymentController.index','Order.OrderController.index','User.AddressController.index']))

                    <div class="uk-width-large-3-10">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h4 class="heading_a uk-margin-bottom">لینک های مرتبط</h4>
                                @if(permission('Payment.PaymentController.index'))
                                    <div class="uk-width-medium-1-1">
                                        <a target="_blank" href="{!! action('Payment\Controllers\Admin\PaymentController@index',['user_id'=>$user->id]) !!}" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">تراکنش های مالی</a>
                                    </div>
                                    <br>
                                @endif
                                @if(permission('Order.OrderController.index'))
                                    <div class="uk-width-medium-1-1">
                                        <a target="_blank" href="{!! action('Order\Controllers\Admin\OrderController@index',['user_id'=>$user->id]) !!}" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">سفارش ها</a>
                                    </div>
                                    <br>
                                @endif
                                @if(permission('User.AddressController.index'))
                                    <div class="uk-width-medium-1-1">
                                        <a target="_blank" href="{!! action('User\Controllers\Admin\AddressController@index',[$user->id]) !!}" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">آدرس ها</a>
                                    </div>
                                    <br>
                                @endif
                                @if(permission('Ticket.TicketController.index'))
                                    <div class="uk-width-medium-1-1">
                                        <a target="_blank" href="{!! action('Ticket\Controllers\Admin\TicketController@index',['user_id'=>$user->id]) !!}" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">پیام های پشتیبانی</a>
                                    </div>
                                    <br>
                                @endif
                                @if(permission('Comments.CommentsController.index'))
                                    <div class="uk-width-medium-1-1">
                                        <a target="_blank" href="{!! action('Comments\Controllers\Admin\CommentsController@index',['user_id'=>$user->id]) !!}" class="md-btn md-btn-primary md-btn-block md-btn-wave-light">نظر های کاربر</a>
                                    </div>
                                    <br>
                                @endif
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>

@endsection

@push('css')
<style>
    .custom-title {
        color: #0b2b44;
        font-weight: bold;
    }
</style>
@endpush