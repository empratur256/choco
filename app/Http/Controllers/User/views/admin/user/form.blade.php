<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-2">
        {!! Form::label('name','نام') !!}
        {!! Form::text('name',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('family','نام خانوادگی') !!}
        {!! Form::text('family',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-3">
        {!! Form::label('email','ایمیل') !!}
        {!! Form::text('email',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-3">
        {!! Form::label('password','رمز عبور') !!}
        {!! Form::password('password',['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-3">
        {!! Form::label('ban','وضعیت') !!}
        {!! Form::select('ban',[0 =>'فعال',1=>'غیر فعال'],null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-clearfix"></div>
    <br>

</div>


@if(count($groups))
    <br>
    <p>گروه مدیریتی</p>
    <div class="uk-grid" data-uk-grid-margin>
        @foreach($groups as $group)
            <div class="uk-width-medium-1-5">
                {!! Form::checkbox('group[]', $group->id,isset($relatedGroups) ? in_array($group->id,$relatedGroups) : false,['data-switchery']) !!}
                <span class="text">{{ $group->name }}</span>
            </div>
        @endforeach
    </div>
@endif

@if(count($departments))
    <br>
    <p>انتخاب دپارتمان</p>
    <div class="uk-grid" data-uk-grid-margin>
        @foreach($departments as $department)
            <div class="uk-width-medium-1-5">
                {!! Form::checkbox('department[]', $department->id,isset($relatedDepartments) ? in_array($department->id,$relatedDepartments) : false,['data-switchery']) !!}
                <span class="text">{{ $department->name }}</span>
            </div>
        @endforeach
    </div>
@endif