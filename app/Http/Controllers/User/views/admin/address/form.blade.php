<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::select('province_id',$provinces,null,['class'=>'md-input province','placeholder' => 'انتخاب استان']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::select('city_id',[],null,['class'=>'md-input city','disabled' => true,'placeholder' => 'انتخاب شهر']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('name_family','نام و نام خانوادگی گیرنده') !!}
        {!! Form::text('name_family',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('mobile','موبایل گیرنده') !!}
        {!! Form::text('mobile',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('tel','تلفن ثابت') !!}
        {!! Form::text('tel',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('postal_code','کد پستی') !!}
        {!! Form::text('postal_code',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('address','آدرس') !!}
        {!! Form::textarea('address',null,['class'=>'md-input']) !!}
    </div>
</div>
@push('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}'
        }
    });
    var def = 0;
            @if(isset($item))
    var def = '{{ $item->city_id }}';
            @endif
            
            @if(count(old('city_id')))
    var def = '{{ old('city_id') }}';
    @endif
    function ajaxCity(val, def) {
        $.ajax({
            url: "{{ action('City\Controllers\Site\CityController@cities') }}",
            type: "POST",
            data: {'id': val},
            success: function (data, textStatus, jqXHR) {
                var city = $('.city');
                city.prop('disabled', false);
                city.empty();
                $.each(data, function (id, value) {
                    city.append(
                        $('<option>', {
                            value: id,
                            text: value
                        }));
                });
                if (def > 0) {
                    city.val(def);
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('مشکلی در دریافت اطلاعات وجود دارد');
            }
        });
    }
    ajaxCity($('.province option:selected').val(), def);
    $(document).ready(function () {
        $('.province').on('change', function () {
            ajaxCity($(this).val());
        })
    });
</script>
@endpush
