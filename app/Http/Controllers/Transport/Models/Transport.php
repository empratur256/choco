<?php

namespace Sedehi\Http\Controllers\Transport\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\City\Models\City;

class Transport extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'transport';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function province()
    {
        return $this->belongsTo(City::class);
    }
}
