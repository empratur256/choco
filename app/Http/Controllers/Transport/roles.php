<?php
return [
    'transport' => [
        'title'  => 'روش ارسال',
        'access' => [
            'TransportController' => [
                'لیست'   => 'index',
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
