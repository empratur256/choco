<?php

namespace Sedehi\Http\Controllers\Transport\Controllers\Site;

use Illuminate\Http\Request;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Transport\Models\Transport;

class ShippingController extends Controller
{

    public function byAddress()
    {
        $items = [];
        if (request()->has('address')) {
            $address = auth()->user()->addresses()->find(request()->get('address'));
            if (is_null($items)) {
                return response()->json($items);
            }
            $items = Transport::select(['id', 'price', 'title', 'description'])
                              ->where('province_id', $address->province_id)
                              ->get();
            if ($items->count() == 0) {
                $items = Transport::select(['id', 'price', 'title', 'description'])->whereNull('province_id')->get();
            }
        }

        foreach ($items as $item) {
            if ($item->price > 0) {
                $item->price = trans('site.carts.shipping_price').' '.number_format($item->price).' '.trans('site.currency');
            } else {
                $item->price = trans('site.free');
            }
        }

        return response()->json($items);
    }
}
