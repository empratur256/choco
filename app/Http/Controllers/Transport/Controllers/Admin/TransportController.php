<?php

namespace Sedehi\Http\Controllers\Transport\Controllers\Admin;

use Sedehi\Http\Controllers\City\Models\City;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Transport\Models\Transport;
use Sedehi\Http\Controllers\Transport\Requests\Admin\TransportRequest;
use Illuminate\Http\Request;

class TransportController extends Controller
{

    public function index()
    {
        $items = Transport::filter()->with('province')->latest()->paginate(20);

        return view('Transport.views.admin.transport.index', compact('items'));
    }

    public function create()
    {
        $province = City::roots()->pluck('name', 'id');

        return view('Transport.views.admin.transport.add', compact('province'));
    }

    public function store(TransportRequest $request)
    {
        $item        = new Transport();
        $item->title = $request->get('title');
        if ($request->has('province_id')) {
            $item->province_id = $request->get('province_id');
        }
        $item->price       = $request->get('price');
        $item->description = $request->get('description');
        $item->save();

        return redirect()
            ->action('Transport\Controllers\Admin\TransportController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item     = Transport::findOrFail($id);
        $province = City::roots()->pluck('name', 'id');

        return view('Transport.views.admin.transport.edit', compact('item', 'province'));
    }

    public function update(TransportRequest $request, $id)
    {
        $item        = Transport::findOrFail($id);
        $item->title = $request->get('title');
        if ($request->has('province_id')) {
            $item->province_id = $request->get('province_id');
        } else {
            $item->province_id = null;
        }
        $item->price       = $request->get('price');
        $item->description = $request->get('description');
        $item->save();

        return redirect()
            ->action('Transport\Controllers\Admin\TransportController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(TransportRequest $request)
    {
        Transport::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
