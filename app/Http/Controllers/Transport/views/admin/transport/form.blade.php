<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::select('province_id',$province,null,['class'=>'md-input','placeholder' => 'استان']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('price','مبلغ') !!}
        {!! Form::text('price',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('description','توضیح کوتاه') !!}
        {!! Form::text('description',null,['class'=>'md-input']) !!}
    </div>
</div>
