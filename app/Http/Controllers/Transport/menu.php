<?php
return [
    'transport' => [
        'order'   => 60,
        'title'   => 'روش ارسال',
        'icon'    => 'fa fa-truck',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'Transport\Controllers\Admin\TransportController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'Transport\Controllers\Admin\TransportController@create',
                'parameters' => [],
            ],
        ],
    ],
];
