<?php
Route::group([
                 'prefix'     => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace'  => 'Transport\Controllers\Admin',
             ], function () {
    Route::resource('transport', 'TransportController', ['except' => ['show']]);
});


Route::post('shipping', 'Transport\Controllers\Site\ShippingController@byAddress');