<?php

namespace Sedehi\Http\Controllers\Product\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Sedehi\Http\Controllers\Product\Models\Product;

class ProductRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('product.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Product::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Product::where('author_id',auth()->user()->id)->find($this->route()->parameter('product'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {

        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'title'               => 'required|max:45',
                    'code'                => [
                        'required',
                        'alpha_dash',
//                        Rule::unique('product'),
                    ],
                    'category_id'         => 'required',
                    'price_type'          => 'required',
                    'price'               => 'required_if:price_type,fixed|numeric',
                    'quantity'            => 'required_if:price_type,fixed|numeric',
                    'product.*.quantity'  => 'required_if:price_type,dynamic|numeric',
                    'product.*.price'     => 'required_if:price_type,dynamic|numeric',
                    'discount_type'       => 'required_if:discount_status,1',
                    'discount'            => 'required_if:discount_status,1|numeric',
                    'discount_time_start' => 'required_if:discount_timer,1',
                    'minute_time_start'   => 'required_if:discount_timer,1',
                    'hour_time_start'     => 'required_if:discount_timer,1',
                    'discount_time_end'   => 'required_if:discount_timer,1',
                    'minute_time_end'     => 'required_if:discount_timer,1',
                    'hour_time_end'       => 'required_if:discount_timer,1',
                    'product_picture'     => 'required|image',

                ];
            break;
            case 'update':
                return [
                    'title'               => 'required|max:45',
                    'code'                => [
                        'required',
                        'alpha_dash',
//                        Rule::unique('product')->ignore($this->route()->parameter('product')),
                    ],
                    'category_id'         => 'required',
                    'price_type'          => 'required',
                    'price'               => 'required_if:price_type,fixed|numeric',
                    'quantity'            => 'required_if:price_type,fixed|numeric',
                    'product.*.quantity'  => 'required_if:price_type,dynamic|numeric',
                    'product.*.price'     => 'required_if:price_type,dynamic|numeric',
                    'discount_type'       => 'required_if:discount_status,1',
                    'discount'            => 'required_if:discount_status,1|numeric',
                    'discount_time_start' => 'required_if:discount_timer,1',
                    'minute_time_start'   => 'required_if:discount_timer,1',
                    'hour_time_start'     => 'required_if:discount_timer,1',
                    'discount_time_end'   => 'required_if:discount_timer,1',
                    'minute_time_end'     => 'required_if:discount_timer,1',
                    'hour_time_end'       => 'required_if:discount_timer,1',
                    'product_picture'     => 'image',

                ];
            break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'product_picture.required'        => 'لطفا تصویر اصلی محصول را انتخاب کنید',
            'discount.required_if'            => 'لطفا مقدار تخفیف را وارد کنید',
            'price.required_if'               => 'لطفا مبلغ محصول را وارد کنید',
            'quantity.required_if'            => 'لطفا تعداد موجودی محصول را وارد کنید',
//            'product.*.size_id.required_if'   => 'لطفا ابعاد قیمت را تکمیل کنید',
            'product.*.price.required_if'     => 'لطفا مبلغ قیمت را تکمیل کنید',
            'product.*.quantity.required_if'  => 'لطفا تعداد موجودی را تکمیل کنید',
            'discount_time_start.required_if' => 'لطفا تاریخ شروع تخفیف را تکمیل کنید',
            'discount_time_end.required_if' => 'لطفا تاریخ پایان تخفیف را تکمیل کنید',
        ];
    }
}
