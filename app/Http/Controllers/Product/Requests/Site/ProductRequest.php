<?php

namespace Sedehi\Http\Controllers\Product\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Product\Models\Product;

class ProductRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('product.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Product::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Product::where('author_id',auth()->user()->id)->find($this->route()->parameter('product'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'favorite':
                return [
                    'id' => 'required|numeric',
                ];
            break;
        }

        return [];
    }

}
