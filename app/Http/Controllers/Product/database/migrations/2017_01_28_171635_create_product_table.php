<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product', function(Blueprint $table){
            $table->increments('id');
            $table->integer('category_id')->unsigned()->index();
            $table->integer('brand_id')->nullable()->default(null)->unsigned()->index();
            $table->string('title')->index();
            $table->string('code')->index();
            $table->text('description');
            $table->bigInteger('price')->nullable()->default(null)->unsigned()->index();
            $table->bigInteger('max_price')->nullable()->default(null)->unsigned()->index();
            $table->bigInteger('min_price')->nullable()->default(null)->unsigned()->index();
            $table->boolean('active')->default(0)->index();
            $table->string('picture')->nullable()->default(null);
            $table->integer('quantity')->default(0)->unsigned()->index();
            $table->integer('discount')->nullable()->default(null)->index();
            $table->enum('discount_type', [
                'price',
                'percent',
            ])->nullable()->default(null)->index();

            $table->enum('price_type', [
                'fixed',
                'dynamic',
            ])->default('fixed')->index();
            $table->timestamp('discount_time_start')->nullable()->index();
            $table->timestamp('discount_time_end')->nullable()->index();
            $table->integer('rate')->unsigned()->default(0)->index();
            $table->integer('comment_count')->unsigned()->default(0)->index();
            $table->integer('order_count')->unsigned()->default(0)->index();
            $table->tinyInteger('show_homepage')->unsigned()->default(0)->index();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')->references('id')->on('category')->onDelete('cascade')->onUpdate('cascade');
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
