<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowProductLangs extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('product', function(Blueprint $table){
            $table->tinyInteger('show_en')->default(1)->unsigned()->index();
            $table->tinyInteger('show_fa')->default(1)->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *x`
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
