<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPriceTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function(Blueprint $table){
            $table->increments('id');
            $table->integer('product_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('size_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('price')->unsigned()->index();
            $table->integer('quantity')->unsigned()->index();
            $table->timestamps();
            $table->index('created_at');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
