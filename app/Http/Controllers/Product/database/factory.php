<?php
use Faker\Factory as Faker;

$factory->define(\Sedehi\Http\Controllers\Product\Models\Product::class, function($faker){
    $faker = Faker::create('fa_IR');
    $brand = null;
    if($faker->boolean){
        $brand = \Sedehi\Http\Controllers\Brand\Models\Brand::inRandomOrder()->first()->id;
    }
    $deletedAt = null;
    if($faker->boolean(1)){
        $deletedAt = \Carbon\Carbon::now()->year(rand(2015, 2016))->day(rand(1, 30))->hour(rand(1, 23))
                                   ->minute(rand(1, 59))->second(rand(1, 59));
    }
    do{
        $code = $faker->randomNumber;
    }while(\Sedehi\Http\Controllers\Product\Models\Product::where('code', $code)->count());
    if($faker->boolean()){
        $priceType = 'dynamic';
        $price     = 0;
        $maxPrice  = 0;
        $minPrice  = 0;
    }else{
        $priceType = 'fixed';
        $price     = rand(100000, 10000000);
        $maxPrice  = $price;
        $minPrice  = $price;
    }
    $quantity = rand(0, 100);

    $discount            = null;
    $discount_type       = null;
    $discount_time_start = null;
    $discount_time_end   = null;
    $showHomepage        = 0;
    if($faker->boolean(10)){
        if($faker->boolean()){
            $discount_type = 'percent';
            $discount      = rand(1, 80);
        }else{
            $discount_type = 'price';
            $discount      = rand(1000, rand(2000, 10000));
        }
        if($faker->boolean()){
            $discount_time_start = \Carbon\Carbon::now()->year(2017)->month(rand(1, 5))->day(rand(1, 30))
                                                 ->hour(rand(1, 23))->minute(rand(1, 59))->second(rand(1, 59));
            $discount_time_end = $discount_time_start->addHours(rand(1, 500));
        }
        $showHomepage = $faker->boolean();
    }
    $frameType = 'none';
    if($faker->boolean(30)){
        $frameType = $faker->randomElement([
                                               'custom',
                                               'all',
                                               'custom',
                                           ]);
    }

    return [
        'category_id'         => \Sedehi\Http\Controllers\Category\Models\Category::inRandomOrder()->first()->id,
        'brand_id'            => $brand,
        'discount'            => $discount,
        'discount_type'       => $discount_type,
        'discount_time_start' => $discount_time_start,
        'discount_time_end'   => $discount_time_end,
        'title'               => $faker->realText(rand(10, 35)),
        'code'                => $code,
        'description'         => $faker->text(300),
        'price'               => $price,
        'active'              => $faker->boolean(80),
        'price_type'          => $priceType,
        'quantity'            => $quantity,
        'max_price'           => $maxPrice,
        'min_price'           => $minPrice,
        'frame_type'          => $frameType,
        'rate'                => rand(0, 5),
        'comment_count'       => rand(0, 500),
        'order_count'         => rand(0, 1000),
        'show_homepage'       => $showHomepage,
        'deleted_at'          => $deletedAt,
    ];
});
