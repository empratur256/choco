<?php

namespace Sedehi\Http\Controllers\Product\Controllers\Admin;

use Carbon\Carbon;
use DB;
use Exception;
use Log;
use Sedehi\Http\Controllers\Brand\Models\Brand;
use Sedehi\Http\Controllers\Category\Models\Category;

use Sedehi\Http\Controllers\Product\Models\Details;
use Sedehi\Http\Controllers\Product\Models\Picture;
use Sedehi\Http\Controllers\Product\Models\Price;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Controllers\Product\Requests\Admin\ProductRequest;
use Illuminate\Http\Request;
use File;
use Image;
use Validator;

class ProductController extends Controller
{

    public $uploadPath = 'uploads/product/';

    public $imageSize = [
        [
            'width' => 111,
            'height' => 111,
        ],
        [
            'width' => 226,
            'height' => 226,
        ],
        [
            'width' => 435,
            'height' => 435,
        ],
        [
            'width' => 1024,
        ],
        [
            'width' => 175,
            'height' => 175,
        ],
    ];

    public function index()
    {
        $items = Product::with([
            'category' => function ($query) {
                $query->withTrashed();
            },
        ])->filter()->latest()->paginate(20);

        return view('Product.views.admin.product.index', compact('items'));
    }

    public function create()
    {
        $categories = Category::getNestedList('name_fa', 'id', ' - ');
        $brands = Brand::pluck('title_fa', 'id');
        $details = Details::pluck('title', 'id');


        return view('Product.views.admin.product.add',
            compact('categories', 'brands', 'details'));
    }

    public function store(ProductRequest $request)
    {

        if ($request->get('discount_type') == 'percent' && $request->get('discount') > 100) {
            return redirect()->back()->withInput()->with('error', 'تخفیف نمی تواند بیشتر از 100٪ باشد');
        }
        DB::beginTransaction();
        try {
            $item = new Product();
            $item->title = $request->get('title');
            $item->code = $request->get('code');
            $item->category_id = $request->get('category_id');
            if ($request->has('brand_id')) {
                $item->brand_id = $request->get('brand_id');
            }
            $item->active = $request->has('active');
            $item->description = $request->get('description');
            $item->show_homepage = $request->has('show_homepage');
            //$item->show_en         = $request->has('show_en');
            //$item->show_fa         = $request->has('show_fa');
            $item->show_fa = 1;
            $item->show_en = 0;

            $item->need_to_produce = $request->has('need_to_produce');
            $item->price_type = $request->get('price_type');

            if ($request->get('price_type') == 'fixed') {
                $item->price = $request->get('price');
                $item->max_price = $request->get('price');
                $item->min_price = $request->get('price');
                $item->quantity = $request->get('quantity');
            }
            if ($request->get('discount_status') == 1) {
                $item->discount_type = $request->get('discount_type');
                $item->discount = $request->get('discount');
                if ($request->get('discount_timer') == 1) {
                    $item->discount_time_start = Carbon::createFromTimestamp($request->get('discount_time_start_timestamp'))
                        ->hour($request->get('hour_time_start'))
                        ->minute($request->get('minute_time_start'))
                        ->second(0);
                    $item->discount_time_end = Carbon::createFromTimestamp($request->get('discount_time_end_timestamp'))
                        ->hour($request->get('hour_time_end'))
                        ->minute($request->get('minute_time_end'))
                        ->second(0);
                }
            }
//            $item->frame_type = $request->get('frame_type');
            $item->save();
            if ($request->get('price_type') == 'dynamic') {
                foreach (array_filter(array_filter_recursive($request->get('product'))) as $price) {
                    if (count($price) == 3) {
                        $p = new Price();
                        $p->price = $price['price'];
                        $p->quantity = $price['quantity'];
                        $p->product_id = $item->id;
                        $p->save();
                    }
                }
            }

            $this->uploadPath = $this->uploadPath . '/' . $item->id . '/';
            if ($request->hasFile('product_picture')) {
                if (!File::isDirectory(public_path($this->uploadPath))) {
                    File::makeDirectory(public_path($this->uploadPath), 0775, true);
                }
                $file = $request->file('product_picture');
                $realPath = $file->getRealPath();
                $destinationPath = public_path($this->uploadPath);
                $fileName = time() . $file->hashName();
                Image::make($realPath)->widen(1920, function ($constraint) {
                    $constraint->upsize();
                })->save($destinationPath . $fileName, 90);
                $this->createImages($realPath, $fileName);
                $item->picture = $fileName;
            }
            if ($request->get('price_type') == 'dynamic') {
                $dbPrice = Price::where('product_id', $item->id)->where('quantity', '>', 0)->get();
                $item->max_price = $dbPrice->max('price');
                $item->min_price = $dbPrice->min('price');
                $item->quantity = $dbPrice->sum('quantity');
                $item->price = $dbPrice->min('price');
            }
            if ($request->get('discount_type') == 'price' && $request->get('discount') > $item->price) {
                return redirect()->back()->withInput()->with('error', 'مبلغ تخفیف نمی تواند از مبلغ محصول بیشتر باشد');
            }
            $item->save();
            $details = [];
            foreach (array_filter(array_filter_recursive($request->get('detail'))) as $detail) {
                if (count($detail) == 2) {
                    if (is_numeric($detail['id'])) {
                        $details[$detail['id']] = ['data' => $detail['data']];
                    } else {
                        $existDetail = Details::where('slug', utf8_slug($detail['id']))->first();
                        if (is_null($existDetail)) {
                            $addDetail = new Details();
                            $addDetail->title = $detail['id'];
                            $addDetail->slug = utf8_slug($detail['id']);
                            $addDetail->save();
                            $detailId = $addDetail->id;
                        } else {
                            $detailId = $existDetail->id;
                        }
                        $details[$detailId] = ['data' => $detail['data']];
                    }
                }
            }
            $item->details()->sync($details);
            DB::commit();

            return redirect()
                ->action('Product\Controllers\Admin\PictureController@create', $item->id)
                ->with('success', 'محصول شما با موفقیت ایجاد لطفا تصاویر محصول را انتخاب کنید');
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar add kardan product ' . $e);

            return redirect()->back()->withInput()->with('error', 'مشکلی در ذخیره اطلاعات');
        }
    }

    public function edit($id)
    {
        $item = Product::findOrFail($id);
        $brands = Brand::pluck('title_fa', 'id');
        $categories = Category::getNestedList('name_fa', 'id', ' - ');
//        $sizes                 = Size::pluck('title', 'id');
//        $galleries             = Gallery::pluck('title', 'id');
        $details = Details::pluck('title', 'id');
//        $frames                = Frame::where('quantity', '>', 0)->get();
        $item->discount_status = ($item->discount > 0) ? 1 : 0;
        $item->discount_timer = (!is_null($item->discount_time_start) && !is_null($item->discount_time_end)) ? 1 : 0;
        if ($item->discount_timer == 1) {
            $discount_time_start = jdate('Y-m-d', $item->discount_time_start->timestamp, '', '', 'en');
            $discount_time_end = jdate('Y-m-d', $item->discount_time_end->timestamp, '', '', 'en');
            $item->discount_time_start_timestamp = $item->discount_time_start->timestamp;
            $item->discount_time_end_timestamp = $item->discount_time_end->timestamp;
            $item->minute_time_start = $item->discount_time_start->minute;
            $item->hour_time_start = $item->discount_time_start->hour;
            $item->minute_time_end = $item->discount_time_end->minute;
            $item->hour_time_end = $item->discount_time_end->hour;
        }

        return view('Product.views.admin.product.edit',
            compact('item', 'categories', 'brands', 'details',
                'discount_time_end', 'discount_time_start'));
    }

    public function update(ProductRequest $request, $id)
    {
        if ($request->get('discount_type') == 'percent' && $request->get('discount') > 100) {
            return redirect()->back()->withInput()->with('error', 'تخفیف نمی تواند بیشتر از 100٪ باشد');
        }
        DB::beginTransaction();
        try {
            $item = Product::findOrFail($id);
            $item->title = $request->get('title');
            $item->code = $request->get('code');
            $item->category_id = $request->get('category_id');
            // $item->show_en         = $request->has('show_en');
            //$item->show_fa         = $request->has('show_fa');
            $item->show_fa = 1;
            $item->show_en = 0;

            $item->need_to_produce = $request->has('need_to_produce');
            if ($request->has('brand_id')) {
                $item->brand_id = $request->get('brand_id');
            } else {
                $item->brand_id = null;
            }
            $item->active = $request->has('active');
            $item->description = $request->get('description');
            $item->show_homepage = $request->has('show_homepage');
            $item->price_type = $request->get('price_type');
//            $item->frame_type    = $request->get('frame_type');
            if ($request->get('price_type') == 'fixed') {
                $item->price = $request->get('price');
                $item->max_price = $request->get('price');
                $item->min_price = $request->get('price');
                $item->quantity = $request->get('quantity');
                Price::where('product_id', $item->id)->delete();
            }
            if ($request->get('discount_status') == 1) {
                $item->discount_type = $request->get('discount_type');
                $item->discount = $request->get('discount');
                if ($request->get('discount_timer') == 1) {
                    $item->discount_time_start = Carbon::createFromTimestamp($request->get('discount_time_start_timestamp'))
                        ->hour($request->get('hour_time_start'))
                        ->minute($request->get('minute_time_start'))
                        ->second(0);
                    $item->discount_time_end = Carbon::createFromTimestamp($request->get('discount_time_end_timestamp'))
                        ->hour($request->get('hour_time_end'))
                        ->minute($request->get('minute_time_end'))
                        ->second(0);
                }
            } else {
                $item->discount = null;
                $item->discount_time_start = null;
                $item->discount_time_end = null;
            }
            $item->save();
            if ($request->get('price_type') == 'dynamic') {
                Price::where('product_id', $item->id)->delete();
                foreach (array_filter(array_filter_recursive($request->get('product'))) as $price) {
                    if (count($price) == 3) {
                        $p = new Price();
                        $p->price = $price['price'];
                        $p->quantity = $price['quantity'];
                        $p->size_id = $price['size_id'];
                        $p->product_id = $item->id;
                        $p->save();
                    }
                }
            }
            $this->uploadPath = $this->uploadPath . '/' . $item->id . '/';
            if ($request->hasFile('product_picture')) {
                if (!File::isDirectory(public_path($this->uploadPath))) {
                    File::makeDirectory(public_path($this->uploadPath), 0775, true);
                }
                $file = $request->file('product_picture');
                $realPath = $file->getRealPath();
                $destinationPath = public_path($this->uploadPath);
                $fileName = time() . $file->hashName();
                Image::make($realPath)->widen(1920, function ($constraint) {
                    $constraint->upsize();
                })->save($destinationPath . $fileName, 90);
                $this->createImages($realPath, $fileName);
                removeImage($this->uploadPath, $item->picture);
                $item->picture = $fileName;
            }
            if ($request->get('price_type') == 'dynamic') {
                $dbPrice = Price::where('product_id', $item->id)->where('quantity', '>', 0)->get();
                $item->max_price = $dbPrice->max('price');
                $item->min_price = $dbPrice->min('price');
                $item->quantity = $dbPrice->sum('quantity');
                $item->price = $dbPrice->min('price');
            }
            if ($request->get('discount_type') == 'price' && $request->get('discount') > $item->price) {
                return redirect()->back()->withInput()->with('error', 'مبلغ تخفیف نمی تواند از مبلغ محصول بیشتر باشد');
            }
            $item->save();
//
            $details = [];
            foreach (array_filter(array_filter_recursive($request->get('detail'))) as $detail) {
                if (count($detail) == 2) {
                    if (is_numeric($detail['id'])) {
                        $details[$detail['id']] = ['data' => $detail['data']];
                    } else {
                        $existDetail = Details::where('slug', utf8_slug($detail['id']))->first();
                        if (is_null($existDetail)) {
                            $addDetail = new Details();
                            $addDetail->title = $detail['id'];
                            $addDetail->slug = utf8_slug($detail['id']);
                            $addDetail->save();
                            $detailId = $addDetail->id;
                        } else {
                            $detailId = $existDetail->id;
                        }
                        $details[$detailId] = ['data' => $detail['data']];
                    }
                }
            }
            $item->details()->sync($details);
            DB::commit();

            return redirect()
                ->action('Product\Controllers\Admin\ProductController@index')
                ->with('success', 'اطلاعات با موفقیت ذخیره شد');
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar add kardan product ' . $e);

            return redirect()->back()->withInput()->with('error', 'مشکلی در ذخیره اطلاعات');
        }
    }

    public function destroy(ProductRequest $request)
    {
        Product::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

    public function show($id)
    {
        $item = Product::withTrashed()->findOrFail($id);

        return redirect()->action('Product\Controllers\Site\ProductController@show', [
            $item->id,
            utf8_slug($item->title),
            'admin_check' => 1,
        ]);
    }

}
