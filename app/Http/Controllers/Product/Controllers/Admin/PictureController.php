<?php

namespace Sedehi\Http\Controllers\Product\Controllers\Admin;

use Sedehi\Http\Controllers\Product\Models\Picture;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Product\Models\Product;
use Illuminate\Http\Request;
use File;
use Image;
use Validator;

class PictureController extends Controller
{

    protected $uploadPath = 'uploads/product/';

    public $imageSize = [
        [
            'width'  => 111,
            'height' => 111,
        ],
        [
            'width'  => 226,
            'height' => 226,
        ],
        [
            'width'  => 435,
            'height' => 435,
        ],
        [
            'width' => 1024,
        ],
        [
            'width'  => 175,
            'height' => 175,
        ],
    ];

    public function create($id)
    {
        $product = Product::withTrashed()->findOrFail($id);

        return view('Product.views.admin.picture.add', compact('product'));
    }

    public function store(Request $request, $id)
    {
        $product          = Product::withTrashed()->findOrFail($id);
        $this->uploadPath = $this->uploadPath.'/'.$product->id.'/';
        if(!File::isDirectory(public_path($this->uploadPath))){
            File::makeDirectory(public_path($this->uploadPath), 0775, true);
        }
        $failedFiles = [];
        $fileRemove = [];
        if($request->has('file_remove')){
            foreach($request->get('file_remove') as $fileId => $fileStatus){
                if($fileStatus == 1){
                    $fileRemove[] = $fileId;
                }
            }
        }
        if(count($fileRemove)){
            $oldPictures = Picture::whereIn('id', $fileRemove)->where('product_id', $product->id)->get();
            foreach($oldPictures as $pic){
                removeImage($this->uploadPath, $pic->picture);
            }
            Picture::whereIn('id', $fileRemove)->where('product_id', $product->id)->delete();
        }
        if($request->hasFile('pictures')){
            foreach($request->file('pictures') as $file){
                $validator = Validator::make(['picture' => $file], [
                    'picture' => 'image',
                ]);
                if($validator->passes()){
                    $realPath        = $file->getRealPath();
                    $destinationPath = public_path($this->uploadPath);
                    $fileName        = time().$file->hashName();
                    Image::make($realPath)->widen(1920, function($constraint){
                        $constraint->upsize();
                    })->save($destinationPath.$fileName, 90);
                    $this->createImages($realPath, $fileName);
                    $picture             = new Picture();
                    $picture->picture    = $fileName;
                    $picture->product_id = $product->id;
                    $picture->save();
                }else{
                    $failedFiles[] = 'فایل تصویری با نام '.$file->getClientOriginalName().' از نوع تصویر نمی باشد و آپلود نشده است';
                }
            }
        }
        if(count($failedFiles)){
            return redirect()->back()->with('error', $failedFiles);
        }

        return redirect()->action('Product\Controllers\Admin\ProductController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

}
