<?php

namespace Sedehi\Http\Controllers\Product\Controllers\Site;

use Cart;
use Gloudemans\Shoppingcart\Exceptions\InvalidRowIDException;
use Sedehi\Http\Controllers\Brand\Models\Brand;
use Sedehi\Http\Controllers\Category\Models\Category;
use Sedehi\Http\Controllers\Comments\Models\Comments;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Controllers\Product\Requests\Site\ProductRequest;

class ProductController extends Controller
{
    public function index($id = null, $title = null)
    {
        $sorts = [
            1 => trans('site.index.news'),
            2 => trans('site.homepage.best_seller'),
            3 => trans('site.index.cheap') ,
            4 => trans('site.index.expensive') ,
        ];
        $items = Product::query();
        $categories = [];
        if (request()->has('category')) {
            $id = request()->get('category');
        }
        if (!is_null($id)) {

            $category   = Category::findOrFail($id);
            $categories = $category->children()->pluck('id')->push($id);
            $items->whereIn('category_id', $categories);

        }
        $priceMin = clone $items;
        $items->filter([
            'search' => [
                'scope' => 'SearchTitle',
            ],
            'price' => [
                'between' => [
                    'price_from',
                    'price_to',
                ],
            ],
            'brand' => [
                'scope' => 'BrandSearch',
            ],
            'category' => [
                'scope' => 'CategorSearch',
            ],
            'sort' => [
                'scope' => 'sort',
            ],
            'status' => [
                'scope' => 'available',
            ],
        ]);
        $priceMin->filter([
            'search' => [
                'scope' => 'SearchTitle',
            ],
        ]);

        $priceMax = clone $priceMin;
        $priceMin = $priceMin->active()->min('min_price');
        $priceMax = $priceMax->active()->max('max_price');

        if($priceMax == $priceMin) {
            $priceMin = 0;
        }

        $items = $items->active()->sort()->paginate(15);

        $brands = Brand::query();

        $brands->whereHas('products', function ($query) use ($id, $categories) {
            if (!is_null($id) && count($categories)) {
                $query->whereIn('category_id', $categories);
            }
            $query->active();
            $query->filter([
                'search' => [
                    'scope' => 'SearchTitle',
                ],
                'price' => [
                    'between' => [
                        'price_from',
                        'price_to',
                    ],
                ],
            ]);
        });

        if (request()->has('category')) {
            $categories = Category::findOrFail(request()->get('category'))->children();
            $selectedCategory = Category::findOrFail(request()->get('category'));
        } else {
            $categories = Category::roots();
            $selectedCategory = null;
        }

        $categories = $categories->filter()->get();

        $brands = $brands->get();

        return view('Product.views.site.index', compact(
                'items',
                'brands',
                'sorts',
                'priceMax',
                'priceMin',
                'categories',
                'selectedCategory'
            )
        );
    }

    public function show($id, $title)
    {
        $isAdmin = false;
        if (request()->has('admin_check')) {
            if (permission('Product.ProductController.show')) {
                $isAdmin = true;
            }
        }
        $product = Product::query();
        if ($isAdmin) {
            $product->withTrashed();
        } else {
            // scope inke barae in lanuge ro biare
        }
        $product = $product->active()->with([
            'prices',
            'category',
            'brand',
            'details',
        ])->findOrFail($id);

        $cart = collect();
        if (request()->has('cart')) {
            try {
                $cart = Cart::get(request()->get('cart'));
            } catch (InvalidRowIDException $e) {
            }
        }
        $comments = Comments::where('product_id', $product->id)->language()->Confirmed()->with([
            'user' => function (
                $query
            ) {
                $query->withTrashed();
            },
        ])->simplePaginate(7);

        $productWithSimilarBrands = Product::where('brand_id', $product->brand_id)
            ->where('id', '<>', $product->id)
            ->where('quantity', '<>', 0)
            ->take(5)
            ->get();

        $productWithSimilarCategory = Product::where('category_id', $product->category_id)
            ->where('id', '<>', $product->id)
            ->where('quantity', '<>', 0)
            ->take(4)
            ->get();

        return view('Product.views.site.show', compact(
                'product',
                'cart',
                'comments',
                'productWithSimilarBrands',
                'productWithSimilarCategory'
            )
        );
    }

    public function favorite(ProductRequest $request)
    {
        $product = Product::active()->findOrfail($request->get('id'));
        $toggle = $product->favorite()->toggle(auth()->user()->id);
        if (count($toggle['attached'])) {
            return redirect()->back()->with('success', trans('site.add_fav'));
        } elseif (count($toggle['detached'])) {
            return redirect()->back()->with('success', trans('site.delete_fav'));
        }
    }

    public function price()
    {
        $product = Product::active()->findOrFail(request()->get('id'));
        $price = $product->prices()->where('size_id', request()->get('size'))->firstOrFail();
        $data = [];
        if ($price->quantity > 0) {
            $data['priceWithDiscount'] = number_format(finalPrice($product, $price));
            if ($price->price != finalPrice($product, $price)) {
                $data['price'] = number_format($price->price);
            } else {
                $data['price'] = '';
            }
            $data['status'] = trans('site.products.available');
            $data['active'] = 1;
        } else {
            $data['price'] = '';
            $data['priceWithDiscount'] = '';
            $data['status'] = trans('site.products.unavailable');
            $data['active'] = 0;
        }

        return response()->json($data);
    }
}
