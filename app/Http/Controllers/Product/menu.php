<?php
return [
    'product' => [
        'order'   => 70,
        'title'   => 'محصولات',
        'icon'    => 'fa fa-product-hunt',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'Product\Controllers\Admin\ProductController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'Product\Controllers\Admin\ProductController@create',
                'parameters' => [],
            ],
        ],
    ],
];
