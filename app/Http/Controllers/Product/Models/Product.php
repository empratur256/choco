<?php

namespace Sedehi\Http\Controllers\Product\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Brand\Models\Brand;
use Sedehi\Http\Controllers\Category\Models\Category;
use Sedehi\Http\Controllers\Comments\Models\Comments;
use Sedehi\Http\Controllers\User\Models\User;

class Product extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'product';
    public    $timestamps = true;
    public    $dates      = [
        'discount_time_start',
        'discount_time_end',
    ];
    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'search'     => [
            'scope' => 'SearchTitle',
        ],
        'price'      => [
            'between' => [
                'price_from',
                'price_to',
            ],
        ],
        'brand'      => [
            'scope' => 'BrandSearch',
        ],
        'category_id',
        'code',
        'active',
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function scopeDiscounts($query)
    {
        return $query->whereNotNull('discount')->where('discount', '>', 0)->whereIn('discount_type', [
                'price',
                'percent',
            ])->where(function($query){
                $query->whereNull('discount_time_start')->orWhere(function($query){
                    $query->where('discount_time_start', '<', Carbon::now());
                    $query->where('discount_time_end', '>', Carbon::now());
                });
            });
    }

    public function scopeSearchTitle($query)
    {
        return $query->where('title', 'LIKE', '%'.request()->get('search').'%');
    }


    public function scopeSort($query)
    {
        $column = 'created_at';
        $order  = 'DESC';
        if(request()->has('sort')){
            switch(request()->get('sort')){
                case 2 :
                    $column = 'order_count';
                break;
                case 3 :
                    $column = 'price';
                    $order  = 'ASC';
                break;
                case 4 :
                    $column = 'price';
                    $order  = 'DESC';
                break;
            }
        }

        return $query->orderBy($column, $order);
    }

    public function scopeCategorSearch($query)
    {
        return $query->where('category_id',request()->get('category'));
    }

    public function scopeBrandSearch($query)
    {
        return $query->whereHas('brand', function($query){
            $query->whereIn('id', request()->get('brand'));
        });
    }

    public function scopeAvailable($query)
    {
        if( request()->has( 'status' ) && request()->get( 'status' ) == 'available' ){
            return $query->where('quantity','>',0);
        }
    }

    public function scopeActive($query)
    {
        $locale = app()->getLocale();
        if($locale == 'fa'){
            $query->where('show_fa', 1);
        }
        if($locale == 'en'){
            $query->where('show_en', 1);
        }

        return $query->where('active', 1);
    }
    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }

    public function details()
    {
        return $this->belongsToMany(Details::class, 'product_detail', 'product_id', 'detail_id')->withPivot('data');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function favorite()
    {
        return $this->belongsToMany(User::class, 'product_favorite');
    }

}
