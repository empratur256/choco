<?php

namespace Sedehi\Http\Controllers\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class Picture extends Model
{

    use  Filterable;

    protected $table      = 'product_picture';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
