<?php

namespace Sedehi\Http\Controllers\Product\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Size\Models\Size;

class Price extends Model
{

    use  Filterable;

    protected $table      = 'product_prices';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

}
