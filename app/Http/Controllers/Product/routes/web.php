<?php
Route::group([
                 'prefix' => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace' => 'Product\Controllers\Admin',
             ], function(){
    Route::resource('product', 'ProductController');
    Route::resource('product.picture', 'PictureController', [
        'only' => [
            'create',
            'store',
        ],
    ]);
});
Route::get('products/{id?}/{title?}', 'Product\Controllers\Site\ProductController@index');
Route::get('products/show/{id}/{title}', 'Product\Controllers\Site\ProductController@show');
Route::post('products/favorite', 'Product\Controllers\Site\ProductController@favorite')->middleware('dashboard');
Route::post('products/price', 'Product\Controllers\Site\ProductController@price');
