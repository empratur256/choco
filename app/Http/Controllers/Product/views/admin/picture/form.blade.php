<div class="uk-grid" data-uk-grid-margin>
    @php $i=9; @endphp
    @php $i = $i-count($product->pictures) @endphp
    @foreach($product->pictures as $picture)
        <div class="uk-width-medium-1-3">
            <div class="md-card">
                <div class="md-card-content">
                    <h3 class="heading_a uk-margin-small-bottom">تصویر</h3>
                    {!! Form::file('pictures[]',['class' => 'dropify','remove' => '.file_remove_'.$picture->id,'data-default-file' => (isset($picture)? asset('uploads/product/'.$product->id.'/'.$picture->picture) : '')]) !!}
                    {!! Form::hidden('file_remove['.$picture->id.']',0,['class' => 'file_remove_'.$picture->id]) !!}

                </div>
            </div>
            <div class="uk-clearfix"></div>
            <br>
        </div>
    @endforeach
    @for($i;$i>=1;$i--)
        <div class="uk-width-medium-1-3">
            <div class="md-card">
                <div class="md-card-content">
                    <h3 class="heading_a uk-margin-small-bottom">تصویر</h3>
                    {!! Form::file('pictures[]',['class' => 'dropify']) !!}

                </div>
            </div>
            <div class="uk-clearfix"></div>
            <br>
        </div>
    @endfor
</div>
