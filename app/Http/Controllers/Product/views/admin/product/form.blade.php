<div class="md-card-content">
    <div class="uk-tab-center">
        <ul class="uk-tab" data-uk-tab="{connect:'#product'}">
            <li class="uk-active" aria-expanded="true"><a href="#">اطلاعات</a></li>
            <li aria-expanded="false"><a href="#">قیمت و تخفیف</a></li>
            <li aria-expanded="false"><a href="#">تصاویر</a></li>
            <li aria-expanded="false"><a href="#">جزئیات</a></li>
        </ul>
    </div>
    <ul id="product" class="uk-switcher uk-margin">
        <li aria-hidden="false" class="uk-active">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    {!! Form::label('title','عنوان') !!}
                    {!! Form::text('title',null,['class'=>'md-input']) !!}
                </div>
                <div class="uk-width-medium-1-2">
                    {!! Form::label('code','کد محصول') !!}
                        {!! Form::text('code',null,['class'=>'md-input']) !!}
                </div>
                <div class="uk-width-medium-1-2">
                    {!! Form::select('category_id',$categories,null,['class'=>'md-input','placeholder' => 'انتخاب دسته بندی']) !!}
                </div>
                <div class="uk-width-medium-1-2">
                    {!! Form::select('brand_id',$brands,null,['class'=>'md-input','placeholder' => 'انتخاب تولید کننده']) !!}
                </div>
                <div class="uk-width-medium-1-2">
                    <span class="icheck-inline" style="line-height: 70px">
                        {!! Form::checkbox('active',1,(isset($item)? $item->active : true),['data-switchery','id' => 'active']) !!}
                        {!! Form::label('active','فعال',['class' => 'inline-label']) !!}
                    </span>
                </div>
                <div class="uk-width-medium-1-2">
                    <span class="icheck-inline">
                        {!! Form::checkbox('need_to_produce',1,null,['data-md-icheck','id' => 'need_to_produce']) !!}
                        {!! Form::label('need_to_produce','نیاز به پروسه تولید',['class' => 'inline-label']) !!}
                    </span>

                </div>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    {!! Form::textarea('description',null,['class'=>'md-input ckeditor']) !!}
                </div>
            </div>
        </li>
        <li>
            <div class="uk-grid" data-uk-grid-margin>

                <div class="uk-width-medium-1-2">
                    <h3 class="heading_a uk-float-left">نوع قیمت گذاری</h3>
                    <span class="icheck-inline uk-float-right">
                        {!! Form::checkbox('show_homepage',1,null,['data-md-icheck','id' => 'show_homepage']) !!}
                        {!! Form::label('show_homepage','نمایش تخفیف در صفحه اصلی',['class' => 'inline-label']) !!}
                    </span>
                    <div class="uk-clearfix"></div>
                    <br>
                    <span class="icheck-inline">
                        {!! Form::radio('price_type','fixed',(isset($item))? $item->price_type : true,['data-md-icheck','id'=>'price_type_fixed']) !!}
                        {!! Form::label('price_type_fixed','قیمت ثابت',['class' => 'inline-label']) !!}
                    </span>
                    <span class="icheck-inline">
                        {!! Form::radio('price_type','dynamic',null,['data-md-icheck','id'=>'price_type_dynamic']) !!}
                        {!! Form::label('price_type_dynamic','قیمت متغیر',['class' => 'inline-label']) !!}
                    </span>

                    <div class="fixed_price">
                        <div class="uk-width-medium-1-1">
                            {!! Form::label('price','قیمت') !!}
                            {!! Form::number('price',null,['class'=>'md-input']) !!}
                        </div>
                        <div class="uk-width-medium-1-1">
                            {!! Form::label('quantity','تعداد') !!}
                            {!! Form::number('quantity',null,['class'=>'md-input']) !!}
                        </div>
                    </div>
                    <div class="dynamic_price">
                        @if(count(old('product')) || (isset($item) && count($item->prices)))
                            @php $products =[] @endphp

                            @if((isset($item) && count($item->prices)))
                                @php $products = $item->prices @endphp
                            @endif
                            @if(count(old('product')) > 0)
                                @php $products = old('product') @endphp
                            @endif

                            @foreach($products as $product)
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-4">
                                        {!! Form::label('product','تعداد') !!}
                                        {!! Form::number('product['.$loop->index.'][quantity]',$product['quantity'],['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-4">
                                        {!! Form::label('product','قیمت') !!}
                                        {!! Form::number('product['.$loop->index.'][price]',$product['price'],['class'=>'md-input','min' => '100']) !!}
                                    </div>

                                    @if($loop->index > 0)
                                        <div class="uk-width-medium-1-4 btns">
                                            <button type="button" class="btn btn-danger btn-sm remove-row">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    @else
                                        <div class="uk-width-medium-1-4 btns">
                                            <button type="button" class="btn btn-primary btn-sm clone">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        @else
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-4">
                                    {!! Form::label('product','تعداد') !!}
                                    {!! Form::number('product[0][quantity]',null,['class'=>'md-input']) !!}
                                </div>
                                <div class="uk-width-medium-1-4">
                                    {!! Form::label('product','قیمت') !!}
                                    {!! Form::number('product[0][price]',null,['class'=>'md-input','min' => '100']) !!}
                                </div>
                                <div class="uk-width-medium-1-4 btns">
                                    <button type="button" class="btn btn-primary btn-sm clone">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                        @endif


                    </div>
                </div>
                <div class="uk-width-medium-1-2">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2 ">
                            {!! Form::select('discount_status',[0=>'تخفیف ندارد',1=>'تخفیف دارد'],null,['class'=>'md-input uk-width-medium-1-2']) !!}
                        </div>
                        <div class="uk-width-medium-1-2 discount_container">
                            <h3 class="heading_a">نوع تخفیف</h3>
                            <br>
                            <span class="icheck-inline">
                                {!! Form::radio('discount_type','price',(isset($item))? $item->discount_type : true,['data-md-icheck','id'=>'discount_type_price']) !!}
                                {!! Form::label('discount_type_price','مبلغ',['class' => 'inline-label']) !!}
                            </span>
                            <span class="icheck-inline">
                                {!! Form::radio('discount_type','percent',null,['data-md-icheck','id'=>'discount_type_percent']) !!}
                                {!! Form::label('discount_type_percent','درصد',['class' => 'inline-label']) !!}
                            </span>
                        </div>

                    </div>

                    <div class="uk-width-medium-1-1 discount_container">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                {!! Form::label('discount','مقدار تخفیف') !!}
                                {!! Form::number('discount',null,['class'=>'md-input','min' => '1']) !!}
                            </div>
                            <div class="uk-width-medium-1-2">
                                {!! Form::select('discount_timer',[0 => 'بدون زمان بندی',1 => 'همراه با زمان بندی'],null,['class'=>'md-input']) !!}
                            </div>
                        </div>
                        <div class="uk-grid discount_timer" data-uk-grid-margin>
                            <div class="uk-width-medium-2-4">
                                {!! Form::label('discount_time_start','تاریخ شروع') !!}
                                {!! Form::text('discount_time_start',isset($discount_time_start)? $discount_time_start : null,['class'=>'md-input date','timestamp' =>'.discount_time_start_timestamp']) !!}
                                {!! Form::hidden('discount_time_start_timestamp',null,['class' => 'discount_time_start_timestamp']) !!}
                            </div>
                            <div class="uk-width-medium-1-4">
                                {!! Form::label('minute_time_start','دقیقه شروع') !!}
                                {!! Form::selectRange('minute_time_start',0,59,null,['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-4">
                                {!! Form::label('hour_time_start','ساعت شروع') !!}
                                {!! Form::selectRange('hour_time_start',0,23,null,['class'=>'md-input']) !!}
                            </div>
                        </div>
                        <div class="uk-grid discount_timer" data-uk-grid-margin>
                            <div class="uk-width-medium-2-4">
                                {!! Form::label('discount_time_end','تاریخ پایان') !!}
                                {!! Form::text('discount_time_end',isset($discount_time_end)? $discount_time_end : null,['class'=>'md-input date','timestamp' => '.discount_time_end_timestamp']) !!}
                                {!! Form::hidden('discount_time_end_timestamp',null,['class' => 'discount_time_end_timestamp']) !!}
                            </div>
                            <div class="uk-width-medium-1-4">
                                {!! Form::label('minute_time_end','دقیقه پایان') !!}
                                {!! Form::selectRange('minute_time_end',0,59,null,['class'=>'md-input']) !!}
                            </div>
                            <div class="uk-width-medium-1-4">
                                {!! Form::label('hour_time_end','ساعت پایان') !!}
                                {!! Form::selectRange('hour_time_end',0,23,null,['class'=>'md-input']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-2-4 uk-push-1-4">
                    <div class="md-card">
                        <div class="md-card-content">
                            <h3 class="heading_a uk-margin-small-bottom">تصویر اصلی</h3>
                            {!! Form::file('product_picture',['class' => 'dropify','remove' => 'file_remove','data-default-file' => (isset($item)? asset('uploads/product/'.$item->id.'/'.$item->picture) : '')]) !!}
                        </div>
                    </div>
                </div>
            </div>
            @if(isset($item))
                <a href="{!! action('Product\Controllers\Admin\PictureController@create',$item->id) !!}" class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect add-picture">ویرایش دیگر تصاویر محصول</a>
            @endif
            <div class="uk-clearfix"></div>
        </li>

        <li>
            <button type="button" class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect add-detail">اضافه کردن</button>
            <div class="uk-clearfix"></div>
            <br>
            <div class="detail-container">
                @if(count(old('detail')) || (isset($item) && count($item->details)))
                    @php $detailList =[] @endphp
                    @if((isset($item) && count($item->details)))
                        @php $detailList = $item->details @endphp
                    @endif
                    @if(count(old('detail')) > 0)
                        @php $detailList = old('detail') @endphp
                    @endif

                    @foreach($detailList as $detail)

                        @if(isset($detail['id'])  && !is_numeric($detail['id']))
                            @php $details = [$detail['id'] => $detail['id']] @endphp
                        @endif
                        @if(count(old('detail')) == 0 && (isset($item) && count($item->details)))
                            @php $detail['data'] = $detail->pivot->data @endphp
                        @endif
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-5">
                                {!! Form::label('detail','عنوان') !!}
                                <br>
                                {!! Form::select('detail['.$loop->index.'][id]',$details,(isset($detail['id'])) ? $detail['id'] : null,['class' => 'md-input details']) !!}
                            </div>
                            <div class="uk-width-medium-2-5">
                                {!! Form::label('data','اطلاعات') !!}
                                {!! Form::text('detail['.$loop->index.'][data]',$detail['data'],['class'=>'md-input','min' => '1']) !!}
                            </div>

                            <div class="uk-width-medium-1-5">
                                <button type="button" class="btn btn-primary btn-sm removeBtn" style="display: block">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                            <div class="uk-clearfix"></div>
                            <br>
                        </div>
                    @endforeach
                @else
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-2-5">
                            {!! Form::label('detail','عنوان') !!}
                            <br>
                            {!! Form::select('detail[0][id]',$details,null,['class' => 'md-input details']) !!}
                        </div>
                        <div class="uk-width-medium-2-5">
                            {!! Form::label('data','اطلاعات') !!}
                            {!! Form::text('detail[0][data]',null,['class'=>'md-input','min' => '1']) !!}
                        </div>
                        <div class="uk-width-medium-1-5">
                            <button type="button" class="btn btn-primary btn-sm removeBtn">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <div class="uk-clearfix"></div>
                        <br>
                    </div>
                @endif
            </div>
        </li>
    </ul>
</div>
@push('js')
{!! Html::script('assets/admin/js/select2.full.min.js') !!}
{!! Html::script('assets/admin/js//i18n/fa.js') !!}
<script>
    $(document).ready(function () {


        $('select[name="discount_status"]').on('change', function () {
            if ($(this).val() == 0) {
                $('input[name="discount"]').val('');
                $('select[name="discount_timer"]').val(0);
                $('input[name="discount_time_start"]').val('');
                $('input[name="minute_time_start"]').val('');
                $('input[name="hour_time_start"]').val('');
                $('input[name="discount_time_end"]').val('');
                $('input[name="minute_time_end"]').val('');
                $('input[name="hour_time_end"]').val('');
            }
        });

        $('select[name="discount_timer"]').on('change', function () {
            if ($(this).val() == 0) {
                $('input[name="discount_time_start"]').val('');
                $('input[name="minute_time_start"]').val('');
                $('input[name="hour_time_start"]').val('');
                $('input[name="discount_time_end"]').val('');
                $('input[name="minute_time_end"]').val('');
                $('input[name="hour_time_end"]').val('');
            }
        });

        var select2Options = {
            tags: true,
            language: "fa",
            dir: "rtl"
        };
        $('.details').select2(select2Options);
        var checked_radio = $('input[name="price_type"]:checked');

        if (checked_radio.val() == 'dynamic') {
            $('.fixed_price').css('display', 'none');
            $('.dynamic_price').css('display', 'block');
        } else if (checked_radio.val() == 'fixed') {
            $('.fixed_price').css('display', 'block');
            $('.dynamic_price').css('display', 'none');
        }

        var checked_frame = $('input[name="frame_type"]:checked');

        if (checked_frame.val() != 'none') {
            $('.quantity-alert').css('display', 'block');
        } else {
            $('.quantity-alert').css('display', 'none');
        }

        if (checked_frame.val() == 'custom') {
            $('.selective-frame').css('display', 'block');
        } else {
            $('.selective-frame').css('display', 'none');
        }


        $('input[name="frame_type"]').on('ifChecked', function (event) {
            if (event.currentTarget.value != 'none') {
                $('.quantity-alert').css('display', 'block');
            } else {
                $('.quantity-alert').css('display', 'none');
            }

            if (event.currentTarget.value == 'custom') {
                $('.selective-frame').css('display', 'block');
            } else {
                $('.selective-frame').css('display', 'none');
            }
        });


        $('input[name="price_type"]').on('ifChecked', function (event) {
            if (event.currentTarget.value == 'dynamic') {
                $('.fixed_price').css('display', 'none');
                $('.dynamic_price').css('display', 'block');
            } else if (event.currentTarget.value == 'fixed') {
                $('.fixed_price').css('display', 'block');
                $('.dynamic_price').css('display', 'none');
            }
        });


        $('body').on('click', '.remove-row', function () {
            $(this).parent().parent().remove();
        });
        $('body').on('click', '.clone', function () {

            var parent = $(this).parent().parent().parent();
            var trashButton = '<button type="button" class="btn btn-danger btn-sm remove-row">' +
                '<i class="fa fa-trash"></i>' +
                '</button>';

            $(this).parent().parent().clone().appendTo(parent)
                .each(function (index) {
                    $(this).find("input").each(function (index) {
                        this.value = '';
                    });
                });


            parent.children().each(function (index) {
                $(this).find("input").each(function () {
                    this.name = this.name.replace(/\[[0-9]+\]/g, '[' + index + ']');
                });
                $(this).find("select").each(function () {
                    this.name = this.name.replace(/\[[0-9]+\]/g, '[' + index + ']');
                });
                $('.btns').not(':first').html(trashButton);
            });
        });


        $('select[name="discount_status"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.discount_container').css('display', 'block');
            } else {
                $('.discount_container').css('display', 'none');
            }
        });

        if ($('select[name="discount_status"]').val() == 1) {
            $('.discount_container').css('display', 'block');
        } else {
            $('.discount_container').css('display', 'none');
        }

        $('select[name="discount_timer"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.discount_timer').css('display', 'block');
            } else {
                $('.discount_timer').css('display', 'none');
            }
        });
        if ($('select[name="discount_timer"]').val() == 1) {
            $('.discount_timer').css('display', 'block');
        } else {
            $('.discount_timer').css('display', 'none');
        }
        var counter = $('.picture-box').length;
        $('.add-picture').on('click', function () {
            $('.pictures_container > .uk-hidden').first().removeClass('uk-hidden');
            counter--;
            if (counter == 0) {
                $(this).addClass('uk-hidden');
            }

        });


        $('.add-detail').on('click', function () {
            $('.details').select2('destroy');
            $('.detail-container').children().first().clone().appendTo($('.detail-container')).each(function (index) {
                $(this).find("input").each(function (index) {
                    this.value = '';
                });
            });

            $('.details').select2(select2Options);

            $('.detail-container').children().each(function (index) {
                $(this).find("input").each(function () {
                    this.name = this.name.replace(/\[[0-9]+\]/g, '[' + index + ']');
                });
                $(this).find("select").each(function () {
                    this.name = this.name.replace(/\[[0-9]+\]/g, '[' + index + ']');
                });
            });
            $('.removeBtn').not(':first').css('display', 'block');
        });
        $('body').on('click', '.removeBtn', function () {
            $(this).parent().parent().remove();
        });

    });

</script>
@endpush
@push('css')
{!! Html::style('assets/admin/css/select2.min.css') !!}

<style>
    .fixed_price, .dynamic_price, .discount_container, .discount_timer, .quantity-alert, .selective-frame, .removeBtn {
        display: none;
    }

    .dropdown-menu {
        position: inherit;
    }

    .select2 {
        width: 100% !important;
    }

    .select2-container--default .select2-selection--single {
        border: none;
        border-bottom: 1px solid rgba(0, 0, 0, .12);
        border-radius: 0px;
    }
</style>
@endpush