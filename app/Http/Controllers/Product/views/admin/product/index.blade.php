@extends('views.layouts.admin.master')
@section('content')
    @include('Product.views.admin.product.search')
    {!! Form::open(['action'=>['Product\Controllers\Admin\ProductController@destroy',1],'method' =>'DELETE']) !!}
    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('Product.ProductController.destroy'))
                                <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید ؟');" class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">حذف</button>
                            @endif
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            <a class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" href="{!! action('Product\Controllers\Admin\ProductController@create') !!}" style="margin-left: 15px !important;">ایجاد</a>
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Product\Controllers\Admin\ProductController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>
                            <th class="uk-width-1-10  uk-text-center small_col">
                                {!! Form::checkbox('',1,null,['class' => 'check_all','data-md-icheck']) !!}
                            </th>
                            <th class="uk-width-2-10 uk-text-center">کد</th>
                            <th class="uk-width-2-10 uk-text-center">عنوان</th>
                            <th class="uk-width-2-10 uk-text-center">دسته بندی</th>
                            <th class="uk-width-2-10 uk-text-center">قیمت (تومان)</th>
                            <th class="uk-width-2-10 uk-text-center">تعداد</th>
                            <th class="uk-width-2-10 uk-text-center">وضعیت</th>
                            <th class="uk-width-2-10 uk-text-center">.....</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            <tr class="uk-text-center">
                                <td class="uk-text-center uk-table-middle small_col">
                                    {!! Form::checkbox('deleteId[]',$item->id,null,['class' => 'check_row','data-md-icheck']) !!}
                                </td>
                                <td>{{ $item->code }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->category->name }}</td>
                                <td>@if($item->price != $item->max_price) {{number_format($item->min_price)}} تا {{number_format($item->max_price) }}  @else {{ number_format($item->price) }} @endif</td>
                                <td>{{ number_format($item->quantity) }}</td>
                                <td>@if($item->active) <span class="uk-badge uk-badge-success">فعال</span> @else
                                        <span class="uk-badge uk-badge-danger">غیر فعال</span> @endif</td>

                                <td class="uk-text-center">
                                    @if(permission('Product.ProductController.edit'))
                                        <a href="{!! action('Product\Controllers\Admin\ProductController@edit',$item->id) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                                    @endif
                                    @if(permission('Product.ProductController.show'))
                                        <a target="_blank" href="{!! action('Product\Controllers\Admin\ProductController@show',$item->id) !!}"><i class="md-icon material-icons">visibility</i></a>
                                    @endif
                                    @if(permission('Comments.CommentsController.index'))
                                        <a target="_blank" href="{!! action('Comments\Controllers\Admin\CommentsController@index',['product_id' => $item->id]) !!}"><i class="md-icon material-icons">comment</i></a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection