@extends('views.layouts.site.master')
@section('title')
    @if(!is_null($selectedCategory))
        {{trans('site.products.list',['type'=>$selectedCategory->name_fa])}}
    @else
        @lang('site.products.list',['type' => trans('site.products.products')])
    @endif
@endsection
@section('content')
    <div class="red_box">
        <div class="container">
            <h3>
                @if(!is_null($selectedCategory))
                    {{trans('site.products.list',['type'=>$selectedCategory->name_fa])}}
                @else
                    @lang('site.products.list',['type' => trans('site.products.products')])
                @endif
            </h3>
            <div class="divider-title"></div>
            <p> @lang('site.products.textTitle') </p>
        </div>
    </div>
    <div id="choco-list">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div style="padding-top: 1em;padding-bottom: 0.5em" class="bread-crumbs">
                        <span> @lang('site.home') </span>
                        @if(App::isLocale('en'))
                            <span><i class="flaticon-right-arrow"></i> </span>
                        @else
                            <span> <i class="flaticon-left-arrow-1"></i></span>
                        @endif
                        <span>
                            @if(!is_null($selectedCategory))
                                {{trans('site.products.list',['type'=>$selectedCategory->name_fa])}}
                            @else
                                @lang('site.products.list',['type'=>trans('site.products.products')])
                            @endif
                        </span>
                    </div>
                </div>
                <div class="container text-center">  @include('views.errors.errors')</div>
                {!! Form::open(['method' => 'GET','class' => 'search']) !!}
                <div class="list col-lg-3 col-md-3 col-sm-3 col-xs-3 col-xxs-12 pull-right ">
                    <div class="hidden-xxs">
                        <h5 class="border" style="margin-bottom: 1em"><span> <i
                                        class="fa fa-circle"></i> </span>@lang('site.products.brands')
                        </h5>
                        @foreach($brands as $brand)
                            <div class="padding-bottom">
                                <label for="{{ 'brand_'.$brand->id }}">
                                    {!! Form::checkbox('brand['.$brand->id.']',$brand->id,request()->has('brand.'.$brand->id),['id' => 'brand_'.$brand->id,'class' => 'brands_2']) !!}
                                    <span style="cursor:pointer;">{{ $brand->title }}</span>
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="hidden-xxs">
                        <h5 class="border" style="margin-bottom: 1em"><span>
                                <i class="fa fa-circle"></i> </span> @lang('site.products.categories')
                        </h5>
                        @foreach($categories as $category)
                            <div class="padding-bottom category"><a data-value="{{$category->id}}">
                                    <i class="fa fa-circle-o"></i>
                                    {{ $category->name_fa }} </a>
                            </div>
                        @endforeach
                        <div class="padding-bottom category">
                            <a href="{{action('Product\Controllers\Site\ProductController@index')}}"><i
                                        class="fa fa-circle-o"></i>
                                @lang('site.back')
                            </a>
                        </div>
                    </div>
                    <div class="list-search" style="padding-bottom: 1em">
                        <p class="border"><span><i class="fa fa-circle"></i></span> @lang('site.search')
                        </p>
                        <form>
                            <div id="custom-search-input">
                                <div class="form-group has-feedback">
                                    {{ Form::text('search',request('search'),['class' => "search-query form-control",'placeholder' => trans('site.header.search')]) }}
                                    <button hidden type="submit"></button>
                                    <span class="flaticon-search form-control-feedback"></span>
                                </div>
                            </div>
                        </form>
                    </div>
                    @if(count($items))
                        <div class="range-slider price-range">
                            <p class="border align-right"> @lang('site.products.price_between')
                                <span><i class="fa fa-circle"></i></span>
                            </p>

                            @php $priceFrom = $priceMin @endphp
                            @if(request()->has('price_from'))
                                @php $priceFrom = request()->get('price_from') @endphp
                            @endif
                            @php $priceTo = $priceMax @endphp
                            @if(request()->has('price_to'))
                                @php $priceTo = request()->get('price_to') @endphp
                            @endif
                            <div style="padding-top: 3em">
                                <input id="price-range" type="text" value="" data-slider-min="{{$priceMin}}"
                                       data-slider-max="{{$priceMax}}" data-slider-step="10"
                                       data-slider-value="[{{$priceFrom}},{{$priceTo}}]"
                                       data-value="100,10000" style="">
                            </div>
                            {!! Form::hidden('price_from', null, ['id' => 'from']) !!}
                            {!! Form::hidden('price_to', null, ['id' => 'to']) !!}
                            {{ Form::hidden('sort',request('sort'), ['id' => 'sort']) }}
                            {{ Form::hidden('status',request('status'), ['id' => 'status']) }}
                            {{ Form::hidden('category',request('category'), ['id' => 'category']) }}
                        </div>
                    @endif
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-xs-12 products">
                    <div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-left"
                             style="padding-left: 3px; padding-bottom: 2.5em;">
                            {!! Form::select('sort-select',$sorts,request('sort'),['class' => 'selectpicker','tabindex'=>'-98', 'id' => 'sort-select']) !!}
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xs-12"
                             style="padding-right:5px; padding-bottom: 2.5em; ">
                            <h5 style="color:#ff5e5e;font-weight: bold"> @lang('site.products.paginate_count',['total' =>$items->total(),'current' => $items->count() ]) </h5>
                        </div>
                        @if (request()->has('search'))
                            {!! Form::hidden('search',request('search')) !!}
                        @endif
                        {!! Form::close() !!}
                        @if(count($items))
                            <div class="product-list">
                                @foreach($items as $item)
                                    <div class="product product-padding col-lg-15 col-md-15 col-sm-3 col-xs-4"
                                         style='float: right;'>
                                        <div class="thumbnail">
                                            <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}"
                                               target="_blank">
                                                <img src="{{ asset('uploads/product/'.$item->id.'/175x175-'.$item->picture) }}"
                                                     alt="{{$item->title}}"
                                                     class="img-responsive">
                                                <div class="caption">
                                                    <p style="color:#989898">{{ $item->title }}</p>
                                                    @if($item->price > 0 && $item->quantity > 0)
                                                        @if($item->price != finalPrice($item) && finalPrice($item) > 0)
                                                            <p class="text-danger">
                                                                <strike>{{number_format($item->price)}} @lang('site.currency')</strike>
                                                            </p>
                                                        @endif
                                                        <p class="text-success">{{number_format(finalPrice($item))}} @lang('site.currency')</p>
                                                    @else
                                                        @lang('site.products.unavailable')
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                        <div class="overlay">
                                        </div>
                                        <div class="overlay-content">
                                            <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}">
                                                <span class="cart-icon-overlay"><i
                                                            class="flaticon-shopping-cart"></i></span>
                                                <p>@lang('site.products.add_to_cart') </p>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <p style="text-align: center;">@lang('site.products.no_item')</p>
                        @endif
                    </div>
                    <div>
                        <div class="col-xs-12 pull-right space-2-20">
                            <div class="pull-right check-sort">
                                <span> @lang('site.products.showing') </span>
                                <input type="checkbox"
                                       class="status" {{(request()->has( 'status' ) && request()->get( 'status' ) == 'available') ? '' : 'checked'}}>
                            </div>
                        </div>
                    </div>
                    <span class="pull-left">{!! $items->appends(Request::except('page'))->links() !!}</span>
                </div>
            </div>
        </div>
    </div>


@endsection
@push('css')
    {{ Html::style('assets/site/plugins/bootstrap-slider/css/bootstrap-slider.min.css') }}
    {{ Html::style('assets/site/plugins/bootstrap-select/bootstrap-select.min.css')}}
    {{ Html::style('assets/site/css/ion.rangeSlider.css') }}
    {{ Html::style('assets/site/css/ion.rangeSlider.skinFlat.css') }}
    {{ Html::style('assets/site/css/chocolate-list.css') }}
@endpush

@push('js')
    <script>
        var str='@lang('site.index.news')';
    </script>
    {{ Html::script('assets/site/plugins/bootstrap-select/bootstrap-select.min.js')}}
    {{ Html::script('assets/site/plugins/bootstrap-slider/js/bootstrap-slider.min.js') }}
    {{ Html::script('assets/site/js/ion.rangeSlider.js')}}
    {{ Html::script('assets/site/js/list.js')}}
    {{ Html::script('assets/site/plugins/bootstrap-checkbox/bootstrap-checkbox.min.js')}}


    <script>

        $(document).ready(function () {
            $('.brands_2').on('change', function () {
                var values = $('#price-range').val().split(',');
                $('#from').val(values[0]);
                $('#to').val(values[1]);
                if ($('#sort').val() == "") {
                    $('#sort').val('1');
                }
                if ($('#status').val() == "") {
                    $('#status').val('all');
                }
                if ($(this).is(':checked')) {
                    $('#brand').val($(this).val());

                }
                $('.search').submit();
            });

            $('.status').checkboxpicker({
                offLabel: "@lang('site.products.available')",
                onLabel: "@lang('site.products.all')",
                onActiveCls: 'btn-danger',
                toggleKeyCodes: [13, 32]
            });
            $('.status').checkboxpicker().on('change', function () {

                var url = $(this)[0]['baseURI'];
                var values = $('#price-range').val().split(',');
                $('#from').val(values[0]);
                $('#to').val(values[1]);
                if ($('#sort').val() == "") {
                    $('#sort').val('1');
                }

                if ($(this)[0]['baseURI'].indexOf('status=available') > 0) {
                    $('#status').val('all');
                    $('.search').submit();
                }
                else {
                    $('#status').val('available');
                    console.log($(this)[0]['baseURI'].split('&'));
                    $('.search').submit();
                }

            });


            var mySlider = new Slider("#price-range",
                {tooltip: 'always', tooltip_split: true});


            $(".min-slider-handle, .max-slider-handle").on("mouseup", function (slideEvt) {
                var values = $('#price-range').val().split(',');
                $('#from').val(values[0]);
                $('#to').val(values[1]);
                if ($('#sort').val() == "") {
                    $('#sort').val('1');
                }
                if ($('#status').val() == "") {
                    $('#status').val('all');
                }
                $('.search').submit();
            });

            $('#from').on('change', function (e) {
                var value = mySlider.getValue();
                value[0] = parseInt($(this).val());
                mySlider.setValue(value);
                if ($('#sort').val() == "") {
                    $('#sort').val('1');
                }
                if ($('#status').val() == "") {
                    $('#status').val('all');
                }
                $('.search').submit();
            });

            $('#to').on('change', function (e) {
                var value = mySlider.getValue();
                value[1] = parseInt($(this).val());
                mySlider.setValue(value);
                if ($('#sort').val() == "") {
                    $('#sort').val('1');
                }
                if ($('#status').val() == "") {
                    $('#status').val('all');
                }
                $('.search').submit();
            });


            $('#list-view').on('click', function (e) {
                $(this).toggleClass('active');
                $('#large-view').toggleClass('active');

            });

            $('#large-view').on('click', function (e) {
                $(this).toggleClass('active');
                $('#list-view').toggleClass('active');
                $('#profile').tab('show');
            });

            $('select[name="sort-select"],.brands').on('change', function () {
                $('#sort').val($(this).val());
                var values = $('#price-range').val().split(',');
                if ($('#status').val() == "") {
                    $('#status').val('all');
                }
                $('#from').val(values[0]);
                $('#to').val(values[1]);
                $('.search').submit();
            });

            $('.category a').click(function () {
                if ($('#sort').val() == "") {
                    $('#sort').val('1');
                }
                var values = $('#price-range').val().split(',');
                $('#category').val(($(this).data('value')));
                if ($('#status').val() == "") {
                    $('#status').val('all');
                }
                $('#from').val(values[0]);
                $('#to').val(values[1]);
                $('.search').submit();

            });

        });

    </script>
@endpush
