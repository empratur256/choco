@extends('views.layouts.site.master')
@section('title',$product->title)
@section('content')
    <div class="red_box">
        <div class="container">
            <h3 class="title-page">{{ $product->title }}</h3>
            <div class="divider-title"></div>
            <p> @lang('site.products.title_top',['type'=>$product->title]) </p>
        </div>
    </div>
    <div class="single-product">
        <div class="container">
            <div class="row">
                <div class="col-md-12 bread-crumbs"
                     style="padding-top: 1em;padding-bottom: .6em;border-bottom:1px solid #dedede;">
                    <span> @lang('site.home') </span>
                    @if(App::isLocale('en'))
                        <span> <i class="flaticon-right-arrow"></i> </span>
                    @else
                        <span> <i class="flaticon-left-arrow-1"></i> </span>
                    @endif
                    <span>{{ $product->brand->title }} </span>
                    @if(App::isLocale('en'))
                        <span> < </span>
                    @else
                        <span> <i class="flaticon-left-arrow-1"></i> </span>
                    @endif
                    <span>{{ $product->category->name_fa }}  </span>
                </div>

                <div style="margin-top: 50px;" class="container text-center">@include('views.errors.errors')</div>
                <div class="col-lg-push-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 product-img text-center">


                    <img class="img-responsive"
                         src="{{ asset('uploads/product/'.$product->id.'/435x435-'.$product->picture) }}" type="button"
                         data-toggle="modal" data-target="#carouselModal">
                    <span class="icon-zoom-img" type="button" data-toggle="modal" data-target="#carouselModal">
                        <i class="flaticon-search"></i>
                    </span>


                </div>
                <div class="modal fade" id="carouselModal" tabindex="-1" role="dialog"
                     aria-labelledby="carouselModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            class="fa fa-remove"></span></button>
                                <div class="divider divider-graydark"></div>
                                <div id="sync1" class="owl-carousel">
                                    <div class="item"><img
                                                src="{{ asset('uploads/product/'.$product->id.'/1024xauto-'.$product->picture) }}"
                                                class="img-responsive"></div>
                                    @foreach($product->pictures as $picture)
                                        <div class="item"><img
                                                    src="{{ asset('uploads/product/'.$product->id.'/1024xauto-'.$picture->picture) }}"
                                                    class="img-responsive"></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-pull-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 product-details">
                    <div class="details pull-right col-xs-12">
                        <div style="padding-top: 2em;padding-bottom: 4em">
                            <h4 style="padding-bottom: 1em;font-weight: bold;color: #727272;">{{ $product->title }}</h4>
                            <div style="padding-bottom: 1em">
                                @for($i=1;$i<=5;$i++)
                                    <i
                                            class="fa fa-star"
                                            @if($i <= $product->rate)
                                            aria-hidden="true"
                                            style="color:gold"
                                            @else
                                            aria-hidden="false"
                                            style="color:#e1e1e1"
                                            @endif
                                    >
                                    </i>
                                @endfor
                            </div>
                            @if(!auth()->check())
                                <p style="padding-bottom: 1em">@lang('site.products.text_pleas')
                                    <a style="color:rgb(255, 82, 83);"
                                       href="{{action('Auth\Controllers\Site\AuthController@showLoginForm')}}">
                                        @lang('site.products.messege_login')</a>
                                </p>
                            @endif
                            <p style="padding-bottom: 1em;color:#727272;font-weight: bolder;">@lang('site.status')
                                <span style="  color:{{($product->quantity > 0 ? '#40ab34' : '#FF5253')}}">
                                    {{($product->quantity > 0 ? trans('site.products.available') : trans('site.products.unavailable'))}}
                                </span></p>
                            @if($product->price > 0 && $product->quantity > 0)
                                @if($product->price != finalPrice($product) && finalPrice($product) > 0)
                                    <p style="color:red">
                                        <strike>{{number_format($product->price)}} @lang('site.currency')</strike></p>
                                @endif
                                <p style="color:#0ba521">{{number_format(finalPrice($product))}} @lang('site.currency')</p>
                            @endif
                            <h5 style="padding-bottom: 0.5em; font-weight: bolder; color: #727272;"> @lang('site.products.Introduction') </h5>
                            @if(!($product->description == null))
                                <p>{!! $product->description !!}</p>
                            @else
                                <p class="text-center">@lang('site.products.no_info')</p>
                            @endif

                        </div>
                        <div class="pull-right" style="padding-bottom: 1em">
                            <div class="pull-right" style="padding-bottom: 1em">
                                {!! Form::open(['action' => ['Cart\Controllers\Site\CartController@add',$product->id],'novalidate' => true, 'id' => 'add-to-shopping-cart' , 'style' => 'display: inline-block']) !!}
                                @if(count($cart))
                                    {!! Form::hidden('cart',$cart->rowId) !!}
                                @endif
                                @if($product->quantity > 0 )
                                    <div class="pull-right number-buy" style="margin-top: 5px">
                                        <span style=""> @lang('site.quantity') </span>
                                        <button type="button" id="inc-count"
                                                style="width: 21px; border-radius: 100%;border: 1px solid grey;background-color: white;color: grey;"
                                                class="inc-count btn-default">
                                            +
                                        </button>
                                        {!! Form::text('quantity', (count($cart))? $cart->qty : 1, ['class' => 'int counter','style'=>'width: 20px', 'id' => 'global-count','max' => $product->quantity]) !!}
                                        <button type="button" id="dec-global"
                                                style="width: 21px; border-radius: 100%;border: 1px solid grey;background-color: white;color: grey;"
                                                class="dec-count btn-default">
                                            -
                                        </button>
                                    </div>

                                    <div class="pull-right button-buy">
                                        <button type="submit"
                                                class="btn btn-sm btn-success">@lang('site.products.add_to_cart')</button>
                                    </div>
                                @endif
                                {{ Form::close() }}
                            </div>

                            @if(auth()->check())
                                <div class="like pull-right" style="height:  30px">
                                    {!! Form::open(['action' => 'Product\Controllers\Site\ProductController@favorite', 'class' => 'pull-right' , 'style' => 'display: inline-block']) !!}
                                    {!! Form::hidden('id',$product->id) !!}
                                    @if($product->favorite()->where('users.id',auth()->user()->id)->count())
                                        <label style="cursor: pointer" for="favorite">
                                            <button hidden id="favorite" name="favorite" type="submit">
                                            </button>
                                            <i style="color: red;" class="fa fa-heart"
                                               aria-hidden="true"></i>
                                        </label>
                                    @else
                                        <label style="cursor: pointer" for="favorite">
                                            <button hidden name="favorite" id="favorite" type="submit">
                                            </button>
                                            <i style="color: black" class="fa fa-heart" aria-hidden="true"></i>
                                        </label>
                                    @endif

                                    {!! Form::close() !!}
                                </div>
                            @endif
                            <div type="button" style="cursor: pointer;" class="share pull-right" data-toggle="modal"
                                 data-target=".bs-example-modal-sm">
                                <i style="color: black;" class="fa fa-share-alt" aria-hidden="true"></i></div>
                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
                                 aria-labelledby="mySmallModalLabel">
                                <div class="modal-dialog modal-sm" role="document">
                                    <div class="modal-content">
                                        <div class="social-share">
                                            <strong class="grayish-text"> @lang('site.products.share') : </strong>
                                            <a target="_blank"
                                               href="https://www.facebook.com/sharer/sharer.php?u={!! urlencode(action('Product\Controllers\Site\ProductController@show',[$product->id,utf8_slug($product->title)])) !!}">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-facebook fa-stack-1x fa-invers"></i>
                            </span>
                                            </a>
                                            <a target="_blank"
                                               href="https://telegram.me/share/url?url={!! urlencode(action('Product\Controllers\Site\ProductController@show',[$product->id,utf8_slug($product->title)])) !!}">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-telegram fa-stack-1x fa-inverse"></i>
                            </span>
                                            </a>
                                            <a target="_blank"
                                               href="https://twitter.com/home?status={!! urlencode(action('Product\Controllers\Site\ProductController@show',[$product->id,'twitter'])) !!}">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                                            </a>
                                            <a target="_blank"
                                               href="https://www.linkedin.com/shareArticle?title={{urlencode($product->title)}}&mini=true&url={!! urlencode(action('Product\Controllers\Site\ProductController@show',[$product->id,utf8_slug($product->title)])) !!}">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                            </span>
                                            </a>
                                            <a target="_blank"
                                               href="https://plus.google.com/share?url={!! urlencode(action('Product\Controllers\Site\ProductController@show',[$product->id,utf8_slug($product->title)])) !!}">
                            <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                            </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="{{ ($productWithSimilarBrands->count()) ? 'col-lg-9' : 'col-lg-12' }} product-tabs">

                    <div class="info-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                                      data-toggle="tab">@lang('site.products.introduction')</a>
                            </li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                       data-toggle="tab">@lang('site.products.technical')</a></li>

                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab"
                                                       data-toggle="tab">@lang('site.products.comments')</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                @if(!($product->description == null))
                                    {!! $product->description !!}
                                @else
                                    <div class="text-center">@lang('site.products.no_info')</div>
                                @endif
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                @if(count($product->details))
                                    <div class="purple-text title">@lang('site.products.more_about')</div>
                                    <table class="table table-striped">
                                        <thead>
                                        <tr style="color: black">
                                            <td>@lang('site.products.title')</td>
                                            <td>@lang('site.products.details')</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($product->details as $detail)
                                            <tr style="color: black">
                                                <td>{{($detail->title)}}</td>
                                                <td>{{($detail->pivot->data)}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    @lang('site.products.no_detail')
                                @endif
                            </div>
                            <div role="tabpanel" class="tab-pane" id="messages">
                                @if($product->comment_count > 0)
                                    @foreach($comments as $comment)
                                        <div class="comment-box row">

                                            <div class="img-container col-md-2 pull-right">
                                                <img src="{{ asset('assets/user-small-icon.png') }}"
                                                     class="img-responsive img-circle">
                                            </div>
                                            <div class="comment col-md-10">
                                                <div class="name">
                                                    <div class="user-name grayish-text">{{ $comment->user->name.' '.$comment->user->family }}</div>
                                                    <div class="rating-comment commented">
                                                        <div class="rating">
                                                            @for($i=1;$i<=5;$i++)
                                                                <span class="fa fa-star @if($i <= $comment->rate) active @endif"></span>
                                                            @endfor

                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="text-comment ">{{ strip_tags($comment->text) }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                    {!! $comments->appends(Request::except('page') + ['comments' => 1])->links() !!}
                                @else
                                    @lang('site.products.no_comment')
                                @endif
                                @if(auth()->check())

                                    {!! Form::open(['action' => ['Comments\Controllers\Site\CommentsController@create',$product->id],'class'=>'row']) !!}
                                    <div class="commenting col-md-12 space-2-20">
                                        <div class="rating-comment pull-right">
                                            <span class="text-top">@lang('site.products.rate_it'): </span>
                                            <div class="rating stars">
                                                @for($i=5;$i>=1;$i--)
                                                    <input type="radio" class="star star-{{$i}}" id="star-{{$i}}"
                                                           name="rate" value="{{$i}}"/>
                                                    <label for="star-{{$i}}" class="star star-{{$i}}"
                                                           title="{{$i}} @lang('site.products.stars')">{{$i}}</label>
                                                @endfor
                                            </div>

                                        </div>

                                        {{ Form::textarea('text', null, ['class' => 'form-control', 'rows' => '4', 'placeholder' => trans('site.products.send_your_comment'), 'lang' => 'fa']) }}
                                    </div>
                                    <div id="send" class="col-md-12">
                                        <button class="btn dark-blue-btn"
                                                type="submit">@lang('site.products.send_comment')</button>
                                    </div>
                                    {!! Form::close() !!}
                                @else
                                    @if(!auth()->check())
                                        <p style="padding-bottom: 1em">@lang('site.products.messege')
                                            <a style="color:rgb(255, 82, 83);"
                                               href="{{action('Auth\Controllers\Site\AuthController@showLoginForm')}}">
                                                @lang('site.products.messege_login')</a>
                                        </p>
                                    @endif
                                @endif

                            </div>

                        </div>
                    </div>
                    @if($productWithSimilarCategory->count())
                        <div id="#similar" class="similar-products">
                            <h5 class="title"> @lang('site.products.other_category') </h5>
                            <div class="col-lg-12 col-md-12 col-xs-12 products text-center">
                                @foreach($productWithSimilarCategory as $item)
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 product">
                                        <div class="border">
                                        </div>
                                        <div class="overlay">
                                        </div>
                                        <div class="overlay-content">
                                            <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}">
                                                <img style="border-radius: 100%; height: 40px; width: 40px;"
                                                     src="{{asset('assets/site/img/eye.png')}}">
                                                <p> @lang('site.products.add_to_cart') </p>
                                            </a>
                                        </div>
                                        <img class="img-responsive"
                                             src="{{ asset('uploads/product/'.$item->id.'/111x111-'.$item->picture) }}">
                                        <p style="padding-top: 2em"> {{ $item->title }}</p>
                                        <p style="padding-top: 1em"> {{ number_format( $item->price )}} @lang('site.currency') </p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

                @if($productWithSimilarBrands->count())
                    <div class="col-lg-3 col-md-3 col-xs-12 other-products">
                        <h5 class="title"> @lang('site.products.other_brand') </h5>
                        <div class="pull-right col-md-12 col-xs-12" style="background-color: white">
                            @foreach($productWithSimilarBrands as $item)
                                <div class="col-md-12 col-xs-12 col-sm-12 product-item">
                                    <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}">
                                        <div class="col-md-3 img-path pull-right">
                                            <img class="pull-right img-responsive"
                                                 src="{{ asset('uploads/product/'.$item->id.'/111x111-'.$item->picture) }}">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="col-md-12 title-product">
                                                <h4 class="text-right text-muted">{{ $item->title }}</h4>
                                            </div>
                                            <div class="col-md-12 price-product">
                                                <span class="text-right text-success">{{ number_format($item->price) }} @lang('site.currency')</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('css')
    {{Html::style('assets/site/plugins/bootstrap-select/bootstrap-select.min.css')}}
    {{ Html::style('assets/site/css/single-product.css') }}
@endpush
@push('js')
    {{Html::script('assets/site/plugins/bootstrap-select/bootstrap-select.min.js')}}
    <script>

        $(document).ready(function () {
            $('.counter').on('change', function (e) {
                if ($(this).val() < 0) {
                    $(this).val(0);
                }
            });


            $('.inc-count').on('click', function (e) {
                e.preventDefault();
                var value = parseInt($(this).next().val());
                $(this).next().val(value + 1);
            });


            $('.dec-count').on('click', function (e) {
                e.preventDefault();
                var value = parseInt($(this).prev().val());
                if (value <= 1) {
                    return;
                }
                $(this).prev().val(value - 1);
            });


            $('.submit').on('click', function () {
                $('.update-form').submit();
            });
        });
        $(document).ready(function () {

            var sync1 = $("#sync1");

            sync1.owlCarousel({
                items: 1,
                dots: false,
                slideSpeed: 1000,
                pagination: false,
                rtl: true,
                nav: true,
//                afterAction : syncPosition,
                responsiveRefreshRate: 200,
            });

        });
    </script>
@endpush