<?php
return [
    'product' => [
        'title'  => 'محصولات',
        'access' => [
            'ProductController' => [
                'لیست'   => 'index',
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'نمایش'  => 'show',
                'حذف'    => 'destroy',
            ],
            'PictureController' => [
                'آپلود تصویر' => [
                    'create',
                    'store',
                ],
            ],
        ],
    ],
];
