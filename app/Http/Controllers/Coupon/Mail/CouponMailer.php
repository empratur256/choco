<?php

namespace Sedehi\Http\Controllers\Coupon\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CouponMailer extends Mailable
{

    use Queueable, SerializesModels;

    public $level      = 'info';
    public $introLines = [];
    public $greeting;
    public $reminder;
    public $actionUrl;

    /**
     * Create a new message instance.
     * @return void
     */
    public function __construct($code)
    {
        $this->introLines = [
            trans('site.hi'),
            trans('site.carts.coupon_mail', ['code' => $code]),
        ];
    }

    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('site.coupon_code'))->view('views.emails.email');
    }
}
