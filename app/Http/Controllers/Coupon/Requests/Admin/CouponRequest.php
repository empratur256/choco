<?php

namespace Sedehi\Http\Controllers\Coupon\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Coupon\Models\Coupon;

class CouponRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('coupon.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Coupon::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Coupon::where('author_id',auth()->user()->id)->find($this->route()->parameter('coupon'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'title'         => 'required',
                    'discount'      => 'required',
                    'discount_type' => 'required',
                    'count'         => 'required|numeric|max:50000|min:2',
                ];
            break;
            case 'update':
                return [
                    'title'         => 'required',
                    'discount'      => 'required',
                    'discount_type' => 'required',
                ];
            break;
            case 'saveSetting':
                return [
                    'ref_coupon_discount'   => 'required|numeric',
                    'discount_type'         => 'required',
                    'ref_coupon_day_expire' => 'required|numeric',
                ];
            break;
        }

        return [];
    }

}
