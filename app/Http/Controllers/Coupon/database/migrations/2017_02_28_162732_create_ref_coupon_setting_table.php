<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefCouponSettingTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('setting')){
            Schema::table('setting', function(Blueprint $table){
                $table->tinyInteger('ref_coupon')->default(1);
                $table->integer('ref_coupon_day_expire')->default(30);
                $table->integer('ref_coupon_discount')->default(10000)->unsigned();
                $table->enum('discount_type', [
                    'price',
                    'percent',
                ])->default('price');
                $table->integer('max_discount')->unsigned()->nullable()->default(null);
            });
        }else{
            Schema::create('setting', function(Blueprint $table){
                $table->tinyInteger('ref_coupon')->default(1);
                $table->integer('ref_coupon_day_expire')->default(30);
                $table->integer('ref_coupon_discount')->default(10000)->unsigned();
                $table->enum('discount_type', [
                    'price',
                    'percent',
                ])->default('price');
                $table->integer('max_discount')->unsigned()->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
