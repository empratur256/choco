<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefCouponTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('ref_coupon', function(Blueprint $table){
            $table->increments('id');
            $table->string('code')->unique()->index();
            $table->timestamp('used_at')->nullable()->default(null)->index();
            $table->timestamp('expired_at')->nullable()->default(null)->index();
            $table->integer('user_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('order_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('user_used_id')->unsigned()->nullable()->default(null)->index();
            $table->timestamps();
            $table->index('created_at');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_used_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref_coupon');
    }
}
