<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponCodeTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_code', function(Blueprint $table){
            $table->increments('id');
            $table->string('code')->unique()->index();
            $table->integer('coupon_id')->unsigned()->index();
            $table->timestamp('used_at')->nullable()->default(null)->index();
            $table->integer('user_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('order_id')->unsigned()->nullable()->default(null)->index();
            $table->timestamps();
            $table->index('created_at');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('coupon_id')->references('id')->on('coupon')->onDelete('cascade')->onUpdate('cascade');
        });
        DB::update("ALTER TABLE coupon_code AUTO_INCREMENT = 1000;");
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_code');
    }
}
