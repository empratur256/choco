<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->timestamp('expired_at')->nullable()->default(null)->index();
            $table->integer('discount')->unsigned()->index();
            $table->enum('discount_type', [
                'price',
                'percent',
            ])->default('price')->index();
            $table->integer('max_discount')->unsigned()->nullable()->default(null);
            $table->timestamps();
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
