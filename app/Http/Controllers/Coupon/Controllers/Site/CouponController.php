<?php

namespace Sedehi\Http\Controllers\Coupon\Controllers\Site;

use Illuminate\Http\Request;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Coupon\Models\Code;
use Sedehi\Http\Controllers\Coupon\Models\PresentedCouponCode;
use Sedehi\Http\Controllers\Setting\Models\Setting;

class CouponController extends Controller
{

    public function index()
    {
        $items = auth()->user()->presentedCouponCode()->paginate(10);

        return view('Coupon.views.site.index', compact('items'));
    }

    public function calculateDiscounts($totalPrice = 0, $setting = null)
    {
        if(is_null($setting)){
            $setting = Setting::first();
        }
        $discount = 0;
        $code     = null;
        if(session()->get('coupon_type') == 'public'){
            $code = Code::where('code', session()->get('code'))->where('coupon_id', session()->get('coupon_id'))
                        ->ValidCode()->find(session()->get('code_id'));
            if(!is_null($code)){
                $coupon = $code->coupon;
                if($coupon->discount_type == 'percent'){
                    $discount = ($totalPrice * $coupon->discount) / 100;
                    if(!is_null($coupon->max_discount)){
                        if($discount > $coupon->max_discount){
                            $discount = $coupon->max_discount;
                        }
                    }
                }else{
                    $discount = $coupon->discount;
                }
            }
        }elseif(session()->get('coupon_type') == 'presented'){
            $code = PresentedCouponCode::where('user_id', auth()->user()->id)->where('code', session()->get('code'))
                                       ->ValidCode()->first();
            if(!is_null($code)){
                if($setting->discount_type == 'percent'){
                    $discount = ($totalPrice * $setting->ref_coupon_discount) / 100;
                    if(!is_null($setting->max_discount)){
                        if($discount > $setting->max_discount){
                            $discount = $setting->max_discount;
                        }
                    }
                }else{
                    $discount = $setting->ref_coupon_discount;
                }
            }
        }

        return compact('discount', 'code');
    }
}
