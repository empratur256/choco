<?php

namespace Sedehi\Http\Controllers\Coupon\Controllers\Admin;

use Carbon\Carbon;
use DB;
use Excel;
use Exception;
use Log;
use Sedehi\Http\Controllers\Coupon\Models\Code;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Coupon\Models\Coupon;
use Sedehi\Http\Controllers\Coupon\Requests\Admin\CouponRequest;
use Illuminate\Http\Request;

class CouponController extends Controller
{

    public function index()
    {
        $items = Coupon::withCount(['codes'])->filter()->latest()->paginate(20);

        return view('Coupon.views.admin.coupon.index', compact('items'));
    }

    public function create()
    {
        return view('Coupon.views.admin.coupon.add');
    }

    public function store(CouponRequest $request)
    {
        set_time_limit(300000);
        DB::beginTransaction();
        try {
            $item              = new Coupon();
            $item->title       = $request->get('title');
            $item->description = $request->get('description');
            $item->discount    = $request->get('discount');
            $item->language    = $request->get('language');
            if ($request->has('expired_at')) {
                $item->expired_at = Carbon::createFromTimestamp($request->get('expired_at_timestamp'))->endOfDay();
            }
            $item->discount_type = $request->get('discount_type');
            if ($request->has('max_discount')) {
                $item->max_discount = $request->get('max_discount');
            }
            $item->save();
            $codes   = $this->generateUniqueCode($item->id, 8, $request->get('count'));
            $inserts = [];
            foreach ($codes as $code) {
                $inserts[] = [
                    'code'       => $code,
                    'coupon_id'  => $item->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
            $inserts = array_chunk($inserts, 100);
            foreach ($inserts as $insert) {
                Code::insert($insert);
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dare sakhte code coupon '.$e);

            return redirect()->back()->withInput()->with('error', 'مشکل در ذخیره اطلاعات');
        }

        return redirect()
            ->action('Coupon\Controllers\Admin\CouponController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Coupon::findOrFail($id);
        
        if (!is_null($item->expired_at)) {
            $item->expired_at_timestamp = $item->expired_at->timestamp;
            $item->expired_at           = jdate('Y-m-d', $item->expired_at->timestamp, '', '', 'en');
        }

        return view('Coupon.views.admin.coupon.edit', compact('item'));
    }

    public function update(CouponRequest $request, $id)
    {
        $item              = Coupon::findOrFail($id);
        $item->title       = $request->get('title');
        $item->description = $request->get('description');
        $item->discount    = $request->get('discount');
        $item->language    = $request->get('language');
        if ($request->has('expired_at')) {
            $item->expired_at = Carbon::createFromTimestamp($request->get('expired_at_timestamp'))->endOfDay();
        } else {
            $item->expired_at = null;
        }
        $item->discount_type = $request->get('discount_type');
        if ($request->has('max_discount')) {
            $item->max_discount = $request->get('max_discount');
        } else {
            $item->max_discount = null;
        }
        $item->save();

        return redirect()
            ->action('Coupon\Controllers\Admin\CouponController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(CouponRequest $request)
    {
        Coupon::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

    public function export($id)
    {
        set_time_limit(300000);
        $item  = Coupon::findOrFail($id);
        $codes = $item->codes()->select('id', 'code', 'used_at')->get();
        Excel::create($item->title, function ($excel) use ($codes) {
            $excel->sheet('Sheetname', function ($sheet) use ($codes) {
                $sheet->fromModel($codes);
            });
        })->download('xls');
    }

    public function setting()
    {
        $item = Setting::first();

        return view('Coupon.views.admin.setting.setting', compact('item'));
    }

    public function saveSetting(CouponRequest $request)
    {
        $setting = Setting::first();
        if (is_null($setting)) {
            $setting = new Setting();
        }
        $setting->ref_coupon = $request->has('ref_coupon');
        if ($request->get('ref_coupon_day_expire') > 0) {
            $setting->ref_coupon_day_expire = $request->get('ref_coupon_day_expire');
        } else {
            $setting->ref_coupon_day_expire = null;
        }
        $setting->ref_coupon_discount = $request->get('ref_coupon_discount');
        $setting->discount_type       = $request->get('discount_type');
        if ($request->has('max_discount') && $request->get('discount_type') == 'percent') {
            $setting->max_discount = $request->get('max_discount');
        } else {
            $setting->max_discount = null;
        }
        $setting->save();

        return back()->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function generateUniqueCode($string = '', $lenght = 16, $count = 1)
    {
        if ($count > 1) {
            $codes   = collect();
            $counter = 0;
            while ($counter != $count) {
                $codes->push($this->generateUniqueCode($string, $lenght, 1));
                $codes   = $codes->unique();
                $counter = $codes->count();
            }

            return $codes;
        }
        $long = str_replace(".", "", uniqid(mt_rand()).time().str_random(15).time() * time());
        $code = $string.strtoupper(substr(str_shuffle(str_repeat($long, $lenght)), 0, $lenght));

        return $code;
    }

}
