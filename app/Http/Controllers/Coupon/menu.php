<?php
return [
    'coupon' => [
        'order'   => 130,
        'title'   => 'کوپن های تخفیف',
        'icon'    => 'fa fa-cube',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index'   => [
                'title'      => 'لیست',
                'action'     => 'Coupon\Controllers\Admin\CouponController@index',
                'parameters' => [],
            ],
            'Add'     => [
                'title'      => 'ایجاد',
                'action'     => 'Coupon\Controllers\Admin\CouponController@create',
                'parameters' => [],
            ],
            'Setting' => [
                'title'  => 'تنظیمات',
                'action' => 'Coupon\Controllers\Admin\CouponController@setting',
            ],
        ],
    ],
];
