<?php

namespace Sedehi\Http\Controllers\Coupon\Jobs;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use Mail;
use Sedehi\Http\Controllers\Coupon\Controllers\Admin\CouponController;
use Sedehi\Http\Controllers\Coupon\Mail\CouponMailer;
use Sedehi\Http\Controllers\Coupon\Models\PresentedCouponCode;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\User\Models\User;

class MakeCouponForUser
{

    use SerializesModels;

    protected $registrationOfOrder;
    protected $receivingCoupon;
    protected $setting;

    public function __construct($registrationOfOrder, $receivingCoupon, $setting)
    {
        $this->receivingCoupon     = $receivingCoupon;
        $this->registrationOfOrder = $registrationOfOrder;
        $this->setting             = $setting;
    }

    public function handle()
    {
        DB::beginTransaction();
        try{
            $setting             = $this->setting;
            $registrationOfOrder = $this->registrationOfOrder;
            if(is_null($registrationOfOrder)){
                return true;
            }
            $registrationOfOrder->presented_by = null;
            $registrationOfOrder->save();
            $receivingCoupon = $this->receivingCoupon;
            if(is_null($receivingCoupon)){
                return true;
            }
            if($registrationOfOrder->created_at->diffInDays() <= config('site.day_for_get_coupon')){
                $coupon          = new PresentedCouponCode;
                $coupon->code    = app(CouponController::class)->generateUniqueCode('RF-'.$registrationOfOrder->id, 5);
                $coupon->user_id = $receivingCoupon->id;
                if($setting->ref_coupon_day_expire > 0){
                    $coupon->expired_at = Carbon::now()->addDays($setting->ref_coupon_day_expire);
                }else{
                    $coupon->expired_at = null;
                }
                $coupon->save();
            }
            DB::commit();
            Mail::to($receivingCoupon->email)->send(new CouponMailer($coupon->code));
        }catch(Exception $e){
            DB::rollBack();
            Log::alert('moshkel dar transaction job MakeCouponForUser '.$e);
        }
    }

    public function failed(Exception $exception)
    {
        Log::alert('fail shodan job MakeCouponForUser '.$exception);
    }
}
