<?php
return [
    'coupon' => [
        'title'  => 'کوپن های تخفیف',
        'access' => [
            'CouponController' => [
                'لیست'    => [
                    'index',
                    'export',
                ],
                'ایجاد'   => [
                    'create',
                    'store',
                ],
                'ویرایش'  => [
                    'edit',
                    'update',
                ],
                'حذف'     => 'destroy',
                'تنظیمات' => [
                    'setting',
                    'saveSetting',
                ],
            ],
        ],
    ],
];
