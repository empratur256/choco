@extends('views.layouts.site.master')
@section('title', trans('site.my_coupon'))
@section('content')

<div class="red_box">
 <div class="container">
    <span> @lang('site.my_coupon')</span>
    <p>@lang('site.my_coupon_text') </p>
  </div>
</div>

    <div class="profile-page">
        <div class="container">
            @include('views.errors.errors')
            <div class="row">
                <div class="col-xs-12 left-panel">
                    <div class="row">
                        <div class="col-xs-12 last-orders with-border">
                            <div class="header">
                                <span class="heading green-text">@lang('site.my_coupon')</span>
                            </div>
                            <div class="table-container">
                                <table class="table table-striped table-hover">
                                    <tr>
                                        <th style="text-align: center">@lang('site.coupon_code')</th>
                                        <th style="text-align: center">@lang('site.expire_date')</th>
                                        <th style="text-align: center">@lang('site.status')</th>
                                    </tr>
                                    <tbody>
                                    @forelse($items as $item)
                                        <tr style="text-align: center">
                                            <td>{{$item->code}}</td>
                                            <td>@if(!is_null($item->expired_at)) {{ jdate('Y/m/d',$item->expired_at->timestamp) }} @else @lang('site.no_time_limit') @endif</td>
                                            <td>@if(!is_null($item->used_at) || !is_null($item->user_used_id) || !is_null($item->order_id)) @lang('site.used') @else @lang('site.not_used') @endif</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3" class="text-center">@lang('site.empty_coupon')</td>
                                        </tr>
                                    @endforelse

                                    </tbody>
                                </table>
                            </div>
                            {!! $items->links() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/copons.css') }}
@endpush
@push('js')
<script>
    $(document).ready(function () {
        $('.open-info').on('click', openNext);
    });

    function openNext(e) {
        e.preventDefault();
        $(e.target).closest('tr').next().slideToggle();
    }
</script>
@endpush