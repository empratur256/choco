<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('language','نمایش در سایت') !!}
        {!! Form::select('language',config('app.locales'),null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('description','توضیحات') !!}
        {!! Form::text('description',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('expired_at','تاریخ انقضا') !!}
        {!! Form::text('expired_at',null,['class'=>'md-input date','timestamp' => '.expired_at_timestamp']) !!}
        {!! Form::hidden('expired_at_timestamp',null,['class' => 'expired_at_timestamp']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('discount','مقدار تخفیف') !!}
        {!! Form::number('discount',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::select('discount_type',['price' => 'مبلغ','percent' => 'درصد'],null,['class'=>'md-input discount_type']) !!}
    </div>
    <div class="uk-width-medium-1-1 max_discount">
        {!! Form::label('max_discount','حداکثر مبلغ تخفیف') !!}
        {!! Form::number('max_discount',null,['class'=>'md-input ','min' => 0]) !!}
    </div>

    @if(!isset($item))
        <div class="uk-width-medium-1-1">
            {!! Form::label('count','تعداد کد') !!}
            {!! Form::number('count',null,['class'=>'md-input ','min' => 0]) !!}
        </div>
    @endif
</div>
@push('js')
<script>
    $(document).ready(function () {
        if ($('.discount_type').val() == 'percent') {
            $('.max_discount').css('display', 'block');
        } else {
            $('.max_discount').css('display', 'none');
        }
        $('.discount_type').on('change', function () {
            if ($(this).val() == 'percent') {
                $('.max_discount').css('display', 'block');
            } else {
                $('.max_discount').css('display', 'none');
            }
        });
    });
</script>
@endpush

@push('css')
<style>
    .max_discount {
        display: none;
    }
</style>
@endpush