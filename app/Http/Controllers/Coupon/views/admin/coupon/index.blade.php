@extends('views.layouts.admin.master')
@section('content')
    @include('Coupon.views.admin.coupon.search')
    {!! Form::open(['action'=>['Coupon\Controllers\Admin\CouponController@destroy',1],'method' =>'DELETE']) !!}
    
    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('Coupon.CouponController.destroy'))
                                <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید ؟');" class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">حذف</button>
                            @endif
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            <a class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" href="{!! action('Coupon\Controllers\Admin\CouponController@create') !!}" style="margin-left: 15px !important;">ایجاد</a>
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Coupon\Controllers\Admin\CouponController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>
                            <th class="uk-width-1-10  uk-text-center small_col">
                                {!! Form::checkbox('',1,null,['class' => 'check_all','data-md-icheck']) !!}
                            </th>
                            <th class="uk-width-2-10 uk-text-center">عنوان</th>
                            <th class="uk-width-2-10 uk-text-center">توضیح</th>
                            <th class="uk-width-2-10 uk-text-center">نمایش در سایت</th>
                            <th class="uk-width-2-10 uk-text-center">تعداد</th>
                            <th class="uk-width-2-10 uk-text-center">تخفیف</th>
                            <th class="uk-width-2-10 uk-text-center">تاریخ انقضا</th>
                            <th class="uk-width-2-10 uk-text-center">.....</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            <tr class="uk-text-center">
                                <td class="uk-text-center uk-table-middle small_col">
                                    {!! Form::checkbox('deleteId[]',$item->id,null,['class' => 'check_row','data-md-icheck']) !!}
                                </td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->description }}</td>
                                <td>{{ config('app.locales.'.$item->language) }}</td>
                                <td>{{ $item->codes_count }}</td>
                                <td>{{ number_format($item->discount) }} @if($item->discount_type == 'price') تومان @else درصد @endif</td>
                                <td>
                                    @if(!is_null($item->expired_at))
                                        <span data-uk-tooltip title="{{ $item->expired_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->expired_at->timestamp) }}</span>
                                    @endif
                                </td>
                                <td class="uk-text-center">
                                    @if(permission('Coupon.CouponController.edit'))
                                        <a href="{!! action('Coupon\Controllers\Admin\CouponController@edit',$item->id) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                                    @endif
                                    <a class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" href="{!! action('Coupon\Controllers\Admin\CouponController@export',$item->id) !!}">خروجی اکسل</a>
                                
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection