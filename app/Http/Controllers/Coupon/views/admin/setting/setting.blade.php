@extends('views.layouts.admin.master')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom" id="adminBreadcrumb"></h3>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            @include('views.errors.errors')
                            {!! Form::model($item,['action'=> 'Coupon\Controllers\Admin\CouponController@saveSetting']) !!}
                            <div class="uk-form-row searchPanel">

                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <span class="icheck-inline">
                                            {!! Form::checkbox('ref_coupon',1,null,['data-md-icheck','id' => 'ref_coupon']) !!}
                                            {!! Form::label('ref_coupon','فعال سازی دریافت کوپن با معرفی دوستان',['class' => 'inline-label']) !!}
                                        </span>
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        <div class="uk-alert" data-uk-alert="">برای نامحدود کردن مدت اعتبار کوپن تخفیف 0 وارد شود</div>
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('ref_coupon_day_expire','تعداد روز معتبر بودن کوپن') !!}
                                        {!! Form::text('ref_coupon_day_expire',null,['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('ref_coupon_discount','مقدار تخفیف') !!}
                                        {!! Form::number('ref_coupon_discount',null,['class'=>'md-input','min' => 0]) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::select('discount_type',['price' => 'مبلغ','percent' => 'درصد'],null,['class'=>'md-input discount_type','placeholder' => 'نوع تخفیف']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1 max_discount">
                                        {!! Form::label('max_discount','حداکثر مبلغ تخفیف') !!}
                                        {!! Form::number('max_discount',null,['class'=>'md-input ','min' => 0]) !!}
                                    </div>

                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light " style="margin-top: 10px">ذخیره</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
<script>
    $(document).ready(function () {
        if ($('.discount_type').val() == 'percent') {
            $('.max_discount').css('display', 'block');
        } else {
            $('.max_discount').css('display', 'none');
        }
        $('.discount_type').on('change', function () {
            if ($(this).val() == 'percent') {
                $('.max_discount').css('display', 'block');
            } else {
                $('.max_discount').css('display', 'none');
            }
        });
    });
</script>
@endpush

@push('css')
<style>
    .max_discount {
        display: none;
    }
</style>
@endpush