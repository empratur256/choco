<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Coupon\Controllers\Admin',
             ], function(){
    Route::get('coupon/export/{id}', 'CouponController@export');
    Route::get('coupon/setting', 'CouponController@setting');
    Route::post('coupon/setting', 'CouponController@saveSetting');
    Route::resource('coupon', 'CouponController', ['except' => ['show']]);
});
Route::group([
                 'prefix'     => config('site.dashboard'),
                 'middleware' => 'dashboard',
                 'namespace'  => 'Coupon\Controllers\Site',
             ], function(){
    Route::get('/coupons', 'CouponController@index');
});