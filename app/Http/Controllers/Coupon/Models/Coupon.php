<?php

namespace Sedehi\Http\Controllers\Coupon\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Libs\LanguageTrait;

class Coupon extends Model
{

    use  Filterable, SoftDeletes,LanguageTrait;

    protected $table      = 'coupon';
    public    $timestamps = true;
    public    $dates      = ['expired_at'];

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function codes()
    {
        return $this->hasMany(Code::class);
    }

}
