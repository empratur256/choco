<?php

namespace Sedehi\Http\Controllers\Coupon\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class Code extends Model
{

    use  Filterable;

    protected $table      = 'coupon_code';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function scopeValidCode($query)
    {
        return $query->whereNull('used_at')->whereNull('user_id')->whereNull('order_id')
                     ->whereHas('coupon', function($query){
                         $query->whereNull('expired_at')->orWhere('expired_at', '>', Carbon::now())->language();
                     });
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

}
