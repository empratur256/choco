<?php

namespace Sedehi\Http\Controllers\Coupon\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class PresentedCouponCode extends Model
{

    use  Filterable;

    protected $table      = 'ref_coupon';
    public    $timestamps = true;

    public $dates = ['expired_at'];

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function scopeValidCode($query)
    {
        return $query->whereNull('used_at')->whereNull('user_used_id')->whereNull('order_id')->where(function($query){
            $query->whereNull('expired_at')->orWhere('expired_at', '>', Carbon::now());
        });
    }

}
