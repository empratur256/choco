<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان در مدیریت') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('account_name','نام مالک حساب') !!}
        {!! Form::text('account_name',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('bank_name','نام بانک') !!}
        {!! Form::text('bank_name',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('card_number','شماره کارت') !!}
        {!! Form::text('card_number',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('account_number','شماره حساب') !!}
        {!! Form::text('account_number',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('sheba','شماره شبا') !!}
        {!! Form::text('sheba',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('language','نمایش در سایت') !!}
        {!! Form::select('language',config('app.locales'),null,['class'=>'md-input']) !!}
    </div>
</div>
