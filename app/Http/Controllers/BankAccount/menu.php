<?php
return [
    'bankaccount' => [
        'order'   => 50,
        'title'   => 'حساب های بانکی',
        'icon'    => 'fa fa-bank',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'BankAccount\Controllers\Admin\BankAccountController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'BankAccount\Controllers\Admin\BankAccountController@create',
                'parameters' => [],
            ],
        ],
    ],
];
