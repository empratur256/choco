<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'BankAccount\Controllers\Admin',
             ], function(){
    Route::resource('bankaccount', 'BankAccountController', ['except' => ['show']]);
});
