<?php

namespace Sedehi\Http\Controllers\BankAccount\Controllers\Admin;

use Sedehi\Http\Controllers\Payment\Models\BankAccount;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\BankAccount\Requests\Admin\BankAccountRequest;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{

    public function index()
    {
        $items = BankAccount::filter()->latest()->paginate(20);

        return view('BankAccount.views.admin.bankaccount.index', compact('items'));
    }

    public function create()
    {
        return view('BankAccount.views.admin.bankaccount.add');
    }

    public function store(BankAccountRequest $request)
    {
        $item                 = new BankAccount();
        $item->account_name   = $request->get('account_name');
        $item->bank_name      = $request->get('bank_name');
        $item->title          = $request->get('title');
        $item->card_number    = $request->get('card_number');
        $item->account_number = $request->get('account_number');
        $item->sheba          = $request->get('sheba');
        $item->language       = $request->get('language');
        $item->save();

        return redirect()->action('BankAccount\Controllers\Admin\BankAccountController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = BankAccount::findOrFail($id);

        return view('BankAccount.views.admin.bankaccount.edit', compact('item'));
    }

    public function update(BankAccountRequest $request, $id)
    {
        $item                 = BankAccount::findOrFail($id);
        $item->account_name   = $request->get('account_name');
        $item->bank_name      = $request->get('bank_name');
        $item->title          = $request->get('title');
        $item->card_number    = $request->get('card_number');
        $item->account_number = $request->get('account_number');
        $item->sheba          = $request->get('sheba');
        $item->language       = $request->get('language');
        $item->save();

        return redirect()->action('BankAccount\Controllers\Admin\BankAccountController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(BankAccountRequest $request)
    {
        BankAccount::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
