<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account', function(Blueprint $table){
            $table->increments('id');
            $table->string('account_name');
            $table->string('bank_name');
            $table->string('title');
            $table->string('card_number');
            $table->string('account_number');
            $table->string('sheba');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_account');
    }
}
