<?php

namespace Sedehi\Http\Controllers\BankAccount\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\BankAccount\Models\BankAccount;

class BankAccountRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'account_name'   => 'required',
                    'bank_name'      => 'required',
                    'title'          => 'required',
                    'card_number'    => 'required|card_number',
                    'account_number' => 'required',
                    'sheba'          => 'required|sheba',
                    'language'       => 'required',
                ];
            break;
        }

        return [];
    }

}
