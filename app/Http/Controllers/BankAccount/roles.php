<?php
return [
    'bankaccount' => [
        'title'  => 'حساب های بانکی',
        'access' => [
            'BankAccountController' => [
                'لیست'   => 'index',
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
