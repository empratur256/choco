<?php

namespace Sedehi\Http\Controllers;

use Illuminate\Http\Request;
use Sedehi\Http\Controllers\Category\Models\Category;
use Sedehi\Http\Controllers\Comments\Models\Comments;
use Sedehi\Http\Controllers\Contact\Models\Contact;
use Sedehi\Http\Controllers\CustomOrder\Models\Portfolio;
use Sedehi\Http\Controllers\CustomOrder\Models\Routine;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Payment\Models\Payment;
use Sedehi\Http\Controllers\Payment\Models\Settlement;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Controllers\Slider\Models\Slider;
use Sedehi\Http\Controllers\Ticket\Models\Ticket;
use Sedehi\Http\Requests;

class HomeController extends Controller
{

    public function index()
    {
        $categories = Category::get()->take(7);
        $routine = Routine::orderBy('order')->get();
        $discounts = Product::Discounts()
            ->where('show_homepage', 1)
            ->active()
            ->get();
        $newest = Product::latest()
            ->active()
            ->limit(20)
            ->get();
        $bestsellers = Product::active()
            ->where('order_count', '>', 1)
            ->limit(20)
            ->orderBy('order_count', 'DESC')
            ->get();
        $portfolioCount = Portfolio::count();


        return view('views.site.index', compact(
            'routine',
            'categories',
            'discounts',
            'newest',
            'bestsellers',
            'portfolioCount'
        ));
    }

    public function admin()
    {
        $commentCount = Comments::where('confirm', 0)->where('reject', 0)->count();
        $paymentCount = Payment::where('status', 1)->count();
        $ticketCount = Ticket::where('answered', 0)->count();
        $unreadedContact = Contact::unreaded()->count();
        $unConfirmOrder = Order::where('confirm', 1)
            ->where('paid', 0)
            ->where('processing_stock', 0)
            ->where('ready_to_send', 0)
            ->where('delivered', 0)
            ->count();
        $unPaiOrder = Order::where('confirm', 1)->where('paid', 0)->count();
        $processingStock = Order::where('confirm', 1)->where('processing_stock', 1)->count();
        $readyToSend = Order::where('confirm', 1)->where('ready_to_send', 1)->count();
        $settlementNeedToConfirm = Settlement::where('confirm', 0)->where('reject', 0)->count();
        $customOrderNeedCheck = Order::where('confirm', 1)
            ->where('paid', 0)
            ->where('type', 'custom')
            ->where('processing_stock', 0)
            ->where('ready_to_send', 0)
            ->where('delivered', 0)
            ->count();

        return view('views.admin.index',
            compact('commentCount', 'unPaiOrder', 'paymentCount', 'unreadedContact', 'settlementNeedToConfirm',
                'ticketCount', 'unConfirmOrder', 'processingStock', 'readyToSend', 'customOrderNeedCheck'));
    }
}
