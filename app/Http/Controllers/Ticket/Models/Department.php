<?php

namespace Sedehi\Http\Controllers\Ticket\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class Department extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'department';
    public    $timestamps = true;
    protected $dates      = ['deleted_at'];

    protected $filterable = [
        'name'       => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

}
