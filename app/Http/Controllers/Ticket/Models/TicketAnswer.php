<?php

namespace Sedehi\Http\Controllers\Ticket\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Http\Controllers\User\Models\User;

class TicketAnswer extends Model
{
    protected $table      = 'ticket_answer';
    public    $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
