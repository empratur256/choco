<?php

namespace Sedehi\Http\Controllers\Ticket\Models;

use Illuminate\Database\Eloquent\Model;

class TicketPriority extends Model
{
    protected $table      = 'ticket_priority';
    public    $timestamps = false;
}
