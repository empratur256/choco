<?php

namespace Sedehi\Http\Controllers\Ticket\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\User\Models\User;

class Ticket extends Model
{

    use  Filterable;

    protected $table      = 'ticket';
    public    $timestamps = true;

    protected $filterable = [
        'subject'    => [
            'operator' => 'Like',
        ],
        'priority_id',
        'answered',
        'user_id',
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function priority()
    {
        return $this->belongsTo(TicketPriority::class);
    }

    public function answers()
    {
        return $this->hasMany(TicketAnswer::class);
    }
}
