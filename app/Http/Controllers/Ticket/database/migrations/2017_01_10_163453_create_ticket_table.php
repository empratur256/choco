<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function(Blueprint $table){
            $table->increments('id')->unsigned();
            $table->integer('department_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('priority_id')->unsigned()->index();
            $table->string('subject');
            $table->tinyInteger('readed')->unsigned()->default(0)->index();
            $table->tinyInteger('answered')->unsigned()->default(0)->index();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('department_id')->references('id')->on('department')->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('priority_id')->references('id')->on('ticket_priority')->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->index('created_at');
            $table->index('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket');
    }
}
