<?php

namespace Sedehi\Http\Controllers\Ticket\database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Sedehi\Http\Controllers\Ticket\Models\Department;

class DepartmentTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $datas = [
            'مالی',
            'پشتیبانی',
        ];
        foreach($datas as $data){
            $item = new Department;
            $item->name = $data;
            $item->save();
        }
    }

}
