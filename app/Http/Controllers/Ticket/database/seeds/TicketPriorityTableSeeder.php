<?php

namespace Sedehi\Http\Controllers\Ticket\database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketPriorityTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id'   => 3,
                'name' => 'زیاد',
            ],
            [
                'id'   => 2,
                'name' => 'متوسط',
            ],
            [
                'id'   => 1,
                'name' => 'کم',
            ],
        ];
        DB::table('ticket_priority')->insert($data);
    }
}
