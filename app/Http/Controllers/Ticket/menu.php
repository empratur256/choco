<?php
return [
    'ticket' => [
        'order'   => 170,
        'title'   => 'تیکت',
        'icon'    => 'fa fa-ticket',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'ticket-index'     => [
                'title'      => 'لیست تیکت ها',
                'action'     => 'Ticket\Controllers\Admin\TicketController@index',
                'parameters' => [],
            ],
            'department-index' => [
                'title'      => 'لیست دپارتمان ها',
                'action'     => 'Ticket\Controllers\Admin\DepartmentController@index',
                'parameters' => [],
            ],
            'department-add'   => [
                'title'      => 'ایجاد دپارتمان',
                'action'     => 'Ticket\Controllers\Admin\DepartmentController@create',
                'parameters' => [],
            ],
        ],
    ],
];
