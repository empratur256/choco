<?php

namespace Sedehi\Http\Controllers\Ticket\Controllers\Site;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Log;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Ticket\Models\Department;
use Sedehi\Http\Controllers\Ticket\Models\Ticket;
use Sedehi\Http\Controllers\Ticket\Models\TicketAnswer;
use Sedehi\Http\Controllers\Ticket\Models\TicketPriority;
use Sedehi\Http\Controllers\Ticket\Requests\Site\TicketRequest;
use Sedehi\Http\Controllers\User\Models\Address;

class TicketController extends Controller
{

    public function index()
    {
        $tickets = auth()->user()->tickets()->latest('updated_at')->paginate(10);

        return view('Ticket.views.site.index', compact('tickets'));
    }

    public function create()
    {
        $departments = Department::pluck('name', 'id');
        $priority = TicketPriority::pluck('name', 'id');
        $user = auth()->user();
        $address = Address::where('user_id', $user->id)->first();
        return view('Ticket.views.site.add',
            compact(
                'departments',
                'priority',
                'user',
                'address'
            )
        );
    }

    public function store(TicketRequest $request)
    {
        DB::beginTransaction();
        try {
            $ticket = new Ticket;
            $ticket->department_id = $request->get('department');
            $ticket->subject = $request->get('subject');
            $ticket->priority_id = $request->get('priority');
            $ticket->user_id = auth()->user()->id;
            $ticket->readed = 1;
            $ticket->answered = 0;
            $ticket->save();
            $message = new TicketAnswer;
            $message->ticket_id = $ticket->id;
            $message->user_id = auth()->user()->id;
            $message->text = $request->get('text');
            $message->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar sabte ticket ' . $e);

            return redirect()->action('User\Controllers\Site\DashboardController@index')
                ->with('error', trans('site.ticket.problem_sending_message'));
        }

        return redirect()->action('User\Controllers\Site\DashboardController@index')
            ->with('success', trans('site.ticket.success'));
    }

    public function show($id)
    {
        $ticket = auth()->user()->tickets()->with([
            'priority',
            'department' => function ($query) {
                $query->withTrashed();
            },
            'answers' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
        ])->findOrFail($id);
        $ticket->readed = 1;
        $ticket->save();
        $user = auth()->user();
        $address = Address::where('user_id', $user->id)->first();
        return view('Ticket.views.site.show',
            compact(
                'ticket',
                'user',
                'address'
            )
        );
    }

    public function answer(TicketRequest $request)
    {
        $ticket = auth()->user()->tickets()->findOrFail($request->get('id'));
        $answer = new TicketAnswer();
        $answer->ticket_id = $ticket->id;
        $answer->user_id = auth()->user()->id;
        $answer->text = $request->get('text');
        $answer->save();
        $ticket->answered = 0;
        $ticket->updated_at = Carbon::now();
        $ticket->save();

        return redirect()->back()->with('success', trans('site.ticket.message_success'));
    }
}
