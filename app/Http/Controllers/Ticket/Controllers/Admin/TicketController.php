<?php

namespace Sedehi\Http\Controllers\Ticket\Controllers\Admin;

use Exception;
use File;
use Illuminate\Support\Facades\DB;
use Log;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Ticket\Models\Ticket;
use Sedehi\Http\Controllers\Ticket\Models\TicketAnswer;
use Sedehi\Http\Controllers\Ticket\Models\TicketPriority;
use Sedehi\Http\Controllers\Ticket\Requests\Admin\TicketRequest;

class TicketController extends Controller
{

    private $uploadPath = 'uploads/ticket/';

    public function index()
    {
        $departments = auth()->user()->departments()->getRelatedIds()->toArray();
        $items = Ticket::query();
        $items->with([
                         'department',
                         'priority',
                     ]);
        if(count($departments) > 0){
            $items->whereIn('department_id', $departments);
        }
        $items = $items->filter()->latest('updated_at')->paginate(20);
        $priorities = TicketPriority::pluck('name', 'id');

        return view('Ticket.views.admin.ticket.index', compact('items', 'priorities'));
    }

    public function store(TicketRequest $request)
    {
        DB::beginTransaction();
        try{
            $ticket                = new Ticket();
            $ticket->department_id = $request->get('department_id');
            $ticket->user_id       = auth()->user()->id;
            $ticket->priority_id   = $request->get('priority_id');
            if($request->has('project_id')){
                $ticket->project_id = $request->get('project_id');
            }
            $ticket->subject  = $request->get('subject');
            $ticket->readed   = 0;
            $ticket->answered = 1;
            $ticket->save();
            $answer            = new TicketAnswer();
            $answer->ticket_id = $ticket->id;
            $answer->user_id   = auth()->user()->id;
            $answer->text      = $request->get('text');
            if($request->hasFile('file')){
                $ticketFolder = public_path($this->uploadPath.$ticket->id.'/');
                if(!File::isDirectory($ticketFolder)){
                    File::makeDirectory($ticketFolder, 0777, true);
                }
                $file     = $request->file('file');
                $fileName = time().$file->hashName();
                $file->move($ticketFolder, $fileName);
                $answer->file = $fileName;
            }
            $answer->save();
            DB::commit();
            dd('Done . must redirect to some action ....');

            return redirect()->action('')->with('success', 'پیام شما با موفقیت ارسال شد');
        }catch(Exception $exception){
            DB::rollback();
            Log::error('moshkel dar sabte ticket e admin'.$exception);

            return back()->with('error', 'خطا در ثبت تیکت')->withInput();
        }
    }

    public function show($id)
    {
        $departments = auth()->user()->departments()->getRelatedIds()->toArray();
        $item = Ticket::query();
        $item->with([
                        'department',
                        'priority',
                    ]);
        if(count($departments) > 0){
            $item->whereIn('department_id', $departments);
        }
        $item = $item->findOrFail($id);

        return view('Ticket.views.admin.ticket.show', compact('item'));
    }

    public function answer(TicketRequest $request)
    {
        $departments = auth()->user()->departments()->getRelatedIds()->toArray();
        $ticket = Ticket::query();
        $ticket->with([
                          'department',
                          'priority',
                      ]);
        if(count($departments) > 0){
            $ticket->whereIn('department_id', $departments);
        }
        $ticket = $ticket->findOrFail($request->get('ticket_id'));
        $answer            = new TicketAnswer();
        $answer->ticket_id = $ticket->id;
        $answer->user_id   = auth()->user()->id;
        $answer->text      = $request->get('answer');
        $answer->save();
        $ticket->answered = 1;
        $ticket->readed   = 0;
        $ticket->save();

        return redirect()->action('Ticket\Controllers\Admin\TicketController@index')->with('success', 'پاسخ ارسال شد');
    }

    public function destroy(TicketRequest $request)
    {
        $departments = auth()->user()->departments()->getRelatedIds()->toArray();
        $ticket = Ticket::query();
        $ticket->with([
                          'department',
                          'priority',
                      ]);
        if(count($departments) > 0){
            $ticket->whereIn('department_id', $departments);
        }
        $ticket->whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
