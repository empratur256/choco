<?php

namespace Sedehi\Http\Controllers\Ticket\Controllers\Admin;

use Sedehi\Http\Controllers\Ticket\Models\Department;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Ticket\Requests\Admin\DepartmentRequest;

class DepartmentController extends Controller
{

    public function index()
    {
        $items = Department::filter()->latest()->paginate(20);

        return view('Ticket.views.admin.department.index', compact('items'));
    }

    public function create()
    {
        return view('Ticket.views.admin.department.add');
    }

    public function store(DepartmentRequest $request)
    {
        $item       = new Department();
        $item->name = $request->get('name');
        $item->save();

        return redirect()->action('Ticket\Controllers\Admin\DepartmentController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Department::findOrFail($id);

        return view('Ticket.views.admin.department.edit', compact('item'));
    }

    public function update(DepartmentRequest $request, $id)
    {
        $item       = Department::findOrFail($id);
        $item->name = $request->get('name');
        $item->save();

        return redirect()->action('Ticket\Controllers\Admin\DepartmentController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(DepartmentRequest $request)
    {
        Department::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
