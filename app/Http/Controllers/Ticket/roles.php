<?php
return [
    'ticket' => [
        'title'  => 'تیکت',
        'access' => [
            'TicketController'     => [
                'لیست تیکت ها'            => 'index',
                'مشاهده و پاسخ گویی تیکت' => [
                    'show',
                    'answer',
                    'getAttachment',
                ],
                'حذف تیکت'                => 'destroy',
            ],
            'DepartmentController' => [
                'لیست دپارتمان ها' => 'index',
                'ایجاد دپارتمان'   => [
                    'create',
                    'store',
                ],
                'ویرایش دپارتمان'  => [
                    'edit',
                    'update',
                ],
                'حذف دپارتمان'     => 'destroy',
            ],
        ],
    ],
];
