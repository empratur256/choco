<?php
Route::group([
                 'prefix'     => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace'  => 'Ticket\Controllers\Admin',
             ], function(){
    Route::resource('department', 'DepartmentController', ['except' => ['show']]);
    Route::post('ticket/answer', 'TicketController@answer');
    Route::resource('ticket', 'TicketController', [
        'only' => [
            'index',
            'show',
            'destroy',
        ],
    ]);
});
Route::group([
                 'prefix'     => config('site.dashboard'),
                 'middleware' => 'dashboard',
             ], function(){
    Route::post('ticket/answer', 'Ticket\Controllers\Site\TicketController@answer');
    Route::resource('ticket', 'Ticket\Controllers\Site\TicketController');
});