<?php

namespace Sedehi\Http\Controllers\Ticket\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Ticket\Models\Ticket;

class TicketRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'subject'       => 'required',
                    'text'          => 'required',
                    'department_id' => 'required|exists:department,id',
                    'priority_id'   => 'required|exists:ticket_priority,id',
                    'project_id'    => 'numeric|exists:project,id',
                    'file'          => 'file|mimes:zip|max:10240',
                ];
            break;
            case 'answer':
                return [
                    'ticket_id' => 'required',
                    'answer'    => 'required',
                ];
            break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'file.mimes' => 'نوع فایل باید zip باشد',
        ];
    }
}
