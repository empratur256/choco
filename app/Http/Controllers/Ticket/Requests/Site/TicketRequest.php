<?php

namespace Sedehi\Http\Controllers\Ticket\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Ticket\Models\Ticket;

class TicketRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'store':
                return [
                    'subject'    => 'required',
                    'department' => 'required',
                    'priority'   => 'required',
                    'text'       => 'required',
                ];
            break;
            case 'answer':
                return [
                    'text' => 'required',
                    'id'   => 'required|numeric',
                ];
            break;
        }

        return [];
    }

}
