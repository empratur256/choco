@extends('views.layouts.site.master')
@section('title',trans('site.ticket.send_ticket'))
@section('content')
    <div class="red_box">
        <div class="container">
            <h3> @lang('site.ticket.send_ticket')  </h3>
            <div class="divider-title"></div>
            <p> @lang('site.ticket.top_text') </p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container bread-crumbs">
                <span> @lang('site.home') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i> </span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.ticket.title') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i> </span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.ticket.send_ticket') </span>
            </div>
        </div>

        <div class="container">
            @include('views.errors.errors')
            <div class="col-sm-3 col-sm-push-9 profile text-center" style="">
                <div class="bg-white">
                    <img class="m-profile" src="{{asset('assets/site/img/profile-m.png')}}">
                    <p class="padding-top-down-1">{{$user->name}} {{$user->family}}</p>
                    <p class="padding-top-down-1">{{$user->email}}</p>
                    <p class="padding-top-down-1">{{$user->mobile}}</p>
                    @if($address)
                        <p class="padding-top-down-1" style="padding-bottom: 2.5em">{{$address->address}}</p>
                    @endif
                </div>
            </div>
            <div class="col-sm-9 col-sm-pull-3 bg-white ticket">
                <div class="row">
                    {!! Form::open(['action' => 'Ticket\Controllers\Site\TicketController@store']) !!}
                    <div class="col-lg-11 col-sm-10 col-xs-12" style="padding-bottom: 2em">
                        <div class="form-group title-request">
                            {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => trans('site.ticket.subject')]) !!}
                        </div>
                        <div class="pull-right">
                            <p style="padding-bottom: 0.5em">@lang('site.ticket.department')</p>
                            {{ Form::select('department',$departments, null, ['id' => 'sort-select','class' => 'selectpicker form-control' ,'tabindex' => '-98']) }}
                        </div>
                        <div class="pull-right margin-right-1">
                            <p style="padding-bottom: 0.5em">@lang('site.ticket.priority')</p>
                            {{ Form::select('priority',$priority, null, ['id' => 'sort-select','class' => 'selectpicker form-control' ,'tabindex' => '-98']) }}
                        </div>
                        <div class="form-group col-xs-12 description">
                            {!! Form::textarea('text', null, [
                            'class' => 'form-control',
                             'placeholder' => trans('site.ticket.text'),
                             'style' => 'margin-bottom: 15px',
                             'id' => 'description'
                             ]) !!}
                        </div>
                        <div class="col-lg-12" style="padding-bottom: 4em;padding-top: 0.5em">
                            {{--<div class="pull-right">--}}
                            {{--captche--}}
                            {{--</div>--}}
                            <div class="pull-left">
                                <button type="submit"
                                        class="btn btn-danger submit-button">@lang('site.ticket.send')</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-lg-1 col-sm-2 s-profile hidden-xs">
                        <img class="" src="{{asset('assets/site/img/s-profile.png')}}">
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/send-ticket.css') }}

@endpush

@push('js')


@endpush