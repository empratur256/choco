@extends('views.layouts.site.master')
@section('title',trans('site.ticket.message').' '.$ticket->subject)
@section('content')
    <div class="red_box">
        <div class="container">
            <h3> @lang('site.ticket.message')  </h3>
            <div class="divider-title"></div>
            <p> @lang('site.ticket.top_text2') </p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container bread-crumbs">
                <span> @lang('site.home') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i> </span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.support') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i> </span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.ticket.message') </span>
            </div>
        </div>

        <div class="container">
            <div>@include('views.errors.errors')</div>
            <div class="col-sm-3 col-sm-push-9 profile text-center" style="">
                <div class="bg-white">
                    <img class="m-profile" src="{{asset('assets/site/img/profile-m.png')}}">
                    <p class="padding-top-down-1">{{$user->name}} {{$user->family}}</p>
                    <p class="padding-top-down-1">{{$user->email}}</p>
                    <p class="padding-top-down-1">{{$user->mobile}}</p>
                    @if($address)
                        <p class="padding-top-down-1" style="padding-bottom: 2.5em">{{$address->address}}</p>
                    @endif
                </div>
            </div>
            <div class="col-sm-9 col-sm-pull-3 bg-white ticket">
                <div class="row">
                    <div class="col-xs-12">
                        <h4 class="bolder title-ticket">{{$ticket->subject}}</h4>
                    </div>
                    <div class="col-xs-12">
                        <div class="people-row">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                                    <span><span class="bolder">@lang('site.ticket.department') </span>{{$ticket->department->name}}</span>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                                    <span><span class="bolder">@lang('site.ticket.send_date') </span>{{ jdate(' H:i Y/m/d',$ticket->created_at->timestamp) }}</span>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                                    <span><span class="bolder"> @lang('site.ticket.priority') </span>{{ $ticket->priority->name }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @foreach($ticket->answers as $answer)
                        <div class="col-xs-12">
                            <div class="people-row">
                                <div class="row">
                                    <div class="col-lg-1 pull-right">
                                        <img class="s-profile" style="margin-top: 0;padding-left: 1em"
                                             src="{{asset('assets/site/img/s-profile.png')}}">
                                    </div>
                                    <div class="col-lg-11">
                                        <h5 class="bolder pull-right">@if($answer->user_id == auth()->user()->id) {{ auth()->user()->name.' '.auth()->user()->family }} @else @lang('site.ticket.user') @endif</h5>
                                        <h6 class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i>
                                            {{ jdate('H:i Y/m/d',$answer->created_at->timestamp) }}
                                        </h6>
                                    </div>
                                    <div class="col-md-11 pull-left">
                                        <p class="pull-right defult-text">{{ $answer->text }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{ Form::open(['action' => 'Ticket\Controllers\Site\TicketController@answer']) }}

                    {!! Form::hidden('id',$ticket->id) !!}
                    <div class="form-group col-xs-12 description" style="width:98%">
                        {{ Form::textarea('text', null, ['placeholder' => trans('site.ticket.answer'), 'class' => 'form-control']) }}
                    </div>
                    <div class="col-xs-12" style="padding-bottom: 2em">
                        <button type="submit"
                                class="btn btn-default submit-button pull-left">@lang('site.ticket.send')</button>
                    </div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>

@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/send-ticket.css') }}
@endpush
@push('js')

@endpush