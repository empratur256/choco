@extends('views.layouts.site.master')
@section('title', trans('site.ticket.list'))
@section('content')
<div class="red_box">
    <div class="container">
        <span> @lang('site.favorites') </span>
        <p> @lang('site.favorites_list') </p>
    </div>
</div>
<div class="bg-color">
    <div style="padding-top: 1em;padding-bottom: 1em">
        <div class="container">
            <span> @lang('site.home') </span>
            @if(App::isLocale('en'))
                <span> < </span>
            @else
                <span> &gt; </span>
            @endif
            <span> @lang('site.favorites') </span>
        </div>
    </div>
    <div class="profile-page">
        <div class="container">
            @include('views.errors.errors')
            <div class="row">
                <div class="col-xs-12 left-panel">
                    <div class="row">
                        <div class="col-xs-12 last-orders with-border">
                            <div class="bg-white col-xs-12">
                            <div class="header ">
                                <span class="heading green_text">@lang('site.ticket.list')</span>
                                <a class="btn grayish-btn send-ticket" href="{!! action('Ticket\Controllers\Site\TicketController@create') !!}">@lang('site.ticket.send')</a>
                            </div>
                            <div class="table-container">
                                <table class="table table-striped table-hover">
                                    <tbody>
                                    @forelse($tickets as $ticket)
                                        <tr @if($ticket->readed == 0) style="font-weight: bold" @endif>
                                            <td>
                                                <span class="fa fa-circle grayish-text"></span>
                                                <span>{{ str_limit($ticket->subject,30) }}</span>
                                            </td>
                                            <td>{{ jdate('Y/m/d',$ticket->created_at->timestamp) }}</td>
                                            <td>
                                                <a href="{!! action('Ticket\Controllers\Site\TicketController@show',$ticket->id) !!}" class="dark-blue-text">@lang('site.ticket.show')</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3" class="text-center">@lang('site.ticket.is_empty')</td>
                                        </tr>
                                    @endforelse
                                    
                                    </tbody>
                                </table>
                            </div>
                            <div class="paginate">
                                {!! $tickets->links() !!}
                            </div>
                         </div>
                        </div>
                    
                    </div>
                </div>
            
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')

   {{ Html::style('assets/site/css/public.css') }}
   {{Html::style('assets/site/css/ticket.css')}}

@endpush