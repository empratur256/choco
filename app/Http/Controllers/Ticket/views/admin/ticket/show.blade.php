@extends('views.layouts.admin.master')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom" id="adminBreadcrumb"></h3>

            <div class="uk-width-medium-8-10 uk-container-center">
                <div class="uk-grid uk-grid-collapse" data-uk-grid-margin>
                    @include('views.errors.errors')
                    <div class="uk-width-large-10-10">
                        <div class="md-card md-card-single">
                            <div class="md-card-toolbar">
                                <div class="md-card-toolbar-actions hidden-print">
                                    <span>تاریخ ثبت : {{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                </div>
                                <h3 class="md-card-toolbar-heading-text large">
                                    <span class="uk-text-muted">عنوان : {{ $item->subject }}</span>
                                </h3>
                            </div>
                            <div class="md-card-content padding-reset">
                                <div class="chat_box_wrapper">
                                    <div class="chat_box touchscroll chat_box_colors_a" id="chat">
                                        @foreach($item->answers as $answer)
                                            <div class="chat_message_wrapper @if($answer->user->hasAnyRole()) chat_message_right @endif">
                                                <div class="chat_user_avatar">
                                                    <img class="md-user-image" src="@if(is_null($answer->user->picture)) {{ asset('assets/site/img/user.png') }} @else {{ asset('uploads/avatar/'.$answer->user->picture) }} @endif"/>
                                                </div>
                                                <ul class="chat_message">
                                                    <li>
                                                        <p style="white-space: pre-line">
                                                            {{ $answer->text }}
                                                        </p>
                                                        <br>
                                                        <span class="chat_message_time">{{ $answer->user->name.' '.$answer->user->family }}</span>
                                                        <span class="chat_message_time">{{ jdate('H:i - Y/m/d',$answer->created_at->timestamp) }}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="md-card">
                            <div class="md-card-content">

                                {!! Form::open(['action'=> 'Ticket\Controllers\Admin\TicketController@answer']) !!}
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        {{ Form::label('answer','متن پاسخ') }}
                                        {{ Form::textarea('answer',null,['class' => "md-input",'rows' => 5]) }}
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1" style="text-align: center">
                                        <button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light " style="margin-top: 10px">ارسال</button>
                                    </div>
                                </div>

                                {{ Form::hidden('ticket_id',$item->id) }}

                                {!! Form::close() !!}

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
