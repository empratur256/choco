<?php

namespace Sedehi\Http\Controllers\Brand\Controllers\Admin;

use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Brand\Models\Brand;
use Sedehi\Http\Controllers\Brand\Requests\Admin\BrandRequest;
use File;
use Image;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    protected $uploadPath = 'uploads/brand/';

    public $imageSize = [
        [
            'width'  => 520,
            'height' => 360,
        ],
        [
            'width'  => 128,
            'height' => 86,
        ],
    ];

    public function index()
    {
        $items = Brand::filter()->latest()->paginate(20);

        return view('Brand.views.admin.brand.index', compact('items'));
    }

    public function create()
    {
        return view('Brand.views.admin.brand.add');
    }

    public function store(BrandRequest $request)
    {
        $item           = new Brand();
        $item->title_fa = $request->get('title_fa');
        $item->title_en = $request->get('title_en');
        if($request->hasFile('photo')){
            if(!File::isDirectory(public_path($this->uploadPath))){
                File::makeDirectory(public_path($this->uploadPath), 0775, true);
            }
            $file            = $request->file('photo');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().'photo'.$file->hashName();
            Image::make($realPath)->widen(1920, function($constraint){
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);
            $this->createImages($realPath, $fileName);
            $item->photo = $fileName;
        }
        $item->save();

        return redirect()->action('Brand\Controllers\Admin\BrandController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Brand::findOrFail($id);

        return view('Brand.views.admin.brand.edit', compact('item'));
    }

    public function update(BrandRequest $request, $id)
    {
        $item           = Brand::findOrFail($id);
        $item->title_fa = $request->get('title_fa');
        $item->title_en = $request->get('title_en');
        if($request->hasFile('photo')){
            if(!File::isDirectory(public_path($this->uploadPath))){
                File::makeDirectory(public_path($this->uploadPath), 0775, true);
            }
            $file            = $request->file('photo');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().'photo'.$file->hashName();
            Image::make($realPath)->widen(1920, function($constraint){
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);
            $this->createImages($realPath, $fileName);
            $item->photo = $fileName;
        }
        $item->save();

        return redirect()->action('Brand\Controllers\Admin\BrandController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(BrandRequest $request)
    {
        $items = Brand::whereIn('id', $request->get('deleteId'))->get();
        foreach($items as $item){
            removeImage($this->uploadPath, $item->photo);
        }

        Brand::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
