<?php

namespace Sedehi\Http\Controllers\Brand\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Libs\LanguageTrait;

class Brand extends Model
{

    use  Filterable, SoftDeletes, LanguageTrait;

    protected $table      = 'brand';
    public    $timestamps = true;

    protected $filterable = [
        'title_fa'   => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];


    public function getTitleAttribute()
    {
        return $this['title_'.$this->lang()];
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
