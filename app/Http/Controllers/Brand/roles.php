<?php
return [
    'brand' => [
        'title'  => 'تولید کننده',
        'access' => [
            'BrandController' => [
                'لیست'   => 'index',
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
