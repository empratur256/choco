<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title_fa','عنوان فارسی') !!}
        {!! Form::text('title_fa',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title_en','عنوان انگلیسی') !!}
        {!! Form::text('title_en',null,['class'=>'md-input']) !!}
    </div>

    <div class="uk-width-medium-2-3">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a uk-margin-small-bottom">لوگوی برند</h3>
                {!! Form::file('photo',['class' => 'dropify','data-default-file' => (isset($item)? (!is_null($item->photo))? asset('uploads/brand/'.$item->photo) :'' : '')]) !!}
            </div>
        </div>
    </div>
</div>
