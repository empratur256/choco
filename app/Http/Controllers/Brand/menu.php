<?php
return [
    'brand' => [
        'order'   => 40,
        'title'   => 'برند ها',
        'icon'    => 'fa fa-square',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'Brand\Controllers\Admin\BrandController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'Brand\Controllers\Admin\BrandController@create',
                'parameters' => [],
            ],
        ],
    ],
];
