<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Brand\Controllers\Admin',
             ], function(){
    Route::resource('brand', 'BrandController', ['except' => ['show']]);
});
