<?php

namespace Sedehi\Http\Controllers\Brand\database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Sedehi\Http\Controllers\Brand\Models\Brand;

class BrandTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $datas = [
            'برلیان',
            'ساوین',
            'شهاب فرش',
            'پاتریس',
            'زمرد مشهد',
        ];
        foreach ($datas as $data) {
            $item           = new Brand();
            $item->title_fa = $data;
            $item->title_en = '';
            $item->save();
        }
    }

}
