<?php
use Faker\Factory as Faker;

$factory->define(\Sedehi\Http\Controllers\Brand\Models\Brand::class, function($faker){
    $faker = Faker::create('fa_IR');

    return [
        'title' => $faker->name,
    ];
});
