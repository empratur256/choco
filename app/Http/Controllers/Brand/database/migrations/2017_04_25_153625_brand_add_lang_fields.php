<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BrandAddLangFields extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('brand', function(Blueprint $table){
            $table->renameColumn('title', 'title_fa');
            $table->string('title_en')->index();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('brand', function(Blueprint $table){
            $table->dropColumn([
                                   'title_fa',
                                   'title_en',
                               ]);
        });
    }
}
