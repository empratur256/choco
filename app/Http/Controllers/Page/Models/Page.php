<?php

namespace Sedehi\Http\Controllers\Page\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class Page extends Model
{
    use  Filterable;

    protected $table = 'page';
    public    $timestamps = true;

    protected $filterable = [
        'title'       => [
            'operator' => 'Like',
        ]
    ];
}
