<?php

return [
    'page' => [
        'title'   => 'صفحات',
        'icon'    => 'fa fa-file-text',
        'badge'   => function () {

        },
        'submenu' => [
            'Index' => [
                'title'  => 'لیست صفحات',
                'action' => 'Page\Controllers\Admin\PageController@index',
                'parameters' => []

            ],
            'Add'   => [
                'title'  => 'ایجاد صفحه جدید',
                'action' => 'Page\Controllers\Admin\PageController@create',
                'parameters' => []

            ],
        ]
    ],
];
