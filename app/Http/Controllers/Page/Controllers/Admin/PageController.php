<?php

namespace Sedehi\Http\Controllers\Page\Controllers\Admin;

use File;
use Image;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Page\Models\Page;
use Sedehi\Http\Controllers\Page\Requests\Admin\PageRequest;

class PageController extends Controller
{
    protected $uploadPath = 'uploads/page/';

    protected $protectedPageIds = [1,2,3,4];

    public function index()
    {
        $items = Page::filter()->latest()->paginate(20);

        $protectedPageIds = $this->protectedPageIds;

        return view('Page.views..admin.page.index', compact('items','protectedPageIds'));
    }

    public function create()
    {
        return view('Page.views.admin.page.add');
    }

    public function store(PageRequest $request)
    {
        $item                   = new Page();
        $item->title            = $request->get('title');
        $item->text             = $request->get('text');
        $item->show_in_footer   = $request->has('show_in_footer');
        $item->save();

        return redirect()->action('Page\Controllers\Admin\PageController@index')->with('success', 'اطلاعات با موفقیت ذخیره شد');

    }

    public function edit($id)
    {
        $item = Page::findOrFail($id);
        return view('Page.views.admin.page.edit', compact('item'));
    }

    public function update(PageRequest $request, $id)
    {
        $item                   = Page::findOrFail($id);
        $item->title            = $request->get('title');
        $item->text             = $request->get('text');
        $item->show_in_footer   = $request->has('show_in_footer');
        $item->save();

        return redirect()->action('Page\Controllers\Admin\PageController@index')->with('success', 'اطلاعات با موفقیت ذخیره شد');

    }

    public function destroy(PageRequest $request)
    {
        if (count(array_intersect($request->get('deleteId'),$this->protectedPageIds))) {
            return back()->with('error','صفحات انتخاب شده قابل حذف نمی باشند');
        }
        Page::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
