<?php

namespace Sedehi\Http\Controllers\Page\Controllers\Site;

use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Page\Models\Page;

class PageController extends Controller
{

    public function show($id)
    {
        $page = Page::findOrFail($id);

        return view('Page.views.site.show', compact('page'));
    }
}
