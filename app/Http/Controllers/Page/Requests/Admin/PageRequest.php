<?php

namespace Sedehi\Http\Controllers\Page\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Page\Models\Page;

class PageRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
        {
           $action = explode('@', $this->route()->getActionName());
           $action = end($action);
           switch ($action) {
               case 'destroy':
                   return [
                       'deleteId'   => 'required|array',
                   ];
                   break;
               case 'store':
                   return [
                       'title'      => 'required',
                       'text'       => 'required',
                       'picture'    => 'image|max:10240',
                       'video'      => 'file|mimes:mp4|max:51200',
                   ];
                   break;

               case 'update':
                   return [
                       'title'      => 'required',
                       'text'       => 'required',
                       'picture'    => 'image|max:10240',
                       'video'      => 'file|mimes:mp4|max:51200',
                   ];
                   break;
           }

           return [];
        }

}
