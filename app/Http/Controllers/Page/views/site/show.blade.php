@extends('views.layouts.site.master')
@section('title',$page->title)
@section('content')
    @include('views.errors.errors')
<div class="red_box">
    <div class="container">
        <span>{{$page->title}}</span>

    </div>
</div>
<div class="about-we">
  <div class="container">
    <div class="row">
      
                <div style="padding-top: 1em;padding-bottom: 1em">
                    <span> @lang('site.home') </span>
                    @if(App::isLocale('en'))
                        <span> < </span>
                    @else
                        <span> &gt; </span>
                    @endif
                    <span>{{$page->title}} </span>
                </div>
      
       <div class="col-lg-12 content">
          <h5> {{$page->title}} </h5>
         {!! $page->text !!}
       </div>
    </div>
  </div>
</div>
@endsection
@push('css')
   {{ Html::style('assets/site/css/about-we.css') }}
@endpush
@push('js')

@endpush