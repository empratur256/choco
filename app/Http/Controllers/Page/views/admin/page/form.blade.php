<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان صفحه') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::checkbox('show_in_footer',1,isset($item) ? $item->show_in_footer : false,['data-switchery']) !!}
        <span class="text">نمایش در پایین سایت</span>
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('text','متن') !!}
        <br>
        {!! Form::textarea('text',null,['class'=>'md-input ckeditor']) !!}
    </div>
</div>
