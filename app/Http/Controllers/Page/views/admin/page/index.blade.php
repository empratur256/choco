@extends('views.layouts.admin.master')
@section('content')
    @include('Page.views.admin.page.search')
    {!! Form::open(['action'=>['Page\Controllers\Admin\PageController@destroy',1],'method' =>'DELETE']) !!}

    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('Page.PageController.destroy'))
                                <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید ؟');"  class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">حذف</button>
                            @endif
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            @if(permission('Page.PageController.create'))
                                <a class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" href="{!! action('Page\Controllers\Admin\PageController@create') !!}" style="margin-left: 15px !important;">ایجاد صفحه جدید</a>
                            @endif
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Page\Controllers\Admin\PageController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        <table class="uk-table uk-table-nowrap table_check">
                            <thead>
                            <tr>
                                <th class="uk-width-1-10  uk-text-center small_col">
                                    {!! Form::checkbox('',1,null,['class' => 'check_all','data-md-icheck']) !!}
                                </th>
                                <th class="uk-width-2-10 uk-text-center">نام</th>
                                <th class="uk-width-2-10 uk-text-center">نمایش در پایین سایت</th>
                                <th class="uk-width-2-10 uk-text-center">تاریخ درج</th>
                                <th class="uk-width-2-10 uk-text-center">.....</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr class="uk-text-center">
                                    <td class="uk-text-center uk-table-middle small_col">
                                        @if(!in_array($item->id,$protectedPageIds))
                                            {!! Form::checkbox('deleteId[]',$item->id,null,['class' => 'check_row','data-md-icheck']) !!}
                                        @endif
                                    </td>
                                    <td>{{ $item->title }}</td>
                                    <td>
                                        @if($item->show_in_footer)
                                            <span>بلی</span>
                                        @else
                                            <span>خیر</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                    </td>
                                    <td class="uk-text-center">
                                        @if(permission('Page.PageController.edit'))
                                            <a href="{!! action('Page\Controllers\Admin\PageController@edit',$item->id) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                                        @endif

                                        <a href="#" data-uk-modal="{target:'#link-modal-{{ $item->id }}'}" id="page-link-btn-{{ $item->id }}" data-uk-tooltip title="نمایش لینک">
                                            <i class="md-icon material-icons">link</i>
                                        </a>

                                        <div class="uk-modal" id="link-modal-{{ $item->id }}">
                                            <div class="uk-modal-dialog">
                                                <button type="button" class="uk-modal-close uk-close"></button>
                                                {{ Form::label('link','لینک صفحه') }}
                                                <textarea class="md-input" style="direction: ltr" rows="5">{{ action('Page\Controllers\Site\PageController@show',[$item->id,utf8_slug($item->title)]) }}</textarea>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection