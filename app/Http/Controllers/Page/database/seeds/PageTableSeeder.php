<?php

namespace Sedehi\Http\Controllers\Page\database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Sedehi\Http\Controllers\Page\Models\Page;


class PageTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $text = 'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.';

        $page = new Page();
        $page->title = 'درباره ما';
        $page->text = $text;
        $page->show_in_footer = 1;
        $page->save();

        $page = new Page();
        $page->title = 'حریم خصوصی';
        $page->text = $text;
        $page->show_in_footer = 1;
        $page->save();

        $page = new Page();
        $page->title = 'قوانین';
        $page->text = $text;
        $page->show_in_footer = 1;
        $page->save();

        $page = new Page();
        $page->title = 'مرام نامه';
        $page->text = $text;
        $page->show_in_footer = 1;
        $page->save();

        $page = new Page();
        $page->title = 'رضایت نامه کاربران';
        $page->text = $text;
        $page->show_in_footer = 1;
        $page->save();

        $page = new Page();
        $page->title = 'نحوه کار ما را ببینید';
        $page->text = $text;
        $page->save();

        $page = new Page();
        $page->title = 'قوانین قرارداد';
        $page->text = $text;
        $page->show_in_footer = 0;
        $page->save();

        $page = new Page();
        $page->title = 'دریافتی سایت از پیمانکار';
        $page->text = $text;
        $page->show_in_footer = 0;
        $page->save();
    }

}
