<?php

return [
    'page' => [
        'title'  => 'صفحات',
        'access' => [
            'PageController' => [
                'لیست'   => 'index',
                'ایجاد'  => ['create', 'store'],
                'ویرایش' => ['edit', 'update'],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
