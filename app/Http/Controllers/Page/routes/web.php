<?php
Route::group(['prefix'     => config('site.admin'),
    'middleware' => ['admin'],
    'namespace'  => 'Page\Controllers\Admin',
], function(){
    Route::resource('page', 'PageController', ['except' => ['show']]);
});
Route::group(['namespace' => 'Page\Controllers\Site'], function(){
    Route::get('page/{id}/{title?}', 'PageController@show');
});