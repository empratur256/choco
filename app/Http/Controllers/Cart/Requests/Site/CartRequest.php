<?php

namespace Sedehi\Http\Controllers\Cart\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Sedehi\Http\Controllers\Cart\Models\Cart;

class CartRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'shipping':
                return [
                    'id' => 'required',
                ];
            break;
            case 'remove':
                return [
                    'id' => 'required',
                ];
            break;
            case 'add':
                return [
                    'quantity' => 'required|numeric',
                ];
            break;
            case 'update':
                return [
                    'id'       => 'required',
                    'quantity' => 'required|numeric',
                ];
            break;
            case 'storeShipping':
                return [
                    'ship_address'    => 'required|numeric',
                    'ship'            => 'required|numeric',
                    'invoice_request' => 'required',
                ];
            break;
            case 'coupon':
                return [
                    'coupon' => [
                        'required',
                    ],
                ];
            break;
            case 'submit':
                return [
                    'payment_type' => 'required|in:cash,online,card,account',
                ];
            break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'id.required' => trans('site.data_not_found'),
        ];
    }

}
