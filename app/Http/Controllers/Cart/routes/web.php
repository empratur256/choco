<?php
Route::get('cart', 'Cart\Controllers\Site\CartController@index');
Route::delete('cart/remove', 'Cart\Controllers\Site\CartController@remove');
Route::delete('cart/removeall', 'Cart\Controllers\Site\CartController@removeall');
Route::post('cart/update', 'Cart\Controllers\Site\CartController@update');
Route::post('cart/add/{id}', 'Cart\Controllers\Site\CartController@add');
Route::get('cart/get/{id}', 'Cart\Controllers\Site\CartController@get');
Route::get('cart/user', 'Cart\Controllers\Site\CartController@user');
Route::post('cart/shipping', 'Cart\Controllers\Site\CartController@storeShipping');
Route::get('cart/shipping', 'Cart\Controllers\Site\CartController@shipping');
Route::get('cart/payment', 'Cart\Controllers\Site\CartController@payment');
Route::post('cart/payment', 'Cart\Controllers\Site\CartController@submit');
Route::post('cart/coupon', 'Cart\Controllers\Site\CartController@coupon');
Route::get('cart/review', 'Cart\Controllers\Site\CartController@review');
Route::get('a', function(){
    foreach(Cart::content() as $cart){
        dd($cart);
    }
});