@extends('views.layouts.site.master')
@section('title',trans('site.carts.your'))
@section('content')

    <div class="red_box">
        <div class="container">
            <h3 class="title-page">@lang('site.carts.bag')</h3>
            <div class="divider-title light"></div>
            <p> @lang('site.carts.top_text') </p>
        </div>
    </div>
    <div class="shopping-bag" style="background-color:#f1f1f1">
        <div class="container">
            <div class="text-center">  @include('views.errors.errors')</div>
            <div class="row" style="background-color: white">
                <div class="col-sm-12" style="overflow-x:auto;">
                    <h3 style="padding-top: 2em;color:#727272;font-weight: bolder"> @lang('site.carts.your') </h3>

                    <table class="table table-hover">
                        <thead style="color:#bbbbbb">
                        <tr>
                            <th class="text-right">@lang('site.carts.product_description')</th>
                            <th class="text-center">@lang('site.carts.unit_price')</th>
                            <th class="text-center">@lang('site.quantity')</th>
                            <th class="text-center">@lang('site.carts.total_price')</th>

                        </tr>
                        </thead>
                        <tbody>
                        @php $ids = null @endphp
                        @php $totalPrice = 0 @endphp
                        @forelse(Cart::content() as $cart)


                            @php $allFramesPrices = 0  @endphp
                            @php $ids[$loop->index] = $cart->rowId   @endphp
                            <tr class="product">
                                <td class="col-lg-8 col-sm-8 col-md-8">
                                    {!! Form::open(['action' => 'Cart\Controllers\Site\CartController@remove','method' => 'DELETE']) !!}
                                    {!! Form::hidden('id',$cart->rowId) !!}
                                    <label class="pull-right remove-btn"><img src="{{ asset('assets/site/img/remove-product.png') }}">
                                        <button hidden type="submit" class=" remove" onclick="return confirm('@lang('site.remove')');"></button>
                                    </label>
                                    {!! Form::close() !!}
                                    <a style="padding-left:1em" class="pull-right deleteProduct" href="#">
                                        <img src="{{ asset('uploads/product/'.$cart->id.'/111x111-'.$cart->options->picture) }}">
                                    </a>
                                    <p class="pull-right comment-buy" style="color:#666666">{{$cart->name}}</p>
                                    <br>
                                    <span class="pull-right text-enter" style="color:#9f9f9f;padding-top: 1em;">
                                        {{$cart->content}}
                                    </span>
                                </td>

                                <td class="col-sm-1 col-md-1" style="text-align: center;padding-top: 20px">
                                    <p class="text-center"> {{number_format($cart->price)}} @lang('site.currency')</p>
                                </td>
                                <td class="count-product col-sm-2 col-md-2 text-center">

                                    {{ Form::open(['action' => 'Cart\Controllers\Site\CartController@update','class'=>'update-form']) }}
                                    {!! Form::hidden('id',$cart->rowId) !!}
                                    {!! Form::hidden('product',$cart->id) !!}
                                    <button class="inc-count qtyplus qty" field='quantity'>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    {!! Form::text('quantity', $cart->qty, ['class' => 'int counter']) !!}
                                    <button class="dec-count qtyminus qty" field='quantity'>
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    {{ Form::close() }}


                                </td>

                                <td class="col-lg-1 col-sm-1 col-md-1 text-center" style="padding-top: 20px">
                                    <p> {{ number_format($cart->subtotal) }} @lang('site.currency')</p>
                                </td>

                            </tr>
                            @php $totalPrice += $cart->subtotal  @endphp
                        @empty
                            <tr>

                                <td style="text-align: center" colspan="5">@lang('site.carts.is_empty')</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-12">


                    <div style="padding-bottom: 20px; border-bottom:1px solid #ddd;">


                        <div class="pull-left">
                            {!! Form::open(['action' => 'Cart\Controllers\Site\CartController@removeall','method' => 'DELETE']) !!}

                            <button type="submit" class="btn btn-danger">@lang('site.carts.removeـall')</button>

                            {!! Form::close() !!}


                        </div>

                        <a href="{{action('HomeController@index')}}" type="submit"
                           class="btn continue">@lang('site.carts.continue_shopping')</a>

                    </div>


                    <div class="col-lg-6 col-md-7 col-sm-8 col-xs-12" style="padding:1em 0 0 0">

                        <div class="col-lg-8 col-md-8 col-sm-8 total-buy">
                            <span class="pull-right"> @lang('site.carts.total') </span>
                            <span class="pull-left">{{number_format($totalPrice)}} @lang('site.currency')</span>
                        </div>
                        @php $taxPrice = 0 @endphp
                        @if($setting->tax > 0 && $totalPrice > 0)
                            @php $taxPrice = ($totalPrice * $setting->tax / 100) @endphp
                        @endif
                        @if($setting->tax > 0)
                            <div class="col-lg-8 col-md-8 col-sm-8 total-buy">
                                <span class="pull-right"> @lang('site.carts.tax') </span>
                                <span class="pull-left">{{number_format($taxPrice)}} @lang('site.currency')</span>
                            </div>
                        @endif
                        <div class="col-lg-8 col-md-8 col-sm-8 total-pay">
                            <span class="pull-right"> @lang('site.carts.total_to_pay') </span>
                            <span class="pull-left">{{ number_format($taxPrice + $totalPrice) }} @lang('site.currency')</span>
                        </div>

                    </div>

                    <div class="col-lg-6 col-md-5 col-sm-4 col-xs-12" style="padding:2em 8px 0 0;">
                        <p>@lang('site.carts.add_shipping_price')
                        </p>
                    </div>

                    <div class="col-xs-12 send-manner" style="padding-bottom: 2em;padding-top: 2em">
                        @if(count(Cart::content()))

                            {{ Form::open(['action' => 'Cart\Controllers\Site\CartController@user','method' => 'GET']) }}

                            <button type="submit" class="btn btn-danger pull-left"> @lang('site.carts.to_shipping') </button>

                            {{ Form::close() }}
                        @endif


                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection
@push('css')
    {{ Html::style('assets/site/css/shopping-bag.css') }}
@endpush
@push('js')
    {{ Html::script('assets/site/js/shopping-bag.js') }}
    <script>
        $(document).ready(function () {
            $('.counter').on('change', function (e) {
                if ($(this).val() < 0) {
                    $(this).val(0);
                }
            });


            $('.inc-count').on('click', function (e) {
                e.preventDefault();
                var value = parseInt($(this).next().val());
                $(this).next().val(value);
            });


            $('.dec-count').on('click', function (e) {
                e.preventDefault();
                var value = parseInt($(this).prev().val());
                if (value <= 0) {
                    return;
                }
                $(this).prev().val(value - 1);
            });

            $('.update-form').each(function () {
                $('.qty').on('click', function () {
                    $(this).parent('.update-form').submit()
                });
            })
        })

    </script>
@endpush