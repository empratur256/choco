@extends('views.layouts.site.master')
@section('title',trans('site.cart'))

@section('content')

    <div class="red_box">
        <div class="container">
            <h3 class="title-page"> @lang('site.carts.preview')  </h3>
            <div class="divider-title light"></div>
            <p>@lang('site.carts.preview_top_text')</p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container bread-crumbs">

                <span> @lang('site.home') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i> </span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.carts.preview') </span>
            </div>
        </div>
        <div class="buy-status">
            <div class="container">
                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                    <div class="row">
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/info-pay.png')}}">
                                    <p class="">@lang('site.carts.payment_info')</p>
                                </div>
                                <div class="col-xs-2 padding-top-2 padding-lr-0">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/bag-circle.png')}}">
                                    <p class="">@lang('site.carts.preview')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                            <img class="hidden-xs" src="{{ asset('assets/site/img/green-circle.png') }}">
                            <img class="hidden-xs" src="{{ asset('assets/site/img/green-circle.png') }}">
                            <img class="hidden-xs" src="{{ asset('assets/site/img/green-circle.png') }}">
                        </div>
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/ok-circle.png') }}">
                                    <p class="green_text">@lang('site.carts.shipping_info')</p>
                                </div>
                                <div class="col-xs-2  padding-top-2 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/ok-circle.png') }}">
                                    <p class="green_text">@lang('site.carts.login_to_shop')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container text-center">  @include('views.errors.errors')</div>
        <div id="form" class="container">
            <div class="row" style="">
                <div class="col-sm-12" style="padding-bottom: 4em;">
                    <table class="table table-hover" style="border-bottom: 1px solid #ebebeb">
                        <thead style="color:#bbbbbb">
                        <tr>
                            <th class="text-right"><h5>@lang('site.carts.product_description')</h5></th>
                            <th class="text-center"><h5>@lang('site.carts.unit_price')</h5></th>
                            <th class="text-center"><h5>@lang('site.quantity')</h5></th>
                            <th class="text-center"><h5>@lang('site.carts.total_price')</h5></th>

                        </tr>
                        </thead>
                        <tbody>
                        @php $totalPrice = 0 @endphp
                        @foreach(Cart::content() as $cart)
                            @php $framePrice = 0 @endphp
                            @php $allFramesPrices = 0  @endphp
                            <tr class="product">
                                <td class="col-lg-4 col-sm-4 col-md-4">

                                    <a class="pull-right" style="padding-left:1em" class="" href="#">
                                        <img src="{{ asset('uploads/product/'.$cart->id.'/111x111-'.$cart->options->picture) }}">
                                    </a>
                                    <p class="pull-right comment-buy head-text"
                                       style="color:#666666">{{$cart->name}}</p>
                                    <br>
                                    <span class="pull-right"
                                          style="color:#9f9f9f;padding-top: 1em">{{$cart->content}}</span>
                                </td>

                                <td class="col-sm-2 col-md-2 head-text" style="text-align: center;padding-top: 20px">
                                    <p class="text-center"> {{number_format($cart->price)}} @lang('site.currency')</p>
                                </td>
                                <td class="count-product col-sm-2 col-md-2 text-center head-text"
                                    style="text-align: center;padding-top: 20px">
                                    <p><span class="text-center">{{$cart->qty}}</span> <span>@lang('site.number')</span></p>
                                </td>

                                <td class="col-lg-2 col-sm-2 col-md-2 head-text" style="padding-top: 20px">
                                    <p class="text-center"> {{ number_format($cart->subtotal + $framePrice) }} @lang('site.currency')</p>
                                </td>
                            </tr>
                            @php $totalPrice += $cart->subtotal @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6 col-xs-12 col-sm-push-6">
                    <div class="row factor">
                        <div class="col-xs-12" style="">
                            <div style="padding-bottom:1em; border-bottom:1px solid #ebebeb">
                                <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                                <span class="bolder">@lang('site.carts.your_billing_summary')</span>
                            </div>
                        </div>
                        <div class="col-xs-12" style="padding-top: 2em; padding-bottom: 2em">
                            <div style="float: right;border:1px solid #ebebeb ">
                                <div class="col-xs-12 factor-items "
                                     style="padding-bottom: 1em; padding-top:1em;border-bottom: 1px solid #ebebeb ">
                                    <span class="pull-right">@lang('site.carts.total')</span>
                                    <span class="pull-left">{{number_format($totalPrice)}} @lang('site.currency')</span>
                                </div>
                                <div class="col-xs-12 factor-items"
                                     style="padding-bottom: 1em; padding-top:1em;border-bottom: 1px solid #ebebeb ">
                                    <span class="pull-right">@lang('site.carts.shipping_packing')</span>
                                    @if($shipping->price > 0)
                                        <span class="pull-left">{{ number_format($shipping->price) }} @lang('site.currency')</span>
                                    @else
                                        @lang('site.free')
                                    @endif
                                </div>
                                @php $taxPrice = 0 @endphp
                                @php $taxPrice = ($totalPrice * $setting->tax / 100) @endphp
                                @if($setting->tax > 0)
                                    <div class="col-xs-12 factor-items"
                                         style="padding-bottom: 1em; padding-top:1em;border-bottom: 1px solid #ebebeb ">
                                        <span class="pull-right">@lang('site.carts.tax')</span>
                                        <span class="pull-left">{{number_format($taxPrice)}} @lang('site.currency')</span>
                                    </div>
                                @endif
                                <div class="col-xs-12 text-success factor-items" style="padding-bottom: 1em; padding-top:1em">
                                    <span class="pull-right">@lang('site.carts.total_to_pay')</span>
                                    <span class="pull-left">{{ number_format($taxPrice + $totalPrice + $shipping->price) }} @lang('site.currency')</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-sm-pull-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <div style="padding-bottom:1em; border-bottom:1px solid #ebebeb">
                                <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                                <span class="bolder">@lang('site.carts.shipping_info')</span>
                            </div>
                        </div>
                        <div class="col-xs-12" style="padding-top: 2em">
                            <div style="border:1px solid #ebebeb; padding:15px">
                                <p style="padding-bottom: 0.5em">
                                    @lang('site.carts.order_to',
                                    ['tel' => '<span class="red_text">'.$address->mobile.' - '.$address->tel.'</span>',
                                    'name' => '<span class="green_text">'.$address->name_family.'</span>',
                                    'address' => '<span class="red_text">'.$address->province->name.' - '.$address->city->name.' - '.$address->address.'</span>'
                                    ])
                                </p>
                                <p>
                                    @lang('site.carts.shipping_preview' ,
                                     [
                                     'price' => ($shipping->price > 0) ?
                                     ('<span  class="red_text">' . number_format($shipping->price) . '</span>') :
                                     ('<span  class="red_text">' . trans('site.carts.free') . '</span>') ,
                                     'currency' =>'<span  class="red_text">' . trans('site.currency') . '</span>' ,
                                     'name' => '<span class="red_text">'.$shipping->title.'</span>'
                                     ])
                                </p>

                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 button">
                    <a href="{!! action('Cart\Controllers\Site\CartController@shipping') !!}" class="btn submit pull-right">
                        @lang('site.carts.back_to_shipping')</a>
                    <a href="{!! action('Cart\Controllers\Site\CartController@payment') !!}" class="btn submit pull-left">
                        @lang('site.carts.to_payment')

                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/checkout-step.css') }}
    {{ Html::style('assets/site/css/checkout-step3.css') }}
@endpush

@push('js')
@endpush