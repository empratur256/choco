<div id="form" class="container">
    <div class="row">
        <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">
            <div style="border-bottom :1px solid #ebebeb;padding-bottom: 1em">
                <img src="{{asset('assets/site/img/left-side.png')}}"><span
                        style="font-weight: bolder;"> @lang('site.carts.choose_address')</span>


                <a data-toggle="modal" type="button" data-target="#addressModal" class="pull-left" href="#">
                    <i class="flaticon-plus text-success"></i><span
                            class="text-success"> @lang('site.carts.add_address') </span> </a>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="row">
                @forelse($addresses as $address)
                    <div class="col-sm-4 col-xs-12 pull-right">
                        <div class=" radio choose-address ">
                            <div class="row">
                                <div class="col-xs-9 pull-right">
                                    <div>
                                        <label>
                                            {!! Form::radio('ship_address',$address->id,((session('ship_address') == $address->id) || (session('customOrderData.ship_address') == $address->id))? true : null,['style'=>'float: right;']) !!}

                                            <span class="cr" style="float: initial;margin-left: 1em;"><i
                                                        class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                                            <p class="red_text bolder padding-bottom-05">{{$address->name_family}}
                                            </p>
                                            <p class="defult-text padding-bottom-02" data-toggle="tooltip"
                                               @php  $stradd = ($address->province->name.' - '.$address->city->name.' - '.$address->address) @endphp
                                               title="{{$stradd}}">{{str_limit($stradd,33)}}</p>
                                            <p class="defult-text padding-bottom-02">@lang('site.postal_code')
                                                : {{ $address->postal_code}}</p>
                                            <p class="defult-text">@lang('site.mobile') : {{$address->mobile}}</p>

                                        </label>
                                    </div>
                                </div>

                                <div class="btn-group-vertical col-xs-3 pull-left" role="group"
                                     style="margin: 0 auto 0 auto;">
                                    {!! Form::hidden('deleteId',$address->id) !!}
                                    <button class="btn btn-sm btn-danger deleteForm" type="submit"
                                            data-id="{{$address->id}}">@lang('site.delete')</button>
                                    <a class="btn btn-sm btn-success"
                                       href="{!! url(request()->route()->getPath()).'?address_id='.$address->id !!}">
                                        <span>@lang('site.edit')</span>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                @empty
                    <div class="col-xs-12 pull-right" style="text-align: center;">
                        <div class="checkbox-info col-xs-12">
                            <div class="address">@lang('site.carts.address_empty')</div>
                        </div>
                    </div>
                @endforelse
            </div>
        </div>

        <div class="col-xs-12 text-right padding-top-2 padding-bottom-1 shipings-container">
            <div style="border-bottom :1px solid #ebebeb;padding-bottom: 1em">
                <img src="{{asset('assets/site/img/left-side.png')}}"><span
                        class="bolder"> @lang('site.carts.shipping_type') </span>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="shipings">


                </div>
            </div>
        </div>

        <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">

            <div class="factor">

                <span class="pull-right"><i class="fa fa-angle-double-left"
                                            aria-hidden="true"></i>@lang('site.carts.invoice_request')</span>
                <div class="radio2-res pull-right">
                    <div class="radio2 pull-right" style="text-align: -webkit-right;">
                        <label>
                            {{ Form::radio('invoice_request', 1,((session('invoice_request') == 1) || (session('customOrderData.invoice_request') == 1)) ? true : null, ['id' => 'factor-yes','style'=>'float: right;']) }}

                            <span class="cr" style="float: initial;margin-left: 1em;"><i
                                        class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <span style="position: relative;top: -5px;"> @lang('site.yes') </span>
                        </label>
                    </div>
                    <div class="radio2 pull-right" style="text-align: -webkit-right;">
                        <label>
                            {{ Form::radio('invoice_request', 0,((session('invoice_request') == 0) || (session('customOrderData.invoice_request') == 0))? true : null, ['id' => 'factor-no','style'=>'float: right;']) }}

                            <span class="cr" style="float: initial;margin-left: 1em;"><i
                                        class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <span style="position: relative;top: -5px;"> @lang('site.no') </span>
                        </label>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-xs-12 button">
            @if(request()->is('custom/order/*'))
                <a href="{!! action('CustomOrder\Controllers\Site\OrderController@preview') !!}"
                   class="btn submit pull-right">
                    @lang('site.custom_order.previous_button') </a>
                <button type="submit"
                        class="btn submit pull-left">
                    @lang('site.custom_order.next_button')
                </button>
            @else
                <a href="{!! action('Cart\Controllers\Site\CartController@index') !!}"
                   class="btn submit pull-right">
                    <span class="fa fa-angle-right"> </span> @lang('site.carts.back_to_cat') </a>
                <button type="submit"
                        class="btn submit pull-left submitForm">
                    @lang('site.carts.submit_next')
                </button>
            @endif
        </div>
    </div>
</div>
@push('js')
    <script>
        var token = $('meta[name="csrf-token"]').attr('content');

        $('input[name="ship_address"]').on('change', function (e) {
            getShippings($(this).val());
        });

        @if(session()->has('ship_address') && session()->has('ship'))
        getShippings('{{session('ship_address')}}', '{{session('ship')}}');
        @endif

        @if(count(old()))
        getShippings('{{old('ship_address')}}', '{{old('ship')}}');

        @endif

        function getShippings(address, old) {
            if (address > 0) {
                $.ajax({
                    type: "POST",
                    url: "{{ action('Transport\Controllers\Site\ShippingController@byAddress') }}",
                    data: {
                        _token: token,
                        address: address
                    },
                    dataType: "json",
                    cache: false
                }).fail(function (jqXHR, textStatus, errorThrown) {

                    alert('@lang('site.carts.ajax_error')');

                }).done(function (response) {
                    $('.shipings').html('');
                    $('.shipings-container').slideDown();
                    $.each(response, function (key, val) {
                        var checked = '';

                        if (old == val.id) {
                            checked = 'checked="checked"';
                        }

                        $('.shipings').append(
                            '<div class="col-sm-4 col-xs-12 pull-right">' +
                            '<div class="radio payment-method">' +
                            '<label>' +
                            '<input name="ship" style="float: right;" type="radio" value="' + val.id + '"' + checked + '>' +
                            '<span class="cr" style="float: initial;margin-left: 1em;">' +
                            '<i class="cr-icon glyphicon glyphicon-ok-sign"></i>' +
                            '</span>' +
                            '<p class="red_text bolder padding-bottom-05">' + val.title + '</p>' +
                            '<p class="defult-text">' + val.price + '</p>' +
                            '<br>' +
                            '<p class="defult-text" data-toggle="tooltip"  title="' + val.description + '" style="padding-left: 18px;">' + val.description.substring(0, 37) + (val.description.length > 37 ? '...' : '') + '</p>' +
                            '</label>' +
                            '</div>' +
                            '</div>'
                        );
                    });
                });
            }

        }

    </script>
@endpush
@push('css')
    <style>
        .shipings-container {
            display: none;
        }
    </style>
@endpush
