@extends('views.layouts.site.master')
@section('title',trans('site.login'))

@section('content')
    <div class="red_box">
        <div class="container">
            <h3 class="title-page"> @lang('site.cart') </h3>
            <div class="divider-title light"></div>
            <p> @lang('site.carts.shipping_login') </p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container bread-crumbs">
                <span> @lang('site.home') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i> </span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.cart') </span>
            </div>
        </div>
        <div class="buy-status">
            <div class="container">
                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                    <div class="row">
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/info-pay.png')}}">
                                    <p class="">@lang('site.carts.payment_info')</p>
                                </div>
                                <div class="col-xs-2 padding-top-2 padding-lr-0">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/bag-circle.png')}}">
                                    <p class="">@lang('site.carts.preview')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                            <img class="hidden-xs" src="{{asset('assets/site/img/circle.png')}}">
                            <img class="hidden-xs" src="{{asset('assets/site/img/circle.png')}}">
                            <img class="hidden-xs" src="{{asset('assets/site/img/circle.png')}}">
                        </div>
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/info-product.png')}}">
                                    <p class="">@lang('site.carts.shipping_info')</p>
                                </div>
                                <div class="col-xs-2  padding-top-2 padding-lr-0">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/user.png')}}">
                                    <p class="">@lang('site.carts.login_to_shop')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container text-center">  @include('views.errors.errors')</div>
        <div id="login-form" class="container login-form">
            <div class="row">
                <div class="col-lg-7 col-md-8 col-sm-8 col-xs-12 col-sm-push-2 ">
                    <div style="background-color: white;float:right">
                        <div class="col-xs-12">
                            <h5 class="text-center bolder" style="
    padding-top: 2em;
    padding-bottom: 2em;
">@lang('site.user.login_to_account')</h5>
                        </div>
                        {{Form::open(['action' => 'Auth\Controllers\Site\AuthController@login'])}}
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-xs-12 form-group">
                            {{ Form::label('email',trans('site.user.email')) }}
                            {{ Form::text('email', null, ['class' => 'form-control']) }}
                            <span class="glyphicon glyphicon-envelope defult-icon-color"></span>
                        </div>
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-xs-12 form-group">
                            {{ Form::label('password',trans('site.password')) }}
                            {{ Form::password('password',['class' => 'form-control']) }}
                            <span class="glyphicon glyphicon-lock defult-icon-color"></span>
                        </div>
                        <div class="col-xs-12 text-center" style="margin-top: 2em;margin-bottom: 2em;">
                            <button type="submit" class="btn submit">@lang('site.user.login')</button>
                            <p style="padding-top: 2em;"><a class="text-danger forgotPassword"
                                                            style="padding-bottom: 0.5em;border-bottom:1px solid #ebebeb"
                                                            href="{{action('Auth\Controllers\Site\ReminderController@showReminderForm')}}">@lang('site.user.remember_button')</a>
                            </p>
                        </div>
                            {{ Form::hidden('return',action('Cart\Controllers\Site\CartController@shipping')) }}
                        {{ Form::close() }}

                    </div>
                    <div class="col-xs-12 text-center register" style="">
                        <span>@lang('site.not_registered')</span><a style="color:#33beff"
                                                                    href="{{ action('Auth\Controllers\Site\AuthController@showSignupForm',['return' => url()->current()]) }}">@lang('site.do_register')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/checkout-step.css') }}
    {{ Html::style('assets/site/css/checkout-step1.css') }}
@endpush
@push('js')
@endpush