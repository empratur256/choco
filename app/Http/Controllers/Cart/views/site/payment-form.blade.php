<div id="form" class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-6 col-sm-push-6">
                    <div class="total-price">
                        <span> @lang('site.carts.total') </span>
                        <span class="pull-left">{{ number_format($taxPrice + $finalPrice + $shipping->price) }} @lang('site.currency') </span>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <div class="off">
                        {{ Form::open(['action' => 'Cart\Controllers\Site\CartController@coupon']) }}
                        <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">
                                       @lang('site.carts.coupon_submit')
                                    </button>
                                </span>
                            {{ Form::text('coupon','',['class'=>'form-control']) }}
                            <span style="padding-top: 5px;" class="pull-right"> @lang('site.carts.coupon')  </span>


                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">
            <div style="border-bottom :1px solid #ebebeb;padding-bottom: 1em">
                <img src="{{asset('assets/site/img/left-side.png')}}"><span> @lang('site.carts.payment_type') </span>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="row">
                {!! Form::open(['action' => $formAction]) !!}
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 pull-right">
                    <div class="radio payment-method" style="text-align: -webkit-right;">
                        <label >
                            {{ Form::radio('payment_type', 'online',['style'=>'float: right']) }}
                            <span class="cr" style="float: initial;margin-left: 1em;"><i
                                        class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <p class="red_text bolder padding-bottom-05">@lang('site.carts.payments.online')</p>
                            <h5 class="defult-text padding-bottom-02"
                                style="font-size: 11px;">@lang('site.carts.payments.online_description')</h5>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 pull-right">
                    <div class="radio payment-method" style="text-align: -webkit-right;">
                        <label >
                            {{ Form::radio('payment_type', 'card',['style'=>'float: right']) }}
                            <span class="cr" style="float: initial;margin-left: 1em;"><i
                                        class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <p class="red_text bolder padding-bottom-05">@lang('site.carts.payments.card')</p>
                            <h5 class="defult-text padding-bottom-02"
                                style="font-size: 11px;">@lang('site.carts.payments.card_description')</h5>
                        </label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 pull-right">
                    <div class="radio payment-method" style="text-align: -webkit-right;">
                        <label>
                            {{ Form::radio('payment_type', 'account',['style'=>'float: right']) }}
                            <span class="cr" style="float: initial;margin-left: 1em;"><i
                                        class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <p class="red_text bolder padding-bottom-05">@lang('site.carts.payments.account')</p>
                            <h5 class="defult-text padding-bottom-02"
                                style="font-size: 11px;">@lang('site.carts.payments.account_description')</h5>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 button">
            @if(request()->is('custom/order/*'))
                <a href="{!! action('CustomOrder\Controllers\Site\OrderController@shipping') !!}" class="btn submit pull-right">
                    @lang('site.custom_order.previous_button') </a>
            @else
                <a href="{!! action('Cart\Controllers\Site\CartController@review') !!}" class="btn submit pull-right"> @lang('site.carts.preview') </a>
            @endif
            <button type="submit" class="btn submit pull-left">@lang('site.carts.submit_cart')</button>
        </div>
    </div>
</div>

