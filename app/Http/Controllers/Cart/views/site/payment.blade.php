@extends('views.layouts.site.master')
@section('title', trans('site.carts.payment_info'))
@section('content')
    <div class="red_box">
        <div class="container">
            <h3 class="title-page"> @lang('site.carts.payment_info')  </h3>
            <div class="divider-title light"></div>
            <p> @lang('site.carts.top_text_pay') </p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container bread-crumbs">
                <span> @lang('site.home') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i></span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.carts.payment_info') </span>
            </div>
        </div>
        <div class="buy-status">
            <div class="container">
                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                    <div class="row">
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/info-pay.png') }}">
                                    <p class="">@lang('site.carts.payment_info')</p>
                                </div>
                                <div class="col-xs-2 padding-top-2 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/ok-circle.png') }}">
                                    <p class="green_text">@lang('site.carts.preview')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                            <img class="hidden-xs" src="{{ asset('assets/site/img/green-circle.png') }}">
                            <img class="hidden-xs" src="{{ asset('assets/site/img/green-circle.png') }}">
                            <img class="hidden-xs" src="{{ asset('assets/site/img/green-circle.png') }}">
                        </div>
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/ok-circle.png') }}">
                                    <p class="green_text">@lang('site.carts.shipping_info')</p>
                                </div>
                                <div class="col-xs-2  padding-top-2 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/ok-circle.png') }}">
                                    <p class="green_text">@lang('site.carts.login_to_shop')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="body-panel">
                        <div class="row">
                            <div class="container text-center">  @include('views.errors.errors')</div>
                        @include('Cart.views.site.payment-form')
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/checkout-step.css') }}
{{ Html::style('assets/site/css/checkout-step4.css') }}
@endpush

@push('js')
<script>
    $(document).ready(function(){
        $('#atm-pay').click(function(){
            $(this).prop('checked', true);
            $('#net-pay').prop('checked',false);
        })
        $('#net-pay').click(function(){
            $(this).prop('checked', true);
            $('#atm-pay').prop('checked',false);
        })


    });

</script>

@endpush