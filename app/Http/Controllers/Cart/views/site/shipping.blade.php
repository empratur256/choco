@extends('views.layouts.site.master')
@section('title',trans('site.carts.shipping_page'))
@section('content')
    <div class="red_box">
        <div class="container">
            <h3 class="title-page"> @lang('site.carts.shipping_info')  </h3>
            <div class="divider-title light"></div>
            <p> @lang('site.carts.shipping_page') </p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container bread-crumbs">
                <span> @lang('site.home') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i> </span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.carts.shipping_info') </span>
            </div>
        </div>
        <div class="buy-status">
            <div class="container">
                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                    <div class="row">
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/info-pay.png')}}">
                                    <p class="">@lang('site.carts.payment_info')</p>
                                </div>
                                <div class="col-xs-2 padding-top-2 padding-lr-0">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                    <img src="{{asset('assets/site/img/circle.png')}}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/bag-circle.png')}}">
                                    <p class="">@lang('site.carts.preview')</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                            <img class="hidden-xs" src="{{asset('assets/site/img/circle.png')}}">
                            <img class="hidden-xs" src="{{asset('assets/site/img/circle.png')}}">
                            <img class="hidden-xs" src="{{asset('assets/site/img/circle.png')}}">
                        </div>
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/info-product.png')}}">
                                    <p class="">@lang('site.carts.shipping_info')</p>
                                </div>
                                <div class="col-xs-2  padding-top-2 padding-lr-0">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                    <img src="{{ asset('assets/site/img/green-circle.png') }}">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="{{asset('assets/site/img/ok-circle.png')}}">
                                    <p class="green_text">@lang('site.carts.login_to_shop')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="body-panel">
                    <div class="row">
                        @if(session()->has('info') || session()->has('success') || $errors->has('ship') || $errors->has('ship_address'))
                            <div class="container text-center">  @include('views.errors.errors')</div>
                        @endif
                        {{ Form::open(['action' => 'Cart\Controllers\Site\CartController@storeShipping','class' => 'form']) }}
                        @include('Cart.views.site.shipping-address')

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::open(['action' => ['User\Controllers\Site\AddressController@destroy',1],'method' => 'DELETE','id' => 'removeForm']) !!}
    {!! Form::hidden('return',action('Cart\Controllers\Site\CartController@shipping')) !!}
    {!! Form::hidden('deleteId',null,['id' => 'removeData']) !!}
    {!! Form::close() !!}
@endsection
@push('modal')
    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog"
         aria-labelledby="addressModalLabel">
        <div class="modal-dialog" role="document">
            @if(!is_null($editAddress))
                {!! Form::model($editAddress,['action' => ['User\Controllers\Site\AddressController@update',$editAddress->id],'method' => 'Patch']) !!}
            @else
                {!! Form::open(['action' => 'User\Controllers\Site\AddressController@store']) !!}
            @endif
            {!! Form::hidden('return',action('Cart\Controllers\Site\CartController@shipping')) !!}

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title purple-text" id="addressModalLabel">
                        <span class="fa fa-map-marker"></span> @lang('site.carts.add_address')</h4>
                    <button type="button" class="close" data-dismiss="modal"> <i class="fa fa-remove"></i></button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group @if($errors->has('name_family')) has-error @endif">
                            {{ Form::text('name_family',null,['id' => 'name_family','class' => 'form-control', 'placeholder' =>trans('site.name_family'), 'lang' => 'fa']) }}
                            <span class="help-block">{{ $errors->first('name_family') }}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group @if($errors->has('province_id')) has-error @endif">
                            {{ Form::select('province_id',$provinces,null,['id' => 'province','class' => 'form-control selectpicker','placeholder'=> trans('site.select_province')]) }}
                            <span class="help-block">{{ $errors->first('province_id') }}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group @if($errors->has('city_id')) has-error @endif">
                            {{ Form::select('city_id',[],null,['id' => 'city_id','class' => 'form-control selectpicker', 'disabled' => 'disabled', 'placeholder' => trans('site.select_city')]) }}
                            <span class="help-block">{{ $errors->first('city_id') }}</span>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group @if($errors->has('address')) has-error @endif">
                            {{ Form::text('address',null,['id' => 'address','class' => 'form-control', 'placeholder' => trans('site.address'), 'lang' => 'fa']) }}
                            <span class="help-block">{{ $errors->first('address') }}</span>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12 form-group @if($errors->has('tel')) has-error @endif">
                            {{ Form::text('tel',null,['id' => 'phone','class' => 'form-control number-only', 'placeholder' => trans('site.telephone')]) }}
                            <span class="help-block">{{ $errors->first('tel') }}</span>
                        </div>
                        <div class="col-md-4 col-sm-4 form-group @if($errors->has('phone_code')) has-error @endif">
                            {{ Form::text('phone_code',null,['id' => 'phone_code','class' => 'form-control number-only', 'placeholder' => trans('site.phone_code')]) }}
                            <span class="help-block">{{ $errors->first('phone_code') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 form-group @if($errors->has('mobile')) has-error @endif">
                            {{ Form::text('mobile',null,['id' => 'mobile','class' => 'form-control number-only', 'placeholder' => trans('site.mobile')]) }}
                            <span class="help-block">{{ $errors->first('mobile') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 form-group @if($errors->has('postal_code')) has-error @endif">
                            {{ Form::text('postal_code',null,['id' => 'postal_code','class' => 'form-control number-and-dash', 'placeholder' => trans('site.postal_code')]) }}
                            <span class="help-block">{{ $errors->first('postal_code') }}</span>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="add-address" class="btn btn-success">
                        <span class="fa fa-check"></span>@lang('site.save')
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endpush

@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/checkout-step.css') }}
    {{ Html::style('assets/site/css/checkout-step2.css') }}
@endpush

@push('js')
    @if(App::isLocale('fa'))
        {{ Html::script('assets/site/plugins/farsiType/FarsiType.js') }}
    @endif
    <script>
        var location_action = "{{ action('City\Controllers\Site\CityController@cities') }}";
        var token = $('meta[name="csrf-token"]').attr('content');

        $(document).ready(function () {
            @if(!is_null($editAddress))
            $('#addressModal').modal('show');
            @endif
            @php $errorField = ['tel','address','city_id','province_id','name_family','mobile','postal_code'] @endphp
            @foreach($errorField as $field)
            @if($errors->has($field))
            $('#addressModal').modal('show');
            @continue
            @endif
            @endforeach
            @php $city = 0 @endphp
            @if(!is_null($editAddress))
            @php $city= $editAddress->city_id @endphp

            @endif
            @if(old('city_id'))
            @php $city= old('city_id') @endphp
            @endif
            getCities($('#province').val(), '{{$city}}');
            $(document.body).on('change', '#province', function () {
                if ($(this).val()) {
                    getCities($(this).val());
                }
            });

            $('.number-only').forceNumeric();
            $('.number-and-dash').forceNumericAndDash();
        });

        jQuery.fn.forceNumeric = function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.which || e.keyCode;
                    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                        key >= 48 && key <= 57 ||
                        key >= 96 && key <= 105 ||
                        key == 190 || key == 188 || key == 110 ||
                        key == 8 || key == 9 || key == 13 ||
                        key == 35 || key == 36 ||
                        key == 37 || key == 39 ||
                        key == 46 || key == 45)
                        return true;
                    return false;
                });
            });
        };

        jQuery.fn.forceNumericAndDash = function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.which || e.keyCode;
                    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                        key >= 48 && key <= 57 ||
                        key >= 96 && key <= 105 ||
                        key == 190 || key == 188 || key == 109 || key == 110 ||
                        key == 8 || key == 9 || key == 13 ||
                        key == 35 || key == 36 ||
                        key == 37 || key == 39 ||
                        key == 46 || key == 45 || key == 189)
                        return true;
                    return false;
                });
            });
        };


        $('.submitForm').on('click', function (e) {
            e.preventDefault();
            $('.form').submit();
        });
        $('.deleteForm').on('click', function (e) {
            e.preventDefault();
            if (confirm('@lang('site.carts.remove_data')')) {
                $('#removeData').val($(this).data('id'));
                $('#removeForm').submit();
            }
        });

        function getCities(province_id, old) {
            if (province_id > 0) {
                $.ajax({
                    type: "POST",
                    url: location_action,
                    data: {
                        _token: token,
                        id: province_id
                    },
                    dataType: "json",
                    cache: false
                }).fail(function (jqXHR, textStatus, errorThrown) {

                    alert('@lang('site.carts.error_data')');

                }).done(function (response) {

                    $('#city_id').empty();
                    $('#city_id').prop('disabled', false);
                    $.each(response, function (value, key) {
                        var input = '<option></option>';
                        if (old > 0) {
                            if (old == value) {
                                var input = '<option selected></option>';
                            } else {
                                var input = '<option></option>';
                            }
                        }
                        $('#city_id').append($(input)
                            .attr("value", value).text(key));
                    });
                    $('#city_id').selectpicker('refresh');
                    return true;

                });
            }

        }


    </script>

@endpush