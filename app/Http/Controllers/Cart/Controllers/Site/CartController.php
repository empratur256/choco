<?php

namespace Sedehi\Http\Controllers\Cart\Controllers\Site;

use Carbon\Carbon;
use Cart;
use DB;
use Exception;
use Gloudemans\Shoppingcart\Exceptions\InvalidRowIDException;
use Illuminate\Http\Request;
use Log;
use Sedehi\Http\Controllers\Cart\Requests\Site\CartRequest;
use Sedehi\Http\Controllers\City\Models\City;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Coupon\Controllers\Site\CouponController;
use Sedehi\Http\Controllers\Coupon\Models\Code;
use Sedehi\Http\Controllers\Coupon\Models\PresentedCouponCode;

use Sedehi\Http\Controllers\Order\Models\Address as OrderAddress;

use Sedehi\Http\Controllers\Order\Models\Item;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Product\Models\Price;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Controllers\Setting\Models\Setting;

use Sedehi\Http\Controllers\Transport\Models\Transport;
use Sedehi\Http\Controllers\User\Models\Address;
use Sedehi\Payment\Facades\Payment as BankPayment;

class CartController extends Controller
{


    public function add(CartRequest $request, $id)
    {
        if ($request->has('cart')) {
            try {
                Cart::remove($request->get('cart'));
            } catch (InvalidRowIDException $e) {
            }
        }
        $product = Product::active()->with('prices')->find($id);

        $productPrice = 0;

        if ($product->price_type == 'fixed') {
            if ($request->get('quantity') > $product->quantity) {
                return redirect()->back()->with('error', trans('site.carts.product_max'));
            }
            $productPrice = finalPrice($product);
        }

        Cart::add([
            'id' => $product->id,
            'name' => $product->title,
            'qty' => $request->get('quantity'),
            'price' => ($productPrice > 0) ? $productPrice : 0,
            'options' => [
                'picture' => $product->picture,
            ] ,
        ]);

        return redirect()
            ->action('Cart\Controllers\Site\CartController@index')
            ->with('success', trans('site.carts.product_add'));
    }

    public function remove(CartRequest $request)
    {
        try {
            Cart::remove($request->get('id'));
        } catch (InvalidRowIDException $e) {
            return redirect()->back()->with('error', trans('site.data_not_found'));
        }

        return redirect()->back()->with('success', trans('site.carts.remove'));
    }

    public function removeall(CartRequest $request)
    {
        try {
            Cart::destroy();
        } catch (InvalidRowIDException $e) {
            return redirect()->back()->with('error', trans('site.data_not_found'));
        }

        return redirect()->back()->with('success', trans('site.carts.remove'));
    }

    public function update(CartRequest $request)
    {
        $product = Product::active()->find($request->get('product'));
        if (is_null($product)) {
            return redirect()->back()->with('error', trans('site.carts.product_not_found'));
        }
        try {
            $cart = Cart::get($request->get('id'));
            if ($product->price_type == 'fixed') {
                if ($request->get('quantity') > $product->quantity) {
                    return redirect()->back()->with('error', trans('site.carts.product_max'));
                }
            }

            Cart::update($request->get('id'), $request->get('quantity'));

            return redirect()->back()->with('success', trans('site.data_edited'));
        } catch (InvalidRowIDException $e) {
            return redirect()->back()->with('error', trans('site.data_not_found'));
        }
    }

    public function index()
    {
        return view('Cart.views.site.index');
    }

    public function user()
    {
        if (auth()->check()) {
            return redirect()->action('Cart\Controllers\Site\CartController@shipping');
        }

        return view('Cart.views.site.user');
    }

    public function shipping()
    {
        if (!$this->checkAvailable()) {
            return $this->notAvailable();
        }
        if (auth()->guest()) {
            return $this->needToLogin();
        }
        if (Cart::count() == 0) {
            return $this->emptyCart();
        }
        $editAddress = null;
        if (request()->has('address_id')) {
            $editAddress = Address::where('user_id', auth()->user()->id)->findOrFail(request()->get('address_id'));
            if (strlen($editAddress->tel) > 8) {
                $code = explode('-', $editAddress->tel);
                if (count($code) == 2) {
                    $editAddress->phone_code = $code[0];
                    $editAddress->tel = $code[1];
                }
            }
        }
        $shipping = Transport::all();
        $provinces = City::roots()->active()->orderBy('name')->pluck('name', 'id')->toArray();
        $addresses = auth()->user()->addresses()->with('city', 'province')->get();


        return view('Cart.views.site.shipping', compact('shipping', 'provinces', 'addresses', 'editAddress'));
    }

    public function storeShipping(CartRequest $request)
    {
        if (!$this->checkAvailable()) {
            return $this->notAvailable();
        }
        if (auth()->guest()) {
            return $this->needToLogin();
        }
        if (Cart::count() == 0) {
            return $this->emptyCart();
        }
        session()->put('ship_address', $request->get('ship_address'));
        session()->put('ship', $request->get('ship'));
        session()->put('invoice_request', $request->get('invoice_request'));

        return redirect()->action('Cart\Controllers\Site\CartController@review');
    }

    public function review()
    {
        if (!$this->checkAvailable()) {
            return $this->notAvailable();
        }
        if (auth()->guest()) {
            return $this->needToLogin();
        }
        if (Cart::count() == 0) {
            return $this->emptyCart();
        }
        if (!session()->has([
            'ship_address',
            'ship',
            'invoice_request',
        ])
        ) {
            return $this->needToShipping();
        }
        $shipping = Transport::find(session('ship'));
        $address = Address::where('user_id', auth()->user()->id)->find(session('ship_address'));
        if (is_null($shipping) || is_null($address)) {
            return $this->needToShipping();
        }


        return view('Cart.views.site.review', compact('address', 'shipping'));
    }

    public function payment()
    {
        if (!$this->checkAvailable()) {
            return $this->notAvailable();
        }
        if (auth()->guest()) {
            return $this->needToLogin();
        }
        if (Cart::count() == 0) {
            return $this->emptyCart();
        }
        if (!session()->has([
            'ship_address',
            'ship',
            'invoice_request',
        ])
        ) {
            return $this->needToShipping();
        }
        $shipping = Transport::find(session('ship'));
        $address = Address::where('user_id', auth()->user()->id)->find(session('ship_address'));
        if (is_null($shipping) || is_null($address)) {
            return $this->needToShipping();
        }
        $code = null;
        $coupon = null;
        $setting = Setting::first();
        $totalPrice = $this->cartPrice();
        $taxPrice = 0;
        $discount = 0;
        if (session()->has([
            'coupon_id',
            'code_id',
            'code',
            'coupon_type',
        ])
        ) {
            $discountData = app(CouponController::class)->calculateDiscounts($totalPrice, $setting);
            $discount = $discountData['discount'];
            $code = $discountData['code'];
        }
        $finalPrice = $totalPrice - $discount;
        if ($setting->tax > 0) {
            $taxPrice = ($finalPrice * $setting->tax / 100);
        }
        $formAction = 'Cart\Controllers\Site\CartController@submit';

        return view('Cart.views.site.payment',
            compact('shipping', 'code', 'formAction', 'taxPrice', 'finalPrice', 'totalPrice', 'discount'));
    }

    public function submit(CartRequest $request)
    {
        if (!$this->checkAvailable()) {
            return $this->notAvailable();
        }
        if (auth()->guest()) {
            return $this->needToLogin();
        }
        if (Cart::count() == 0) {
            return $this->emptyCart();
        }
        if (!session()->has([
            'ship_address',
            'ship',
            'invoice_request',
        ])
        ) {
            return $this->needToShipping();
        }
        $shipping = Transport::find(session('ship'));
        $address = Address::where('user_id', auth()->user()->id)->find(session('ship_address'));
        if (is_null($shipping) || is_null($address)) {
            return $this->needToShipping();
        }
        $code = null;
        $coupon = null;
        $setting = Setting::first();
        $totalPrice = $this->cartPrice();
        $taxPrice = 0;
        $discount = 0;
        if (session()->has([
            'coupon_id',
            'code_id',
            'code',
            'coupon_type',
        ])
        ) {
            $discountData = app(CouponController::class)->calculateDiscounts($totalPrice, $setting);
            $discount = $discountData['discount'];
            $code = $discountData['code'];
        }
        $finalPrice = $totalPrice - $discount;
        if ($finalPrice < 0) {
            $finalPrice = 0;
        }
        if ($setting->tax > 0 && $finalPrice > 0) {
            $taxPrice = ($finalPrice * $setting->tax / 100);
        }
        DB::beginTransaction();
        try {
            $order = new Order();
            $order->user_id = auth()->user()->id;
            $order->tax = $setting->tax;
            $order->tax_price = $taxPrice;
            $order->discount = $discount;
            $order->total_price = $finalPrice;
            $order->shipping_price = $shipping->price;
            $order->payment_type = $request->get('payment_type');
            $order->confirm = 1;
            $order->valid = 1;
            $order->invoice_request = session()->get('invoice_request');
            $order->save();
            $orderAddress = new OrderAddress();
            $orderAddress->order_id = $order->id;
            $orderAddress->shipping_id = $shipping->id;
            $orderAddress->province_id = $address->province_id;
            $orderAddress->city_id = $address->city_id;
            $orderAddress->postal_code = $address->postal_code;
            $orderAddress->name_family = $address->name_family;
            $orderAddress->mobile = $address->mobile;
            $orderAddress->tel = $address->tel;
            $orderAddress->address = $address->address;
            $orderAddress->save();
            if (session()->get('coupon_type') == 'public') {
                if (!is_null($code)) {
                    $code->used_at = Carbon::now();
                    $code->user_id = auth()->user()->id;
                    $code->order_id = $order->id;
                    $code->save();
                }
            } elseif (session()->get('coupon_type') == 'presented') {
                if (!is_null($code)) {
                    $code->used_at = Carbon::now();
                    $code->user_used_id = auth()->user()->id;
                    $code->order_id = $order->id;
                    $code->save();
                }
            }
            foreach (Cart::content() as $cart) {
                $product = Product::active()->where('quantity', '>=', $cart->qty)->find($cart->id);
                if ($product->need_to_produce == 1) {
                    $order->need_to_produce = 1;
                }
                if (is_null($product)) {
                    Cart::remove($cart->rowId);

                    return $this->notAvailable();
                }
                $item = new Item();
                $item->order_id = $order->id;
                $item->product_id = $product->id;
                $item->discount = 0;
                $item->quantity = $cart->qty;
                if ($product->need_to_produce == 1) {
                    $item->need_to_produce = 1;
                }

                $item->price = $cart->price;
                $item->save();
                if ($product->price_type == 'dynamic') {
                    $dbPrice = Price::where('product_id', $product->id)->get();
                    $product->max_price = $dbPrice->max('price');
                    $product->min_price = $dbPrice->min('price');
                    $product->quantity = $dbPrice->sum('quantity');
                    $product->price = $dbPrice->min('price');
                } else {
                    $product->quantity -= $cart->qty;
                }
                $product->order_count += 1;
                $product->save();
            }
            $order->save();
            DB::commit();
            Cart::destroy();
            session()->forget([
                'coupon_type',
                'coupon_id',
                'code_id',
                'code',
                'ship_address',
                'ship',
                'invoice_request',
            ]);
            if ($request->get('payment_type') == 'online') {
                return BankPayment::zarinpal()
                    ->callBackUrl(action('Payment\Controllers\Site\PaymentController@verify'))
                    ->amount($order->full_price)
                    ->data([
                        'user_id' => auth()->user()->id,
                        'order_id' => $order->id,
                    ])
                    ->description( trans('site.custom_order.payment') . $order->id)
                    ->request();
            } else {
                return redirect()
                    ->action('Order\Controllers\Site\OrderController@index', ['order' => $order->id])
                    ->with('success', trans('site.order_submit'));
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar submit cart ' . $e);

            return redirect()->back()->with('error', trans('site.order_fail'));
        }
    }

    public function coupon(CartRequest $request)
    {
        $couponExplode = explode('-', $request->get('coupon'));
        if (count($couponExplode) == 1) {
            $code = Code::where('code', $request->get('coupon'))->ValidCode()->first();
            if (is_null($code)) {
                return redirect()->back()->with('error', trans('site.coupon_not_found'));
            }
            $coupon = $code->coupon;
            session()->put('coupon_id', $coupon->id);
            session()->put('code_id', $code->id);
            session()->put('code', $code->code);
            session()->put('coupon_type', 'public');

            return redirect()->back()->with('success', trans('site.coupon_ok'));
        } elseif (count($couponExplode) == 2) {
            $code = PresentedCouponCode::where('user_id', auth()->user()->id)
                ->where('code', $request->get('coupon'))
                ->ValidCode()
                ->first();
            if (is_null($code)) {
                return redirect()->back()->with('error', trans('site.coupon_not_found'));
            }
            session()->put('coupon_id', $code->id);
            session()->put('code_id', $code->id);
            session()->put('code', $code->code);
            session()->put('coupon_type', 'presented');
            if ($request->has('return')) {
                return redirect()->to($request->get('return'))->with('success', trans('site.coupon_ok'));
            }

            return redirect()->back()->with('success', trans('site.coupon_ok'));
        }
    }

    private function emptyCart()
    {
        return redirect()
            ->action('Cart\Controllers\Site\CartController@index')
            ->with('error', trans('site.empty_cart'));
    }

    private function needToLogin()
    {
        $action = action('Auth\Controllers\Site\AuthController@login');

        $action .= '?return='.urlencode(action('Cart\Controllers\Site\CartController@index'));

        return redirect()->to($action)->with('error', trans('site.custom_order.need_to_login'));
    }

    private function needToShipping()
    {
        return redirect()
            ->action('Cart\Controllers\Site\CartController@shipping')
            ->with('info', trans('site.carts.need_to_shipping'));
    }

    private function cartPrice()
    {
        $totalPrice = 0;
        foreach (Cart::content() as $cart) {

            $totalPrice += $cart->subtotal;
        }

        return $totalPrice;
    }

    private function checkAvailable()
    {
        $available = true;
        foreach (Cart::content() as $cart) {
            $product = Product::active()->where('quantity', '>=', $cart->qty)->find($cart->id);
            if (is_null($product)) {
                $available = false;
                Cart::remove($cart->rowId);
                continue;
            }
        }

        return $available;
    }

    private function notAvailable()
    {
        return redirect()
            ->action('Cart\Controllers\Site\CartController@index')
            ->with('info', trans('site.carts.cart_changed'));
    }
}
