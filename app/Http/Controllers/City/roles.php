<?php
return [
    'city' => [
        'title'  => 'شهر های آدرس ',
        'access' => [
            'CityController' => [
                'لیست'   => 'index',
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
