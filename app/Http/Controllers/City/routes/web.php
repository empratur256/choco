<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'City\Controllers\Admin',
             ], function(){
    Route::resource('city', 'CityController', ['except' => ['show']]);
});
Route::post('city', 'City\Controllers\Site\CityController@cities');