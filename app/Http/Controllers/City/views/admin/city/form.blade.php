<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::select('parent_id',$parents,null,['class'=>'md-input','placeholder' => 'بدون والد']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('name','نام') !!}
        {!! Form::text('name',null,['class'=>'md-input']) !!}
    </div>

    <div class="uk-width-medium-1-1">
        {!! Form::checkbox('active',1,(isset($item)? $item->active : true),['data-switchery','id' => 'active']) !!}
        {!! Form::label('active','فعال',['class' => 'inline-label']) !!}
    </div>
</div>

