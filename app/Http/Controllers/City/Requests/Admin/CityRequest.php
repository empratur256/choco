<?php

namespace Sedehi\Http\Controllers\City\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Sedehi\Http\Controllers\City\Models\City;

class CityRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'name' => 'required',
                ];
            break;
            case 'update':
                return [
                    'name' => 'required',
                ];
            break;
        }

        return [];
    }

}
