<?php

namespace Sedehi\Http\Controllers\City\Models;

use Baum\Node;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class City extends Node
{

    use  Filterable, SoftDeletes;

    protected $table       = 'city';
    public    $timestamps  = true;
    protected $orderColumn = 'name';

    protected $filterable = [
        'name'       => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}
