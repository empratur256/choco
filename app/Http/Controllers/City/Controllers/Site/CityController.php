<?php

namespace Sedehi\Http\Controllers\City\Controllers\Site;

use Illuminate\Http\Request;
use Sedehi\Http\Controllers\City\Models\City;
use Sedehi\Http\Controllers\City\Requests\Site\CityRequest;
use Sedehi\Http\Controllers\Controller;

class CityController extends Controller
{

    public function cities(CityRequest $request)
    {
        $cities = City::findOrFail($request->get('id'))->children()->pluck('name', 'id');

        return response()->json($cities);
    }
}
