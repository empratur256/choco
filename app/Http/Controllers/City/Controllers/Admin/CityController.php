<?php

namespace Sedehi\Http\Controllers\City\Controllers\Admin;

use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\City\Models\City;
use Sedehi\Http\Controllers\City\Requests\Admin\CityRequest;

class CityController extends Controller
{

    public function index()
    {
        if(request()->has('parent_id')){
            $items = City::findOrFail(request()->get('parent_id'))->children();
        }else{
            $items = City::roots();
        }
        $items = $items->latest()->filter()->paginate(20);

        return view('City.views.admin.city.index', compact('items'));
    }

    public function create()
    {
        $parents = City::roots()->pluck('name', 'id');

        return view('City.views.admin.city.add', compact('parents'));
    }

    public function store(CityRequest $request)
    {
        $item         = new City();
        $item->name   = $request->get('name');
        $item->active = $request->has('active');
        if($request->has('parent_id')){
            $item->parent_id = $request->get('parent_id');
        }else{
            $item->parent_id = null;
        }
        $item->save();

        return redirect()->action('City\Controllers\Admin\CityController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item    = City::findOrFail($id);
        $parents = City::roots()->where('id', '<>', $id)->pluck('name', 'id');

        return view('City.views.admin.city.edit', compact('item', 'parents'));
    }

    public function update(CityRequest $request, $id)
    {
        $isDirty      = false;
        $item         = City::findOrFail($id);
        $item->name   = $request->get('name');
        $item->active = $request->has('active');
        if($request->has('parent_id')){
            $item->parent_id = $request->get('parent_id');
        }else{
            $item->parent_id = null;
        }
        if($item->isDirty('parent_id')){
            $isDirty = true;
        }
        $item->save();
        if($isDirty){
            City::rebuild(true);
        }

        return redirect()->action('City\Controllers\Admin\CityController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(CityRequest $request)
    {
        $menus = City::whereIn('id', $request->get('deleteId'))->get();
        $ids = [];
        foreach($menus as $menu){
            foreach($menu->getDescendantsAndSelf() as $m){
                $ids[] = $m->id;
            }
        }
        City::whereIn('id', $ids)->delete();
        City::rebuild(true);

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
