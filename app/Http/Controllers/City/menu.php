<?php
return [
    'city' => [
        'order'   => 180,
        'title'   => 'شهر های آدرس ',
        'icon'    => 'fa fa-globe',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'City\Controllers\Admin\CityController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'City\Controllers\Admin\CityController@create',
                'parameters' => [],
            ],
        ],
    ],
];
