<?php
return [
    'warehouse' => [
        'title'   => 'انبار',
        'order'   => 22,
        'icon'    => 'fa fa-server',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'Warehouse\Controllers\Admin\WarehouseController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'Warehouse\Controllers\Admin\WarehouseController@create',
                'parameters' => [],
            ],
        ],
    ],
];
