<?php

namespace Sedehi\Http\Controllers\Warehouse\Controllers\Admin;

use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Warehouse\Models\Warehouse;
use Sedehi\Http\Controllers\Warehouse\Requests\Admin\WarehouseRequest;
use Illuminate\Http\Request;

class WarehouseController extends Controller
{

    public function index()
    {
        $items = Warehouse::filter()->latest()->paginate(20);

        return view('Warehouse.views.admin.warehouse.index', compact('items'));
    }

    public function create()
    {
        return view('Warehouse.views.admin.warehouse.add');
    }

    public function store(WarehouseRequest $request)
    {
        $item          = new Warehouse();
        $item->title   = $request->get('title');
        $item->address = $request->get('address');
        $item->save();

        return redirect()->action('Warehouse\Controllers\Admin\WarehouseController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Warehouse::findOrFail($id);

        return view('Warehouse.views.admin.warehouse.edit', compact('item'));
    }

    public function update(WarehouseRequest $request, $id)
    {
        $item          = Warehouse::findOrFail($id);
        $item->title   = $request->get('title');
        $item->address = $request->get('address');
        $item->save();

        return redirect()->action('Warehouse\Controllers\Admin\WarehouseController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(WarehouseRequest $request)
    {
        Warehouse::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
