<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Warehouse\Controllers\Admin',
             ], function(){
    Route::resource('warehouse', 'WarehouseController', ['except' => ['show']]);
});
