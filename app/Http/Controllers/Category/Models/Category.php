<?php

namespace Sedehi\Http\Controllers\Category\Models;

use Baum\Node;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Libs\LanguageTrait;

class Category extends Node
{

    use  Filterable, SoftDeletes,LanguageTrait;

    protected $table       = 'category';
    public    $timestamps  = true;
    protected $orderColumn = 'order';

    protected $filterable = [
        'name_fa'    => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function getNameAttribute()
    {
        return $this['name_'.$this->lang()];
    }

}
