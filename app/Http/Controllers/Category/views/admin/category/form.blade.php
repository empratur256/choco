<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::select('parent_id',$parents,null,['class'=>'md-input','placeholder' => 'بدون والد']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('name_fa','نام فارسی') !!}
        {!! Form::text('name_fa',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('name_en','نام انگلیسی') !!}
        {!! Form::text('name_en',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('order','ترتیب') !!}
        {!! Form::number('order',isset($item)? $item->order : 1000,['class'=>'md-input']) !!}
    </div>


</div>
