<?php
return [
    'category' => [
        'order'   => 80,
        'title'   => 'دسته بندی محصولات',
        'icon'    => 'fa fa-bars',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'Category\Controllers\Admin\CategoryController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'Category\Controllers\Admin\CategoryController@create',
                'parameters' => [],
            ],
        ],
    ],
];
