<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Category\Controllers\Admin',
             ], function(){
    Route::resource('category', 'CategoryController', ['except' => ['show']]);
});


