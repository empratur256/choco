<?php

namespace Sedehi\Http\Controllers\Category\database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Sedehi\Http\Controllers\Category\Models\Category;

class CategoryTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     * @return void
     */
    public function run()
    {
        $datas = [
            'تابلو فرش',
            'فرش ماشینی',
            'فرش دست بافت',
        ];
        foreach ($datas as $data) {
            $item          = new Category();
            $item->name_fa = $data;
            $item->name_en = '';
            $item->save();
        }
    }

}
