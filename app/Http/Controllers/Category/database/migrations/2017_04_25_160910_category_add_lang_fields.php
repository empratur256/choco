<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoryAddLangFields extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('category', function(Blueprint $table){
            $table->renameColumn('name', 'name_fa');
            $table->string('name_en')->index();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('category', function(Blueprint $table){
            $table->dropColumn([
                                   'name_fa',
                                   'name_en',
                               ]);
        });
    }
}
