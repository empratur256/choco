<?php
use Faker\Factory as Faker;

$factory->define(\Sedehi\Http\Controllers\Category\Models\Category::class, function($faker){
    $faker = Faker::create('fa_IR');
    $parent = null;
    if($faker->boolean(20)){
        $parent = \Sedehi\Http\Controllers\Category\Models\Category::roots()->inRandomOrder()->first();
        if(!is_null($parent)){
            $parent = $parent->id;
        }
    }

    return [
        'name'      => $faker->name,
        'parent_id' => $parent,
    ];
});
