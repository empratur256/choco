<?php
return [
    'category' => [
        'title'  => 'دسته بندی محصولات',
        'access' => [
            'CategoryController' => [
                'لیست'   => ['index'],
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
