<?php

namespace Sedehi\Http\Controllers\Category\Controllers\Admin;

use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Category\Models\Category;
use Sedehi\Http\Controllers\Category\Requests\Admin\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        if(request()->has('parent_id')){
            $items = Category::findOrFail(request()->get('parent_id'))->children();
        }else{
            $items = Category::roots();
        }
        $items = $items->filter()->paginate(20);

        return view('Category.views.admin.category.index', compact('items'));
    }

    public function create()
    {
        $parents = Category::getNestedList('name_fa', 'id', ' - ');

        return view('Category.views.admin.category.add', compact('parents'));
    }

    public function store(CategoryRequest $request)
    {
        $item = new Category();
        if($request->has('parent_id')){
            $item->parent_id = $request->get('parent_id');
        }
        $item->name_fa = $request->get('name_fa');
        $item->name_en = $request->get('name_en');
        $item->order   = $request->get('order');
        $item->save();

        return redirect()->action('Category\Controllers\Admin\CategoryController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item    = Category::findOrFail($id);
        $parents = Category::getNestedList('name_fa', 'id', ' - ');

        return view('Category.views.admin.category.edit', compact('item', 'parents'));
    }

    public function update(CategoryRequest $request, $id)
    {
        set_time_limit(0);
        $isDirty       = false;
        $item          = Category::findOrFail($id);
        $item->name_fa = $request->get('name_fa');
        $item->name_en = $request->get('name_en');
        $item->order   = $request->get('order');
        if($request->has('parent_id')){
            $item->parent_id = $request->get('parent_id');
        }else{
            $item->parent_id = null;
        }
        if($item->isDirty('parent_id')){
            $isDirty = true;
        }
        $item->save();
        if($isDirty){
            Category::rebuild(true);
        }

        return redirect()->action('Category\Controllers\Admin\CategoryController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(CategoryRequest $request)
    {
        set_time_limit(0);
        $menus = Category::whereIn('id', $request->get('deleteId'))->get();
        $ids   = [];
        foreach($menus as $menu){
            foreach($menu->getDescendantsAndSelf() as $m){
                $ids[] = $m->id;
            }
        }
        Category::whereIn('id', $ids)->delete();
        Category::rebuild(true);

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

}
