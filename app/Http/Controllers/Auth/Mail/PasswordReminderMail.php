<?php

namespace Sedehi\Http\Controllers\Auth\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PasswordReminderMail extends Mailable
{

    use Queueable, SerializesModels;

    public $level      = 'info';
    public $actionText = '';
    public $introLines = [];
    public $greeting;
    public $reminder;
    public $actionUrl;
    public $sendToAdmin;

    /**
     * Create a new message instance.
     * @return void
     */
    public function __construct($reminder, $sendToAdmin = false)
    {
        $this->reminder    = $reminder;
        $this->actionText  = trans('site.user.change_password');
        $this->introLines  = [trans('site.email.reminder')];
        $this->sendToAdmin = $sendToAdmin;
        if($this->sendToAdmin){
            $action = action('Auth\Controllers\Admin\ReminderController@showResetPasswordForm', [
                $this->reminder->token,
            ]);
        }else{
            $action = action('Auth\Controllers\Site\ReminderController@showResetPasswordForm', [
                $this->reminder->token,
            ]);
        }
        $this->actionUrl = $action;
    }

    /**
     * Build the message.
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('site.user.reminder'))->view('views.emails.reminder');
    }
}
