<?php

namespace Sedehi\Http\Controllers\Auth\Controllers\Site;

use Sedehi\Http\Controllers\Auth\Requests\Site\AuthRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\User\Models\User;

class AuthController extends Controller
{

    use AuthenticatesUsers;

    protected $captchaRequiredAttemptCount = 3;

    public function showLoginForm()
    {
        if (auth()->user()) {
            return redirect()->action('HomeController@index');
        }
        if (request()->has('return')) {
            session()->flash('return', request()->get('return'));
        }


        return view('Auth.views.site.login');
    }

    public function login(AuthRequest $request)
    {
        $credentials = $request->only([
            'email',
            'password',
        ]);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $login = auth()->attempt($credentials, $request->has('remember_me'));
        if (request()->has('return')) {
            session()->flash('return', request()->get('return'));
        }
        if ($login) {
            if (auth()->user()->ban) {
                auth()->logout();

                return redirect()
                    ->action('Auth\Controllers\Site\AuthController@showLoginForm')->withInput(['return'])
                    ->with('error', trans('site.user.ban'));
            }
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            session()->forget('captcha_required');

            if ($request->has('return')) {
                return redirect()->to($request->get('return'));
            }

            return redirect()->action('User\Controllers\Site\DashboardController@index');
        }
        $this->incrementLoginAttempts($request);
        $attempts_count = $this->limiter()->attempts($this->throttleKey($request));
        if ($attempts_count >= $this->captchaRequiredAttemptCount) {
            session()->put('captcha_required', 'required|');
        }

        return redirect()
            ->action('Auth\Controllers\Site\AuthController@showLoginForm')->withInput(['return'])
            ->with('error', trans('site.user.login_fail'));
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->action('HomeController@index');
    }

    public function showSignupForm()
    {
        if (auth()->user()) {
            return redirect()->action('HomeController@index');
        }
        if (request()->has('return')) {
            session()->flash('return', request()->get('return'));
        }

        return view('Auth.views.site.signup');
    }

    public function signup(AuthRequest $request)
    {
        session()->increment('signup_attempts');
        $user = new User();
        $user->email = $request->get('email');
        $user->name = $request->get('name');
        $user->family = $request->get('family');
        $user->mobile = $request->get('mobile');
        $user->password = bcrypt($request->get('password'));
        $user->coupon_rand = rand(10, 99);

        if ($request->hasCookie('presented_by')) {
            $user->presented_by = $request->cookie('presented_by');
        }


        if ($user->save()) {
            cookie()->forget('presented_by');
            auth()->loginUsingId($user->id);

            if ($request->has('return')) {
                return redirect()->to($request->get('return'))->with('success', trans('site.user.signup'));
            }

            return redirect()
                ->action('User\Controllers\Site\DashboardController@index')
                ->with('success', trans('site.user.signup'));
        }
        abort(500);
    }
}
