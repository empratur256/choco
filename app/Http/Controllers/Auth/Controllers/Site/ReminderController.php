<?php

namespace Sedehi\Http\Controllers\Auth\Controllers\Site;

use Sedehi\Http\Controllers\Auth\Requests\Site\ReminderRequest;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\User\Models\User;
use Sedehi\Http\Controllers\Auth\Models\PasswordReminder;
use Sedehi\Http\Controllers\Auth\Mail\PasswordReminderMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class ReminderController extends Controller
{

    public function showReminderForm()
    {
        return view('Auth.views.site.reminder');
    }

    public function reminder(ReminderRequest $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if(is_null($user)){
            return redirect()->action('Auth\Controllers\Site\ReminderController@showReminderForm')
                             ->with('error', trans('site.user.email_invalid'));
        }
        if($user->ban == 1){
            return redirect()->action('Auth\Controllers\Site\ReminderController@showReminderForm')
                             ->with('error', trans('site.user.ban'));
        }
        $reminder = PasswordReminder::Where('email', $request->get('email'))->first();
        if(!is_null($reminder)){
            if($reminder->created_at->diffInMinutes() < 10){
                return redirect()->action('Auth\Controllers\Site\ReminderController@showReminderForm')
                                 ->with('error', trans('site.user.reminder_time', ['time' => 10 - $reminder->created_at->diffInMinutes()]));
            }
        }
        PasswordReminder::where('created_at', '<', Carbon::now()->addMinutes(config('auth.password.expire')))
                        ->orWhere('email', $request->get('email'))->delete();
        $reminder             = new PasswordReminder();
        $reminder->email      = $request->get('email');
        $reminder->token      = str_random(32);
        $reminder->created_at = Carbon::now();
        $reminder->save();
        Mail::to($request->get('email'))->send(new PasswordReminderMail($reminder));

        return redirect()->action('Auth\Controllers\Site\ReminderController@showReminderForm')
                         ->with('success', trans('site.user.reminder_sent'));
    }

    public function showResetPasswordForm($token)
    {
        return view('Auth.views.site.reset-password')->with('token', $token);
    }

    public function resetPassword(ReminderRequest $request)
    {
        $user = User::where('email', $request->get('email'))->first();
        if(is_null($user)){
            return redirect()
                ->action('Auth\Controllers\Site\ReminderController@showResetPasswordForm', [$request->get('token')])
                ->with('error', trans('site.user.email_invalid'));
        }
        if($user->ban == 1){
            return redirect()
                ->action('Auth\Controllers\Site\ReminderController@showResetPasswordForm', [$request->get('token')])
                ->with('error', trans('site.user.ban'));
        }
        $reminder = PasswordReminder::where('email', $user->email)->where('token', $request->get('token'))->first();
        if(is_null($reminder)){
            return redirect()
                ->action('Auth\Controllers\Site\ReminderController@showResetPasswordForm', [$request->get('token')])
                ->with('error', trans('site.user.login_fail'));
        }
        if($reminder->created_at->addMinutes(config('auth.passwords.users.expire'))->timestamp < time()){
            $reminder->delete();

            return redirect()
                ->action('Auth\Controllers\Site\ReminderController@showResetPasswordForm', [$request->get('token')])
                ->with('error', trans('site.user.token_invalid'));
        }
        $user->password = bcrypt($request->get('password'));
        $user->save();
        $reminder->delete();

        return redirect()->action('Auth\Controllers\Site\AuthController@showLoginForm')
                         ->with('success', trans('site.user.password_changed'));
    }

}