<?php

namespace Sedehi\Http\Controllers\Auth\Controllers\Admin;

use Lang;
use Sedehi\Http\Controllers\Auth\Requests\Admin\AuthRequest;
use Sedehi\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Sedehi\Http\Controllers\User\Controllers\Admin\UserController;
use Sedehi\Http\Controllers\User\Models\UserLog;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $captchaRequiredAttemptCount = 3;

    public function showLoginForm()
    {
        return view('Auth.views.admin.auth.login');
    }

    public function login(AuthRequest $request, UserController $userController)
    {
        $credentials = $request->only([
                                          'email',
                                          'password',
                                      ]);
        if($this->hasTooManyLoginAttempts($request)){
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        $login = auth()->attempt($credentials);
        if($login){
            if(auth()->user()->ban){
                auth()->logout();

                return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm')
                                 ->with('error', 'حساب کاربری شما مسدود شده است');
            }
            if(!auth()->user()->hasAnyRole()){
                auth()->logout();

                return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm')
                                 ->with('error', 'اطلاعات وارد شده اشتباه می باشد');
            }
            $userController->removeLog();
            if(auth()->user()->programmer == 0){
                $log = new UserLog();
                $log->user_id = auth()->user()->id;
                $log->ip = $request->getClientIp();
                $log->save();
            }
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);
            session()->forget('captcha_required');
            if($request->has('return')){
                return redirect()->to($request->get('return'));
            }

            return redirect()->action('HomeController@admin');
        }
        $this->incrementLoginAttempts($request);
        $attempts_count = $this->limiter()->attempts($this->throttleKey($request));
        if($attempts_count >= $this->captchaRequiredAttemptCount){
            session()->put('captcha_required', 'required|');
        }

        return redirect()->back()->withInput($request->except('captcha'))->withErrors([
                                                                                          'error' => Lang::get('auth.failed'),
                                                                                      ]);
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm');
    }
}
