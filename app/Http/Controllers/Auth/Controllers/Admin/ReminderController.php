<?php

namespace Sedehi\Http\Controllers\Auth\Controllers\Admin;

use Sedehi\Http\Controllers\Auth\Mail\PasswordReminderMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Sedehi\Http\Controllers\Auth\Models\PasswordReminder;
use Sedehi\Http\Controllers\Auth\Requests\Admin\AuthRequest;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\User\Models\User;

class ReminderController extends Controller
{

    public function reminder(AuthRequest $request)
    {
        $reminderAction = action('Auth\Controllers\Admin\AuthController@showLoginForm').'#login_password_reset';
        $user = User::where('email', $request->get('email'))->first();
        if(is_null($user)){
            return redirect()->to($reminderAction)->withInput($request->only(['email']))
                             ->with('error', 'ایمیل وارد شده صحیح نمی باشد');
        }
        if($user->ban == 1){
            return redirect()->to($reminderAction)->withInput($request->only(['email']))
                             ->with('error', 'حساب کاربری شما مسدود شده است');
        }
        $reminder = PasswordReminder::Where('email', $request->get('email'))->first();
        if(!is_null($reminder)){
            if($reminder->created_at->diffInMinutes() < 10){
                return redirect()->to($reminderAction)
                                 ->with('error', 'لطفا پوشه spam را چک کنید اگر ایمیلی دریافت نکردید لطفا '.(10 - $reminder->created_at->diffInMinutes()).' دقیقه دیگر درخواست رمز عبور دهید');
            }
        }
        PasswordReminder::where('created_at', '<', Carbon::now()->addMinutes(config('auth.password.expire')))
                        ->orWhere('email', $request->get('email'))->delete();
        $reminder             = new PasswordReminder();
        $reminder->email      = $request->get('email');
        $reminder->token      = str_random(32);
        $reminder->created_at = Carbon::now();
        $reminder->save();
        Mail::to($request->get('email'))->send(new PasswordReminderMail($reminder, true));

        return redirect()->to($reminderAction)
                         ->with('success', 'ایمیل بازیابی رمز عبور ارسال شد لطفا ایمیل خود را چک کنید');
    }

    public function showResetPasswordForm($token)
    {
        return view('Auth.views.admin.auth.reset-password')->with('token', $token);
    }

    public function resetPassword(AuthRequest $request)
    {
        $resetAction = 'Auth\Controllers\Admin\ReminderController@showResetPasswordForm';
        $user = User::where('email', $request->get('email'))->first();
        if(is_null($user)){
            return redirect()->action($resetAction, [$request->get('token')])
                             ->with('error', 'ایمیل وارد شده صحیح نمی باشد');
        }
        if($user->ban == 1){
            return redirect()->action($resetAction, [$request->get('token')])
                             ->with('error', 'حساب کاربری شما مسدود شده است');
        }
        $reminder = PasswordReminder::where('email', $user->email)->where('token', $request->get('token'))->first();
        if(is_null($reminder)){
            return redirect()->action($resetAction, [$request->get('token')])
                             ->with('error', 'اطلاعات وارد شده صحیح نمی باشد');
        }
        if($reminder->created_at->addMinutes(config('auth.passwords.users.expire'))->timestamp < time()){
            $reminder->delete();

            return redirect()->action($resetAction, [$request->get('token')])->with('error', 'کد یکتا منقضی شده');
        }
        $user->password = bcrypt($request->get('password'));
        $user->save();
        $reminder->delete();

        return redirect()->action('Auth\Controllers\Admin\AuthController@showLoginForm')
                         ->with('success', 'رمز عبور با موفقیت تغییر کرد');
    }
}
