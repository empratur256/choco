<?php

namespace Sedehi\Http\Controllers\Auth\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('auth.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Auth::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Auth::where('author_id',auth()->user()->id)->find($this->route()->parameter('auth'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        if($action == 'reminder'){
            $this->redirect = action('Auth\Controllers\Admin\AuthController@showLoginForm').'#login_password_reset';
        }
        switch($action){
            case'login':
                return [
                    'email'    => 'required|email',
                    'password' => 'required|min:6',
                    'captcha'  => $this->session()->get('captcha_required').'captcha',
                ];
            break;
            case'reminder':
                return [
                    'email'   => 'required|email',
                    'captcha' => 'required|captcha',
                ];
            break;
            case'resetPassword':
                return [
                    'email'    => 'required|email',
                    'password' => 'required|min:6|confirmed',
                    'token'    => 'required',
                ];
            break;
        }

        return [];
    }

}
