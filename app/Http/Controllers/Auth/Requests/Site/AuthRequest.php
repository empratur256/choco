<?php

namespace Sedehi\Http\Controllers\Auth\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AuthRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case'login':
                return [
                    'email'    => 'required|email',
                    'password' => 'required|min:6',
                    'captcha'  => $this->session()->get('captcha_required').'captcha',
                ];
                break;
            case'signup':
                $rules = [
                    'name'      => 'required',
                    'family'    => 'required',
                    'agreement' => 'accepted',
                    'email'     => [
                        'required',
                        'email',
                        'max:255',
                        Rule::unique('users')->where(function ($query) {
                            $query->whereNull('deleted_at');
                        }),
                    ],
                    'mobile'     => [
                        'required',
                        'iran_mobile',
                        Rule::unique('users')->where(function ($query) {
                            $query->whereNull('deleted_at');
                        }),
                    ],
                    'password'  => 'required|min:6|confirmed',
                ];
                if ($this->session()->get('signup_attempts') > 1) {
                    return array_add($rules, 'captcha', 'required|captcha');
                }

                return $rules;
                break;
        }

        return [];
    }

    public function response(array $errors)
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        if (request()->has('return')) {
            session()->flash('return', request()->get('return'));
        }
        switch ($action) {
            case 'login':
                return $this->redirector->to(action('Auth\Controllers\Site\AuthController@showLoginForm'))
                                        ->withInput()
                                        ->withErrors($errors, $this->errorBag);
                break;
            case 'signup':
                return $this->redirector->to(action('Auth\Controllers\Site\AuthController@showSignupForm'))
                                        ->withInput()
                                        ->withErrors($errors, $this->errorBag);
                break;
        }
    }

}
