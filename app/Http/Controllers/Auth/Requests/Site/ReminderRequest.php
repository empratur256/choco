<?php

namespace Sedehi\Http\Controllers\Auth\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class ReminderRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case'reminder':
                return [
                    'email'   => 'required|email',
                    'captcha' => 'required|captcha',
                ];
            break;
            case'resetPassword':
                return [
                    'email'    => 'required|email',
                    'password' => 'required|min:6|confirmed',
                    'token'    => 'required',
                ];
            break;
        }

        return [];
    }

}
