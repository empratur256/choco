<?php

namespace Sedehi\Http\Controllers\Auth\tests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Lang;
use Sedehi\Http\Controllers\Role\Models\Role;
use Sedehi\Http\Controllers\User\Models\User;
use TestCase;

class LoginAdminTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * test barae check kardan karbari ke role nadare.
     * @test
     * @return void
     */
    public function login_admin_without_role()
    {
        $user           = new User();
        $user->email    = 'test@site.com';
        $user->name     = 'نوید';
        $user->family   = 'سه دهی';
        $user->password = bcrypt(123456);
        $user->save();
        $this->visit(config('site.admin').'/login')->type('test@site.com', 'email')->type('123456', 'password')
             ->press('ورود به مدیریت')->dontSeeIsAuthenticated();
    }

    /**
     * test barae login admin.
     * @test
     * @return void
     */
    public function test_login_admin_with_role()
    {
        $user           = new User();
        $user->email    = 'test@site.com';
        $user->name     = 'نوید';
        $user->family   = 'سه دهی';
        $user->password = bcrypt(123456);
        $user->save();
        $role             = new Role();
        $role->name       = 'تست';
        $role->permission = serialize(['']);
        $role->save();
        $user->roles()->attach($role);
        $this->get('get', ['Auth\Controllers\Admin\AuthController@showLoginForm']);
        $this->type('test@site.com', 'email')->type('123456', 'password')->press('ورود به مدیریت')
             ->seePageIs(config('site.admin'));
    }

}
