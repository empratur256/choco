<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['throttle'],
              'namespace'  => 'Auth\Controllers\Admin',
             ], function(){
    Route::get('login', 'AuthController@showLoginForm');
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');
    Route::post('reminder', 'ReminderController@reminder');
    Route::get('reset-password/{token}', 'ReminderController@showResetPasswordForm');
    Route::post('reset-password', 'ReminderController@resetPassword');
});
Route::group(['middleware' => 'throttle'], function(){
    Route::get('login', 'Auth\Controllers\Site\AuthController@showLoginForm');
    Route::post('login', 'Auth\Controllers\Site\AuthController@login');
    Route::get('logout', 'Auth\Controllers\Site\AuthController@logout');
    Route::get('reminder', 'Auth\Controllers\Site\ReminderController@showReminderForm');
    Route::post('reminder', 'Auth\Controllers\Site\ReminderController@reminder');
    Route::get('reset-password/{token}', 'Auth\Controllers\Site\ReminderController@showResetPasswordForm');
    Route::post('reset-password', 'Auth\Controllers\Site\ReminderController@resetPassword');
    Route::get('register', 'Auth\Controllers\Site\AuthController@showSignupForm');
    Route::post('register', 'Auth\Controllers\Site\AuthController@signup');
});