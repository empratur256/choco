<?php

namespace Sedehi\Http\Controllers\Auth\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReminder extends Model
{
    public    $table      = 'password_reminder';
    protected $primaryKey = 'email';
}
