@extends('views.layouts.site.master')
@section('title', trans('site.user.login'))

@section('content')
<div class="red_box">
    <div class="container">
        <h1>@lang('site.user.login')</h1>
        <div class="divider-title"></div>
        <p>@lang('site.user.login_text')</p>
    </div>
</div>
<div class="login">
    <div class="container">
        @include('views.errors.errors')
        <div class="row">
            <div class="login_bg col-lg-9 col-md-8 col-sm-8">
                <img class="img-responsive" src="{{ asset('assets/site/img/login-bg.png') }}">
                <div class="comment col-lg-4">
                    {!!  $setting->description !!}
                </div>
            </div>
            <div class="login_form col-lg-3 col-md-4 col-sm-4">
                <div class="white">
                    {{Form::open(['action' => 'Auth\Controllers\Site\AuthController@login'])}}

                        @if(request()->has('return') || session()->has('return'))
                            @php $return = null @endphp
                            @if(session()->has('return'))
                                @php $return = session('return') @endphp
                            @endif
                            @if(request()->has('return'))
                                @php $return = request()->get('return') @endphp
                            @endif
                            {!! Form::hidden('return',$return) !!}
                        @endif

                        <div class="green col-lg-12">
                            <div class="col-lg-12" style="padding-bottom: 2.2em">
                                <h4 style="font-size: 16px">@lang('site.user.login_to_account')</h4>
                            </div>
                            <div class="form-group col-lg-12">
                                {{ Form::label('email',trans('site.user.email')) }}
                                {{ Form::text('email', null, ['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                            <div class="form-group col-lg-12">
                                {{ Form::label('password',trans('site.password')) }}
                                {{ Form::password('password',['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-lock"></span>
                            </div>

                            @if(session()->has('captcha_required'))
                                <div class="form-group col-lg-12">
                                    {{ Form::label('captcha',trans('site.captcha')) }}
                                    {!!Form::text('captcha',null, ['class'=>'form-control col-md-4'])!!}
                                </div>
                                <div class="form-group col-lg-12">
                                    {!! captcha_img() !!}
                                </div>
                            @endif

                            <div class="checkbox checkbox-inline checkbox-circle col-lg-12">
                                {{ Form::checkbox('remember_me',1,null,['id' => "checkbox8"]) }}
                                <label for="checkbox8"></label>
                                <string>@lang('site.user.remember')</string>
                            </div>
                            <div class="col-lg-12 forget_password">
                                <a href="{{action('Auth\Controllers\Site\ReminderController@showReminderForm')}}">
                                    <string style="color:white">@lang('site.user.remember_button')</string>
                                </a>
                            </div>

                        </div>
                        <div class="">
                            <button type="submit" class="btn submit"><span>@lang('site.user.login')</span>
                            </button>
                        </div>
                    {{ Form::close() }}

                    <div>
                        <p style="color:black; padding:0 0 3.4em 0; text-align:center">@lang('site.not_registered')
                            <a href="{{ action('Auth\Controllers\Site\AuthController@showSignupForm') }}">
                                <span style="color:#33beff">@lang('site.do_register')</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                 
                 
@endsection

@push('css')
   {{ Html::style('assets/site/css/login.css') }}
@endpush