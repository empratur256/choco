@extends('views.layouts.site.master')
@section('title', trans('site.user.reminder'))
@section('content')

    <div class="top-section" id="background-pattern">
        <div class="container">
            <div class="bread">
                <span class="place">@lang('site.home')</span>
                <span class="fa fa-angle-double-left divider"></span>
                <span class="place">@lang('site.user.reminder')</span>
            </div>
        </div>
    </div>
    <div class="register-page">
        <div class="container with-border panel">
            <div class="row">
                <div class="col-xs-12">
                    <div class="top-panel border-bottom">
                        <div class="icon-container">
                            <span class="fa fa-user-circle"></span>
                        </div>
                        <div class="title">@lang('site.user.reminder')</div>
                        <div class="sub-title">@lang('site.user.reminder_change_password_text')</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="body-panel">
                        <div class="half-panel container">
                            @include('views.errors.errors')
                            {{Form::open(['action' => 'Auth\Controllers\Site\ReminderController@resetPassword'])}}
                            {!! Form::hidden('token',$token) !!}
                            <div class="form-group col-xs-12">
                                {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('site.user.email')]) }}
                            </div>
                            <div class="form-group col-sm-6 col-xs-12">
                                {{ Form::password('password',  ['class' => 'form-control', 'placeholder' =>trans('site.password')]) }}
                            </div>
                            <div class="form-group col-sm-6 col-xs-12">
                                {{ Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('site.password_confirmation')]) }}
                            </div>

                            <div class="col-xs-12" style="text-align: right;">
                                <button class="btn purple-btn red_bg white_text" type="submit">@lang('site.user.change_password')</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/reminder.css') }}
@endpush