﻿@extends('views.layouts.site.master')
@section('title',trans('site.signup_title'))
@section('content')

    <div class="red_box">
        <div  class="container">
            <h1>@lang('site.signup_title')</h1>
            <div class="divider-title"></div>
            <p>@lang('site.user.signup_text')</p>
        </div>
    </div>
    <div class="register">
        <div style="padding-top:3em;padding-bottom: 3em" class="container">
            @include('views.errors.errors')
            <div class="row">
                <div class="register-img col-lg-7 col-md-7 col-sm-7 col-xs-12  ">
                    <img class="img-responsive" src="{{ asset('assets/site/img/register-bg.png') }}">
                    <div class="comment col-lg-4 col-sm-7 col-xs-10">
                        {!! $setting->description !!}
                    </div>
                </div>
                <div class="form col-lg-5 col-md-5 col-sm-5 col-xs-12 ">
                    <h4>@lang('site.create_new_account')</h4>
                    {{Form::open(['action' => 'Auth\Controllers\Site\AuthController@signup'])}}
                        @if(request()->has('return') || session()->has('return'))
                            @php $return = null @endphp
                            @if(session()->has('return'))
                                @php $return = session('return') @endphp
                            @endif
                            @if(request()->has('return'))
                                @php $return = request()->get('return') @endphp
                            @endif
                            {!! Form::hidden('return',$return) !!}
                        @endif

                        <div class="row padding-bottom-1">
                            <div class="col-lg-6 col-lg-push-6 col-md-12 col-xs-12 form-group ">
                                <label for="name">@lang('site.user.name')</label>
                                {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
                                <span class="glyphicon glyphicon-user"></span>
                            </div>
                            <div class="col-lg-6 col-lg-pull-6 col-md-12 col-xs-12 form-group">
                                <label for="family">@lang('site.user.family')</label>
                                {{ Form::text('family', null, ['class' => 'form-control']) }}
                            </div>
                        </div>
                        <div class="row padding-bottom-1">
                            <div class="col-lg-6 col-lg-push-6 col-md-12 col-xs-12 form-group">
                                <label for="email">@lang('site.user.email')</label>
                                {{ Form::text('email', null, ['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-envelope"></span>
                            </div>
                            <div class="col-lg-6 col-lg-pull-6 col-md-12 col-xs-12 form-group">
                                <label for="telephone">@lang('site.mobile')</label>
                                {{ Form::tel('mobile', null, ['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-phone"></span>
                            </div>
                        </div>
                        <div class="row padding-bottom-1">
                            <div class="col-lg-6 col-lg-push-6 col-md-12 col-xs-12 form-group">
                                <label for="telephone">@lang('site.password')</label>
                                {{ Form::password('password',  ['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-lock"></span>
                            </div>
                            <div class="col-lg-6 col-lg-pull-6 col-md-12 col-xs-12 form-group">
                                <label for="password">@lang('site.password_confirmation')</label>
                                {{ Form::password('password_confirmation',  ['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-lock"></span>
                            </div>
                        </div>

                        @if(session()->get('signup_attempts') > 1)
                            <div class="row">
                                <div class="col-lg-6 col-lg-push-6 col-md-12 col-xs-12 form-group">
                                    <label for="captcha">@lang('site.captcha')</label>
                                    {{ Form::text('captcha', null, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-lg-6 col-lg-push-6 col-md-12 col-xs-12 form-group">
                                    {!! captcha_img() !!}
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            <div class="checkbox checkbox-info checkbox-circle col-lg-12">
                                {{ Form::checkbox('agreement', 1, null, ['id' => 'checkbox8']) }}
                                <label for="checkbox8"></label>
                                <string>
                                    <a href="{!! action('Page\Controllers\Site\PageController@show',[1,utf8_slug(trans('site.rules'))]) !!}">
                                        <text style="color:#33beff">@lang('site.rules')</text>
                                    </a>
                                    @lang('site.accept_rules_partial')
                                </string>
                            </div>
                        </div>
                        <div class="row">
                            <div class="submit-reg col-lg-12 text-center">
                                <button type="submit" class="btn btn-danger">@lang('site.signup_btn')</button>
                                <p>
                                    <br>@lang('site.already_registered')
                                    <a href="{{ action('Auth\Controllers\Site\AuthController@showLoginForm') }}">
                                        <span style="color:#33beff">@lang('site.login')</span>
                                    </a>
                                </p>
                            </div>
                        </div>

                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>

@endsection

@push('css')
    {{ Html::style('assets/site/css/register.css') }}
@endpush