<!DOCTYPE html>
<!--[if lte IE 9]>
<html class="lte-ie9 app_theme_d" lang="fa" dir="rtl"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="fa" dir="rtl" class="app_theme_d"><!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>بازیابی رمز عبور</title>
    <meta name="robots" content="noindex">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>

    {!! Html::style('assets/admin/css/uikit.rtl.css') !!}
    {!! Html::style('assets/admin/css/login_page.min.css') !!}
    {!! Html::style('assets/admin/css/sedehi.css') !!}

</head>
<body class="login_page">
<div class="login_page_wrapper">
    @include('views.errors.errors')
    <div class="md-card" id="login_card">
        <div class="md-card-content large-padding" id="password_reset">
            <h2 class="heading_a uk-margin-large-bottom">بازیابی رمز عبور</h2>
            {!! Form::open(['action' => 'Auth\Controllers\Admin\ReminderController@resetPassword']) !!}
            <div class="uk-form-row">
                {!! Form::label('email','ایمیل') !!}
                {!! Form::text('email',null,['class' => 'md-input','id' => 'email']) !!}
            </div>
            <div class="uk-form-row">
                {!! Form::label('password','رمز عبور') !!}
                {!! Form::password('password',['class' => 'md-input','id' => 'password']) !!}
            </div>
            <div class="uk-form-row">
                {!! Form::label('password_confirmation','تکرار رمز عبور') !!}
                {!! Form::password('password_confirmation',['class' => 'md-input','id' => 'password_confirmation']) !!}
            </div>
            {!! Form::hidden('token',$token) !!}
            <div class="uk-margin-medium-top">
                <button type="submit" class="md-btn md-btn-primary md-btn-block">تغییر رمز عبور</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

</div>

{!! Html::script('assets/admin/js/common.min.js') !!}
{!! Html::script('assets/admin/js/uikit_custom.min.js') !!}
{!! Html::script('assets/admin/js/altair_admin_common.min.js') !!}
{!! Html::script('assets/admin/js/pages/login.min.js') !!}

@include('views.errors.notifications')
</body>

</html>