<!DOCTYPE html>
<!--[if lte IE 9]>
<html class="lte-ie9 app_theme_d" lang="fa" dir="rtl"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="fa" dir="rtl" class="app_theme_d"><!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>مدیریت</title>
    <meta name="robots" content="noindex">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>

    {!! Html::style('assets/admin/css/uikit.rtl.css') !!}
    {!! Html::style('assets/admin/css/login_page.min.css') !!}
    {!! Html::style('assets/admin/css/sedehi.css') !!}

</head>
<body class="login_page">
<div class="login_page_wrapper">
    @include('views.errors.errors')
    <div class="md-card" id="login_card">
        <div class="md-card-content large-padding" id="login_form">
            <div class="login_heading">
                <div class="user_avatar"></div>
            </div>

            {!! Form::open(['action' => 'Auth\Controllers\Admin\AuthController@login']) !!}
            <div class="uk-form-row">
                {!! Form::label('email','ایمیل') !!}
                {!! Form::text('email',null,['class' => 'md-input','id' => 'email']) !!}
            </div>
            <div class="uk-form-row">
                {!! Form::label('password','رمزعبور') !!}
                {!! Form::password('password',['class' => 'md-input','id' => 'password']) !!}
            </div>
            @if(session()->has('captcha_required'))
                <div class="uk-form-row" id="login_captcha_container">
                    {!! Form::label('captcha','عبارت امنیتی') !!}
                    {!!Form::text('captcha',null, ['class'=>'md-input'])!!}
                </div>
            @endif
            <div class="uk-margin-medium-top">
                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">ورود به مدیریت</button>
            </div>

            <div class="uk-margin-top">
                <a href="#" id="password_reset_show">رمزعبور را فراموش کردم!</a>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="md-card-content large-padding" id="login_password_reset" style="display: none">
            <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
            <h2 class="heading_a uk-margin-large-bottom">بازیابی رمز عبور</h2>
            {!! Form::open(['action' => 'Auth\Controllers\Admin\ReminderController@reminder']) !!}
            <div class="uk-form-row">
                {!! Form::label('email','ایمیل') !!}
                {!! Form::text('email',null,['class' => 'md-input','id' => 'email']) !!}
            </div>
            <div class="uk-form-row">
                {!! Form::label('captcha','عبارت امنیتی') !!}
                {!!Form::text('captcha',null, ['class'=>'md-input'])!!}
                <img src="{!! captcha_src() !!}" alt="captcha" class="uk-align-center" id="forget_password_captcha">
            </div>
            <div class="uk-margin-medium-top">
                <button type="submit" class="md-btn md-btn-primary md-btn-block">بازیابی رمز عبور</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

</div>

{!! Html::script('assets/admin/js/common.min.js') !!}
{!! Html::script('assets/admin/js/uikit_custom.min.js') !!}
{!! Html::script('assets/admin/js/altair_admin_common.min.js') !!}
{!! Html::script('assets/admin/js/pages/login.min.js') !!}
<script>
    $(document).ready(function () {
        $('#forget_password_captcha').clone().appendTo($('#login_captcha_container'))
    });
</script>
@include('views.errors.notifications')
</body>

</html>