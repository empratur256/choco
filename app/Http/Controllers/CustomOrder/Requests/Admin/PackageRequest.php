<?php

namespace Sedehi\Http\Controllers\CustomOrder\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;

class PackageRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('customorder.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return CustomOrder::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return CustomOrder::where('author_id',auth()->user()->id)->find($this->route()->parameter('customorder'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
                break;
            case 'store':
                return [
                    'title'                => 'required',
                    'admin_title'          => 'required',
                    'price'                => 'required|numeric|min:0',
                    'major_price'          => 'required|numeric|min:0',
                    'picture'              => 'required|image',
                    'upload_picture_price' => 'required_with:upload_picture|numeric|min:0',
                ];
                break;

            case 'update':
                return [
                    'title'                => 'required',
                    'admin_title'          => 'required',
                    'price'                => 'required|numeric|min:0',
                    'major_price'          => 'required|numeric|min:0',
                    'picture'              => 'image',
                    'upload_picture_price' => 'required_with:upload_picture|numeric|min:0',
                ];
                break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'upload_picture_price.min' => 'قیمت چاپ نباید کوچکتر از صفر باشد',
            'upload_picture_price.required_with' => 'لطفا قیمت چاپ تصویر را وارد کنید',
        ];
    }
}
