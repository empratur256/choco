<?php

namespace Sedehi\Http\Controllers\CustomOrder\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;

class LayoutRequest extends FormRequest
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('customorder.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return CustomOrder::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return CustomOrder::where('author_id',auth()->user()->id)->find($this->route()->parameter('customorder'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
                break;
            case 'store':
                return [
                    'title'                => 'required',
                    'price'                => 'required|numeric|min:0',
                    'major_price'          => 'required|numeric|min:0',
                    'types'                => 'required|array|min:1',
                    'picture'              => 'required|image',
                    'package_price'        => 'required_without:select_package|numeric|min:0',
                    'package_major_price'  => 'required_without:select_package|numeric|min:0',
                    'amount.*.coefficient' => [
                        'required_with:amount.*.amount_id',
                        'numeric',
                        'min:1'
                    ],
                    'packages'             => 'required_with:select_package',
                ];
                break;

            case 'update':
                return [
                    'title'                => 'required',
                    'price'                => 'required|numeric|min:0',
                    'major_price'          => 'required|numeric|min:0',
                    'types'                => 'required|array|min:1',
                    'picture'              => 'image',
                    'package_price'        => 'required_without:select_package|numeric|min:0',
                    'package_major_price'  => 'required_without:select_package|numeric|min:0',
                    'amount.*.coefficient' => [
                        'required_with:amount.*.amount_id',
                        'numeric',
                        'min:1'
                    ],
                    'packages'             => 'required_with:select_package',
                ];
                break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'package_price.required_without'       => 'لطفا قیمت بسته بندی را وارد کنید',
            'package_major_price.required_without' => 'لطفا قیمت عمده بسته بندی را وارد کنید',
            'amount.*.coefficient.required_with'   => 'لطفا اطلاعات ضریب محاسبه قیمت تب میزان سفارش را تکمیل کنید',
            'packages.required_with'               => 'حداقل یک بسته بندی باید انتخاب شود',
            'types.required'                       => 'حداقل یک دسته بندی محصول باید انتخاب شود',
        ];
    }

}
