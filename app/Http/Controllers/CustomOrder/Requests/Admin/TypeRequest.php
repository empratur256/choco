<?php

namespace Sedehi\Http\Controllers\CustomOrder\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TypeRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
                break;
            case 'store':
                return [
                    'title'     =>  'required',
                    'brains'    =>  'required_if:has_brain,1|array',
                ];
            case 'update':
                return [
                    'title'     =>  'required',
                    'brains'    =>  'required_if:has_brain,1|array',
                ];
                break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'brains.required_if'    =>  'حداقل یک مغز برای محصول حجم دار باید انتخاب شود'
        ];
    }
}
