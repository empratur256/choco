<?php

namespace Sedehi\Http\Controllers\CustomOrder\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BaseFlavorRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
                break;
            case 'store':
                return [
                    'title'                    => 'required',
                    'price'                    => 'required|numeric|min:1',
                    'major_price'              => 'required|numeric|min:1',
                    'flavor_price'             => 'required_with:flavor|numeric|min:1',
                    'flavor_major_price'       => 'required_with:flavor|numeric|min:1',
                    'brain_price'              => 'required|numeric|min:1',
                    'brain_major_price'        => 'required|numeric|min:1',
                    'flavor_brain_price'       => 'required_with:flavor|numeric|min:1',
                    'flavor_brain_major_price' => 'required_with:flavor|numeric|min:1',
                ];
                break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'flavor_price.required_with'                 => 'لطفا قیمت طعم مکمل را وارد کنید',
            'flavor_major_price.required_with'           => 'لطفا قیمت عمده طعم مکمل را وارد کنید',
            'flavor_brain_price.required_with'           => 'لطفا قیمت طعم مکمل مغز دار را وارد کنید',
            'flavor_brain_major_price.required_with'     => 'لطفا قیمت عمده طعم مکمل مغز دار را وارد کنید',
        ];
    }

}
