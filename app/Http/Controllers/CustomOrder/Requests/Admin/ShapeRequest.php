<?php

namespace Sedehi\Http\Controllers\CustomOrder\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;

class ShapeRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
                break;
            case 'store':
                return [
                    'title'   => 'required',
                    'picture' => 'required|mimes:png|dimensions:width=500,height=500',
                ];
                break;

            case 'update':
                return [
                    'title'   => 'required',
                    'picture' => 'mimes:png|dimensions:width=500,height=500',
                ];
                break;
        }

        return [];
    }

}
