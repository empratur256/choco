<?php

namespace Sedehi\Http\Controllers\CustomOrder\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;

class RoutineRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('customorder.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return CustomOrder::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return CustomOrder::where('author_id',auth()->user()->id)->find($this->route()->parameter('customorder'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return [
                    'deleteId' => 'required|array',
                ];
                break;
            case 'store':
                return [
                    'title'            => 'required',
                    'min_order'        => 'required|numeric|min:1',
                    'max_order'        => 'required|numeric|min:0',
                    'preparation_time' => 'required|numeric|min:0',
                    'order'            => 'nullable|numeric|min:1',
                    'major'            => 'required|numeric',
                    'picture'          => 'required|image',
                    'materials'        => 'required|array',
                    'types'            => 'required|array',
                    'layout'           => 'required|array',
                    'layout.*.id'      => 'required_with:layout.*.upload_picture,layout.*.change_shape,layout.*.change_weight',

                ];
                break;

            case 'update':
                return [
                    'title'            => 'required',
                    'min_order'        => 'required|numeric|min:1',
                    'max_order'        => 'required|numeric|min:0',
                    'preparation_time' => 'required|numeric|min:0',
                    'order'            => 'nullable|numeric|min:1',
                    'major'            => 'required|numeric',
                    'picture'          => 'image',
                    'types'            => 'required|array',
                    'materials'        => 'required|array',
                    'layout'           => 'required|array',
                    'layout.*.id'      => 'required_with:layout.*.upload_picture,layout.*.change_shape,layout.*.change_weight',

                ];
                break;
        }

        return [];
    }

    public function messages()
    {
        return [
            'layout.*.id.required_with' => 'لطفا اطلاعات چینش های این روال را تکمیل کنید',
            'layout.required'           => 'حداقل یک چینش برای این روال انتخاب کنید',
            'materials.required'            => 'حداقل یک دسته بندی محصول برای این روال انتخاب کنید',
            'types.required'            => 'حداقل یک نوع محصول برای این روال انتخاب کنید',
        ];
    }
}
