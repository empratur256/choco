<?php

namespace Sedehi\Http\Controllers\CustomOrder\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;
use Sedehi\Http\Controllers\Setting\Models\Setting;

class OrderRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);

        switch($action){
            case 'saveShipping':
                return [
                    'ship_address'    => 'required|numeric',
                    'ship'            => 'required|numeric',
                    'invoice_request' => 'required',
                ];
            break;
            case 'submit':
                return [
                    'payment_type' => 'required|in:cash,online,card,account',
                ];
            break;
        }

        return [];
    }
}
