<?php
Route::group([
                 'prefix'     => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace'  => 'CustomOrder\Controllers\Admin',
             ], function () {
    Route::get('custom/order/setting', 'CustomOrderController@setting');
    Route::post('custom/order/setting', 'CustomOrderController@saveSetting');
    Route::resource('custom/order/material', 'MaterialController', ['except' => ['show']]);
    Route::resource('custom/order/material.base', 'BaseFlavorController', ['except' => ['show']]);
    Route::resource('custom/order/type', 'TypeController', ['except' => ['show']]);
    Route::resource('custom/order/routine', 'RoutineController', ['except' => ['show']]);
    Route::resource('custom/order/layout', 'LayoutController', ['except' => ['show']]);
    Route::resource('custom/order/layout.cell', 'CellController', ['except' => ['show']]);
    Route::resource('custom/order/shape', 'ShapeController', ['except' => ['show']]);
    Route::resource('custom/order/flavor', 'FlavorController', ['except' => ['show']]);
    Route::resource('custom/order/portfolio', 'PortfolioController', ['except' => ['show']]);
    Route::resource('custom/order/weight', 'WeightController', ['except' => ['show']]);
    Route::resource('custom/order/amount', 'AmountController', ['except' => ['show']]);
    Route::resource('custom/order/brain', 'BrainController', ['except' => ['show']]);
    Route::resource('custom/order/package-category', 'PackageCategoryController', ['except' => ['show']]);
    Route::resource('custom/order/package', 'PackageController', ['except' => ['show']]);
});
Route::group([
                 'prefix'    => 'custom/order',
                 'namespace' => 'CustomOrder\Controllers\Site',
             ], function () {

    Route::get('/', 'OrderController@index');
    Route::get('/set-routine/{id}/{title?}', 'OrderController@setRoutine');
    Route::post('/type', 'OrderController@saveType');
    Route::get('/layout', 'OrderController@layout');
    Route::post('/layout', 'OrderController@saveLayout');
    Route::get('/cells', 'OrderController@cells');
    Route::post('/cells', 'OrderController@saveCells');
    Route::get('/packaging', 'OrderController@packaging');
    Route::post('/save-packaging', 'OrderController@savePackaging');
    Route::get('preview', 'OrderController@preview');
    Route::post('/upload-cell-picture', 'OrderController@uploadCellPicture');
    Route::post('/remove-cell-picture', 'OrderController@removeCellPicture');
    Route::post('/upload-package-picture', 'OrderController@uploadPackagePicture');
    Route::post('/remove-package-picture', 'OrderController@removePackagePicture');
    Route::post('/get-routine-types', 'OrderController@getRoutineTypes');
    Route::post('/get-base-flavors', 'OrderController@getBaseFlavors');
    Route::post('/get-flavor-and-brain', 'OrderController@getFlavorAndBrain');

    Route::get('/portfolio', 'OrderController@portfolio');
    Route::get('shipping', 'OrderController@shipping');
    Route::post('shipping', 'OrderController@saveShipping');
    Route::get('payment', 'OrderController@payment');
    Route::post('payment', 'OrderController@submit');

    Route::group(['middleware' => 'dashboard'],function() {
        Route::get('reorder/{id}','OrderController@reorder');
    });
});