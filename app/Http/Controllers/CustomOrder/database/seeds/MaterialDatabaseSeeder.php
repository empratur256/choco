<?php

namespace Sedehi\Http\Controllers\CustomOrder\database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Sedehi\Http\Controllers\CustomOrder\Models\Material;


class MaterialDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $datas = ['محصول شکلاتی', 'محصول کاکائویی'];

        foreach ($datas as $data) {
            $item        = new Material();
            $item->title = $data;
            $item->save();
        }
    }

}
