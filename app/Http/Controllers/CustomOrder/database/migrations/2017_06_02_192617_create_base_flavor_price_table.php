<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseFlavorPriceTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_base_flavor_price', function (Blueprint $table) {
            $table->integer('base_id')->unsigned();
            $table->integer('flavor_id')->unsigned();
            $table->bigInteger('price')->unsigned()->index();
            $table->primary(['base_id', 'flavor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_base_flavor_price');
    }
}
