<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutShapeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_layout_shape', function (Blueprint $table) {
            $table->integer('layout_id')->unsigned();
            $table->integer('shape_id')->unsigned();
            $table->primary(['layout_id', 'shape_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_layout_shape');
    }
}
