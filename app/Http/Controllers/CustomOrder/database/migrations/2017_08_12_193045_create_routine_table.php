<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutineTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_routine', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->integer('min_order');
            $table->integer('max_order')->default(0);
            $table->integer('preparation_time');
            $table->boolean('major')->default(0);
            $table->text('description');
            $table->string('picture');
            $table->enum('layout_select', ['all', 'custom']);
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_routine');
    }
}
