<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPackageTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_order_package', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->index();
            $table->text('description')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->boolean('upload_picture')->default(0);
            $table->integer('upload_picture_price')->unsigned()->default(0);
            $table->boolean('custom_design')->default(0);
            $table->softDeletes();
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
