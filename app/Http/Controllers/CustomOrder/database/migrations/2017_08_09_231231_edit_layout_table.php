<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditLayoutTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_order_layout', function (Blueprint $table) {
            $table->integer('weight')->unsigned()->index();
            $table->integer('major_price')->unsigned()->default(0);
            $table->dropColumn('upload_capacity');
            $table->boolean('upload_picture')->default(0)->index();
            $table->boolean('select_package')->default(1)->index();
            $table->integer('package_price')->unsigned()->default(0);
            $table->integer('package_major_price')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
