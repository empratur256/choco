<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutAmountTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_layout_amount', function (Blueprint $table) {
            $table->integer('layout_id')->unsigned();
            $table->integer('amount_id')->unsigned();
            $table->float('coefficient', 8, 0)->default(1);
            $table->primary(['layout_id', 'amount_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_layout_amount');
    }
}
