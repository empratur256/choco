<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomOrderPageDescription extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('setting')){
            Schema::table('setting', function(Blueprint $table){
                $table->text('custom_order_description')->nullable()->default(null);
            });
        }else{
            Schema::create('setting', function(Blueprint $table){
                $table->text('custom_order_description')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
