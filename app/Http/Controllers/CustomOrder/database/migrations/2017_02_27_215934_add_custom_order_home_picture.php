<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomOrderHomePicture extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('setting')){
            Schema::table('setting', function(Blueprint $table){
                $table->text('custom_order_home_picture')->nullable()->default(null);
            });
        }else{
            Schema::create('setting', function(Blueprint $table){
                $table->text('custom_order_home_picture')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
