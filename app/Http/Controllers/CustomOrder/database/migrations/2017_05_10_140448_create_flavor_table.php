<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlavorTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_flavor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->text('description')->nullable()->default(null);
            $table->boolean('active')->default(1)->index();
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_flavor');
    }
}
