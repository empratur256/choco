<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutineLayoutsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_routine_layouts', function (Blueprint $table) {
            $table->integer('routine_id')->unsigned();
            $table->integer('layout_id')->unsigned();
            $table->boolean('upload_picture');
            $table->boolean('change_shape');
            $table->boolean('change_weight');
            $table->primary(['routine_id', 'layout_id']);
            $table->foreign('routine_id')->references('id')->on('custom_order_routine')->onDelete('cascade');
            $table->foreign('layout_id')->references('id')->on('custom_order_layout')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_routine_layouts');
    }
}
