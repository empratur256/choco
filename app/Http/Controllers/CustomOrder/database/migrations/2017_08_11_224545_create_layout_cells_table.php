<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutCellsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_layout_cells', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cell')->unsigned();
            $table->integer('layout_id')->unsigned();
            $table->integer('shape_id')->unsigned()->nullable()->default(null)->index();
            $table->integer('weight_id')->unsigned()->nullable()->default(null)->index();
            $table->unique(['cell', 'layout_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_layout_cells');
    }
}
