<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomOrderPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_package', function (Blueprint $table) {
            $table->increments('id');
            $table->string('admin_title')->index();
            $table->string('title')->index();
            $table->integer('price')->default(0)->unsigned();
            $table->integer('major_price')->default(0)->unsigned();
            $table->boolean('active')->default(1)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_package');
    }
}
