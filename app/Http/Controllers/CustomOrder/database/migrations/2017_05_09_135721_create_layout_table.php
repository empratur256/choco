<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLayoutTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_layout', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->index();
            $table->integer('price')->default(0)->unsigned();
            $table->integer('capacity')->default(0)->unsigned();
            $table->integer('upload_capacity')->default(0)->unsigned();
            $table->text('description')->nullable()->default(null);
            $table->boolean('active')->default(1)->index();
            $table->string('picture')->nullable()->default(null)->index();
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_layout');
    }
}
