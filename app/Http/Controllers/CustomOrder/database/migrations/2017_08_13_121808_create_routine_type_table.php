<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutineTypeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_routine_type', function (Blueprint $table) {
            $table->integer('routine_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->primary(['routine_id', 'type_id']);
            $table->foreign('routine_id')->references('id')->on('custom_order_routine')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('custom_order_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_routine_type');
    }
}
