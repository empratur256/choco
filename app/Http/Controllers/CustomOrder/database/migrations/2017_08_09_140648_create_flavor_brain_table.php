<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlavorBrainTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_flavor_brain', function (Blueprint $table) {
            $table->integer('brain_id')->unsigned();
            $table->integer('flavor_id')->unsigned();
            $table->primary(['brain_id', 'flavor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_flavor_brain');
    }
}
