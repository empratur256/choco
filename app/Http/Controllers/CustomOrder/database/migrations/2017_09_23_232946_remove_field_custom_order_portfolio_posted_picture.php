<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldCustomOrderPortfolioPostedPicture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_order_portfolio', function (Blueprint $table) {
            $table->dropColumn('posted_picture');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
