<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class Routine extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'custom_order_routine';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function layouts()
    {
        return $this->belongsToMany(Layout::class, 'custom_order_routine_layouts')->withPivot([
                                                                                                  'upload_picture',
                                                                                                  'change_shape',
                                                                                                  'change_weight'
                                                                                              ]);
    }

    public function materials()
    {
        return $this->belongsToMany(Material::class, 'custom_order_routine_material', 'routine_id', 'material_id');
    }

    public function types()
    {
        return $this->belongsToMany(Type::class, 'custom_order_routine_type', 'routine_id', 'type_id');
    }

    public function syncLayouts($layouts)
    {
        $items = [];
        foreach ($layouts as $layout) {
            $items[$layout['id']] = [
                'upload_picture' => (isset($layout['upload_picture'])) ? 1 : 0,
                'change_shape'   => (isset($layout['change_shape'])) ? 1 : 0,
                'change_weight'  => (isset($layout['change_weight'])) ? 1 : 0,
            ];
        }

        return $this->layouts()->sync($items);
    }


}
