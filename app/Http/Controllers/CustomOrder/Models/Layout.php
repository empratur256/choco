<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class Layout extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'custom_order_layout';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function scopeActive($query)
    {
        return $query->where('active',1);
    }

    public function types()
    {
        return $this->belongsToMany(Type::class, 'custom_order_layout_type', 'layout_id', 'type_id');
    }

    public function weights()
    {
        return $this->belongsToMany(Weight::class, 'custom_order_layout_weight', 'layout_id', 'weight_id');
    }

    public function packages()
    {
        return $this->belongsToMany(Package::class, 'custom_order_layout_package', 'layout_id', 'package_id');
    }

    public function shapes()
    {
        return $this->belongsToMany(Shape::class, 'custom_order_layout_shape', 'layout_id', 'shape_id');
    }

    public function amounts()
    {
        return $this->belongsToMany(Amount::class, 'custom_order_layout_amount', 'layout_id', 'amount_id')
                    ->withPivot('coefficient');
    }

    public function cells()
    {
        return $this->hasMany(Cell::class);
    }

    public function addAmounts($amounts)
    {
        $items = [];
        foreach ($amounts as $amount) {
            if (count($amount) == 2) {
                $items[$amount['amount_id']] = ['coefficient' => $amount['coefficient']];
            }
        }

        return $this->amounts()->sync($items);
    }


}
