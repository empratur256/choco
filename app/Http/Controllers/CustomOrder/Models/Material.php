<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class Material extends Model
{

    use  Filterable,SoftDeletes;

    protected $table = 'custom_order_material';
    public    $timestamps = true;

    protected $filterable = [
        'title'       => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function scopeActive($query)
    {
        return $query->where('active',1);
    }

    public function baseFlavors()
    {
        return $this->hasMany(BaseFlavor::class);
    }
}
