<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class Portfolio extends Model
{

    use  Filterable;

    protected $table      = 'custom_order_portfolio';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

}
