<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class Type extends Model
{
    use  Filterable,SoftDeletes;

    protected $table = 'custom_order_type';
    public    $timestamps = true;

    protected $filterable = [
        'title'       => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function scopeActive($query)
    {
        return $query->where('active',1);
    }

    public function brains()
    {
        return $this->belongsToMany(Brain::class, 'custom_order_brain_type', 'brain_id', 'type_id');
    }
}
