<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class BaseFlavor extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'custom_order_base_flavor';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function scopeActive($query)
    {
        return $query->where('active',1);
    }

    public function flavors()
    {
        return $this->belongsToMany(Flavor::class, 'custom_order_base_flavor_price', 'base_id', 'flavor_id');
    }

    public function brains()
    {
        return $this->belongsToMany(Brain::class, 'custom_order_flavor_brain', 'flavor_id', 'brain_id');
    }

}
