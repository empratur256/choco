<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class PackageCategory extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'custom_order_package_category';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function package()
    {
        return $this->hasMany(Package::class,'category_id');
    }

}
