<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;

class Cell extends Model
{

    use  Filterable;

    protected $table        = 'custom_order_layout_cells';
    public    $timestamps   = false;

    public function layout()
    {
        return $this->belongsTo(Layout::class, 'layout_id');
    }

    public function shape()
    {
        return $this->belongsTo(Shape::class);
    }

    public function weight()
    {
        return $this->belongsTo(Weight::class);
    }

}
