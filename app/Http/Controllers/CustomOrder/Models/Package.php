<?php

namespace Sedehi\Http\Controllers\CustomOrder\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;

class Package extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'custom_order_package';
    public    $timestamps = true;

    protected $filterable = [
        'admin_title' => [
            'operator' => 'Like',
        ],
        'created_at'  => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function category()
    {
        return $this->belongsTo(PackageCategory::class);
    }

    public function scopeActive($query)
    {
        return $query->where('active',1);
    }
}
