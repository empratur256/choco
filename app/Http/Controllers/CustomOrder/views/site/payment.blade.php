@extends('CustomOrder.views.site.container')
@section('tabs')
        <div class="row">
            <div class="col-xs-12">
                <div class="body-panel">
                    <div class="row">
                        @include('Cart.views.site.payment-form')
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/checkout-step.css') }}
    {{ Html::style('assets/site/css/checkout-step4.css') }}
@endpush

@push('js')
    <script>
        $(document).ready(function(){
            $('#atm-pay').click(function(){
                $(this).prop('checked', true);
                $('#net-pay').prop('checked',false);
            })
            $('#net-pay').click(function(){
                $(this).prop('checked', true);
                $('#atm-pay').prop('checked',false);
            })


        });

    </script>

@endpush
