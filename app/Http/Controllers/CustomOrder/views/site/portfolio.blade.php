@extends('views.layouts.site.master')
@section('title', trans('site.custom_order.portfolio'))
@section('content')
    <div class="red_box">
        <div class="container">
            <h3 class="title-page">@lang('site.custom_order.custom_order')  </h3>
            <div class="divider-title"></div>
            <p> @lang('site.custom_order.portfolio_text') </p>
        </div>
    </div>
    <div class="container" style="padding-top: 5em;padding-bottom: 5em;">
        <div class="col-xs-12">
            @if(count($portfolios))
                <div class="your text-center" style="direction: initial;text-align: -webkit-center;" >
                    @foreach($portfolios as $portfolio)
                        <div><img class="img-responsive" src="{{ asset('uploads/portfolio/520x360-'.$portfolio->performed_picture) }}"></div>
                    @endforeach
                </div>
            @else
                <div style="direction: initial;text-align: -webkit-center;" >
                    <span style="font-weight: bolder">@lang('site.custom_order.portfolio_no_item')</span>
                </div>
            @endif
        </div>
    </div>
@endsection
@push('css')
    {{ Html::style('assets/site/plugins/slick/slick/slick.css') }}
    {{ Html::style('assets/site/plugins/slick/slick/slick-theme.css') }}

    {{ Html::style('assets/site/css/portfolio.css')}}
    {{ Html::style('assets/site/css/public.css')}}
@endpush

@push('js')

    {{ Html::script('assets/site/plugins/slick/slick/slick.min.js') }}
    <script>
        $('.your').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    </script>
@endpush
