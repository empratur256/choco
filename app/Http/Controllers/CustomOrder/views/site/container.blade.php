@extends('views.layouts.site.master')
@php
    $stepActive = 0;
    foreach($steps as $stepKey=>$stepNav){
      if ( request()->route()->getActionName() != $stepNav['action'] ){
            $stepActive++;
        }
      else{
        break;
      }
    }
@endphp
@section('title')@lang('site.custom_order.custom_order') -
    @foreach($steps as $stepKey=>$stepNav)
        {{ request()->route()->getActionName() == $stepNav['action'] ? ($stepNav['trans']) : '' }}
    @endforeach
@endsection
@section('content')
    <div class="red_box">
        <div class="container">
            <h3 class="title-page">
                @lang('site.custom_order.custom_order') -
            @foreach($steps as $stepKey=>$stepNav)
                    {{ request()->route()->getActionName() == $stepNav['action'] ? ($stepNav['trans']) : '' }}
                @endforeach
            </h3>
            <div class="divider-title light"></div>
            <p>  @lang('site.custom_order.top_text') </p>
        </div>
    </div>
    <div class="bg-color">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div style="padding-top: 1em;padding-bottom: 1em">
                        <div class="container bread-crumbs" style="padding-bottom: .5em">
                            <span> @lang('site.home') </span>
                            @if(App::isLocale('en'))
                                <span> <i class="flaticon-right-arrow-1"></i></span>
                            @else
                                <span> <i class="flaticon-left-arrow-1"></i></span>
                            @endif
                            <span> @lang('site.custom_order.custom_order') </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="upload-picture-page">
        <div class="order-section" style="">
            <div class="container margin">
                <div class="col-lg-10 col-md-11 col-sm-12 col-xs-12 col-centered text-center"
                     style="padding-top: 4em;padding-bottom: 3em;">
                    <div class="row order-status">

                        @foreach($steps as $stepKey=>$stepNav)
                            <div class="{{session()->has('customOrderData.packaging_step') ? 'col-sm-2' : 'col-sm-1' }} ">
                                <div class="row">
                                    <div style="text-align: center;" class="col-xs-12 padding-lr-0">
                                        <img class="status-pic"
                                             src="{{ request()->route()->getActionName() == $stepNav['action'] || $stepActive <= $loop->index ? asset('assets/site/img/ok-circle.png') : asset('assets/site/img/not-ok-circle.png')}}">
                                        <p class=" {{ request()->route()->getActionName() == $stepNav['action'] || $stepActive <= $loop->index ? 'green_text' : '' }} ">@lang($stepNav['trans'])</p>
                                    </div>
                                </div>
                            </div>
                            @if(!$loop->last)
                                <div class="{{session()->has('customOrderData.packaging_step') ? 'col-sm-2' : 'col-sm-1' }} hidden-xs">
                                    <div class="row">
                                        <div class="col-xs-12  padding-top-2 padding-lr-0">
                                            <img src="{{ request()->route()->getActionName() == $stepNav['action'] || $stepActive <= $loop->index+1 ? asset('assets/site/img/green-circle.png') : asset('assets/site/img/circle.png')}}">
                                            <img src="{{ request()->route()->getActionName() == $stepNav['action'] || $stepActive <= $loop->index+1 ? asset('assets/site/img/green-circle.png') : asset('assets/site/img/circle.png')}}">
                                            <img src="{{ request()->route()->getActionName() == $stepNav['action'] || $stepActive <= $loop->index+1 ? asset('assets/site/img/green-circle.png') : asset('assets/site/img/circle.png')}}">
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
        <div class="container">
            <br>
           <div class="center" >@include('views.errors.errors')</div>
    @if(auth()->check())
                <div class="tab-content order-step-2">
                    @yield('tabs')
                </div>
            @else
                <div id="login-form" class="container login-form">
                    <div class="col-lg-7 col-md-8 col-sm-8 col-xs-12 col-sm-push-2 ">
                        <div style="background-color: white;float:right">
                            <div class="col-xs-12">
                                <h5 class="text-center bolder" style="
    padding-top: 2em;
    padding-bottom: 2em;
">@lang('site.user.login_to_account')</h5>
                            </div>
                            {{Form::open(['action' => 'Auth\Controllers\Site\AuthController@login'])}}
                            <div class="col-lg-6 col-sm-8 col-xs-10 col-lg-push-3 col-sm-push-2 col-xs-push-1 form-group">
                                {{ Form::label('email',trans('site.user.email')) }}
                                {{ Form::text('email', null, ['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-envelope defult-icon-color"></span>
                            </div>
                            <div class="col-lg-6 col-sm-8 col-xs-10 col-lg-push-3 col-sm-push-2 col-xs-push-1 form-group">
                                {{ Form::label('password',trans('site.password')) }}
                                {{ Form::password('password',['class' => 'form-control']) }}
                                <span class="glyphicon glyphicon-lock defult-icon-color"></span>
                            </div>
                            <div class="col-xs-12 text-center" style="margin-top: 2em;margin-bottom: 2em;">
                                <button type="submit" class="btn btn-default">@lang('site.user.login')</button>
                                <p style="padding-top: 2em;"><a class="red_text"
                                                                style="padding-bottom: 0.5em;border-bottom:1px solid #ebebeb"
                                                                href="{{action('Auth\Controllers\Site\ReminderController@showReminderForm')}}">@lang('site.user.remember_button')</a>
                                </p>
                            </div>
                            {{ Form::close() }}

                        </div>
                        <div class="col-xs-12 text-center register" style="">
                            <span>@lang('site.not_registered')</span><a style="color:#33beff"
                                                                        href="{{ action('Auth\Controllers\Site\AuthController@showSignupForm') }}">@lang('site.do_register')</a>
                        </div>
                    </div>
                </div>
        </div>
        @endif
    </div>
    </div>
@endsection

@push('css')
    @yield('style')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/checkout-step.css') }}
    {{ Html::style('assets/site/css/order2.css') }}


    {{ Html::style('assets/site/css/checkout-step1.css') }}
    <style>

    </style>
@endpush

@push('js')
    @yield('javascript')
@endpush
