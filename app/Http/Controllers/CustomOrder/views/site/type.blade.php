@extends('CustomOrder.views.site.container')
@section('tabs')
    <div class="tab-content">

        <div class="shop-basket-page">
            <div class="row">
                <div class="col-xs-12">
                    <div class="body-panel">
                        <div class="row">
                            {!! Form::open(['action' => 'CustomOrder\Controllers\Site\OrderController@saveType']) !!}
                            {!! Form::hidden('type','carpet') !!}
                            <div class="col-xs-6 order-type">
                                <img src="{!! asset('assets/site/img/type-carpet.png') !!}">
                                <div class="title">@lang('site.custom_order.type_carpet_title')</div>
                                <div class="break">@lang('site.custom_order.type_carpet_description')</div>
                                <button type="submit" class="btn purple-btn">@lang('site.custom_order.type_carpet')</button>
                            </div>
                            {!! Form::close() !!}
                            {!! Form::open(['action' => 'CustomOrder\Controllers\Site\OrderController@saveType']) !!}
                            {!! Form::hidden('type','board') !!}
                            <div class="col-xs-6 order-type">
                                <img src="{!! asset('assets/site/img/type-frame.png') !!}">
                                <div class="title">@lang('site.custom_order.type_board_title')</div>
                                <div class="break">@lang('site.custom_order.type_board_description')</div>
                                <button type="submit" class="btn purple-btn">@lang('site.custom_order.type_board')</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class=" col-xs-12 footer-btn-container">
                        <a href="{!! action('CustomOrder\Controllers\Site\OrderController@index') !!}" class="btn grayish-btn" id="back-to-upload-image" type="button"><span class="fa fa-angle-right"></span> @lang('site.previous_step')
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
