@extends('CustomOrder.views.site.container')
@section('tabs')
    <!-- Tab panes -->
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 bg-white">
                <div class="col-xs-12 padding-top-5 padding-bottom-5">
                    <img class="status-pic center-block img-responsive" src="../assets/site/img/order-step1.png">
                </div>

                @if(strlen(strip_tags($setting->custom_order_description)) > 2)
                    <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3 justified">
                        {!! $setting->custom_order_description !!}
                    </div>
                @endif
                <div class="divider divider-light"></div>

                {!! Form::open(['action' => 'CustomOrder\Controllers\Site\OrderController@saveType']) !!}

                <div class="col-md-6 col-xs-12 col-md-push-3 order-type">
                    <div class="row">
                        <div class="col-xs-12 section-order">
                            <div class="col-md-6"> {{ Form::text('order_count',$orderCount,['class' => 'pull-left form-control','min' => 1]) }}
                            </div>
                            <div class="col-md-6 title-order"> <p class="pull-right" >@lang('site.custom_order.number')</p>
                            </div>
                        </div>
                        <div class="col-xs-12 section-order">
                            <div class="col-md-6"> {{ Form::select('material_id', $materials, $materialId, [ 'class' => 'pull-left form-control' , 'id' => 'material' , 'placeholder' => trans('site.custom_order.select_placeholder')]) }}
                            </div>
                            <div class="col-md-6 title-order">   <p class="pull-right" >@lang('site.custom_order.select_type')</p>
                            </div>
                            <span class="icon-next-section" ><i class="flaticon-down-arrow"></i></span>
                        </div>
                        <div class="col-xs-12 section-order">
                            <div class="col-md-6"> {{ Form::select('has_brain' ,$hasBrainDropdownValues , $hasBrainValue , [ 'class' => 'pull-left form-control' , 'id' => 'has-brain-dropdown' , 'placeholder' => trans('site.custom_order.select_placeholder')]) }}
                            </div>
                            <div class="col-md-6 title-order"> <p class="pull-right" >@lang('site.custom_order.bulky')</p>
                            </div>
                            <span class="icon-next-section" ><i class="flaticon-down-arrow"></i></span>
                        </div>
                        <div id="routine-type-container"></div>

                    </div>
                </div>
                <div class="col-xs-12 space-2-30" style="margin-top: 5em;margin-bottom: 5em">
                    <button type="submit" class="btn submit pull-left" disabled id="submit-btn"> @lang('site.custom_order.next_button') </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/orders.css') }}
@endpush
@section('style')

@endsection

@section('javascript')

    {{ Html::script('assets/site/js/jquery.blockUI.js') }}

    <script>
        var routine_type_action = "{{ action('CustomOrder\Controllers\Site\OrderController@getRoutineTypes') }}";
        var token = $('meta[name="csrf-token"]').attr('content');
        var sessionData = {!! json_encode(session()->get('customOrderData')) !!};
        var oldInputs = {!! json_encode(old()) !!};

        jQuery.ajaxSetup({
            beforeSend: function() {
                $("#submit-btn").prop("disabled",true);
                $.blockUI({ message: '<h4><img src="{{ asset('assets/site/img/loading.gif') }}" />  @lang('site.custom_order.recieving_data')  </h4>',
                    css: {
                        border: 'none',
                        width: '20%',
                        padding: '10px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            },
            complete: function(){
                $.unblockUI();
            },
            success: function() {}
        });

        $(document).ready(function () {
            $('#has-brain-dropdown').on('change', function () {
                if ($(this).val()) {
                    $('#routine-type-container').html('');
                    getRoutineTypes($(this).val());
                }
            });

            $('#routine-type-container').on('change','#routine-type-dropdown', function () {
                if ($(this).val()) {
                    $("#submit-btn").removeAttr("disabled");
                } else {
                    $("#submit-btn").prop("disabled",true);
                }
            });

            if (oldInputs.hasOwnProperty('has_brain')) {
                getRoutineTypes(parseInt(oldInputs.has_brain));
            } else if (sessionData.hasOwnProperty('has_brain')) {
                getRoutineTypes(parseInt(sessionData.has_brain));
            }
        });

        function getRoutineTypes(has_brain) {
            $.ajax({
                type: "POST",
                url: routine_type_action,
                data: {
                    _token: token,
                    has_brain: has_brain
                },
                dataType: "json",
                cache: false
            }).fail(function (jqXHR, textStatus, errorThrown) {

                alert('@lang('site.problem_get_data')');

            }).done(function (response) {

                if (!response.error) {
                    generateRoutineTypesDropdown(response);
                    return true;
                }

                alert('@lang('site.problem_get_data')');
                return false;
            });
        }

        function generateRoutineTypesDropdown(data) {

            var element = '<div class="col-xs-12" style="padding: 1.5em; border:1px solid #ebebeb;margin-top: 7em">';

            element += '<div class="col-md-6"><select name="type_id" id="routine-type-dropdown" class="pull-left form-control"></div>'
            element += '<option value="">{{ trans('site.custom_order.select_placeholder') }}</option>';

            $.each(data.types, function (index,item) {
                element += '<option value="'+ item.id + '"';

                if (oldInputs.hasOwnProperty('type_id') && parseInt(oldInputs.type_id) === item.id) {
                    $("#submit-btn").removeAttr("disabled");
                    element += ' selected';
                } else if (sessionData.hasOwnProperty('type_id') && parseInt(sessionData.type_id) === item.id) {
                    $("#submit-btn").removeAttr("disabled");
                    element += ' selected';
                }

                element += '>';
                element += item.title +'</option>';
            });

            element += '</select></div>';

            element += '<div class="col-md-6 pull-right"> <p class="pull-right">{{ trans('site.custom_order.routine_types_section_title') }}</p></div><span class="icon-next-section" ><i class="flaticon-down-arrow"></i></span>';

            $('#routine-type-container').append(element);
        }

    </script>
@endsection
@push('css')
    <style>
        .form-control{
            width: 65% !important;
            border-radius: 1px !important;
            padding:1px 8px !important;
            font-size: 12px !important;
        }
        .submit{
            padding:7px 25px;
        }
        .about{
            background: #fefefe;
        }
        .upload-picture-page .container{
            margin-bottom:30px !important;
        }
    </style>
@endpush