@extends('CustomOrder.views.site.container')
@section('tabs')
    <div class="col-xs-12 bg-white order-content">
        <div class="row">
            <div class="col-xs-12 bg-white">
                <div class="col-xs-12 padding-top-5 padding-bottom-5">
                    <img class="status-pic center-block img-responsive"
                         src="{{asset('assets/site/img/order-step2.png')}}">
                </div>

                @if(strlen(strip_tags($setting->custom_order_description)) > 2)
                    <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center justified padding-bottom-3">
                        <p class="text-default"
                           style="font-weight:bolder;font-size: 15px;">{!! $setting->custom_order_description !!}</p>
                    </div>
                @endif
            </div><!-- end inner row -->

            <div class="form-group product-chooser">
                <div class="col-md-12">

                    @if(!is_null($selectedLayout))
                        <div class="col-md-12 space-2-50">
                            <h3 class="text-center">@lang('site.custom_order.selected_layout')</h3>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="product-choosed">
                                <img src="{{ asset('uploads/layout/240xauto-'.$selectedLayout->picture) }}"
                                     class="img-rounded col-xs-4 col-sm-6 col-md-4 col-lg-4 pull-right"
                                     alt="{{ $selectedLayout->title }}">
                                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                    <h5 class="text-danger text-md text-right">{{ $selectedLayout->title }}</h5>
                                    <span class="description">{!! $selectedLayout->description !!}</span>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    @endif

                    {{ Form::open(['action' => 'CustomOrder\Controllers\Site\OrderController@saveLayout']) }}
                    <div class="col-md-12">
                        <div class="divider divider-light space-2-20"></div>
                    </div>

                    <div class="col-md-12 layout-select">
                        @foreach($layouts as $layout)
                            <div class="col-xs-12 col-sm-12 col-md-15 col-lg-15">

                                <div class="product-chooser-item layout @if(session('customOrderData.layout_id') == $layout->id) selected @endif">
                                    <button type="button" class="open-modal" data-toggle="modal" data-target="#layoutModal-{{ $loop->iteration }}"><i class="flaticon-search"></i> @lang('site.custom_order.zoom_in')
                                    </button>
                                    {{ Form::radio('layout_id',$layout->id,(session('customOrderData.layout_id') == $layout->id),['placeholder' => trans('site.custom_order.layouts'),'class' => "radio-primary"])}}
                                    <img src="{{ asset('uploads/layout/240xauto-'.$layout->picture) }}"
                                         class="col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Layout Picture">
                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                        <span class="title">{{ $layout->title }}</span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div id="layoutModal-{{ $loop->iteration }}" class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
                                        </div>
                                        <div class="modal-body">
                                            <img src="{{ asset('uploads/layout/900xauto-'.$layout->picture) }}"
                                                 class="img-responsive img-thumbnail">
                                            <span class="description" style="padding: 30px 20px;">{!! $layout->description !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            {!! $layouts->appends(Request::except('page'))->links() !!}
                        </div>
                    </div>
                    <div class="col-md-12 space-2-30">
                        <a href="{{ action('CustomOrder\Controllers\Site\OrderController@index') }}"
                           class="btn submit pull-right"> @lang('site.custom_order.back')</a>
                        <button type="submit" class="btn submit  pull-left"> @lang('site.custom_order.next') </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/orders.css') }}
    {{ Html::style('assets/site/css/order2.css') }}
    {{ Html::style('assets/site/css/component.css') }}
@endpush

@push('js')
    <script type="text/javascript">
        $(function () {
            $('.full-sise').on('click', function () {
                EmptyImgClass = $(this.classList)[2];
                EmptyImgClass = "" + "." + EmptyImgClass;
                EmptyImg = ($(EmptyImgClass).find('.test')).prevObject[1];
                console.log(($(this).nextAll()).find('.t'));
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');
            });
        });
        $(function () {
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
                $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
                $(this).addClass('selected');
                $(this).find('input[type="radio"]').prop("checked", true);

            });
        });
    </script>
@endpush