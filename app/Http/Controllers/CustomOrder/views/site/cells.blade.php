@extends('CustomOrder.views.site.container')
@section('tabs')
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="row">
            {{ Form::open(['action' => 'CustomOrder\Controllers\Site\OrderController@saveCells','files'=> true ,'class' => 'form form-horizontal']) }}

            <div class="col-xs-12 bg-white order-content">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <div class="col-xs-12 padding-top-5 padding-bottom-5">
                            <img class="status-pic center-block img-responsive"
                                 src="{{asset('assets/site/img/order-step3.png')}}">
                        </div>

                        @if(strlen(strip_tags($setting->custom_order_description)) > 2)
                            <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3">
                                <p class="text-default"
                                   style="font-weight:bolder;font-size: 15px;">{!! $setting->custom_order_description !!}</p>
                            </div>
                        @endif
                    </div><!-- end inner row -->

                    <div class="row form-group product-chooser">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="divider divider-light space-2-20"></div>
                            </div>

                            <div class="col-md-12">
                                @foreach($layout->cells as $cell)
                                    <div class="pull-right col-xs-12 col-md-3 col-lg-3">
                                        <div class="cells">
                                            <div class="cell-title">
                                                <h5 class="text-danger" style="font-size: 17px;">@lang('site.custom_order.cells_page.cell_number',['cellNum' => $cell->cell])</h5>
                                            </div>

                                            @if($loop->first)
                                                <a id="copy-cell-data">@lang('site.custom_order.cells_page.copy_cells')</a>
                                            @endif

                                            <div class="form-group">

                                                @php
                                                    $checked = false;
                                                    if (!session()->has('customOrderData.chocolate_data')) {
                                                        $checked = true;
                                                    } else {
                                                       if (!is_null(old('active.'.$cell->cell))) {
                                                           $checked = true;
                                                       }
                                                       if (!is_null(session()->get('customOrderData.chocolate_data.'.$cell->cell))) {
                                                           $checked = true;
                                                       }
                                                    }
                                                @endphp
                                                <div class="checkbox">
                                                    <div class="col-md-6">
                                                        <label class="switch">
                                                            {{ Form::checkbox('active['.$cell->cell.']',1,$checked),['class'=>'form-control' ,  'id'=>'switch']}}
                                                            <div class="slider round"></div>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-4"><span>@lang('site.custom_order.cells_page.cell_is_filled')</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    {{ Form::select('base_flavor_id['.$cell->cell.']',$baseFlavors,get_default_value('base_flavor_id',$cell->cell),['class' => 'base-flavor-dropdown form-control pull-left','data-cell-num' => $cell->cell,'placeholder' => trans('site.custom_order.select_placeholder')])}}
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-4">
                                                    <span>@lang('site.custom_order.base_flavor')</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    {{ Form::select('flavor_id['.$cell->cell.']',[],null,['id' => 'flavor-dropdown-'.$cell->cell,'class'=> 'flavor-dropdown form-control','placeholder' => trans('site.custom_order.select_placeholder'),'disabled' => true])}}
                                                </div>
                                                <div class="col-md-6">
                                                    <span>@lang('site.custom_order.flavor')</span>
                                                </div>
                                            </div>
                                            @if(count($brains) > 0)
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        {{ Form::select('brain_id['.$cell->cell.']',[],null,['id' => 'brain-dropdown-'.$cell->cell,'class'=>'brain-dropdown form-control'])}}
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-4">
                                                        <span>@lang('site.custom_order.cells_page.has_brain')</span>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($shapeStatus == 1 && count($shapes) > 0)
                                                <div class="form-group">
                                                    <div class="col-md-6">

                                                        @php
                                                            $shapeDefaultValue = get_default_value('shape_id',$cell->cell);
                                                        @endphp
                                                        {{ Form::select('shape_id['.$cell->cell.']',$shapes,(!is_null($shapeDefaultValue)) ? $shapeDefaultValue : $cell->shape_id ,['class'=>'shape-dropdown form-control'])}}
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-4">
                                                        <span>@lang('site.custom_order.cells_page.shape')</span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($weightStatus == 1 && count($weights) > 0)
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        @php
                                                            $weightDefaultValue = get_default_value('weight_id',$cell->cell);
                                                        @endphp
                                                        {{ Form::select('weight_id['.$cell->cell.']',$weights,(!is_null($weightDefaultValue)) ? $weightDefaultValue : $cell->weight_id ,['class'=>'weight-dropdown form-control'])}}
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-4">
                                                        <span>@lang('site.custom_order.cells_page.weight')</span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($uploadStatus == 1)
                                                <lable>@lang('site.custom_order.upload')</lable>
                                                <div class="space-2-20"></div>
                                                @php
                                                    $oldPictureUrl = '';
                                                    $oldPictureSize = 0;
                                                    if (session()->has('customOrderData.chocolate_data.'.$cell->cell.'.picture')) {
                                                        $oldPictureUrl = asset(session('customOrderData.chocolate_data.'.$cell->cell.'.picture_thumb'));
                                                        if (File::exists(public_path(session('customOrderData.chocolate_data.'.$cell->cell.'.picture')))) {
                                                            $oldPictureSize = File::size(session('customOrderData.chocolate_data.'.$cell->cell.'.picture'));
                                                        }
                                                    }
                                                @endphp
                                                <div class="dropzone" fileMaxSize="{{ $setting->custom_order_picture_max_size }}" name="{{ $cell->cell }}"
                                                     data-old-picture="{{ $oldPictureUrl }}" data-old-size="{{$oldPictureSize}}">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 space-2-30">
                    <a href="{{ action('CustomOrder\Controllers\Site\OrderController@layout') }}"
                       class="btn submit pull-right"> @lang('site.custom_order.previous_button') </a>
                    <button type="submit" class="btn submit  pull-left"> @lang('site.custom_order.next_button')</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
@endsection

@section('javascript')
    <script>
        var allFlavors = {!! json_encode($allFlavors) !!};
        var brains = {!! json_encode($brains) !!};
        var uploadPictureMaxSize = {{ $setting->custom_order_picture_max_size }};

        $(document).ready(function () {
            @foreach($layout->cells as $cell)
            $.each(allFlavors, function (index, baseFlavor) {
                var baseFlavorDefaultValue = {!! (!is_null(get_default_value('base_flavor_id',$cell->cell))) ? '"'.get_default_value('base_flavor_id',$cell->cell).'"' : 'null' !!};
                var flavorDefaultValue = {!! (!is_null(get_default_value('flavor_id',$cell->cell))) ? '"'.get_default_value('flavor_id',$cell->cell).'"' : 'null' !!};

                if (baseFlavor.id === parseInt(baseFlavorDefaultValue)) {
                    fillOrDisableFlavorDropdown(baseFlavor, flavorDefaultValue, {{ $cell->cell }});
                }

                        @if(count($brains) > 0)
                var brainDefaultValue = {!! (!is_null(get_default_value('brain_id',$cell->cell))) ? '"'.get_default_value('brain_id',$cell->cell).'"' : 'null' !!};
                fillOrHideBrainDropdown(brains, brainDefaultValue, {{ $cell->cell }});
                @endif
            });
            @endforeach

            $(document).on('change', '.base-flavor-dropdown', function () {
                if ($(this).val()) {
                    var currentValue = $(this).val();
                    var cellNumber = $(this).attr('data-cell-num');

                    $.each(allFlavors, function (index, baseFlavor) {
                        if (baseFlavor.id === parseInt(currentValue)) {
                            fillOrDisableFlavorDropdown(baseFlavor, null, cellNumber);
                        }
                    });
                }
            });

            $(document).on('click', '#copy-cell-data', function () {

                var c = confirm('@lang('site.custom_order.cells_page.copy_cells_confirmation')');

                if (c) {

                    // copy base flavor and flavor
                    var baseFlavorValue = $(".base-flavor-dropdown").val();

                    if (baseFlavorValue) {

                        var flavorValue = $(".flavor-dropdown").val();

                        $('select.base-flavor-dropdown').each(function() {
                            $(this).val(baseFlavorValue);
                            var cellNumber = $(this).attr('data-cell-num');
                            $.each(allFlavors, function (index, baseFlavor) {
                                if (baseFlavor.id === parseInt(baseFlavorValue)) {
                                    fillOrDisableFlavorDropdown(baseFlavor, flavorValue, cellNumber);
                                }
                            });
                        });
                    }

                    // copy brain
                    var brainValue = $(".brain-dropdown").val();

                    if (brainValue) {
                        $('select.brain-dropdown').val(brainValue);
                    }

                    // copy shape
                    var shapeValue = $(".shape-dropdown").val();

                    if (shapeValue) {
                        $('select.shape-dropdown').val(shapeValue);
                    }

                    // copy weight
                    var weightValue = $(".weight-dropdown").val();

                    if (weightValue) {
                        $('select.weight-dropdown').val(weightValue);
                    }
                }
            });
        });

        function fillOrDisableFlavorDropdown(baseFlavor, defaultValue, cellNumber) {

            var dropdownSelector = $("#flavor-dropdown-" + cellNumber);

            if (baseFlavor.flavors.length > 0) {

                dropdownSelector.removeAttr('disabled');

                var html = '';

                html += '<option value="">{{ trans('site.custom_order.no_flavor') }}</option>';

                $.each(baseFlavor.flavors, function (index, flavor) {
                    html += '<option value="' + flavor.id + '"';

                    if (defaultValue !== null && parseInt(defaultValue) === flavor.id) {
                        html += ' selected';
                    }

                    html += '>';
                    html += flavor.title;
                    html += '</option>';
                });

                dropdownSelector.html(html);

            } else {
                dropdownSelector.attr('disabled', 'disabled');
            }
        }

        function fillOrHideBrainDropdown(brains, defaultValue, cellNumber) {
            if (brains.length > 0) {

                var html = '';

                html += '<option value="">{{ trans('site.custom_order.select_placeholder') }}</option>';

                $.each(brains, function (index, brain) {
                    html += '<option value="' + brain.id + '"';

                    if (defaultValue !== null && parseInt(defaultValue) === brain.id) {
                        html += ' selected';
                    }

                    html += '>';
                    html += brain.title;
                    html += '</option>';
                });

                $("#brain-dropdown-" + cellNumber).html(html);

            } else {
                $("#brain-dropdown-" + cellNumber).addClass('hidden');
            }
        }

        var uploadMaxSizeInByte = {{ $setting->custom_order_picture_max_size }} * 1024 * 1024;

        Dropzone.autoDiscover = false;
        $('.dropzone').each(function (index) {
            var cellNumber = $(this).attr("name");
            var oldPictureUrl = $(this).attr("data-old-picture");
            var oldPictureSize = $(this).attr("data-old-size");
            $(this).dropzone({
                maxFilesize: uploadMaxSizeInByte,
                thumbnailWidth: 200,
                thumbnailHeight:140,
                acceptedFiles: "image/jpeg,image/png",
                url: '{{ action('CustomOrder\Controllers\Site\OrderController@uploadCellPicture') }}',
                paramName: 'cell_picture[' + cellNumber + ']',
                params: {
                    '_token': "{{ csrf_token() }}"
                },
                addRemoveLinks: true,
                dictDefaultMessage: ' <div class="icon" style="text-align: center; font-size: 20px;"><i class="flaticon-cloud-computing" ></i></div> {{ trans('site.custom_order.drag_drop') }} ',
                dictRemoveFile: '{{ trans('site.custom_order.delete_picture') }}',
                dictCancelUpload:'{{ trans('site.custom_order.cells_page.cancel_upload') }}',
                dictCancelUploadConfirmation:'{{ trans('site.custom_order.cells_page.cancel_upload_confirm') }}',
                maxFiles: 1,
                init: function () {
                    if (oldPictureUrl.length > 1) {
                        var mockFile = {name: '@lang('site.custom_order.cells_page.picture')', size: oldPictureSize};
                        this.options.addedfile.call(this, mockFile);
                        this.options.thumbnail.call(this, mockFile, oldPictureUrl);
                        this.emit("complete", mockFile);
                    }
                    this.on("error", function(file,errorMessage) {
                        if (file.size > uploadMaxSizeInByte) {
                            this.removeFile(file);
                            alert("@lang('site.custom_order.cells_page.upload_validate_size',['size' => $setting->custom_order_picture_max_size])");
                        }
                        if (file.type !== "image/jpeg" && file.type !== "image/png") {
                            this.removeFile(file);
                            alert("@lang('site.custom_order.cells_page.upload_validate_mime')");
                        }
                    });
                    this.on('success', function (file,response) {
                        if(response.error) {
                            this.removeFile(file);
                            alert('@lang('site.custom_order.cells_page.upload_error')');
                        }
                    });
                    this.on("maxfilesexceeded", function(file) {
                        alert('@lang('site.custom_order.cells_page.upload_only_one_picture')');
                        this.removeAllFiles();
                        this.addFile(file);
                    });

                    this.on('addedfile', function(file) {
                        if (oldPictureUrl.length > 1) {
                            this.removeFile(mockFile);
                        }
                    });

                    var dropzone = this;

                    this.on( 'removedfile' ,function(file) {
                        $.ajax({
                            type: 'POST',
                            url: '{{ action('CustomOrder\Controllers\Site\OrderController@removeCellPicture') }}',
                            data: {
                                _token: "{{ csrf_token() }}",
                                cell_number: cellNumber
                            },
                            dataType: 'html',
                            success: function(result){
                                var response = JSON.parse(result);
                                dropzone.removeAllFiles();
                                if(response.error) {
                                    alert('@lang('site.custom_order.cells_page.upload_error')');
                                }
                            }
                        })
                    });
                }
            });
        });
    </script>
@endsection