@extends('CustomOrder.views.site.container')
@section('tabs')
    <div role="tabpanel" class="tab-pane active" id="show-price">
        <div class="body-panel">
            {{ Form::open(['class' => 'form','action' => 'CustomOrder\Controllers\Site\OrderController@saveShipping']) }}
            @include('Cart.views.site.shipping-address')
        </div>
        {{ Form::close() }}
    </div>

    </div>
    <!------------      #price-section  ***end***         ------------->
    {!! Form::open(['action' => ['User\Controllers\Site\AddressController@destroy',1],'method' => 'DELETE','id' => 'removeForm']) !!}
    {!! Form::hidden('return',action('CustomOrder\Controllers\Site\OrderController@shipping')) !!}
    {!! Form::hidden('deleteId',null,['id' => 'removeData']) !!}
    {!! Form::close() !!}
@endsection
@push('modal')
    <div class="modal fade" id="addressModal" tabindex="-1" role="dialog"
         aria-labelledby="addressModalLabel">
        <div class="modal-dialog" role="document">
            @if(!is_null($editAddress))
                {!! Form::model($editAddress,['action' => ['User\Controllers\Site\AddressController@update',$editAddress->id],'method' => 'Patch']) !!}
            @else
                {!! Form::open(['action' => 'User\Controllers\Site\AddressController@store']) !!}
            @endif
            {!! Form::hidden('return',action('CustomOrder\Controllers\Site\OrderController@shipping')) !!}

            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title purple-text" id="addressModalLabel">
                        <span class="fa fa-map-marker"></span> @lang('site.carts.add_address')</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-push-2 form-group @if($errors->has('name_family')) has-error @endif">
                            {{ Form::text('name_family',null,['id' => 'name_family','class' => 'form-control', 'placeholder' =>trans('site.name_family'), 'lang' => 'fa']) }}
                            <span class="help-block">{{ $errors->first('name_family') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-10 col-sm-push-2 form-group @if($errors->has('province_id')) has-error @endif">
                            {{ Form::select('province_id',$provinces,null,['id' => 'province','class' => 'form-control selectpicker','placeholder'=> trans('site.select_province')]) }}
                            <span class="help-block">{{ $errors->first('province_id') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-10 col-sm-push-2 form-group @if($errors->has('city_id')) has-error @endif">
                            {{ Form::select('city_id',[],null,['id' => 'city_id','class' => 'form-control selectpicker', 'disabled' => 'disabled', 'placeholder' => trans('site.select_city')]) }}
                            <span class="help-block">{{ $errors->first('city_id') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-10 col-sm-push-2 form-group @if($errors->has('address')) has-error @endif">
                            {{ Form::text('address',null,['id' => 'address','class' => 'form-control', 'placeholder' => trans('site.address'), 'lang' => 'fa']) }}
                            <span class="help-block">{{ $errors->first('address') }}</span>
                        </div>
                        <div class="col-sm-7 col-sm-push-2 form-group @if($errors->has('tel')) has-error @endif">
                            {{ Form::text('tel',null,['id' => 'phone','class' => 'form-control number-only', 'placeholder' => trans('site.telephone')]) }}
                            <span class="help-block">{{ $errors->first('tel') }}</span>
                        </div>
                        <div class="col-sm-3 col-sm-push-2 form-group @if($errors->has('phone_code')) has-error @endif">
                            {{ Form::text('phone_code',null,['id' => 'phone_code','class' => 'form-control number-only', 'placeholder' => trans('site.phone_code')]) }}
                            <span class="help-block">{{ $errors->first('phone_code') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-10 col-sm-push-2 form-group @if($errors->has('mobile')) has-error @endif">
                            {{ Form::text('mobile',null,['id' => 'mobile','class' => 'form-control number-only', 'placeholder' => trans('site.mobile')]) }}
                            <span class="help-block">{{ $errors->first('mobile') }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-10 col-sm-push-2 form-group @if($errors->has('postal_code')) has-error @endif">
                            {{ Form::text('postal_code',null,['id' => 'postal_code','class' => 'form-control number-and-dash', 'placeholder' => trans('site.postal_code')]) }}
                            <span class="help-block">{{ $errors->first('postal_code') }}</span>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="add-address" class="btn grayish-btn">
                        <span class="fa fa-check"></span>@lang('site.save')
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endpush
@push('css')

    {{ Html::style('assets/site/css/checkout-step2.css') }}
@endpush

@push('js')
    @if(App::isLocale('fa'))
        {{ Html::script('assets/site/plugins/farsiType/FarsiType.js') }}
    @endif
    <script>
        var location_action = "{{ action('City\Controllers\Site\CityController@cities') }}";
        var token = $('meta[name="csrf-token"]').attr('content');

        $(document).ready(function () {
            @if(!is_null($editAddress))
            $('#addressModal').modal('show');
            @endif
            @php $errorField = ['tel','address','city_id','province_id','name_family','mobile','postal_code'] @endphp
            @foreach($errorField as $field)
            @if($errors->has($field))
            $('#addressModal').modal('show');
            @continue
            @endif
            @endforeach
            @php $city = 0 @endphp
            @if(!is_null($editAddress))
            @php $city= $editAddress->city_id @endphp

            @endif
            @if(old('city_id'))
            @php $city= old('city_id') @endphp
            @endif
            getCities($('#province').val(), '{{$city}}');
            $(document.body).on('change', '#province', function () {
                if ($(this).val()) {
                    getCities($(this).val());
                }
            });

            $('.number-only').forceNumeric();
            $('.number-and-dash').forceNumericAndDash();
        });

        jQuery.fn.forceNumeric = function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.which || e.keyCode;
                    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                        key >= 48 && key <= 57 ||
                        key >= 96 && key <= 105 ||
                        key == 190 || key == 188 || key == 110 ||
                        key == 8 || key == 9 || key == 13 ||
                        key == 35 || key == 36 ||
                        key == 37 || key == 39 ||
                        key == 46 || key == 45)
                        return true;
                    return false;
                });
            });
        };

        jQuery.fn.forceNumericAndDash = function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.which || e.keyCode;
                    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                        key >= 48 && key <= 57 ||
                        key >= 96 && key <= 105 ||
                        key == 190 || key == 188 || key == 109 || key == 110 ||
                        key == 8 || key == 9 || key == 13 ||
                        key == 35 || key == 36 ||
                        key == 37 || key == 39 ||
                        key == 46 || key == 45 || key == 189)
                        return true;
                    return false;
                });
            });
        };


        $('.submitForm').on('click', function (e) {
            e.preventDefault();
            $('.form').submit();
        });
        $('.deleteForm').on('click', function (e) {
            e.preventDefault();
            if (confirm('@lang('site.carts.remove_data')')) {
                $('#removeData').val($(this).data('id'));
                $('#removeForm').submit();
            }
        });

        function getCities(province_id, old) {
            if (province_id > 0) {
                $.ajax({
                    type: "POST",
                    url: location_action,
                    data: {
                        _token: token,
                        id: province_id
                    },
                    dataType: "json",
                    cache: false
                }).fail(function (jqXHR, textStatus, errorThrown) {

                    alert('@lang('site.carts.error_data')');

                }).done(function (response) {

                    $('#city_id').empty();
                    $('#city_id').prop('disabled', false);
                    $.each(response, function (value, key) {
                        var input = '<option></option>';
                        if (old > 0) {
                            if (old == value) {
                                var input = '<option selected></option>';
                            } else {
                                var input = '<option></option>';
                            }
                        }
                        $('#city_id').append($(input)
                            .attr("value", value).text(key));
                    });
                    $('#city_id').selectpicker('refresh');
                    return true;

                });
            }

        }

    </script>
@endpush
