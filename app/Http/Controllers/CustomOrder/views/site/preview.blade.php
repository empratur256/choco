@extends('CustomOrder.views.site.container')
@section('title',trans('site.custom_order.preview_page.title'))
@section('tabs')
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="row">
            <div class="bg-white">
                <div class="container">
                    <div class="col-xs-12 bg-white">
                        <div class="col-xs-12 padding-top-5 padding-bottom-5">
                            <img class="status-pic center-block img-responsive"
                                 src="{{ asset('assets/site/img/order-step5.png') }}">
                        </div>

                        @if(strlen(strip_tags($setting->custom_order_description)) > 2)
                            <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3">
                                <p class="text-default"
                                   style="font-weight:bolder;font-size: 15px;">{!! $setting->custom_order_description !!}</p>
                            </div>
                        @endif

                    </div><!-- end inner row -->

                    <div class="row ">

                        <div class="col-md-12 space-2-30">
                            <img src="{{asset('assets/site/img/left-side.png')}}" >
                            <h4 class="text-dark preview-title padded-bottom"> @lang('site.custom_order.preview_page.order_count_and_type')</h4>
                            <div class="divider divider-light"></div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                                <div class="cells">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-right"><span class="text-dark"> @lang('site.custom_order.preview_page.order_count') :<span class="text-danger"> {{ session('customOrderData.order_count') }} @lang('site.custom_order.preview_page.number') </span></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                                <div class="cells">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-right"><span class="text-dark"> @lang('site.custom_order.preview_page.product_type') :<span class="text-danger">{{ $material->title }}</span></span></div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                                <div class="cells">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-right"><span class="text-dark"> @lang('site.custom_order.preview_page.flat_has_brain') :
                                                @if(session()->get('customOrderData.has_brain') == 1)
                                                    <span class="text-danger">@lang('site.custom_order.preview_page.has_brain')</span>
                                                @else
                                                    <span class="text-danger">@lang('site.custom_order.preview_page.flat')</span>
                                                @endif
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                                <div class="cells">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-right"><span class="text-dark"> @lang('site.custom_order.preview_page.product_category') :<span class="text-danger">{{ $type->title }}</span></span></div>
                                    </div>
                                </div>
                            </div>

                            @if(!is_null($amount))
                                <br>
                                <br>
                                <br>
                                <div class="col-md-6 pull-right">
                                    <div class="cells">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12 pull-right"><span class="text-dark"> @lang('site.custom_order.preview_page.amount_count') : <span class="text-danger">{{ $amount->title }}</span></span></div>

                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="sapce-2-20"></div>

                        </div>
                        <div class="col-md-12 space-2-50">
                            <img src="{{asset('assets/site/img/left-side.png')}}" >
                            <h4 class="text-dark preview-title padded-bottom"> @lang('site.custom_order.preview_page.cells_status') </h4>

                            <div class="divider divider-light"></div>
                        </div>

                        <div class="col-md-12 layout-show">

                            @foreach(session('customOrderData.chocolate_data') as $cellNumber => $cell)
                                <div class="col-md-3 col-sm-4 col-xs-12 pull-right cell-item">
                                    <div class="cells">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="container">
                                                    <span class="text-danger"> @lang('site.custom_order.preview_page.cell_number',['cellNumber' => $cellNumber]) </span>
                                                </div>
                                            </div>
                                            <div class="col-md-12 padded-vertical">
                                                <div class="col-md-6 col-md-6 col-sm-6 col-xs-6 pull-right">
                                                    <span class="text-dark"> @lang('site.custom_order.base_flavor') : </span>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <span class="text-danger">{{ $baseFlavors[$cell['base_flavor_id']] }}</span>
                                                </div>
                                            </div>
                                            <div class="col-md-12 padded-vertical">
                                                <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                                                    <span class="text-dark"> @lang('site.custom_order.flavor') :</span>
                                                </div>
                                                <div class=" col-md-6 col-sm-6 col-xs-6">
                                                    <span class="text-danger">
                                                        @if(isset($cell['flavor_id']) && !is_null($cell['flavor_id']))
                                                            {{ $flavors[$cell['flavor_id']] }}
                                                        @else
                                                            <span> - </span>
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                            @if(session('customOrderData.has_brain') == 1)
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                                                        <span class="text-dark"> @lang('site.custom_order.brain') :</span>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <span class="text-danger">
                                                            {{ $brains[$cell['brain_id']] }}
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($shapeStatus == 1)
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                                                        <span class="text-dark"> @lang('site.custom_order.shape') :</span>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <span class="text-danger">
                                                            {{ $shapes[$cell['shape_id']] }}
                                                        </span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($weightStatus == 1)
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                                                        <span class="text-dark"> @lang('site.custom_order.weight') :</span>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <span class="text-danger"> {{ $weights[$cell['weight_id']] }} @lang('site.custom_order.gram') </span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($uploadStatus == 1)
                                                <div class="col-md-12 padded-vertical">
                                                <div class="col-md-12">
                                                    @if (session()->has('customOrderData.chocolate_data.'.$cellNumber.'.picture'))
                                                        <span class="dark-text"> @lang('site.custom_order.preview_page.uploaded_picture') :</span>
                                                    @else
                                                        <span class="dark-text"> @lang('site.custom_order.preview_page.no_picture_uploaded')</span>
                                                    @endif
                                                </div>
                                                <div class="col-md-12 padded-vertical">
                                                    <div style="position: relative;" class="img-uploaded">
                                                        @php
                                                            $src = asset('assets/site/img/nimg.png');
                                                            if (session()->has('customOrderData.chocolate_data.'.$cellNumber.'.picture')) {
                                                                $src = asset(session('customOrderData.chocolate_data.'.$cellNumber.'.picture_thumb'));
                                                            }
                                                        @endphp

                                                        @php
                                                            $srcShablon = '';
                                                            if ($shapeStatus == 1) {
                                                                $srcShablon = asset('uploads/shape/500x500-'.$shapesShablon[$cell['shape_id']]);
                                                            }
                                                        @endphp
                                                        <img src="{{ $src }}" class="img-responsive">
                                                        @if ($shapeStatus == 1)
                                                            <div style="width: 100%;height: 100%; position: absolute;right: 0 ; top: 0; left: 0;">
                                                                <img style="height: 100% ;width: 100%" src="{{ $srcShablon }}" alt="" class="img-responsive shablon">
                                                            </div>
                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-12 space-2-50">

                            @if($layout->select_package == 1 && !is_null($package))
                                <div class="col-md-6 col-sm-12 col-xs-12 detail-pack  pull-right">
                                    <div class="col-md-12 space-2-30">
                                        <img src="{{asset('assets/site/img/left-side.png')}}" >
                                        <h4 class="text-dark preview-title padded-bottom"> @lang('site.custom_order.preview_page.packaging_details') </h4>
                                        <div class="divider divider-light"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="cells">
                                            <div class="row">
                                                <div class="col-md-5 pull-right">
                                                    <div class="col-md-12 pack-name ">
                                                        <div class="img-pack">
                                                            <img src="{{ asset('uploads/package/240xauto-'.$package->picture) }}" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="title text-center padded-vertical">
                                                            <h4 class="text-danger">{{ $package->title }}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="text-dark padded-vertical">
                                                        <p> @lang('site.custom_order.preview_page.uploaded_picture') :</p>
                                                    </div>
                                                    <br>
                                                    @if(session()->has('customOrderData.package_picture'))
                                                        <div>
                                                            <img class="img-responsive" src="{{ asset(session('customOrderData.package_picture_thumb')) }}" alt="package-picture">
                                                        </div>
                                                    @else
                                                        <div class="img-uploaded padded-vertical">
                                                            <p class="btn btn-default btn-block  text-center"> @lang('site.custom_order.preview_page.no_picture_uploaded') </p>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif


                            <div class="col-md-6 col-sm-12 col-xs-12 pull-right">
                                <div class="col-md-12 space-2-30">
                                   <img src="{{asset('assets/site/img/left-side.png')}}" >
                                    <h4 class="text-dark preview-title padded-bottom"> @lang('site.custom_order.preview_page.payment_details') </h4>
                                    <div class="divider divider-light"></div>
                                </div>
                                <div class="col-md-12 factor-items ">
                                    <div class="row final-price">
                                        <div>
                                            <div class="col-md-12 price-item padded-vertical">
                                                <div class="col-md-6 pull-right">
                                                    <p class="text-right"> @lang('site.custom_order.preview_page.one_product_price')</p>
                                                </div>
                                                <div class="col-md-6 pull-right">
                                                    <p class="text-left"> {{ number_format($singleProductPrice) }} @lang('site.currency') </p>
                                                </div>
                                            </div>
                                            <div class="divider divider-light"></div>
                                            <div class="col-md-12 price-item padded-vertical">
                                                <div class="col-md-6 pull-right ">
                                                    <p class="text-right"> @lang('site.custom_order.preview_page.order_price')</p>
                                                </div>
                                                <div class="col-md-6 pull-right">
                                                    <p class="text-left"> {{ number_format($totalPrice) }} @lang('site.currency') </p>
                                                </div>

                                            </div>

                                            @if($setting->tax > 0)
                                                <div class="divider divider-light"></div>
                                                <div class="col-md-12 price-item padded-vertical">
                                                    <div class="col-md-6 pull-right">
                                                        <p class="text-right">@lang('site.carts.tax')</p>
                                                    </div>
                                                    <div class="col-md-6 pull-right">
                                                        <p class="text-left"> {{ number_format($taxPrice) }} @lang('site.currency') </p>
                                                    </div>

                                                </div>
                                            @endif

                                            <div class="divider divider-light"></div>
                                            <div class="col-md-12 price-item padded-vertical">
                                                <div class="col-md-6 pull-right">
                                                    <p class="text-right">@lang('site.carts.total_to_pay')</p>
                                                </div>
                                                <div class="col-md-6 pull-right">
                                                    <p class="text-left"> {{ number_format($totalPrice + $taxPrice) }} @lang('site.currency') </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="alert space-2-20 alert-warning alert-dismissable">
                                            <h6><strong>@lang('site.custom_order.preview_page.point') :</strong> @lang('site.custom_order.preview_page.price_might_changes')</h6>
                                            <h6><strong>@lang('site.custom_order.preview_page.point') :</strong> @lang('site.custom_order.preview_page.tax_might_changes')</h6>
                                            <h6><strong>@lang('site.custom_order.preview_page.point') :</strong> @lang('site.custom_order.preview_page.preprare_time',['time' => $routine->preparation_time])</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 space-2-30">
                            <a href="@if(!is_null($package)) {{ action('CustomOrder\Controllers\Site\OrderController@packaging') }} @else {{ action('CustomOrder\Controllers\Site\OrderController@cells') }} @endif" class="btn submit pull-right">
                                <span>@lang('site.custom_order.previous_button')</span>
                            </a>
                            <a href="{{ action('CustomOrder\Controllers\Site\OrderController@shipping') }}" class="btn submit pull-left">@lang('site.custom_order.next_button')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ​<!-- end outer row -->
@endsection