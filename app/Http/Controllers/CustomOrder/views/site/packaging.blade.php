@extends('CustomOrder.views.site.container')
@section('tabs')
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="row">
            <div class="col-xs-12 bg-white packaging">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <div class="col-xs-12 padding-top-5 padding-bottom-5">
                            <img class="status-pic center-block img-responsive"
                                 src="{{asset('assets/site/img/order-step4.png')}}">
                        </div>

                        @if(strlen(strip_tags($setting->custom_order_description)) > 2)
                            <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3">
                                <p class="text-default"
                                   style="font-weight:bolder;font-size: 15px;">{!! $setting->custom_order_description !!}</p>
                            </div>
                        @endif

                    </div><!-- end inner row -->

                    <div class="row product-chooser">
                        <div class="container">
                            {{ Form::open(['action' => 'CustomOrder\Controllers\Site\OrderController@savePackaging','class' => 'form']) }}

                                <div class="col-md-12">
                                    @if(count($amounts) > 0)
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-sm-6  col-sm-push-3 col-xs-12"
                                                     style="border: 1px solid #ebebeb;padding: 1em;">
                                                    <div class="row">
                                                        <div class="col-xs-6 pull-right">
                                                            <p>@lang('site.custom_order.packaging_page.select_amount')</p>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            @foreach($amounts as $amount)
                                                                <label class="checkbox-inline">
                                                                    <label>
                                                                        {{ Form::radio('amount_id',$amount->id,(session()->has('customOrderData.amount_id') && session('customOrderData.amount_id') == $amount->id) ? true : false,['style' => 'margin-left: 1em']) }}
                                                                        <span class="red_text">{{ $amount->title }}</span>
                                                                    </label>
                                                                </label>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(isset($selectedPackage) && !is_null($selectedPackage))
                                        <div class="col-md-12 space-2-50">
                                            <h3 class="text-center">@lang('site.custom_order.packaging_page.selected_package')</h3>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="product-choosed">

                                                        <img src="{{ asset('uploads/package/240xauto-'.$selectedPackage->picture) }}"
                                                             class="img-rounded col-xs-4 col-sm-3 col-md-3 col-lg-3 pull-right"
                                                             alt="Mobile and Desktop">
                                                        <div class="col-xs-8 col-sm-9 col-md-9 col-lg-9">
                                                            <h4 class="title-selected text-danger padding-bottom-1">{{ $selectedPackage->title }}</h4>
                                                            <p class="description">{!! $selectedPackage->description !!}</p>
                                                        </div>

                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(count($packages) > 0)
                                        <div class="col-xs-12 col-sm-12 col-md-12 space-2-50">
                                            <div class="row">
                                                <ul class="nav nav-pills">
                                                    @foreach($packageCategories as $category)
                                                        <li @if((request()->has('category_id') && request('category_id') == $category->id) || (!request()->has('category_id') && $loop->first)) class="active" @endif>
                                                            <a href="{{ action('CustomOrder\Controllers\Site\OrderController@packaging',['category_id' => $category->id]) }}">
                                                                {{ $category->title }}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>

                                                <div class="tab-content">
                                                    <div id="package-category-{{ $category->id }}" class="tab-pane fade in active">
                                                        <div class="row">
                                                            <div class="col-md-12 content-area">

                                                                @foreach($packages as $package)
                                                                    <div class="col-xs-12 pull-right col-sm-12 col-md-3 col-lg-3 layout-select">
                                                                        <div class="product-chooser-item @if((session('customOrderData.package_id') == $package->id) || (!is_null(old('package_id')) && old('package_id') == $package->id)) selected @endif">
                                                                            <button type="button" class="open-modal" data-toggle="modal" data-target="#layoutModal-{{ $loop->iteration }}"><i class="flaticon-search"></i> @lang('site.custom_order.zoom_in')
                                                                            </button>
                                                                            {{ Form::radio('package_id',$package->id,((session('customOrderData.package_id') == $package->id) || (!is_null(old('package_id')) && old('package_id') == $package->id))) }}
                                                                            <img src="{{ asset('uploads/package/240xauto-'.$package->picture) }}"
                                                                                 class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12"
                                                                                 alt="">
                                                                            <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                                                                <span class="title">{{ $package->title }}</span>
                                                                            </div>

                                                                            @if($package->upload_picture == 1)
                                                                                <div class="col-md-12 col-sm-12 col-xs-12 control-group @if(((session('customOrderData.package_id') == $package->id) || (!is_null(old('package_id')) && old('package_id') == $package->id)) && session()->has('customOrderData.package_picture')) show @endif">

                                                                                    <div class="checkbox checkbox-success checkbox-circle">

                                                                                        @php
                                                                                            $checkedUploadPicture = false;
                                                                                            if (((session('customOrderData.package_id') == $package->id) && (session()->has('customOrderData.package_picture'))) || (!is_null(old('upload_picture_enable')) && old('upload_picture_enable') == $package->id)) {
                                                                                                $checkedUploadPicture = true;
                                                                                            }
                                                                                        @endphp
                                                                                        <input type="checkbox" name="upload_picture_enable" id="{{ $package->id }}" value="{{ $package->id }}" @if($checkedUploadPicture) checked @endif/>
                                                                                        <label class="" for="{{ $package->id }}">@lang('site.custom_order.packaging_page.upload_picture')
                                                                                        </label>

                                                                                    </div>
                                                                                    <div class="space-2-20"></div>
                                                                                    @php
                                                                                        $oldPictureUrl = '';
                                                                                        $oldPictureSize = 0;
                                                                                        if (((session('customOrderData.package_id') == $package->id) || (!is_null(old('package_id')) && old('package_id') == $package->id)) && session()->has('customOrderData.package_picture')) {
                                                                                            $oldPictureUrl = asset(session('customOrderData.package_picture_thumb'));
                                                                                            if (File::exists(public_path(session('customOrderData.package_picture')))) {
                                                                                                $oldPictureSize = File::size(session('customOrderData.package_picture'));
                                                                                            }
                                                                                        }
                                                                                    @endphp
                                                                                    <div class="dropzone" data-package-id="{{ $package->id }}"
                                                                                         data-old-picture="{{ $oldPictureUrl }}" data-old-size="{{$oldPictureSize}}">
                                                                                    </div>
                                                                                </div>
                                                                                <!-- Modal -->

                                                                            @endif

                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="layoutModal-{{ $loop->iteration }}" class="modal fade" tabindex="-1" role="dialog"
                                                                         aria-labelledby="myModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-lg">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-remove"></i></button>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="col-md-12"><img src="{{ asset('uploads/package/900xauto-'.$package->picture) }}"
                                                                                                                class="img-responsive img-thumbnail"></div>
                                                                                    <span class="description-modal" style="padding: 30px 20px;">{!! $package->description !!}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="text-center">
                                        {!! $packages->appends(Request::except('page'))->links() !!}
                                            </div>

                                    @endif
                                </div>

                                <div class="col-md-12 space-2-50">
                                    <a href="{{ action('CustomOrder\Controllers\Site\OrderController@cells') }}" class="btn submit pull-right">
                                        <span>@lang('site.custom_order.previous_button')</span>
                                    </a>
                                    <button type="submit" class="btn submit pull-left"> @lang('site.custom_order.next_button') </button>
                                </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ​<!-- end outer row -->
@endsection

@section('style')
    <style>
        .nav-tabs > li, .nav-pills > li {
            float: none;
            display: inline-block;
        }

        .nav-tabs {
            text-align: center;
        }
        .product-chooser-item .img-rounded{
            margin-top: 20px;
        }

        .nav-tabs > .active > a, .nav-tabs > .active > a:hover, .nav-tabs > .active > a:focus {
            border-color: #d45500;
            border-bottom-color: transparent;
        }

        .nav-tabs {
            border-bottom: 1px solid #d45500;
        }

        .nav-pills {
            text-align: center;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            background: #ff5b5b;
            border-radius: 2px;
        }

        .tab-content {
            border: 1px solid #ececec;
        }
        .content-area{
            margin-top:30px;
            margin-bottom: 30px;
        }
        .product-chooser .product-chooser-item span.description{
            text-align: center;
            display: block;
        }
        .dropzone{
            display: none;
        }
        .selected .show .dropzone{
            display: block !important;
        }
    </style>
    {{ Html::style('assets/site/css/order4.css') }}
@endsection

@section('javascript')
    <script>
        $(function () {
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
                $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
                $(this).addClass('selected');
                $(this).find('input[type="radio"]').prop("checked", true);

            });
        });

        $('.checkbox input[type="checkbox"]').not('.disabled').on('click', function () {
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
                $(this).parent().parent().addClass('show');
            } else {
                $box.prop("checked", false);
                $(this).parent().parent().removeClass('show');

            }
        });

        var uploadMaxSizeInByte = {{ $setting->custom_order_picture_max_size }} * 1024 * 1024;

        Dropzone.autoDiscover = false;

        $('.dropzone').each(function (index) {
            var packageId = $(this).attr("data-package-id");
            var oldPictureUrl = $(this).attr("data-old-picture");
            var oldPictureSize = $(this).attr("data-old-size");

            $(this).dropzone({
                url: '{{ action('CustomOrder\Controllers\Site\OrderController@uploadPackagePicture') }}',
                paramName: 'package_picture',
                maxFilesize: uploadMaxSizeInByte ,
                thumbnailWidth: 200,
                thumbnailHeight:140,
                acceptedFiles: "image/jpeg,image/png",
                params: {
                    '_token': "{{ csrf_token() }}",
                    'package_id': packageId
                },
                addRemoveLinks: true,
                dictCancelUpload:'{{ trans('site.custom_order.cells_page.cancel_upload') }}',
                dictCancelUploadConfirmation:'{{ trans('site.custom_order.cells_page.cancel_upload_confirm') }}',
                dictDefaultMessage: ' <div class="icon" style="text-align: center; font-size: 20px;"><i class="flaticon-cloud-computing" ></i></div> {{ trans('site.custom_order.drag_drop') }} ',
                dictRemoveFile: '{{ trans('site.custom_order.delete_picture') }}',
                maxFiles: 1,
                init: function () {
                    if (oldPictureUrl.length > 1) {
                        var mockFile = {name: '@lang('site.custom_order.cells_page.picture')', size: oldPictureSize};
                        this.options.addedfile.call(this, mockFile);
                        this.options.thumbnail.call(this, mockFile, oldPictureUrl);
                        this.emit("complete", mockFile);
                    }

                    this.on("error", function (file, errorMessage) {
                        if (file.size > uploadMaxSizeInByte) {
                            this.removeFile(file);
                            alert("@lang('site.custom_order.cells_page.upload_validate_size',['size' => $setting->custom_order_picture_max_size])");
                        }
                        if (file.type !== "image/jpeg" && file.type !== "image/png") {
                            this.removeFile(file);
                            alert("@lang('site.custom_order.cells_page.upload_validate_mime')");
                        }
                    });

                    this.on('success', function (file,response) {
                        if(response.error) {
                            this.removeFile(file);
                            alert('@lang('site.custom_order.cells_page.upload_error')');
                        }
                    });

                    this.on("maxfilesexceeded", function (file) {
                        alert('@lang('site.custom_order.cells_page.upload_only_one_picture')');
                        this.removeAllFiles();
                        this.addFile(file);
                    });

                    this.on('addedfile', function (file) {
                        if (oldPictureUrl.length > 1) {
                            this.removeFile(mockFile);
                        }
                    });

                    var dropzone = this;

                    this.on('removedfile', function (file) {
                        $.ajax({
                            type: 'POST',
                            url: '{{ action('CustomOrder\Controllers\Site\OrderController@removePackagePicture') }}',
                            data: {
                                _token: "{{ csrf_token() }}"
                            },
                            dataType: 'html',
                            success: function (result) {
                                var response = JSON.parse(result);
                                dropzone.removeAllFiles();
                                if(response.error) {
                                    alert('@lang('site.custom_order.cells_page.upload_error')');
                                }
                            }
                        })
                    });
                }
            });
        });
    </script>

@endsection
