<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
</div>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        <div class="uk-alert uk-alert-warning">تصویر فقط باید با پسوند png باشد و بصورت شابلون</div>
        <div class="uk-alert uk-alert-warning">تصویر در سایز 500 در 500 پیکسل باشد و شابلون در مرکز آن قرار بگیرد</div>
    </div>
    <div class="uk-width-medium-1-3">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a uk-margin-small-bottom">تصویر</h3>
                {!! Form::file('picture',['class' => 'dropify','remove' => 'file_remove','data-default-file' => (isset($item)? (!is_null($item->picture))? asset('uploads/shape/'.$item->picture) :'' : '')]) !!}
                {!! Form::hidden('file_remove',0) !!}
            </div>
        </div>
    </div>
</div>
