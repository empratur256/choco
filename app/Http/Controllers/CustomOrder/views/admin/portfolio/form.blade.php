<div class="uk-width-medium-1-1">
    <div class="uk-alert" data-uk-alert="">ابعاد پیشنهادی 520 در 360</div>
</div>
<div class="uk-grid" data-uk-grid-margin>


    <div class="uk-width-medium-2-3">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a uk-margin-small-bottom">تصویر اجرا شده</h3>
                {!! Form::file('performed_picture',['class' => 'dropify','data-default-file' => (isset($item)? (!is_null($item->performed_picture))? asset('uploads/portfolio/'.$item->performed_picture) :'' : '')]) !!}
            </div>
        </div>
    </div>
</div>
