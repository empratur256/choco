@extends('views.layouts.admin.master')
@section('content')
    @include('CustomOrder.views.admin.cell.search')
    {!! Form::open(['action'=>['CustomOrder\Controllers\Admin\CellController@destroy',$layout->id,1],'method' =>'DELETE']) !!}

    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('CustomOrder.CellController.destroy'))
                                <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید ؟');" class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">حذف</button>
                            @endif
                            @if(permission('CustomOrder.CellController.create'))
                                <a class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" href="{!! action('CustomOrder\Controllers\Admin\CellController@create',$layout->id) !!}" style="margin-left: 15px !important;">ایجاد</a>
                            @endif
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('CustomOrder\Controllers\Admin\CellController@index',$layout->id) !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            @unless($layout->active)
                <div class="uk-alert uk-alert-warning" data-uk-alert="">
                    <a href="#" class="uk-alert-close uk-close"></a>
                    چینش مورد نظر شما غیر فعال می باشد برای نمایش در سایت بعد از وارد کردن اطلاعات خانه های شکلات آن را فعال کنید.
                </div>
            @endunless
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>
                            <th class="uk-width-1-10  uk-text-center small_col">
                                {!! Form::checkbox('',1,null,['class' => 'check_all','data-md-icheck']) !!}
                            </th>
                            <th class="uk-width-2-10 uk-text-center">شماره</th>
                            <th class="uk-width-2-10 uk-text-center">شکل</th>
                            <th class="uk-width-2-10 uk-text-center">وزن</th>
                            <th class="uk-width-2-10 uk-text-center">.....</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            <tr class="uk-text-center">
                                <td class="uk-text-center uk-table-middle small_col">
                                    {!! Form::checkbox('deleteId[]',$item->id,null,['class' => 'check_row','data-md-icheck']) !!}
                                </td>
                                <td>{{ $item->cell }}</td>
                                <td>{{ $item->shape->title }}</td>
                                <td>{{ $item->weight->title }} گرم</td>
                                <td class="uk-text-center">
                                    @if(permission('CustomOrder.CellController.edit'))
                                        <a href="{!! action('CustomOrder\Controllers\Admin\CellController@edit',[$layout->id,$item->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection