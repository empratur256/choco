<div class="uk-grid" data-uk-grid-margin>

    <div class="uk-width-medium-1-1">
        @unless($layout->active)
            <div class="uk-alert uk-alert-warning" data-uk-alert="">
                <a href="#" class="uk-alert-close uk-close"></a>
                چینش مورد نظر شما غیر فعال می باشد برای نمایش در سایت بعد از وارد کردن اطلاعات خانه های شکلات آن را فعال کنید.
            </div>
        @endunless
    </div>
    <div class="uk-width-medium-1-3">
        {!! Form::label('cell','شماره خانه') !!}
        {!! Form::text('cell',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-3">
        {!! Form::label('shape_id','شکل') !!}
        {!! Form::select('shape_id',$shapes,null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-3">
        {!! Form::label('weight_id','وزن') !!}
        {!! Form::select('weight_id',$weights,null,['class'=>'md-input']) !!}
    </div>
</div>
