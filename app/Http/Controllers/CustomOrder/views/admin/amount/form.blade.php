<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        <span class="icheck-inline">
            {!! Form::checkbox('active',1,(isset($item)? $item->active : true),['data-switchery','id' => 'active']) !!}
            {!! Form::label('active','فعال',['class' => 'inline-label']) !!}
        </span>
    </div>
</div>
