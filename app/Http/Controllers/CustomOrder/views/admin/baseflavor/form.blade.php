<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-2">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        <span class="icheck-inline">
            {!! Form::checkbox('active',1,(isset($item)? $item->active : true),['data-switchery','id' => 'active']) !!}
            {!! Form::label('active','فعال',['class' => 'inline-label']) !!}
        </span>
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('price','قیمت طعم پایه') !!}
        {!! Form::number('price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('major_price','قیمت عمده طعم پایه') !!}
        {!! Form::number('major_price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('flavor_price','قیمت طعم مکمل') !!}
        {!! Form::number('flavor_price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('flavor_major_price','قیمت عمده طعم مکمل') !!}
        {!! Form::number('flavor_major_price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('flavor_brain_price','قیمت طعم مکمل مغز دار') !!}
        {!! Form::number('flavor_brain_price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('flavor_brain_major_price','قیمت عمده طعم مکمل مغز دار') !!}
        {!! Form::number('flavor_brain_major_price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('brain_price','قیمت مغز دار') !!}
        {!! Form::number('brain_price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('brain_major_price','قیمت عمده مغز دار') !!}
        {!! Form::number('brain_major_price',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-2">
        <h2>طعم های مکمل</h2>
        <div class="uk-grid" data-uk-grid-margin>
            @foreach($flavors as $flavor)
                <div class="uk-width-medium-1-2">
                    {!! Form::label('flavor_'.$flavor->id,$flavor->title) !!}
                    {!! Form::checkbox('flavor[]',$flavor->id,(isset($item))? $item->flavors->where('id',$flavor->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'flavor_'.$flavor->id]) !!}
                </div>
            @endforeach
        </div>
    </div>
</div>


