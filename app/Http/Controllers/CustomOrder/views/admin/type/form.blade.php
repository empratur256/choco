<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-3">
        <span class="icheck-inline">
            {!! Form::checkbox('active',1,(isset($item)? $item->active : true),['data-switchery','id' => 'active']) !!}
            {!! Form::label('active','فعال',['class' => 'inline-label']) !!}
        </span>
    </div>
    <div class="uk-width-medium-1-3">
        <span class="icheck-inline">
            {!! Form::checkbox('has_brain',1,(isset($item)? $item->has_brain : false),['data-switchery','id' => 'has_brain']) !!}
            {!! Form::label('has_brain','حجم دار',['class' => 'inline-label']) !!}
        </span>
    </div>
</div>

<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-3 brains">
        <b>مغزها</b>
        <br><br>
        <div class="uk-grid" data-uk-grid-margin>
            @foreach($brains as $brain)
                <div class="uk-width-medium-1-2">
                    {!! Form::checkbox('brains[]',$brain->id,(isset($item))? $item->brains->where('id',$brain->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'brain_'.$brain->id]) !!}
                    {!! Form::label('brain_'.$brain->id,$brain->title) !!}
                </div>
            @endforeach
        </div>
    </div>
</div>

@push('js')
    <script>
        $(document).ready(function () {
            if ($('#has_brain').is(':checked')) {
                $('.brains').removeClass('uk-hidden');
            } else {
                $('.brains').addClass('uk-hidden');
            }
            $('#has_brain').on('change', function () {
                if ($(this).is(':checked')) {
                    $('.brains').removeClass('uk-hidden');
                } else {
                    $('.brains').addClass('uk-hidden');
                }
            });
        });
    </script>
@endpush