@extends('views.layouts.admin.master')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom" id="adminBreadcrumb"></h3>
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            @include('views.errors.errors')
                            {!! Form::model($item,['action'=> 'CustomOrder\Controllers\Admin\CustomOrderController@saveSetting','files' => true]) !!}
                            <div class="uk-form-row searchPanel">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('custom_order_description','توضیحات صفحات سفارش') !!}
                                        <br>
                                        {!! Form::textarea('custom_order_description',null,['class'=>'md-input ckeditor']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('custom_order_picture_coefficient','ضریب محاسبه هزینه آپلود عکس') !!}
                                        {!! Form::text('custom_order_picture_coefficient',null,['class'=>'md-input']) !!}
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        {!! Form::label('custom_order_picture_max_size','حداکثر حجم آپلود تصاویر سفارش (مگابایت)') !!}
                                        {!! Form::text('custom_order_picture_max_size',null,['class'=>'md-input']) !!}
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <button type="submit" class="md-btn md-btn-primary md-btn-wave-light waves-effect waves-button waves-light " style="margin-top: 10px">ذخیره</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
