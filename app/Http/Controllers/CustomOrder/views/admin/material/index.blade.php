@extends('views.layouts.admin.master')
@section('content')
    @include('CustomOrder.views.admin.material.search')
    {!! Form::open(['action'=>['CustomOrder\Controllers\Admin\MaterialController@destroy',1],'method' =>'DELETE']) !!}

    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('CustomOrder.MaterialController.destroy'))
                                <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید ؟');" class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">حذف</button>
                            @endif
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            @if(permission('CustomOrder.MaterialController.create'))
                                <a class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect" href="{!! action('CustomOrder\Controllers\Admin\MaterialController@create') !!}" style="margin-left: 15px !important;">ایجاد</a>
                            @endif
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('CustomOrder\Controllers\Admin\MaterialController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>
                            <th class="uk-width-1-10  uk-text-center small_col">
                                {!! Form::checkbox('',1,null,['class' => 'check_all','data-md-icheck']) !!}
                            </th>
                            <th class="uk-width-2-10 uk-text-center">عنوان</th>
                            <th class="uk-width-2-10 uk-text-center">تاریخ درج</th>
                            <th class="uk-width-2-10 uk-text-center">.....</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            <tr class="uk-text-center">
                                <td class="uk-text-center uk-table-middle small_col">
                                    {!! Form::checkbox('deleteId[]',$item->id,null,['class' => 'check_row','data-md-icheck']) !!}
                                </td>
                                <td>{{ $item->title }}</td>
                                <td>
                                    <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                </td>
                                <td class="uk-text-center">

                                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}" aria-haspopup="true" aria-expanded="false">
                                        <button class="md-btn">عملیات <i class="material-icons"></i></button>
                                        <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" tabindex="">
                                            <ul class="uk-nav uk-nav-dropdown">
                                                @if(permission('CustomOrder.MaterialController.edit'))
                                                    <li>
                                                        <a href="{!! action('CustomOrder\Controllers\Admin\MaterialController@edit',$item->id) !!}">ویرایش</a>
                                                    </li>
                                                @endif
                                                @if(permission('CustomOrder.BaseFlavorController.index'))
                                                    <li>
                                                        <a href="{!! action('CustomOrder\Controllers\Admin\BaseFlavorController@index',$item->id) !!}">طعم های پایه</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                    @if(permission('CustomOrder.MaterialController.edit'))
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection