<div class="md-card-content">
    <div class="uk-tab-center">
        <ul class="uk-tab" data-uk-tab="{connect:'#product'}">
            <li class="uk-active" aria-expanded="true"><a href="#">اطلاعات</a></li>
            <li aria-expanded="false"><a href="#">دسته بندی محصول</a></li>
            <li aria-expanded="false"><a href="#">وزن ها</a></li>
            <li aria-expanded="false"><a href="#">میزان سفارش</a></li>
            <li aria-expanded="false"><a href="#">اشکال مجاز</a></li>
            <li aria-expanded="false" class="package_tab"><a href="#">بسته بندی ها</a></li>
        </ul>
    </div>
    <ul id="product" class="uk-switcher uk-margin">
        <li aria-hidden="false" class="uk-active">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    {!! Form::label('title','عنوان') !!}
                    {!! Form::text('title',null,['class'=>'md-input']) !!}
                </div>
                <div class="uk-width-medium-1-2">
                    {!! Form::label('price','قیمت') !!}
                    {!! Form::text('price',null,['class'=>'md-input']) !!}
                </div>
                <div class="uk-width-medium-1-2">
                    {!! Form::label('major_price','قیمت عمده') !!}
                    {!! Form::text('major_price',null,['class'=>'md-input']) !!}
                </div>

                @if(isset($item))
                    <div class="uk-width-medium-1-2">
                    <span class="icheck-inline">
                        {!! Form::checkbox('active',1,(isset($item)? $item->active : false),['data-switchery','id' => 'active']) !!}
                        {!! Form::label('active','فعال',['class' => 'inline-label']) !!}
                    </span>
                    </div>
                @endif
                <div class="uk-width-medium-1-4">
                    <span class="icheck-inline">
                        {!! Form::checkbox('select_package',1,(isset($item)? $item->select_package : true),['data-switchery','id' => 'select_package']) !!}
                        {!! Form::label('select_package','امکان انتخاب بسته بندی',['class' => 'inline-label']) !!}
                    </span>
                </div>
                <div class="uk-width-medium-3-4 ">
                    <div class="uk-grid select_package_price uk-hidden" data-uk-grid-margin>

                        <div class="uk-width-medium-1-2">
                            {!! Form::label('package_price','قیمت برای بسته بندی') !!}
                            {!! Form::text('package_price',null,['class'=>'md-input']) !!}
                        </div>
                        <div class="uk-width-medium-1-2">
                            {!! Form::label('package_major_price','قیمت عمده برای بسته بندی') !!}
                            {!! Form::text('package_major_price',null,['class'=>'md-input']) !!}
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-1-2">
                    {!! Form::label('description','توضیحات') !!}
                    <br>
                    {!! Form::textarea('description',null,['class'=>'md-input ckeditor']) !!}
                </div>
                <div class="uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-content">
                            <h3 class="heading_a uk-margin-small-bottom">تصویر</h3>
                            {!! Form::file('picture',['class' => 'dropify','data-default-file' => (isset($item)? (!is_null($item->picture))? asset('uploads/layout/'.$item->picture) :'' : '')]) !!}
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li aria-hidden="false">
            <div class="uk-grid" data-uk-grid-margin>
                @foreach($types as $type)
                    <div class="uk-width-medium-1-2">
                        <span class="icheck-inline">
                            {!! Form::checkbox('types[]',$type->id,(isset($item))? $item->types->where('id',$type->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'type_'.$type->id]) !!}
                            {!! Form::label('type_'.$type->id,$type->title) !!}
                        </span>
                    </div>
                @endforeach
            </div>
        </li>
        <li aria-hidden="false">
            <div class="uk-grid" data-uk-grid-margin>
                @foreach($weights as $weight)
                    <div class="uk-width-medium-1-2">
                        <span class="icheck-inline">
                            {!! Form::checkbox('weights[]',$weight->id,(isset($item))? $item->weights->where('id',$weight->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'weight_'.$weight->id]) !!}
                            {!! Form::label('weight_'.$weight->id,$weight->title.' گرم') !!}
                        </span>
                    </div>
                @endforeach
            </div>
        </li>
        <li aria-hidden="false">
            <div class="uk-grid" data-uk-grid-margin>
                @foreach($amounts as $amount)
                    <div class="uk-width-medium-1-2">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-2-4">
                                {!! Form::checkbox('amount['.$amount->id.'][amount_id]',$amount->id,(isset($item))? $item->amounts->where('id',$amount->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'amount_amount_'.$amount->id]) !!}
                                {!! Form::label('amount_amount_'.$amount->id,$amount->title) !!}
                            </div>
                            <div class="uk-width-medium-2-4">
                                {!! Form::label('coefficient','ضریب محاسبه قیمت') !!}
                                @php $coefficient = null @endphp
                                @if(isset($item))
                                    @if($item->amounts->where('id',$amount->id)->count())
                                        @php $coefficient = $item->amounts->where('id',$amount->id)->first()->pivot->coefficient @endphp
                                    @endif

                                @endif
                                {!! Form::number('amount['.$amount->id.'][coefficient]',$coefficient,['class'=>'md-input']) !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </li>
        <li aria-hidden="false">
            <div class="uk-grid" data-uk-grid-margin>
                @foreach($shapes as $shape)
                    <div class="uk-width-medium-1-2">
                        <span class="icheck-inline">
                            {!! Form::checkbox('shapes[]',$shape->id,(isset($item))? $item->shapes->where('id',$shape->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'shape_'.$shape->id]) !!}
                            {!! Form::label('shape_'.$shape->id,$shape->title) !!}
                        </span>
                    </div>
                @endforeach
            </div>
        </li>
        <li aria-hidden="false">
            <div class="uk-grid" data-uk-grid-margin>
                @foreach($packageCategories as $category)
                    <div class="uk-width-medium-1-2">
                        <h2 style="font-size: 16px !important;font-weight: bold !important;">{{ $category->title }}</h2>
                        @foreach($category->package as $package)
                            <div class="uk-width-medium-1-1">
                                <span class="icheck-inline">
                                    {!! Form::checkbox('packages[]',$package->id,(isset($item))? $item->packages->where('id',$package->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'package_'.$package->id]) !!}
                                    {!! Form::label('package_'.$package->id,$package->admin_title) !!}
                                </span>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </li>


    </ul>
</div>
@push('js')
    <script>

        $(document).ready(function () {
            if (!$('#select_package').is(':checked')) {
                $('.select_package_price').removeClass('uk-hidden');
                $('.package_tab').addClass('uk-hidden');
            } else {
                $('.select_package_price').addClass('uk-hidden');
                $('.package_tab').removeClass('uk-hidden');
            }

            $('#select_package').on('change', function () {
                if (!$(this).is(':checked')) {
                    $('.select_package_price').removeClass('uk-hidden');
                    UIkit.notify({
                        message: "قیمت بسته بندی قیمتی می باشد که برای بسته بندی پیشفرض به سفارش اضافه می شود",
                        status: "warning",
                        timeout: 5000,
                        pos: "top-left"
                    });
                    $('.package_tab').addClass('uk-hidden');

                } else {
                    $('.select_package_price').addClass('uk-hidden');
                    $('.package_tab').removeClass('uk-hidden');
                }
            });


        });
    </script>
@endpush