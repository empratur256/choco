<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-2">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('major','نوع قیمت') !!}
        {!! Form::select('major',[0 => 'قیمت معمولی',1=> 'قیمت عمده'],null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        <div class="uk-alert uk-alert-warning">
            در صورت وارد کردن 0 در فیلد حداکثر تعداد سفارش این تعداد نا محدود می شود
        </div>
    </div>
    <div class="uk-width-medium-1-4">
        {!! Form::label('min_order','حداقل تعداد سفارش') !!}
        {!! Form::number('min_order',null,['class'=>'md-input','min' => 1]) !!}
    </div>
    <div class="uk-width-medium-1-4">
        {!! Form::label('max_order','حداکثر تعداد سفارش') !!}
        {!! Form::number('max_order',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-4">
        {!! Form::label('preparation_time','زمان آمده شدن سفارش') !!}
        {!! Form::number('preparation_time',null,['class'=>'md-input','min' => 0]) !!}
    </div>
    <div class="uk-width-medium-1-4">
        {!! Form::label('order','ترتیب نمایش') !!}
        {!! Form::number('order',null,['class'=>'md-input','min' => 1]) !!}
    </div>
    <div class="uk-width-medium-1-1">
        <h2 style="font-weight: bold !important;font-size: 16px !important;">دسته بندی محصول مجاز در این روال</h2>
        <div class="uk-grid" data-uk-grid-margin>
            @foreach($types as $type)
                <div class="uk-width-medium-1-4">
                    <span class="icheck-inline">
                        {!! Form::checkbox('types[]',$type->id,(isset($item))? $item->types->where('id',$type->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'types_'.$type->id]) !!}
                        {!! Form::label('types_'.$type->id,$type->title) !!}
                    </span>
                </div>
            @endforeach
        </div>
    </div>
    <div class="uk-width-medium-1-1">
        <h2 style="font-weight: bold !important;font-size: 16px !important;">نوع محصول مجاز در این روال</h2>
        <div class="uk-grid" data-uk-grid-margin>
            @foreach($materials as $material)
                <div class="uk-width-medium-1-4">
                    <span class="icheck-inline">
                        {!! Form::checkbox('materials[]',$material->id,(isset($item))? $item->materials->where('id',$material->id)->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'materials_'.$material->id]) !!}
                        {!! Form::label('materials_'.$material->id,$material->title) !!}
                    </span>
                </div>
            @endforeach
        </div>
    </div>
    <div class="uk-width-medium-1-3">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a uk-margin-small-bottom">تصویر</h3>
                {!! Form::file('picture',['class' => 'dropify','data-default-file' => (isset($item)? (!is_null($item->picture))? asset('uploads/routine/'.$item->picture) :'' : '')]) !!}
            </div>
        </div>
    </div>
    <div class="uk-width-medium-2-3">
        {!! Form::label('description','توضیحات') !!}
        <br>
        {!! Form::textarea('description',null,['class'=>'md-input ckeditor']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        <h2 style="font-weight: bold !important;font-size: 16px !important;">چینش های این روال</h2>
        <table class="uk-table uk-table-nowrap table_check">
            <thead>
            <tr>
                <th class="uk-width-2-10 uk-text-center">عنوان چینش</th>
                <th class="uk-width-2-10 uk-text-center">امکان آپلود عکس</th>
                <th class="uk-width-2-10 uk-text-center">امکان تغییر شکل</th>
                <th class="uk-width-2-10 uk-text-center">امکان تغییر وزن</th>
            </tr>
            </thead>
            <tbody>
            @foreach($layouts as $layout)
                @php $layoutItem = null @endphp
                @if(isset($item) && $item->layouts->where('id',$layout->id)->count())
                    @php $layoutItem = $item->layouts->where('id',$layout->id)->first() @endphp
                @endif

                <tr @if(!is_null($layoutItem)) class="row_checked" @endif>
                    <td>
                         <span class="icheck-inline">
                             {!! Form::checkbox('layout['.$layout->id.'][id]',$layout->id,(!is_null($layoutItem))? $layoutItem->count() : 0,['class' => 'check_row','data-md-icheck','id' => 'layout_id_'.$layout->id]) !!}
                             {!! Form::label('layout_id_'.$layout->id,$layout->title) !!}
                         </span>
                    </td>
                    <td>
                          <span class="icheck-inline">
                              {!! Form::checkbox('layout['.$layout->id.'][upload_picture]',$layout->id,(!is_null($layoutItem))? $layoutItem->pivot->upload_picture : 0,['class' => 'check_row','data-md-icheck','id' => 'layout_picture_'.$layout->id]) !!}
                              {!! Form::label('layout_picture_'.$layout->id,'آپلود عکس') !!}
                          </span>
                    </td>
                    <td>
                          <span class="icheck-inline">
                              {!! Form::checkbox('layout['.$layout->id.'][change_shape]',$layout->id,(!is_null($layoutItem))? $layoutItem->pivot->change_shape : 0,['class' => 'check_row','data-md-icheck','id' => 'layout_shape_'.$layout->id]) !!}
                              {!! Form::label('layout_shape_'.$layout->id,'تغییر شکل') !!}
                          </span>
                    </td>
                    <td>
                        <span class="icheck-inline">
                            {!! Form::checkbox('layout['.$layout->id.'][change_weight]',$layout->id,(!is_null($layoutItem))? $layoutItem->pivot->change_weight : 0,['class' => 'check_row','data-md-icheck','id' => 'layout_weight_'.$layout->id]) !!}
                            {!! Form::label('layout_weight_'.$layout->id,'تغییر وزن') !!}
                        </span>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

