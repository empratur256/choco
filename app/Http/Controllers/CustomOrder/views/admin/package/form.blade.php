<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('category_id','نوع') !!}
        {!! Form::select('category_id',$categories,null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('title','عنوان در سایت') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('admin_title','عنوان در مدیریت') !!}
        {!! Form::text('admin_title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('price','قیمت') !!}
        {!! Form::number('price',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('major_price','قیمت عمده') !!}
        {!! Form::number('major_price',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-6">
        <span class="icheck-inline">
            {!! Form::checkbox('active',1,(isset($item)? $item->active : true),['data-switchery','id' => 'active']) !!}
            {!! Form::label('active','فعال',['class' => 'inline-label']) !!}
        </span>
    </div>
    <div class="uk-width-medium-2-6">
        {{--<span class="icheck-inline">--}}
            {{--{!! Form::checkbox('custom_design',1,(isset($item)? $item->custom_design : true),['data-switchery','id' => 'custom_design']) !!}--}}
            {{--{!! Form::label('custom_design','امکان طراحی اختصاصی در سایت چاپ',['class' => 'inline-label']) !!}--}}
        {{--</span>--}}
    </div>
    <div class="uk-width-medium-2-6">
        <span class="icheck-inline">
            {!! Form::checkbox('upload_picture',1,(isset($item)? $item->upload_picture : true),['data-switchery','id' => 'upload_picture']) !!}
            {!! Form::label('upload_picture','امکان آپلود تصویر روی جعبه',['class' => 'inline-label']) !!}
        </span>
    </div>
    <div class="uk-width-medium-1-6 uk-hidden upload_picture_price">
        {!! Form::label('upload_picture_price','قیمت چاپ') !!}
        {!! Form::number('upload_picture_price',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        {!! Form::label('description','توضیحات') !!}
        <br>
        {!! Form::textarea('description',null,['class'=>'md-input ckeditor']) !!}
    </div>
    <div class="uk-width-medium-1-2">
        <div class="md-card">
            <div class="md-card-content">
                <h3 class="heading_a uk-margin-small-bottom">تصویر</h3>
                {!! Form::file('picture',['class' => 'dropify','remove' => 'file_remove','data-default-file' => (isset($item)? asset('uploads/package/'.$item->picture) : '')]) !!}
                {!! Form::hidden('file_remove',0) !!}
            </div>
        </div>
    </div>
</div>
@push('js')
    <script>

        $(document).ready(function () {
            if ($('#upload_picture').is(':checked')) {
                $('.upload_picture_price').removeClass('uk-hidden');
            } else {
                $('.upload_picture_price').addClass('uk-hidden');
            }
            $('#upload_picture').on('change', function () {
                if ($(this).is(':checked')) {
                    $('.upload_picture_price').removeClass('uk-hidden');
                } else {
                    $('.upload_picture_price').addClass('uk-hidden');
                }
            });
        });
    </script>
@endpush