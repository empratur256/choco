<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Flavor;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\FlavorRequest;
use Illuminate\Http\Request;

class FlavorController extends Controller
{

    public function index()
    {
        $items = Flavor::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.flavor.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.flavor.add');
    }

    public function store(FlavorRequest $request)
    {
        $item              = new Flavor();
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\FlavorController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Flavor::findOrFail($id);

        return view('CustomOrder.views.admin.flavor.edit', compact('item'));
    }

    public function update(FlavorRequest $request, $id)
    {
        $item              = Flavor::findOrFail($id);
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\FlavorController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(FlavorRequest $request)
    {
        Flavor::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
