<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Shape;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\ShapeRequest;
use Illuminate\Http\Request;
use File;
use Image;

class ShapeController extends Controller
{

    public $uploadPath = 'uploads/shape/';

    public $imageSize = [
        [
            'width'  => 500,
            'height' => 500
        ]
    ];

    public function index()
    {
        $items = Shape::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.shape.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.shape.add');
    }

    public function store(ShapeRequest $request)
    {
        $item        = new Shape();
        $item->title = $request->get('title');
        if ($request->hasFile('picture')) {

            if (!File::isDirectory(public_path($this->uploadPath))) {
                File::makeDirectory(public_path($this->uploadPath), 0775, true);
            }

            $file            = $request->file('picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().$file->hashName();

            Image::make($realPath)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);

            $this->createImages($realPath, $fileName);

            $item->picture = $fileName;
        }
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\ShapeController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Shape::findOrFail($id);

        return view('CustomOrder.views.admin.shape.edit', compact('item'));
    }

    public function update(ShapeRequest $request, $id)
    {
        $item        = Shape::findOrFail($id);
        $item->title = $request->get('title');
        if ($request->hasFile('picture')) {

            removeImage($this->uploadPath, $item->picture);

            $file            = $request->file('picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().$file->hashName();

            Image::make($realPath)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);

            $this->createImages($realPath, $fileName);

            $item->picture = $fileName;
        } else {
            if ($request->get('file_remove') == 1) {
                removeImage($this->uploadPath, $item->picture);
                $item->picture = null;
            }
        }
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\ShapeController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(ShapeRequest $request)
    {

        Shape::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }


}
