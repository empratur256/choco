<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Models\Portfolio;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\PortfolioRequest;
use Illuminate\Http\Request;
use File;
use Image;

class PortfolioController extends Controller
{

    protected $uploadPath = 'uploads/portfolio/';

    public $imageSize = [
        [
            'width'  => 520,
            'height' => 360,
        ],
        [
            'width'  => 128,
            'height' => 86,
        ],
    ];

    public function index()
    {
        $items = Portfolio::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.portfolio.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.portfolio.add');
    }

    public function store(PortfolioRequest $request)
    {
        $item = new Portfolio();

        if($request->hasFile('performed_picture')){
            if(!File::isDirectory(public_path($this->uploadPath))){
                File::makeDirectory(public_path($this->uploadPath), 0775, true);
            }
            $file            = $request->file('performed_picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().'performed_picture'.$file->hashName();
            Image::make($realPath)->widen(1920, function($constraint){
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);
            $this->createImages($realPath, $fileName);
            $item->performed_picture = $fileName;
        }
        $item->save();

        return redirect()->action('CustomOrder\Controllers\Admin\PortfolioController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Portfolio::findOrFail($id);

        return view('CustomOrder.views.admin.portfolio.edit', compact('item'));
    }

    public function update(PortfolioRequest $request, $id)
    {
        $item = Portfolio::findOrFail($id);

        if($request->hasFile('performed_picture')){
            removeImage($this->uploadPath, $item->posted_picture);
            $file            = $request->file('performed_picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().'performed_picture'.$file->hashName();
            Image::make($realPath)->widen(1920, function($constraint){
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);
            $this->createImages($realPath, $fileName);
            $item->performed_picture = $fileName;
        }
        $item->save();

        return redirect()->action('CustomOrder\Controllers\Admin\PortfolioController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(PortfolioRequest $request)
    {
        $items = Portfolio::whereIn('id', $request->get('deleteId'))->get();
        foreach($items as $item){
            removeImage($this->uploadPath, $item->performed_picture);
        }
        Portfolio::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

}
