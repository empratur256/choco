<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use DB;
use Exception;
use Log;
use Sedehi\Http\Controllers\CustomOrder\Models\Amount;
use Sedehi\Http\Controllers\CustomOrder\Models\Layout;
use Sedehi\Http\Controllers\CustomOrder\Models\PackageCategory;
use Sedehi\Http\Controllers\CustomOrder\Models\Shape;
use Sedehi\Http\Controllers\CustomOrder\Models\Type;
use Sedehi\Http\Controllers\CustomOrder\Models\Weight;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\LayoutRequest;
use File;
use Image;

class LayoutController extends Controller
{

    public $uploadPath = 'uploads/layout/';

    public $imageSize = [
        [
            'width'  => 900,

        ],
        [
            'width' => 240,
        ]
    ];

    public function index()
    {
        $items = Layout::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.layout.index', compact('items'));
    }

    public function create()
    {
        $weights    = Weight::all();
        $amounts    = Amount::all();
        $shapes     = Shape::all();
        $types      = Type::all();
        $packageCategories = PackageCategory::with('package')->has('package')->get();

        return view('CustomOrder.views.admin.layout.add', compact(
            'weights',
            'amounts',
            'shapes',
            'packageCategories',
            'types'
        ));
    }

    public function store(LayoutRequest $request)
    {
        DB::beginTransaction();
        try {
            $item                 = new Layout();
            $item->title          = $request->get('title');
            $item->price          = $request->get('price');
            $item->major_price    = $request->get('major_price');
            $item->description    = $request->get('description');
            $item->active         = 0;
            $item->weight         = 0;
            $item->capacity       = 0;

            if (!$request->has('select_package')) {
                $item->select_package      = $request->has('select_package');
                $item->package_price       = $request->get('package_price');
                $item->package_major_price = $request->get('package_major_price');
            }


            if ($request->hasFile('picture')) {

                if (!File::isDirectory(public_path($this->uploadPath))) {
                    File::makeDirectory(public_path($this->uploadPath), 0775, true);
                }

                $file            = $request->file('picture');
                $realPath        = $file->getRealPath();
                $destinationPath = public_path($this->uploadPath);
                $fileName        = time().$file->hashName();

                Image::make($realPath)->widen(1920, function ($constraint) {
                    $constraint->upsize();
                })->save($destinationPath.$fileName, 90);

                $this->createImages($realPath, $fileName);

                $item->picture = $fileName;
            }

            $item->save();

            if ($request->has('types')) {
                $item->types()->attach($request->get('types'));
            }
            if ($request->has('weights')) {
                $item->weights()->attach($request->get('weights'));
            }
            if ($request->has('shapes')) {
                $item->shapes()->attach($request->get('shapes'));
            }
            if ($request->has('packages') && $request->has('select_package')) {
                $item->packages()->attach($request->get('packages'));
            }
            if ($request->has('amount')) {
                $item->addAmounts($request->get('amount'));
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar add layout '.$e);

            return redirect()->back()->withInput()->with('error', 'مشکل در ذخیره اطلاعات');
        }


        return redirect()
            ->action('CustomOrder\Controllers\Admin\CellController@create', $item->id)
            ->with('success', ['اطلاعات با موفقیت ذخیره شد', 'لطفا سلول های شکلات این چینش را تعریف کنید']);
    }

    public function edit($id)
    {
        $item       = Layout::findOrFail($id);
        $weights    = Weight::all();
        $amounts    = Amount::all();
        $shapes     = Shape::all();
        $types      = Type::all();
        $packageCategories = PackageCategory::with('package')->has('package')->get();

        return view('CustomOrder.views.admin.layout.edit',
                    compact('item', 'types', 'weights', 'amounts', 'shapes', 'packageCategories'));
    }

    public function update(LayoutRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $item                 = Layout::findOrFail($id);
            $item->title          = $request->get('title');
            $item->price          = $request->get('price');
            $item->major_price    = $request->get('major_price');
            $item->description    = $request->get('description');

            if ($request->has('active')) {
                if ($item->cells()->count() == 0) {
                    return redirect()
                        ->back()
                        ->withInput()
                        ->with('error', 'به دلیل تعریف نشدن خانه های شکلات امکان فعال سازی نمی باشد');
                } else {
                    $item->active = $request->has('active');
                }
            }else{
                $item->active = 0;
            }

            if (!$request->has('select_package')) {
                $item->select_package      = $request->has('select_package');
                $item->package_price       = $request->get('package_price');
                $item->package_major_price = $request->get('package_major_price');
            } else {
                $item->select_package      = 1;
                $item->package_price       = 0;
                $item->package_major_price = 0;
            }

            if ($request->hasFile('picture')) {

                removeImage($this->uploadPath, $item->picture);

                $file            = $request->file('picture');
                $realPath        = $file->getRealPath();
                $destinationPath = public_path($this->uploadPath);
                $fileName        = time().$file->hashName();

                Image::make($realPath)->widen(1920, function ($constraint) {
                    $constraint->upsize();
                })->save($destinationPath.$fileName, 90);

                $this->createImages($realPath, $fileName);

                $item->picture = $fileName;
            }

            $item->save();

            if ($request->has('types')) {
                $item->types()->sync($request->get('types'));
            }

            if ($request->has('weights')) {
                $item->weights()->sync($request->get('weights'));
            } else {
                $item->weights()->sync([]);
            }

            if ($request->has('shapes')) {
                $item->shapes()->sync($request->get('shapes'));
            } else {
                $item->shapes()->sync([]);
            }

            if ($request->has('packages') && $request->has('select_package')) {
                $item->packages()->sync($request->get('packages'));
            }

            if (!$request->has('select_package')) {
                $item->packages()->sync([]);
            }

            if ($request->has('amount')) {
                $item->addAmounts($request->get('amount'));
            } else {
                $item->addAmounts([]);
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar add layout '.$e);

            return redirect()->back()->withInput()->with('error', 'مشکل در ذخیره اطلاعات');
        }

        return redirect()
            ->action('CustomOrder\Controllers\Admin\LayoutController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(LayoutRequest $request)
    {
        $items = Layout::whereIn('id', $request->get('deleteId'))->get();

        foreach ($items as $item) {
            $item->cells()->delete();
            $item->delete();
        }

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
