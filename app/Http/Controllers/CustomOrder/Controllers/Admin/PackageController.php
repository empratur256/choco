<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use File;
use Image;
use Sedehi\Http\Controllers\CustomOrder\Models\Package;
use Sedehi\Http\Controllers\CustomOrder\Models\PackageCategory;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\PackageRequest;
use Illuminate\Http\Request;

class PackageController extends Controller
{

    protected $uploadPath = 'uploads/package/';

    public $imageSize = [
        [
            'width' => 240,
        ],
        [
            'width'  => 900,
        ],
    ];

    public function index()
    {
        $items = Package::with('category')->filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.package.index', compact('items'));
    }

    public function create()
    {
        $categories = PackageCategory::pluck('title', 'id');

        return view('CustomOrder.views.admin.package.add', compact('categories'));
    }

    public function store(PackageRequest $request)
    {
        $item                       = new Package();
        $item->admin_title          = $request->get('admin_title');
        $item->title                = $request->get('title');
        $item->price                = $request->get('price');
        $item->major_price          = $request->get('major_price');
        $item->category_id          = $request->get('category_id');
        $item->description          = $request->get('description');
        $item->active               = $request->has('active');
        $item->upload_picture       = $request->has('upload_picture');
        $item->custom_design        = $request->has('custom_design');
        $item->upload_picture_price = ($request->has('upload_picture_price')) ? $request->get('upload_picture_price') : 0;

        if ($request->hasFile('picture')) {
            if (!File::isDirectory(public_path($this->uploadPath))) {
                File::makeDirectory(public_path($this->uploadPath), 0775, true);
            }
            $file            = $request->file('picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().$file->hashName();
            Image::make($realPath)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);
            $this->createImages($realPath, $fileName);
            $item->picture = $fileName;
        }
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\PackageController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item       = Package::findOrFail($id);
        $categories = PackageCategory::pluck('title', 'id');

        return view('CustomOrder.views.admin.package.edit', compact('item', 'categories'));
    }

    public function update(PackageRequest $request, $id)
    {
        $item                       = Package::findOrFail($id);
        $item->admin_title          = $request->get('admin_title');
        $item->title                = $request->get('title');
        $item->price                = $request->get('price');
        $item->major_price          = $request->get('major_price');
        $item->category_id          = $request->get('category_id');
        $item->description          = $request->get('description');
        $item->active               = $request->has('active');
        $item->upload_picture       = $request->has('upload_picture');
        $item->custom_design        = $request->has('custom_design');
        $item->upload_picture_price = ($request->has('upload_picture_price')) ? $request->get('upload_picture_price') : 0;

        if ($request->hasFile('picture')) {
            if (!File::isDirectory(public_path($this->uploadPath))) {
                File::makeDirectory(public_path($this->uploadPath), 0775, true);
            }
            removeImage($this->uploadPath, $item->picture);
            $file            = $request->file('picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().$file->hashName();
            Image::make($realPath)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);
            $this->createImages($realPath, $fileName);
            $item->picture = $fileName;
        }
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\PackageController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(PackageRequest $request)
    {
        Package::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
