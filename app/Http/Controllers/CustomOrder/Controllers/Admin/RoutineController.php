<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Layout;
use Sedehi\Http\Controllers\CustomOrder\Models\Routine;
use Sedehi\Http\Controllers\CustomOrder\Models\Material;
use Sedehi\Http\Controllers\CustomOrder\Models\Type;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\RoutineRequest;
use File;
use Image;

class RoutineController extends Controller
{

    public $uploadPath = 'uploads/routine/';

    public $imageSize = [
        [
            'width'  => 350
        ]
    ];

    public function index()
    {
        $items = Routine::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.routine.index', compact('items'));
    }

    public function create()
    {
        $layouts    = Layout::all();
        $materials  = Material::all();
        $types      = Type::all();

        return view('CustomOrder.views.admin.routine.add', compact('layouts', 'types', 'materials'));
    }

    public function store(RoutineRequest $request)
    {
        if ($request->get('max_order') > 0 && $request->get('min_order') > $request->get('max_order')) {
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'مقدار حداقل سفارش نمی تواند از حداکثر سفارش بیشتر باشد');
        }
        $item                   = new Routine();
        $item->title            = $request->get('title');
        $item->min_order        = $request->get('min_order');
        $item->max_order        = $request->get('max_order');
        $item->preparation_time = $request->get('preparation_time');
        $item->order            = $request->get('order');
        $item->major            = $request->get('major');
        $item->description      = $request->get('description');
        $item->layout_select    = 'custom';

        if ($request->hasFile('picture')) {

            if (!File::isDirectory(public_path($this->uploadPath))) {
                File::makeDirectory(public_path($this->uploadPath), 0775, true);
            }

            $file            = $request->file('picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().$file->hashName();

            Image::make($realPath)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);

            $this->createImages($realPath, $fileName);

            $item->picture = $fileName;
        }
        $item->save();

        if ($request->has('materials')) {
            $item->materials()->attach($request->get('materials'));
        }

        if ($request->has('types')) {
            $item->types()->attach($request->get('types'));
        }

        if ($request->has('layout')) {
            $item->syncLayouts($request->get('layout'));
        }

        return redirect()
            ->action('CustomOrder\Controllers\Admin\RoutineController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item       = Routine::findOrFail($id);
        $layouts    = Layout::all();
        $materials  = Material::all();
        $types      = Type::all();

        return view('CustomOrder.views.admin.routine.edit', compact('item', 'layouts', 'types', 'materials'));
    }

    public function update(RoutineRequest $request, $id)
    {
        if ($request->get('max_order') > 0 && $request->get('min_order') > $request->get('max_order')) {
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'مقدار حداقل سفارش نمی تواند از حداکثر سفارش بیشتر باشد');
        }

        $item                   = Routine::findOrFail($id);
        $item->title            = $request->get('title');
        $item->min_order        = $request->get('min_order');
        $item->max_order        = $request->get('max_order');
        $item->preparation_time = $request->get('preparation_time');
        $item->order            = $request->get('order');
        $item->major            = $request->get('major');
        $item->description      = $request->get('description');
        $item->layout_select    = 'custom';
        if ($request->hasFile('picture')) {

            removeImage($this->uploadPath, $item->picture);

            $file            = $request->file('picture');
            $realPath        = $file->getRealPath();
            $destinationPath = public_path($this->uploadPath);
            $fileName        = time().$file->hashName();

            Image::make($realPath)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath.$fileName, 90);

            $this->createImages($realPath, $fileName);

            $item->picture = $fileName;
        }
        $item->save();

        if ($request->has('types')) {
            $item->types()->sync($request->get('types'));
        } else {
            $item->types()->sync([]);
        }

        if ($request->has('materials')) {
            $item->materials()->sync($request->get('materials'));
        } else {
            $item->materials()->sync([]);
        }

        if ($request->has('layout')) {
            $item->syncLayouts($request->get('layout'));
        } else {
            $item->syncLayouts([]);
        }

        return redirect()
            ->action('CustomOrder\Controllers\Admin\RoutineController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(RoutineRequest $request)
    {

        Routine::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }


}
