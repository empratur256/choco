<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Material;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\MaterialRequest;

class MaterialController extends Controller
{
    public function index()
    {
        $items = Material::filter()->latest()->paginate(10);

        return view('CustomOrder.views.admin.material.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.material.add');
    }

    public function store(MaterialRequest $request)
    {
        $item              = new Material();
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\MaterialController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Material::findOrFail($id);

        return view('CustomOrder.views.admin.material.edit', compact('item'));
    }

    public function update(MaterialRequest $request, $id)
    {
        $item              = Material::findOrFail($id);
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\MaterialController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(MaterialRequest $request)
    {
        Material::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
