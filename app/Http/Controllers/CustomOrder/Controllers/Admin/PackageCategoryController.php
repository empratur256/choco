<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\PackageCategory;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\PackageCategoryRequest;
use Illuminate\Http\Request;

class PackageCategoryController extends Controller
{

    public function index()
    {
        $items = PackageCategory::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.package.category.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.package.category.add');
    }

    public function store(PackageCategoryRequest $request)
    {
        $item             = new PackageCategory();
        $item->title       = $request->get('title');
        $item->save();

        return redirect()->action('CustomOrder\Controllers\Admin\PackageCategoryController@index')->with('success', 'اطلاعات با موفقیت ذخیره شد');

    }

    public function edit($id)
    {
        $item = PackageCategory::findOrFail($id);
        return view('CustomOrder.views.admin.package.category.edit', compact('item'));
    }

    public function update(PackageCategoryRequest $request, $id)
    {
        $item             = PackageCategory::findOrFail($id);
        $item->title       = $request->get('title');
        $item->save();

        return redirect()->action('CustomOrder\Controllers\Admin\PackageCategoryController@index')->with('success', 'اطلاعات با موفقیت ذخیره شد');

    }

    public function destroy(PackageCategoryRequest $request)
    {
        PackageCategory::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
