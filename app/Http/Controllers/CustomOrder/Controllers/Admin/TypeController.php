<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Brain;
use Sedehi\Http\Controllers\CustomOrder\Models\Type;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\TypeRequest;

class TypeController extends Controller
{
    public function index()
    {
        $items = Type::filter()->latest()->paginate(10);

        return view('CustomOrder.views.admin.type.index', compact('items'));
    }

    public function create()
    {
        $brains = Brain::all();

        return view('CustomOrder.views.admin.type.add',compact('brains'));
    }

    public function store(TypeRequest $request)
    {
        $item              = new Type();
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->has_brain   = $request->has('has_brain');
        $item->save();

        if ($request->has('brains') && count($request->get('brains')) > 0) {
            $item->brains()->sync($request->get('brains'));
        }

        return redirect()
            ->action('CustomOrder\Controllers\Admin\TypeController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Type::findOrFail($id);

        $brains = Brain::all();

        return view('CustomOrder.views.admin.type.edit', compact('item','brains'));
    }

    public function update(TypeRequest $request, $id)
    {
        $item              = Type::findOrFail($id);
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->has_brain   = $request->has('has_brain');
        $item->save();

        if ($request->has('brains') && count($request->get('brains')) > 0) {
            $item->brains()->sync($request->get('brains'));
        } else {
            $item->brains()->detach();
        }

        return redirect()
            ->action('CustomOrder\Controllers\Admin\TypeController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(TypeRequest $request)
    {
        Type::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
