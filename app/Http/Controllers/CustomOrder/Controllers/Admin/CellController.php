<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Cell;
use Sedehi\Http\Controllers\CustomOrder\Models\Layout;
use Sedehi\Http\Controllers\CustomOrder\Models\Shape;
use Sedehi\Http\Controllers\CustomOrder\Models\Weight;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\CellRequest;
use Illuminate\Http\Request;

class CellController extends Controller
{

    public function index($id)
    {
        $layout = Layout::findOrFail($id);
        $items  = $layout->cells()->with([
                                             'shape'  => function ($query) {
                                                 $query->withTrashed();
                                             },
                                             'weight' => function ($query) {
                                                 $query->withTrashed();
                                             }
                                         ])->filter()->orderBy('cell', 'ASC')->paginate(20);

        return view('CustomOrder.views.admin.cell.index', compact('layout', 'items'));
    }

    public function create($id)
    {
        $layout = Layout::findOrFail($id);

        $shapes  = Shape::pluck('title', 'id');
        $weights = Weight::pluck('title', 'id');

        return view('CustomOrder.views.admin.cell.add', compact('layout', 'shapes', 'weights'));
    }

    public function store(CellRequest $request, $id)
    {
        $layout = Layout::findOrFail($id);

        $item            = new Cell();
        $item->cell      = $request->get('cell');
        $item->layout_id = $layout->id;
        $item->shape_id  = $request->get('shape_id');
        $item->weight_id = $request->get('weight_id');
        $item->save();

        $cells            = $layout->cells()->with('weight')->get();
        $layout->capacity = $cells->count();
        $layout->weight   = $cells->sum('weight.title');
        $layout->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\CellController@index', $layout->id)
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id, $cell)
    {
        $layout  = Layout::findOrFail($id);
        $item    = $layout->cells()->findOrFail($cell);
        $shapes  = Shape::pluck('title', 'id');
        $weights = Weight::pluck('title', 'id');

        return view('CustomOrder.views.admin.cell.edit', compact('item', 'shapes', 'weights', 'layout'));
    }

    public function update(CellRequest $request, $id, $cell)
    {
        $layout          = Layout::findOrFail($id);
        $item            = Cell::findOrFail($cell);
        $item->cell      = $request->get('cell');
        $item->shape_id  = $request->get('shape_id');
        $item->weight_id = $request->get('weight_id');
        $item->save();

        $cells            = $layout->cells()->with('weight')->get();
        $layout->capacity = $cells->count();
        $layout->weight   = $cells->sum('weight.title');
        $layout->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\CellController@index', $layout->id)
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(CellRequest $request, $id)
    {
        $layout = Layout::findOrFail($id);

        Cell::whereIn('id', $request->get('deleteId'))->where('layout_id', $layout->id)->delete();

        $cells            = $layout->cells()->with('weight')->get();
        $layout->capacity = $cells->count();
        $layout->weight   = $cells->sum('weight.title');
        $layout->save();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
