<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\CustomOrderRequest;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\Controller;

class CustomOrderController extends Controller
{
    public function setting()
    {
        $item = Setting::first();

        return view('CustomOrder.views.admin.customorder.setting', compact('item'));
    }

    public function saveSetting(CustomOrderRequest $request)
    {
        $setting = Setting::first();
        if (is_null($setting)) {
            $setting = new Setting();
        }
        $setting->custom_order_description         = request('custom_order_description');
        $setting->custom_order_picture_coefficient = request()->get('custom_order_picture_coefficient');
        $setting->custom_order_picture_max_size = request()->get('custom_order_picture_max_size');
        $setting->save();

        return back()->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }
}
