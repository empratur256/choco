<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Weight;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\WeightRequest;
use Illuminate\Http\Request;

class WeightController extends Controller
{

    public function index()
    {
        $items = Weight::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.weight.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.weight.add');
    }

    public function store(WeightRequest $request)
    {
        $item         = new Weight();
        $item->title  = $request->get('title');
        $item->active = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\WeightController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Weight::findOrFail($id);

        return view('CustomOrder.views.admin.weight.edit', compact('item'));
    }

    public function update(WeightRequest $request, $id)
    {
        $item         = Weight::findOrFail($id);
        $item->title  = $request->get('title');
        $item->active = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\WeightController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(WeightRequest $request)
    {
        Weight::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
