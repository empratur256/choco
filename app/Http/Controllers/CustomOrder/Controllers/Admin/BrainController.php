<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Brain;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Models\CustomOrder;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\BrainRequest;
use Illuminate\Http\Request;

class BrainController extends Controller
{

    public function index()
    {
        $items = Brain::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.brain.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.brain.add');
    }

    public function store(BrainRequest $request)
    {
        $item              = new Brain();
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\BrainController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Brain::findOrFail($id);

        return view('CustomOrder.views.admin.brain.edit', compact('item'));
    }

    public function update(BrainRequest $request, $id)
    {
        $item              = Brain::findOrFail($id);
        $item->title       = $request->get('title');
        $item->active      = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\BrainController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(BrainRequest $request)
    {
        Brain::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
