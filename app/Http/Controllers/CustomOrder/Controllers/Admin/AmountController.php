<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\Amount;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\AmountRequest;
use Illuminate\Http\Request;

class AmountController extends Controller
{

    public function index()
    {
        $items = Amount::filter()->latest()->paginate(20);

        return view('CustomOrder.views.admin.amount.index', compact('items'));
    }

    public function create()
    {
        return view('CustomOrder.views.admin.amount.add');
    }

    public function store(AmountRequest $request)
    {
        $item         = new Amount();
        $item->title  = $request->get('title');
        $item->active = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\AmountController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Amount::findOrFail($id);

        return view('CustomOrder.views.admin.amount.edit', compact('item'));
    }

    public function update(AmountRequest $request, $id)
    {
        $item         = Amount::findOrFail($id);
        $item->title  = $request->get('title');
        $item->active = $request->has('active');
        $item->save();

        return redirect()
            ->action('CustomOrder\Controllers\Admin\AmountController@index')
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(AmountRequest $request)
    {
        Amount::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
