<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Admin;

use Sedehi\Http\Controllers\CustomOrder\Models\BaseFlavor;
use Sedehi\Http\Controllers\CustomOrder\Models\Flavor;
use Sedehi\Http\Controllers\CustomOrder\Models\Material;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\CustomOrder\Requests\Admin\BaseFlavorRequest;
use Validator;

class BaseFlavorController extends Controller
{

    public function index($id)
    {
        $material = Material::findOrFail($id);

        $items = BaseFlavor::where('material_id', $material->id)->filter()->latest()->paginate(10);

        return view('CustomOrder.views.admin.baseflavor.index', compact('items', 'id'));
    }

    public function create($id)
    {
        $material   = Material::findOrFail($id);
        $flavors    = Flavor::all();

        return view('CustomOrder.views.admin.baseflavor.add', compact('id', 'flavors', 'material'));
    }

    public function store(BaseFlavorRequest $request, $id)
    {
        $material = Material::findOrFail($id);

        $item                           = new BaseFlavor();
        $item->material_id              = $material->id;
        $item->title                    = $request->get('title');
        $item->price                    = $request->get('price');
        $item->major_price              = ($request->has('major_price')) ? $request->get('major_price') : 0;
        $item->active                   = $request->has('active');
        $item->flavor_price             = ($request->has('flavor_price')) ? $request->get('flavor_price') : 0;
        $item->flavor_major_price       = ($request->has('flavor_major_price')) ? $request->get('flavor_major_price') : 0;
        $item->brain_price              = ($request->has('brain_price')) ? $request->get('brain_price') : 0;
        $item->brain_major_price        = ($request->has('brain_major_price')) ? $request->get('brain_major_price') : 0;
        $item->flavor_brain_price       = ($request->has('flavor_brain_price')) ? $request->get('flavor_brain_price') : 0;
        $item->flavor_brain_major_price = ($request->has('flavor_brain_major_price')) ? $request->get('flavor_brain_major_price') : 0;
        $item->save();

        if ($request->has('flavor')) {
            $item->flavors()->attach($request->get('flavor'));
        }

        return redirect()
            ->action('CustomOrder\Controllers\Admin\BaseFlavorController@index', $id)
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id, $base)
    {
        $material   = Material::findOrFail($id);
        $item       = BaseFlavor::findOrFail($base);
        $flavors    = Flavor::all();

        return view('CustomOrder.views.admin.baseflavor.edit', compact('material','item', 'id', 'flavors'));
    }

    public function update(BaseFlavorRequest $request, $id, $base)
    {
        $material = Material::findOrFail($id);

        $rules = [
            'title'                    => 'required',
            'price'                    => 'required|numeric|min:1',
            'major_price'              => 'required|numeric|min:1',
            'brain_price'              => 'required|numeric|min:1',
            'brain_major_price'        => 'required|numeric|min:1',
        ];

        if ($request->has('flavor') && count($request->get('flavor')) > 0) {
            $rules = array_add($rules,'flavor_price','required|numeric|min:1');
            $rules = array_add($rules,'flavor_major_price','required|numeric|min:1');
            $rules = array_add($rules,'flavor_brain_price','required|numeric|min:1');
            $rules = array_add($rules,'flavor_brain_major_price','required|numeric|min:1');
        } else {
            $rules = array_add($rules,'flavor_price','numeric|min:0');
            $rules = array_add($rules,'flavor_major_price','numeric|min:0');
            $rules = array_add($rules,'flavor_brain_price','numeric|min:0');
            $rules = array_add($rules,'flavor_brain_major_price','numeric|min:0');
        }

        $validator = Validator::make($request->all(),$rules);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $item                           = BaseFlavor::where('material_id', $material->id)->findOrFail($base);
        $item->title                    = $request->get('title');
        $item->price                    = $request->get('price');
        $item->major_price              = ($request->has('major_price')) ? $request->get('major_price') : 0;
        $item->active                   = $request->has('active');
        $item->flavor_price             = ($request->has('flavor_price')) ? $request->get('flavor_price') : 0;
        $item->flavor_major_price       = ($request->has('flavor_major_price')) ? $request->get('flavor_major_price') : 0;
        $item->brain_price              = ($request->has('brain_price')) ? $request->get('brain_price') : 0;
        $item->brain_major_price        = ($request->has('brain_major_price')) ? $request->get('brain_major_price') : 0;
        $item->flavor_brain_price       = ($request->has('flavor_brain_price')) ? $request->get('flavor_brain_price') : 0;
        $item->flavor_brain_major_price = ($request->has('flavor_brain_major_price')) ? $request->get('flavor_brain_major_price') : 0;
        $item->save();

        $item->flavors()->sync(($request->has('flavor')) ? $request->get('flavor') : []);

        return redirect()
            ->action('CustomOrder\Controllers\Admin\BaseFlavorController@index', $id)
            ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(BaseFlavorRequest $request)
    {
        BaseFlavor::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

}
