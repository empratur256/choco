<?php

namespace Sedehi\Http\Controllers\CustomOrder\Controllers\Site;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Log;
use Sedehi\Http\Controllers\City\Models\City;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Coupon\Controllers\Site\CouponController;
use Sedehi\Http\Controllers\CustomOrder\Models\BaseFlavor;
use Sedehi\Http\Controllers\CustomOrder\Models\Brain;
use Sedehi\Http\Controllers\CustomOrder\Models\Flavor;
use Sedehi\Http\Controllers\CustomOrder\Models\Layout;
use Sedehi\Http\Controllers\CustomOrder\Models\Package;
use Sedehi\Http\Controllers\CustomOrder\Models\PackageCategory;
use Sedehi\Http\Controllers\CustomOrder\Models\Portfolio;
use Sedehi\Http\Controllers\CustomOrder\Models\Routine;
use Sedehi\Http\Controllers\CustomOrder\Models\Shape;
use Sedehi\Http\Controllers\CustomOrder\Models\Material;
use Sedehi\Http\Controllers\CustomOrder\Models\Weight;
use Sedehi\Http\Controllers\CustomOrder\Requests\Site\OrderRequest;
use Sedehi\Http\Controllers\Order\Models\Cells;
use Sedehi\Http\Controllers\Order\Models\Item;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\Transport\Models\Transport;
use Sedehi\Http\Controllers\User\Models\Address;
use Sedehi\Payment\Facades\Payment as BankPayment;
use Sedehi\Http\Controllers\Order\Models\Address as OrderAddress;

class OrderController extends Controller
{
    private $defaultSettingFlag = 0;

    public function portfolio()
    {
        $portfolios = Portfolio::inRandomOrder()->limit(24)->get();

        return view('CustomOrder.views.site.portfolio', compact('portfolios'));
    }

    public function setRoutine($id)
    {
        $routine = Routine::findOrFail($id);

        session()->forget('customOrderData');

        $customOrderData = [
            'routine_id'=>  $routine->id,
            'routine_title'=>  $routine->title
        ];

        session()->put('customOrderData',$customOrderData);

        return redirect()->action('CustomOrder\Controllers\Site\OrderController@index');
    }

    public function index()
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        $orderCount = session()->get('customOrderData.order_count');
        $typeId = session()->get('customOrderData.type_id');
        $materialId = session()->get('customOrderData.material_id');
        $hasBrainValue = session()->get('customOrderData.has_brain');

        $routine = Routine::with([
            'types' =>  function($query) {
                return $query->active();
            }
        ])->findOrFail(session()->get('customOrderData.routine_id'));

        $materials = $routine->materials()->active()->pluck('title','id');

        $hasBrainDropdownValues = [];

        $flatType = $routine->types->where('has_brain',0)->first();
        $bulkyType = $routine->types->where('has_brain',1)->first();

        if (!is_null($flatType)) {
            $hasBrainDropdownValues = array_add($hasBrainDropdownValues,0,trans('site.custom_order.flat'));
        }

        if (!is_null($bulkyType)) {
            $hasBrainDropdownValues = array_add($hasBrainDropdownValues,1,trans('site.custom_order.large'));
        }

        return view('CustomOrder.views.site.index',compact(
            'orderCount',
            'typeId',
            'materialId',
            'materials',
            'hasBrainDropdownValues',
            'hasBrainValue'
        ));
    }

    public function saveType(Request $request)
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        $validator = Validator::make($request->all(),[
            'order_count'   =>  'required|numeric|min:1',
            'has_brain'     =>  'required|numeric|in:0,1',
            'type_id'       =>  'required|numeric',
            'material_id'   =>  'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $routine = Routine::findOrFail(session()->get('customOrderData.routine_id'));

        if ($request->get('order_count') < $routine->min_order) {
            return back()->withInput()->with('error',trans('site.custom_order.validation.routine_min_order_error',[
                'min_order' =>  $routine->min_order
            ]));
        }

        if ($routine->max_order > 0) {
            if ($request->get('order_count') > $routine->max_order) {
                return back()->withInput()->with('error',trans('site.custom_order.validation.routine_max_order_error',[
                    'max_order' =>  $routine->max_order
                ]));
            }
        }

        try {

            $material = $routine->materials()->active()->findOrFail($request->get('material_id'));

            $type = $routine->types()->active()->findOrFail($request->get('type_id'));

        } catch (Exception $exception) {
            return back()->withInput()->with('error',trans('site.custom_order.validation.input_data_incorrect'));
        }

        session()->put('customOrderData.order_count',$request->get('order_count'));
        session()->put('customOrderData.has_brain',$request->get('has_brain'));
        session()->put('customOrderData.type_id',$type->id);
        session()->put('customOrderData.material_id',$material->id);

        // remove some sessions if product type changes
        if (session()->has('customOrderData.layout_id')) {
            if ($routine->layout_select == 'all') {
                $layout = Layout::active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->find(session()->get('customOrderData.layout_id'));
            } elseif ($routine->layout_select == 'custom') {
                $layout = $routine->layouts()->active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->find(session()->get('customOrderData.layout_id'));
            }

            if (is_null($layout)) {
                session()->forget('customOrderData.layout_id');
            } else {
                $layoutTypes = $layout->types()->pluck('id')->toArray();

                if (!in_array($type->id,$layoutTypes)) {
                    session()->forget('customOrderData.layout_id');
                }
            }
        }

        return redirect()->action('CustomOrder\Controllers\Site\OrderController@layout');
    }

    public function layout()
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        $routine = Routine::with([
            'types' =>  function($query) {
                return $query->active();
            }
        ])->findOrFail(session()->get('customOrderData.routine_id'));

        $layouts = [];

        if ($routine->layout_select == 'all') {
            $layouts = Layout::active()->whereHas('types',function ($query) use ($routine) {
                return $query->where('id',session()->get('customOrderData.type_id'));
            })->paginate(5);
        } elseif ($routine->layout_select == 'custom') {
            $layouts = $routine->layouts()->active()->whereHas('types',function ($query) use ($routine) {
                return $query->where('id',session()->get('customOrderData.type_id'));
            })->paginate(5);
        }

        $selectedLayout = null;

        if (session()->has('customOrderData.layout_id')) {
            $selectedLayout = Layout::active()->findOrFail(session()->get('customOrderData.layout_id'));
        }

        return view('CustomOrder.views.site.layout',compact(
            'routine',
            'layouts',
            'selectedLayout'
        ));
    }

    public function saveLayout(Request $request)
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        // Validate input data
        $validator = Validator::make($request->all(),[
            'layout_id' =>  ((!session()->has('customOrderData.layout_id')) ? 'required|' : null).'numeric'
        ],[
            'layout_id.required'    =>  trans('site.custom_order.validation.layout_required')
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $routine = Routine::with([
            'types' =>  function($query) {
                return $query->active();
            }
        ])->findOrFail(session()->get('customOrderData.routine_id'));

        if (session()->has('customOrderData.layout_id') && !$request->has('layout_id')) {
            if ($routine->layout_select == 'all') {
                $layout = Layout::active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->findOrFail(session()->get('customOrderData.layout_id'));
            } elseif ($routine->layout_select == 'custom') {
                $layout = $routine->layouts()->active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->findOrFail(session()->get('customOrderData.layout_id'));
            }
        } else {
            if ($routine->layout_select == 'all') {
                $layout = Layout::active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->findOrFail($request->get('layout_id'));
            } elseif ($routine->layout_select == 'custom') {
                $layout = $routine->layouts()->active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->findOrFail($request->get('layout_id'));
            }
        }

        if (session()->has('customOrderData.layout_id') && session()->get('customOrderData.layout_id') != $layout->id) {
            session()->forget('customOrderData.chocolate_data');
            session()->forget('customOrderData.amount_id');
            session()->forget('customOrderData.package_id');
            session()->forget('customOrderData.package_upload_picture_enable');
            session()->forget('customOrderData.package_custom_design');
            session()->forget('customOrderData.package_custom_design_completed');
        }

        session()->put('customOrderData.layout_id',$layout->id);

        return redirect()->action('CustomOrder\Controllers\Site\OrderController@cells');
    }

    public function cells()
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        list($routine, $layout, $uploadStatus, $weightStatus, $shapeStatus) = $this->getLayoutAndRoutineData();

        $shapes = $layout->shapes->pluck('title','id');

        $weights = $layout->weights->pluck('title','id');

        $material = $routine->materials()->active()->findOrFail(session()->get('customOrderData.material_id'));

        $type = $routine->types()->active()->findOrFail(session()->get('customOrderData.type_id'));

        $baseFlavors = $material->baseFlavors()->active()->pluck('title','id');

        $allFlavors = $material->baseFlavors()->active()->select('id','title')->with([
            'flavors'   =>  function($query) {
                return $query->select('id','title')->active();
            }
        ])->get();

        $brains = [];

        if ($type->has_brain == 1) {
            $brains = $type->brains()->active()->select('id','title')->get();
        }

        return view('CustomOrder.views.site.cells',compact(
            'layout',
            'weightStatus',
            'shapeStatus',
            'uploadStatus',
            'baseFlavors',
            'allFlavors',
            'brains',
            'shapes',
            'weights'
        ));
    }

    public function saveCells(Request $request)
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        // Validate input data
        $validator = Validator::make($request->all(),[
            'active'    =>  'required|array|min:1',
            'active.*'  =>  'numeric|in:0,1'
        ],[
            'active.required' =>  trans('site.custom_order.validation.select_at_least_one_cell'),
            'active.min' =>  trans('site.custom_order.validation.select_at_least_one_cell'),
            'active.array' =>  trans('site.custom_order.validation.cell_status_must_be_array')
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        list($routine, $layout, $uploadStatus, $weightStatus, $shapeStatus) = $this->getLayoutAndRoutineData();

        // Collect data for validation
        if (count($layout->shapes) > 0) {
            $validShapeIds = implode(',',$layout->shapes->pluck('id')->toArray());
        } else {
            $validShapeIds = implode(',',Shape::pluck('id')->toArray());
        }

        if (count($layout->weights) > 0) {
            $validWeightIds = implode(',',$layout->weights->pluck('id')->toArray());
        } else {
            $validWeightIds = implode(',',Weight::active()->pluck('id')->toArray());
        }

        $activeCellNumbers = $this->validateAndReturnActiveCells(
            $request,
            $routine,
            $shapeStatus,
            $weightStatus,
            $validShapeIds,
            $validWeightIds
        );

        // if validate function returns response error then return it
        if (!is_array($activeCellNumbers)) {
            return $activeCellNumbers;
        }

        // remove disabled cells data in session
        $sessionChocolateData = session()->get('customOrderData.chocolate_data');

        if (!is_null($sessionChocolateData) && count($sessionChocolateData) > 0) {
            foreach ($sessionChocolateData as $cellNum => $data) {
                if (!in_array($cellNum,$activeCellNumbers)) {
                    unset($sessionChocolateData[$cellNum]);
                }
            }
        }

        // generate chocolate data array for save in session
        $sessionChocolateData = $this->createOrUpdateChocolatesArray(
            $layout,
            $request,
            $sessionChocolateData,
            $activeCellNumbers
        );

        // put new data in session
        session()->put('customOrderData.chocolate_data',$sessionChocolateData);

        return redirect()->action('CustomOrder\Controllers\Site\OrderController@packaging');
    }

    public function packaging()
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        if ($this->needToFillCellData()) {
            return $this->needToFillCellDataRedirect();
        }

        $layout = Layout::with([
            'amounts'   =>  function($query) {
                return $query->active();
            },
            'packages'   =>  function($query) {
                return $query->active();
            }
        ])->active()->findOrFail(session()->get('customOrderData.layout_id'));

        $amounts = $layout->amounts;

        $packageCategories = [];

        $packages = [];

        $selectedPackage = null;

        if ($layout->select_package == 1) {

            $categoryIds = $layout->packages->pluck('category_id')->unique();

            $packageCategories = PackageCategory::whereIn('id',$categoryIds)->get();

            if (count($packageCategories) > 0) {
                if (request()->has('category_id')) {
                    $packages = $layout->packages()->active()->where('category_id',request()->get('category_id'))->paginate(4);
                } else {
                    $packages = $layout->packages()->active()->where('category_id',$packageCategories->first()->id)->paginate(4);
                }
            }

            if (session()->has('customOrderData.package_id')) {
                $selectedPackage = $layout->packages()->active()->findOrFail(session('customOrderData.package_id'));
            }
        }

        if (count($amounts) == 0 && count($packages) == 0) {
            return redirect()->action('CustomOrder\Controllers\Site\OrderController@preview');
        }

        session()->put('customOrderData.packaging_step',1);

        return view('CustomOrder.views.site.packaging',compact(
            'layout',
            'amounts',
            'packageCategories',
            'packages',
            'selectedPackage',
            'amounts'
        ));
    }

    public function savePackaging(Request $request)
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        if ($this->needToFillCellData()) {
            return $this->needToFillCellDataRedirect();
        }

        $layout = Layout::active()->findOrFail(session()->get('customOrderData.layout_id'));

        $amountCount = $layout->amounts()->active()->count();

        $rules = [
            'amount_id'    =>  [
                (($amountCount > 0) ? 'required' : null),
                'numeric',
                Rule::exists('custom_order_amount','id')->where('active',1)
            ],
            'package_id'  =>  (($layout->select_package == 1) ? 'required|' : null) . 'numeric'
        ];

        // Validate input data
        $validator = Validator::make($request->all(),array_filter_recursive($rules));

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        session()->put('customOrderData.amount_id',$request->get('amount_id'));

        if ($layout->select_package == 1) {
            $package = Package::active()->findOrFail($request->get('package_id'));
            session()->put('customOrderData.package_id',$request->get('package_id'));
            session()->put('customOrderData.package_upload_picture_enable',$package->upload_picture);
            session()->put('customOrderData.package_custom_design',($package->custom_design == 1 && $request->get('custom_design') == 1) ? 1 : 0);
            session()->put('customOrderData.package_custom_design_completed',0);

            // remove picture if it's not uploaded for this package id
            if (session()->has('customOrderData.package_picture') && !is_null(session()->get('customOrderData.package_picture'))) {

                $sessionPicturePackageId = explode('_',File::name(session()->get('customOrderData.package_picture')))[2];

                if ($sessionPicturePackageId != $package->id) {
                    if (str_contains(session()->get('customOrderData.package_picture'),'uploads/tmp/')) {
                        if (File::exists(public_path(session()->get('customOrderData.package_picture')))) {
                            File::delete(public_path(session()->get('customOrderData.package_picture')));
                        }
                        if (File::exists(public_path(session()->get('customOrderData.package_picture_thumb')))) {
                            File::delete(public_path(session()->get('customOrderData.package_picture_thumb')));
                        }
                    }

                    session()->forget('customOrderData.package_picture');
                    session()->forget('customOrderData.package_picture_thumb');
                }
            }
        }

        return redirect()->action('CustomOrder\Controllers\Site\OrderController@preview');
    }

    public function preview()
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        if ($this->needToFillCellData()) {
            return $this->needToFillCellDataRedirect();
        }

        if ($this->needToSelectAmountAndPackage()) {
            return $this->needToSelectAmountAndPackageRedirect();
        }

        $brains = [];
        $shapes = [];
        $shapesShablon = [];
        $weights = [];
        $taxPrice = 0;

        list($routine, $layout, $uploadStatus, $weightStatus, $shapeStatus) = $this->getLayoutAndRoutineData(true);

        $material = $routine->materials()->active()->findOrfail(session('customOrderData.material_id'));

        $type = $routine->types->where('id',session('customOrderData.type_id'))->first();

        $setting = Setting::firstOrFail();

        $sessionChocolateData = session('customOrderData.chocolate_data');

        // calculate price and return needed data
        $data = $this->getData(
            $routine,
            $layout,
            $uploadStatus,
            $sessionChocolateData,
            $setting
        );

        $totalPrice   = $data['totalPrice'];

        if ($setting->tax > 0) {
            $taxPrice = ($totalPrice * $setting->tax / 100);
        }

        $baseFlavors = BaseFlavor::pluck('title','id')->toArray();

        $flavors = Flavor::pluck('title','id')->toArray();

        if (session('customOrderData.has_brain') == 1) {
            $brains = Brain::pluck('title','id')->toArray();
        }

        if ($shapeStatus == 1) {
            $shapesShablon = Shape::pluck('picture','id')->toArray();
            $shapes = Shape::pluck('title','id')->toArray();
        }

        if ($weightStatus == 1) {
            $weights = Weight::pluck('title','id')->toArray();
        }

        $amount = $data['amount'];
        $package = $data['package'];
        $singleProductPrice = $data['singleProductPrice'];

        return view('CustomOrder.views.site.preview', compact(
            'singleProductPrice',
            'material',
            'type',
            'routine',
            'uploadStatus',
            'shapeStatus',
            'weightStatus',
            'baseFlavors',
            'flavors',
            'brains',
            'shapes',
            'shapesShablon',
            'weights',
            'totalPrice',
            'taxPrice',
            'layout',
            'package',
            'amount'
        ));
    }

    public function shipping()
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        if ($this->needToFillCellData()) {
            return $this->needToFillCellDataRedirect();
        }

        if ($this->needToSelectAmountAndPackage()) {
            return $this->needToSelectAmountAndPackageRedirect();
        }
        $editAddress = null;
        if (request()->has('address_id')) {
            $editAddress = Address::where('user_id', auth()->user()->id)->findOrFail(request()->get('address_id'));
            if (strlen($editAddress->tel) > 8) {
                $code = explode('-', $editAddress->tel);
                if (count($code) == 2) {
                    $editAddress->phone_code = $code[0];
                    $editAddress->tel        = $code[1];
                }
            }
        }
        $shipping  = Transport::all();
        $provinces = City::roots()->active()->orderBy('name')->pluck('name', 'id')->toArray();
        $addresses = auth()->user()->addresses()->with('city', 'province')->get();

        return view('CustomOrder.views.site.shipping',
            compact('editAddress', 'provinces', 'address', 'shipping', 'addresses'));
    }

    public function saveShipping(OrderRequest $request)
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        if ($this->needToFillCellData()) {
            return $this->needToFillCellDataRedirect();
        }

        if ($this->needToSelectAmountAndPackage()) {
            return $this->needToSelectAmountAndPackageRedirect();
        }

        session()->put('customOrderData.ship_address', $request->get('ship_address'));
        session()->put('customOrderData.ship', $request->get('ship'));
        session()->put('customOrderData.invoice_request', $request->get('invoice_request'));

        return redirect()->action('CustomOrder\Controllers\Site\OrderController@payment');
    }

    public function payment()
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        if ($this->needToFillCellData()) {
            return $this->needToFillCellDataRedirect();
        }

        if ($this->needToSelectAmountAndPackage()) {
            return $this->needToSelectAmountAndPackageRedirect();
        }

        if (is_null(session('customOrderData.ship_address')) ||
            is_null(session('customOrderData.ship')) ||
            is_null(session('customOrderData.invoice_request'))
        ) {
            return $this->needToShipping();
        }

        $shipping = Transport::find(session('customOrderData.ship'));
        $address  = Address::where('user_id', auth()->user()->id)->find(session('customOrderData.ship_address'));
        if (is_null($shipping) || is_null($address)) {
            return $this->needToShipping();
        }
        $code       = null;
        $coupon     = null;
        $setting    = Setting::firstOrFail();
        $taxPrice   = 0;
        $discount   = 0;

        list($routine, $layout, $uploadStatus, $weightStatus, $shapeStatus) = $this->getLayoutAndRoutineData(true);

        $sessionChocolateData = session('customOrderData.chocolate_data');

        // calculate price and return needed data
        $data = $this->getData(
            $routine,
            $layout,
            $uploadStatus,
            $sessionChocolateData,
            $setting
        );

        $totalPrice = $data['totalPrice'];

        if (session()->has([
            'coupon_id',
            'code_id',
            'code',
            'coupon_type',
        ])
        ) {
            $discountData = app(CouponController::class)->calculateDiscounts($totalPrice, $setting);
            $discount     = $discountData['discount'];
            $code         = $discountData['code'];
        }

        $finalPrice = $totalPrice - $discount;

        if ($setting->tax > 0) {
            $taxPrice = ($finalPrice * $setting->tax / 100);
        }

        $formAction = 'CustomOrder\Controllers\Site\OrderController@submit';

        return view('CustomOrder.views.site.payment',
            compact('taxPrice', 'totalPrice', 'formAction', 'shipping', 'code', 'discount', 'finalPrice'));
    }

    public function submit(OrderRequest $request)
    {
        if (auth()->guest()) {
            return $this->needToLogin();
        }

        if ($this->needToSelectRoutine()) {
            return $this->needToSelectRoutineRedirect();
        }

        if ($this->needToSelectType()) {
            return $this->needToSelectTypeRedirect();
        }

        if ($this->needToSelectLayout()) {
            return $this->needToSelectLayoutRedirect();
        }

        if ($this->needToFillCellData()) {
            return $this->needToFillCellDataRedirect();
        }

        if ($this->needToSelectAmountAndPackage()) {
            return $this->needToSelectAmountAndPackageRedirect();
        }

        if (is_null(session('customOrderData.ship_address')) ||
            is_null(session('customOrderData.ship')) ||
            is_null(session('customOrderData.invoice_request'))
        ) {
            return $this->needToShipping();
        }

        $shipping = Transport::find(session('customOrderData.ship'));
        $address  = Address::where('user_id', auth()->user()->id)->find(session('customOrderData.ship_address'));
        if (is_null($shipping) || is_null($address)) {
            return $this->needToShipping();
        }

        $setting = Setting::firstOrFail();
        $code       = null;
        $coupon     = null;
        $taxPrice    = 0;
        $discount    = 0;

        list($routine, $layout, $uploadStatus, $weightStatus, $shapeStatus) = $this->getLayoutAndRoutineData(true);

        $sessionChocolateData = session('customOrderData.chocolate_data');

        // calculate price and return needed data
        $data = $this->getData(
            $routine,
            $layout,
            $uploadStatus,
            $sessionChocolateData,
            $setting
        );

        if (session()->has([
            'coupon_id',
            'code_id',
            'code',
            'coupon_type',
        ]))
        {
            $discountData = app(CouponController::class)->calculateDiscounts($data['totalPrice'], $setting);
            $discount     = $discountData['discount'];
            $code         = $discountData['code'];
        }

        $finalPrice = $data['totalPrice'] - $discount;

        if ($setting->tax > 0 && $finalPrice > 0) {
            $taxPrice = ($finalPrice * $setting->tax / 100);
        }

        DB::beginTransaction();
        try {
            $order                  = new Order();
            $order->user_id         = auth()->user()->id;
            $order->tax             = $setting->tax;
            $order->tax_price       = $taxPrice;
            $order->discount        = $discount;
            $order->total_price     = $finalPrice;
            $order->shipping_price  = $shipping->price;
            $order->payment_type    = $request->get('payment_type');
            $order->confirm         = 1;
            $order->type            = 'custom';
            $order->invoice_request = session()->get('customOrderData.invoice_request');
            $order->need_to_produce = 1;

            $package    = $data['package'];
            $amount     = $data['amount'];

            if ($layout->select_package == 1) {

                if ($package->custom_design == 1) {

                    if (session('customOrderData.package_custom_design') == 0 ||
                        (session('customOrderData.package_custom_design') == 1 && session('customOrderData.package_custom_design_completed') == 1)
                    ) {
                        $order->valid = 1;
                    } else {
                        $order->valid = 0;
                    }

                } else {
                    $order->valid = 1;
                }

            } else {
                $order->valid = 1;
            }

            $order->custom_order_count = session('customOrderData.order_count');
            $order->routine_id = $routine->id;
            $order->major = $routine->major;
            $order->layout_id = $layout->id;
            $order->layout_price = $data['layoutPrice'];

            if (!is_null($amount)) {
                $order->amount_id = $amount->id;
            }

            if (!is_null($package)) {

                $order->package_id = session('customOrderData.package_id');

                if ($package->upload_picture == 1 && !is_null(session('customOrderData.package_picture'))) {
                    $order->package_picture = File::basename(session('customOrderData.package_picture'));
                }

                if ($package->custom_design == 1 &&
                    session('customOrderData.package_custom_design') == 1 &&
                    session('customOrderData.package_custom_design_completed') == 1)
                {
                    $order->package_custom_design = 1;
                }
            }

            $order->package_price_without_picture = $data['packagePriceWithoutPicture'];
            $order->package_total_price = $data['packageTotalPrice'];

            $order->custom_product_type_id = session('customOrderData.type_id');
            $order->custom_product_has_brain = session('customOrderData.has_brain');
            $order->custom_product_material_id = session('customOrderData.material_id');

            $order->save();

            $cellsData = [];

            foreach ($sessionChocolateData as $cellNumber => $chocolateData) {

                $cell = [
                    'cell_number'   =>  $cellNumber,
                    'order_id'   =>  $order->id,
                    'base_flavor_id'   =>  $chocolateData['base_flavor_id'],
                    'flavor_id'   =>  $chocolateData['flavor_id'],
                    'brain_id'   =>  $chocolateData['brain_id'],
                    'weight_id'   =>  $chocolateData['weight_id'],
                    'shape_id'   =>  $chocolateData['shape_id'],
                    'picture'   =>  null,
                    'picture_thumb'   =>  null
                ];

                if (array_key_exists('picture',$chocolateData) && !is_null($chocolateData['picture'])) {
                    $cell['picture'] = File::basename($chocolateData['picture']);
                    $cell['picture_thumb'] = File::basename($chocolateData['picture_thumb']);
                }

                $cellsData[] = $cell;
            }

            Cells::insert($cellsData);

            $orderItem = new Item();
            $orderItem->order_id = $order->id;
            $orderItem->quantity = 1;
            $orderItem->need_to_produce = 1;
            $orderItem->save();

            $orderAddress              = new OrderAddress();
            $orderAddress->order_id    = $order->id;
            $orderAddress->shipping_id = $shipping->id;
            $orderAddress->province_id = $address->province_id;
            $orderAddress->city_id     = $address->city_id;
            $orderAddress->postal_code = $address->postal_code;
            $orderAddress->name_family = $address->name_family;
            $orderAddress->mobile      = $address->mobile;
            $orderAddress->tel         = $address->tel;
            $orderAddress->address     = $address->address;
            $orderAddress->save();

            if (session()->get('coupon_type') == 'public') {
                if (!is_null($code)) {
                    $code->used_at  = Carbon::now();
                    $code->user_id  = auth()->user()->id;
                    $code->order_id = $order->id;
                    $code->save();
                }
            } elseif (session()->get('coupon_type') == 'presented') {
                if (!is_null($code)) {
                    $code->used_at      = Carbon::now();
                    $code->user_used_id = auth()->user()->id;
                    $code->order_id     = $order->id;
                    $code->save();
                }
            }

            // move package picture to order folder
            if (!is_null($package)) {

                if ($package->upload_picture == 1 && !is_null(session('customOrderData.package_picture'))) {

                    $this->createTempDirectory();
                    $this->createOrderDirectory($order);

                    /*
                     *  if the picture is in temp folder then move it to order folder and if it's already in
                     *  an old order then copy it to new order
                     */

                    if (str_contains(session()->get('customOrderData.package_picture'),'uploads/tmp/')) {
                        File::move(
                            public_path(session('customOrderData.package_picture')),
                            public_path('uploads/order/'.$order->id.'/'.File::basename(session('customOrderData.package_picture')))
                        );

                        File::move(
                            public_path(session('customOrderData.package_picture_thumb')),
                            public_path('uploads/order/'.$order->id.'/'.File::basename(session('customOrderData.package_picture_thumb')))
                        );
                    } else {
                        File::copy(
                            public_path(session('customOrderData.package_picture')),
                            public_path('uploads/order/'.$order->id.'/'.File::basename(session('customOrderData.package_picture')))
                        );

                        File::copy(
                            public_path(session('customOrderData.package_picture_thumb')),
                            public_path('uploads/order/'.$order->id.'/'.File::basename(session('customOrderData.package_picture_thumb')))
                        );
                    }
                }
            }

            // move cells pictures to order folder
            foreach ($sessionChocolateData as $cellNumber => $chocolateData) {
                if (array_key_exists('picture',$chocolateData) && !is_null($chocolateData['picture'])) {

                    $this->createTempDirectory();
                    $this->createOrderDirectory($order);

                    if (str_contains($chocolateData['picture'],'uploads/tmp/')) {

                        File::move(
                            public_path($chocolateData['picture']),
                            public_path('uploads/order/'.$order->id.'/'.File::basename($chocolateData['picture']))
                        );

                        File::move(
                            public_path($chocolateData['picture_thumb']),
                            public_path('uploads/order/'.$order->id.'/'.File::basename($chocolateData['picture_thumb']))
                        );

                    } else {

                        File::copy(
                            public_path($chocolateData['picture']),
                            public_path('uploads/order/'.$order->id.'/'.File::basename($chocolateData['picture']))
                        );

                        File::copy(
                            public_path($chocolateData['picture_thumb']),
                            public_path('uploads/order/'.$order->id.'/'.File::basename($chocolateData['picture_thumb']))
                        );
                    }
                }
            }

            session()->forget([
                'coupon_type',
                'coupon_id',
                'code_id',
                'code',
                'ship_address',
                'ship',
                'invoice_request',
                'type',
                'customOrderData'
            ]);

            DB::commit();

            if ($request->get('payment_type') == 'online') {
                return BankPayment::zarinpal()
                    ->callBackUrl(action('Payment\Controllers\Site\PaymentController@verify'))
                    ->amount($order->full_price)
                    ->data([
                        'user_id'  => auth()->user()->id,
                        'order_id' => $order->id,
                    ])
                    ->description(trans('site.custom_order.payment').$order->id)
                    ->request();
            } else {
                return redirect()
                    ->action('Order\Controllers\Site\OrderController@index', ['order' => $order->id])
                    ->with('success', trans('site.order_submit'));
            }

        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar submit custom order '.$e);
            return redirect()->back()->withInput()->with('error', trans('site.order_fail'));
        }
    }

    public function reorder($order_id)
    {
        $order = auth()->user()->orders()->with('cells')->custom()->valid()->findOrFail($order_id);

        $routine = Routine::find($order->routine_id);

        if ($routine->layout_select == 'all') {
            $layout = Layout::with('cells')->active()->find($order->layout_id);
        } elseif ($routine->layout_select == 'custom') {
            $layout = $routine->layouts()->with('cells')->active()->find($order->layout_id);
        }

        $type = $routine->types()->active()->find($order->custom_product_type_id);

        $material = $routine->materials()->active()->find($order->custom_product_material_id);

        if (is_null($routine) || is_null($layout) || is_null($type) || is_null($material)) {
            return redirect()->back()->with('error',trans('site.custom_order.reorder_invalid_data'));
        }

        $customOrderData = [
            'routine_id'    =>  $routine->id,
            'routine_title' =>  $routine->title,
            'order_count'   =>  $order->custom_order_count,
            'type_id'       =>  $order->custom_product_type_id,
            'material_id'   =>  $order->custom_product_material_id,
            'layout_id'     =>  $order->layout_id,
            'has_brain'     =>  $order->custom_product_has_brain,
        ];

        if (!is_null($order->amount_id)) {
            $customOrderData = array_add($customOrderData,'amount_id',$order->amount_id);
        }

        if ($layout->select_package == 1 && !is_null($order->package_id)) {
            $customOrderData = array_add($customOrderData,'package_id',$order->package_id);

            if (!is_null($order->package_picture)) {
                $customOrderData = array_add($customOrderData,'package_picture','uploads/order/'.$order->id.'/'.$order->package_picture);
                $customOrderData = array_add(
                    $customOrderData,
                    'package_picture_thumb',
                    'uploads/order/'.$order->id.'/'.str_replace('package','package_thumb',$order->package_picture));
            }
        }

        $chocolateData = [];

        foreach ($layout->cells as $layoutCell) {
            foreach ($order->cells as $orderCell) {
                if ($layoutCell->cell == $orderCell->cell_number) {
                    $chocolateData = array_add($chocolateData,$orderCell->cell_number,[
                        'base_flavor_id'    =>  $orderCell->base_flavor_id,
                        'flavor_id'    =>  $orderCell->flavor_id,
                        'brain_id'    =>  $orderCell->brain_id,
                        'weight_id'    =>  $orderCell->weight_id,
                        'shape_id'    =>  $orderCell->shape_id,
                    ]);

                    if (!is_null($orderCell->picture)) {
                        $picturePath    = 'uploads/order/'.$orderCell->order_id.'/'.$orderCell->picture;
                        $thumbPath      = 'uploads/order/'.$orderCell->order_id.'/'.$orderCell->picture_thumb;
                        $chocolateData[$orderCell->cell_number]['picture'] = $picturePath;
                        $chocolateData[$orderCell->cell_number]['picture_thumb'] = $thumbPath;
                    }
                }
            }
        }

        $customOrderData = array_add($customOrderData,'chocolate_data',$chocolateData);

        session()->put('customOrderData',$customOrderData);

        return redirect()->action('CustomOrder\Controllers\Site\OrderController@index');
    }

    private function needToLogin()
    {
        $action = action('Auth\Controllers\Site\AuthController@login');

        $action .= '?return='.urlencode(action('CustomOrder\Controllers\Site\OrderController@index'));

        return redirect()->to($action)->with('error', trans('site.custom_order.need_to_login'));
    }

    private function needToSelectRoutine()
    {
        if (!session()->has('customOrderData') || !session()->has('customOrderData.routine_id')) {
            return true;
        }

        return false;
    }

    private function needToSelectRoutineRedirect()
    {
        return redirect()->action('HomeController@index')
                            ->with('popup-error', trans('site.custom_order.routine_select_error'));
    }

    private function needToSelectType()
    {
        if (!session()->has('customOrderData') ||
            !session()->has('customOrderData.order_count') ||
            !session()->has('customOrderData.type_id') ||
            !session()->has('customOrderData.material_id')
        ) {
            return true;
        }

        return false;
    }

    private function needToSelectTypeRedirect()
    {
        return redirect()
            ->action('CustomOrder\Controllers\Site\OrderController@index')
            ->with('error', trans('site.custom_order.type_select_error'));
    }

    private function needToSelectLayout()
    {
        if (!session()->has('customOrderData') ||
            !session()->has('customOrderData.layout_id')
        ) {
            return true;
        }

        return false;
    }

    private function needToSelectLayoutRedirect()
    {
        return redirect()
            ->action('CustomOrder\Controllers\Site\OrderController@layout')
            ->with('error', trans('site.custom_order.layout_select_error'));
    }

    private function needToFillCellData()
    {
        if (!session()->has('customOrderData.chocolate_data') ||
            count(session()->get('customOrderData.chocolate_data')) == 0
        ) {
            return true;
        }

        return false;
    }

    private function needToFillCellDataRedirect()
    {
        return redirect()
            ->action('CustomOrder\Controllers\Site\OrderController@cells')
            ->with('error', trans('site.custom_order.cells_select_error'));
    }

    private function needToSelectAmountAndPackage()
    {
        $layout = Layout::active()->findOrFail(session()->get('customOrderData.layout_id'));

        $amountCount = $layout->amounts()->active()->count();

        if ((($amountCount > 0) && !session()->has('customOrderData.amount_id')) ||
            ($layout->select_package == 1 && !session()->has('customOrderData.package_id'))
        ) {
            return true;
        }

        return false;
    }

    private function needToSelectAmountAndPackageRedirect()
    {
        return redirect()
            ->action('CustomOrder\Controllers\Site\OrderController@packaging')
            ->with('error', trans('site.custom_order.amount_package_select_error'));
    }

    private function needToShipping()
    {
        return redirect()
            ->action('CustomOrder\Controllers\Site\OrderController@shipping')
            ->with('info', trans('site.carts.need_to_shipping'));
    }

    public function uploadCellPicture(Request $request)
    {
        try {

            $inputData = $request->file('cell_picture');

            $cellNumber = key($inputData);

            if (auth()->check()) {

                $setting = Setting::firstOrFail();

                $validator = Validator::make($request->all(),[
                    'cell_picture' => 'required|array|min:1',
                    'cell_picture.*' => 'required|image|max:'.($setting->custom_order_picture_max_size * 1024),
                ]);

                if ($validator->fails()) {
                    return response()->json(['error' => true]);
                }

                $tempPath = public_path('uploads/tmp/');

                if (!File::isDirectory($tempPath)) {
                    File::makeDirectory($tempPath);
                }

                if (!File::isDirectory(public_path('uploads/order/'))) {
                    File::makeDirectory(public_path('uploads/order/'));
                }

                if (session()->has('customOrderData.chocolate_data.'.$cellNumber.'.picture')) {
                    if (str_contains(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture'),'uploads/tmp/')) {
                        if (File::exists(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture')))) {
                            File::delete(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture')));
                        }
                        if (File::exists(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture_thumb')))) {
                            File::delete(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture_thumb')));
                        }
                    }
                }

                $file           = $inputData[$cellNumber];
                $fileName       = time().'_'.$cellNumber.'_'.auth()->user()->id.'_'.$file->hashName();
                $thumbFileName  = time().'_'.$cellNumber.'_'.auth()->user()->id.'_thumb_'.$file->hashName();
                Image::make($file)->save($tempPath.$fileName, 90);
                Image::make($file)->widen(350, function ($constraint) {
                    $constraint->upsize();
                })->save($tempPath.$thumbFileName, 90);
                session()->put('customOrderData.chocolate_data.'.$cellNumber.'.picture', 'uploads/tmp/'.$fileName);
                session()->put('customOrderData.chocolate_data.'.$cellNumber.'.picture_thumb', 'uploads/tmp/'.$thumbFileName);

                return response()->json(['error' => false]);
            }

            return response()->json(['error' => true]);

        } catch (Exception $exception) {
            Log::alert('moshkel dar upload tasvire cell dar sefaresh ekhtesasi '.$exception);
            return response()->json(['error' => true]);
        }
    }

    public function removeCellPicture(Request $request)
    {
        try {

            $validator = Validator::make($request->all(),[
                'cell_number' => 'required|numeric|min:1'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => true]);
            }

            $cellNumber = $request->get('cell_number');

            if (auth()->check()) {
                if (session()->has('customOrderData.chocolate_data.'.$cellNumber.'.picture')) {

                    if (str_contains(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture'),'uploads/tmp/')) {
                        if (File::exists(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture')))) {
                            File::delete(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture')));
                        }
                        if (File::exists(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture_thumb')))) {
                            File::delete(public_path(session()->get('customOrderData.chocolate_data.'.$cellNumber.'.picture_thumb')));
                        }
                    }

                    session()->forget('customOrderData.chocolate_data.'.$cellNumber.'.picture');
                    session()->forget('customOrderData.chocolate_data.'.$cellNumber.'.picture_thumb');
                }

                return response()->json(['error' => false]);
            }

            return response()->json(['error' => true]);

        } catch (Exception $exception) {
            Log::alert('moshkel dar hazfe tasvire cell dar sefaresh ekhtesasi '.$exception);
            return response()->json(['error' => true]);
        }
    }

    public function uploadPackagePicture(Request $request)
    {
        try {

            if (auth()->check()) {

                $setting = Setting::firstOrFail();

                $validator = Validator::make($request->all(),[
                    'package_id'    =>  'required|numeric',
                    'package_picture' => 'required|image|max:'.($setting->custom_order_picture_max_size * 1024),
                ]);

                if ($validator->fails()) {
                    return response()->json(['error' => true]);
                }

                $tempPath = public_path('uploads/tmp/');
                if (!File::isDirectory($tempPath)) {
                    File::makeDirectory($tempPath);
                }
                if (!File::isDirectory(public_path('uploads/order/'))) {
                    File::makeDirectory(public_path('uploads/order/'));
                }

                if (session()->has('customOrderData.package_picture')) {
                    if (str_contains(session()->get('customOrderData.package_picture'),'uploads/tmp/')) {
                        if (File::exists(public_path(session()->get('customOrderData.package_picture')))) {
                            File::delete(public_path(session()->get('customOrderData.package_picture')));
                        }
                        if (File::exists(public_path(session()->get('customOrderData.package_picture_thumb')))) {
                            File::delete(public_path(session()->get('customOrderData.package_picture_thumb')));
                        }
                    }
                }

                $file           = $request->file('package_picture');
                $fileName       = time().'_'.auth()->user()->id.'_'.$request->get('package_id').'_package_'.$file->hashName();
                $thumbFileName  = time().'_'.auth()->user()->id.'_'.$request->get('package_id').'_package_thumb_'.$file->hashName();
                Image::make($file)->save($tempPath.$fileName, 90);
                Image::make($file)->widen(240, function ($constraint) {
                    $constraint->upsize();
                })->save($tempPath.$thumbFileName, 90);
                session()->put('customOrderData.package_picture', 'uploads/tmp/'.$fileName);
                session()->put('customOrderData.package_picture_thumb', 'uploads/tmp/'.$thumbFileName);

                return response()->json(['error' => false]);
            }

            return response()->json(['error' => true]);

        } catch (Exception $exception) {
            Log::alert('moshkel dar hazfe tasvire package dar sefaresh ekhtesasi '.$exception);
            return response()->json(['error' => true]);
        }
    }

    public function removePackagePicture(Request $request)
    {
        try {

            if (auth()->check()) {
                if (session()->has('customOrderData.package_picture')) {
                    if (str_contains(session()->get('customOrderData.package_picture'),'uploads/tmp/')) {
                        if (File::exists(public_path(session()->get('customOrderData.package_picture')))) {
                            File::delete(public_path(session()->get('customOrderData.package_picture')));
                        }
                        if (File::exists(public_path(session()->get('customOrderData.package_picture_thumb')))) {
                            File::delete(public_path(session()->get('customOrderData.package_picture_thumb')));
                        }
                    }

                    session()->forget('customOrderData.package_picture');
                    session()->forget('customOrderData.package_picture_thumb');

                    return response()->json(['error' => false]);
                }

                return response()->json(['error' => true]);
            }

            return response()->json(['error' => true]);

        } catch (Exception $exception) {
            Log::alert('moshkel dar hazfe tasvire package dar sefaresh ekhtesasi '.$exception);
            return response()->json(['error' => true]);
        }
    }

    public function getRoutineTypes(Request $request)
    {
        if (auth()->check()) {

            $routine = Routine::findOrFail(session()->get('customOrderData.routine_id'));

            $types = $routine->types()->active()->select('id','title')->where('has_brain',$request->get('has_brain'))->get();

            return response()->json([
                'error' => false,
                'types' => $types
            ]);
        }

        return response()->json(['error' => true]);
    }

    public function getBaseFlavors(Request $request)
    {
        if (auth()->check()) {

            $productType = Material::active()->findOrFail($request->get('product_type_id'));

            $baseFlavors = $productType->baseFlavors()->active()->select('id','title')->get();

            return response()->json([
                'error' => false,
                'baseFlavors' => $baseFlavors
            ]);
        }

        return response()->json(['error' => true]);
    }

    public function getFlavorAndBrain(Request $request)
    {
        if (auth()->check()) {

            $baseFlavor = BaseFlavor::active()->findOrFail($request->get('base_flavor_id'));

            $flavors = $baseFlavor->flavors()->active()->select('id','title')->get();

            $brains = $baseFlavor->brains()->active()->select('id','title')->get();

            return response()->json([
                'error' => false,
                'flavors' => $flavors,
                'brains' => $brains
            ]);
        }

        return response()->json(['error' => true]);
    }

    private function createOrderDirectory($order)
    {
        if (!File::isDirectory(public_path('uploads/order/'.$order->id.'/'))) {
            File::makeDirectory(public_path('uploads/order/'. $order->id),0777,true);
        }
    }

    private function createTempDirectory()
    {
        if (!File::isDirectory(public_path('uploads/tmp/'))) {
            File::makeDirectory(public_path('uploads/tmp/'),0777,true);
        }
    }

    protected function getLayoutAndRoutineData($preview = false)
    {
        $shapeStatus = $this->defaultSettingFlag;
        $weightStatus = $this->defaultSettingFlag;
        $uploadStatus = $this->defaultSettingFlag;

        $routine = Routine::with([
            'types' =>  function($query) {
                return $query->active();
            }
        ])->findOrFail(session()->get('customOrderData.routine_id'));

        if ($routine->layout_select == 'all') {
            if ($preview) {
                $layout = $routine->layouts()->with([
                    'amounts' => function ($query) {
                        return $query->active();
                    }
                ])->active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->find(session()->get('customOrderData.layout_id'));
            } else {
                $layout = $routine->layouts()->with([
                    'cells',
                    'shapes',
                    'weights'   =>  function($query) {
                        return $query->active();
                    }
                ])->active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->find(session()->get('customOrderData.layout_id'));
            }

            if (!is_null($layout)) {
                $weightStatus = $layout->pivot->change_weight;
                $shapeStatus = $layout->pivot->change_shape;
                $uploadStatus = $layout->pivot->upload_picture;
            } else {
                if ($preview) {
                    $layout = Layout::with([
                        'amounts' => function ($query) {
                            return $query->active();
                        }
                    ])->active()->whereHas('types',function ($query) use ($routine) {
                        return $query->where('id',session()->get('customOrderData.type_id'));
                    })->findOrFail(session()->get('customOrderData.layout_id'));
                } else {
                    $layout = Layout::with([
                        'cells',
                        'shapes',
                        'weights'   =>  function($query) {
                            return $query->active();
                        }
                    ])->active()->whereHas('types',function ($query) use ($routine) {
                        return $query->where('id',session()->get('customOrderData.type_id'));
                    })->findOrFail(session()->get('customOrderData.layout_id'));
                }
            }

        } elseif ($routine->layout_select == 'custom') {

            if ($preview) {
                $layout = $routine->layouts()->with([
                    'amounts' => function ($query) {
                        return $query->active();
                    }
                ])->active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->findOrFail(session()->get('customOrderData.layout_id'));
            } else {
                $layout = $routine->layouts()->with([
                    'cells',
                    'shapes',
                    'weights'   =>  function($query) {
                        return $query->active();
                    }
                ])->active()->whereHas('types',function ($query) use ($routine) {
                    return $query->where('id',session()->get('customOrderData.type_id'));
                })->findOrFail(session()->get('customOrderData.layout_id'));
            }

            $weightStatus = $layout->pivot->change_weight;
            $shapeStatus = $layout->pivot->change_shape;
            $uploadStatus = $layout->pivot->upload_picture;
        }

        return array($routine, $layout, $uploadStatus, $weightStatus, $shapeStatus);
    }

    private function validateAndReturnActiveCells($request, $routine, $shapeStatus, $weightStatus, $validShapeIds, $validWeightIds)
    {
        $material = $routine->materials()->active()->findOrFail(session()->get('customOrderData.material_id'));

        $type = $routine->types()->active()->findOrFail(session()->get('customOrderData.type_id'));

        $baseFlavorIds = $material->baseFlavors()->active()->pluck('id')->toArray();

        $validBaseFlavorIds = implode(',',$baseFlavorIds);

        $validFlavorIds = implode(',',Flavor::active()->pluck('id')->toArray());

        $validBrainIds = implode(',',Brain::active()->pluck('id')->toArray());

        $activeCellNumbers = [];

        foreach ($request->get('active') as $cellNum => $value) {
            if ($value == 1) {
                $validator = Validator::make($request->all(),[
                    'base_flavor_id.'.$cellNum   =>  'required|numeric|in:'.$validBaseFlavorIds,
                    'shape_id.'.$cellNum   =>  (($shapeStatus == 1) ? 'required|' : null) . 'numeric|in:'.$validShapeIds,
                    'weight_id.'.$cellNum   =>  (($weightStatus == 1) ? 'required|' : null) . 'numeric|in:'.$validWeightIds,
                    'flavor_id.'.$cellNum   =>  'numeric|in:'.$validFlavorIds,
                    'brain_id.'.$cellNum   =>  (($type->has_brain == 1) ? 'required|' : null) .'numeric|in:'.$validBrainIds,
                ],[
                    'base_flavor_id.*.required' =>  trans('site.custom_order.validation.base_flavor_required',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'base_flavor_id.*.numeric' =>  trans('site.custom_order.validation.base_flavor_numeric',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'base_flavor_id.*.in' =>  trans('site.custom_order.validation.base_flavor_in',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'flavor_id.*.numeric' =>  trans('site.custom_order.validation.flavor_numeric',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'flavor_id.*.in' =>  trans('site.custom_order.validation.flavor_in',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'shape_id.*.required' =>  trans('site.custom_order.validation.shape_required',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'shape_id.*.numeric' =>  trans('site.custom_order.validation.shape_numeric',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'shape_id.*.in' =>  trans('site.custom_order.validation.shape_in',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'weight_id.*.required' =>  trans('site.custom_order.validation.weight_required',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'weight_id.*.numeric' =>  trans('site.custom_order.validation.weight_numeric',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'weight_id.*.in' =>  trans('site.custom_order.validation.weight_in',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'brain_id.*.required' =>  trans('site.custom_order.validation.brain_required',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'brain_id.*.numeric' =>  trans('site.custom_order.validation.brain_numeric',[
                        'cellNum'   =>  $cellNum
                    ]),
                    'brain_id.*.in' =>  trans('site.custom_order.validation.brain_in',[
                        'cellNum'   =>  $cellNum
                    ]),
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                }

                $activeCellNumbers[] = $cellNum;
            }
        }

        return $activeCellNumbers;
    }

    private function createOrUpdateChocolatesArray($layout, $request, $sessionChocolateData, $activeCellNumbers)
    {
        foreach ($activeCellNumbers as $cellNumber) {
            $sessionChocolateData[$cellNumber]['base_flavor_id'] = $request->input('base_flavor_id.'.$cellNumber);

            if ($request->has('flavor_id.'.$cellNumber)) {
                $sessionChocolateData[$cellNumber]['flavor_id'] = $request->input('flavor_id.'.$cellNumber);
            } else {
                $sessionChocolateData[$cellNumber]['flavor_id'] = null;
            }

            if ($request->has('brain_id.'.$cellNumber)) {
                $sessionChocolateData[$cellNumber]['brain_id'] = $request->input('brain_id.'.$cellNumber);
            } else {
                $sessionChocolateData[$cellNumber]['brain_id'] = null;
            }

            if ($request->has('shape_id.'.$cellNumber)) {
                $sessionChocolateData[$cellNumber]['shape_id'] = $request->input('shape_id.'.$cellNumber);
            } else {
                $cellDefaultShape = $layout->cells->where('cell',$cellNumber)->first()->shape_id;
                $sessionChocolateData[$cellNumber]['shape_id'] = $cellDefaultShape;
            }

            if ($request->has('weight_id.'.$cellNumber)) {
                $sessionChocolateData[$cellNumber]['weight_id'] = $request->input('weight_id.'.$cellNumber);
            } else {
                $cellDefaultWeight = $layout->cells->where('cell',$cellNumber)->first()->weight_id;
                $sessionChocolateData[$cellNumber]['weight_id'] = $cellDefaultWeight;
            }
        }

        return $sessionChocolateData;
    }

    private function getData($routine, $layout, $uploadStatus, $sessionChocolateData, $setting)
    {
        $cellsPrice = 0;

        $priceCalculateIsMajor = ($routine->major == 1) ? true : false;

        $allWeights = Weight::active()->pluck('title','id')->toArray();

        // calculate cells price
        foreach ($sessionChocolateData as $chocolateData) {

            $baseFlavor = BaseFlavor::active()->findOrFail($chocolateData['base_flavor_id']);

            // calculate price per gram when only base flavor filled and flavor and brain are empty
            if ((!array_key_exists('flavor_id',$chocolateData) || is_null($chocolateData['flavor_id'])) &&
                (!array_key_exists('brain_id',$chocolateData) || is_null($chocolateData['brain_id']))
            ) {
                if ($priceCalculateIsMajor) {
                    $pricePerGram = $baseFlavor->major_price / 1000;
                } else {
                    $pricePerGram = $baseFlavor->price / 1000;
                }
            }

            // calculate price per gram when flavor and brain filled
            if ((array_key_exists('flavor_id',$chocolateData) && !is_null($chocolateData['flavor_id'])) &&
                (array_key_exists('brain_id',$chocolateData) && !is_null($chocolateData['brain_id']))
            ) {
                if ($priceCalculateIsMajor) {
                    $pricePerGram = $baseFlavor->flavor_brain_major_price / 1000;
                } else {
                    $pricePerGram = $baseFlavor->flavor_brain_price / 1000;
                }
            }

            // calculate price per gram when only flavor filled
            if ((array_key_exists('flavor_id',$chocolateData) && !is_null($chocolateData['flavor_id'])) &&
                (!array_key_exists('brain_id',$chocolateData) || is_null($chocolateData['brain_id']))
            ) {
                if ($priceCalculateIsMajor) {
                    $pricePerGram = $baseFlavor->flavor_major_price / 1000;
                } else {
                    $pricePerGram = $baseFlavor->flavor_price / 1000;
                }
            }

            // calculate price per gram when only brain filled
            if ((!array_key_exists('flavor_id',$chocolateData) || is_null($chocolateData['flavor_id'])) &&
                (array_key_exists('brain_id',$chocolateData) && !is_null($chocolateData['brain_id']))
            ) {
                if ($priceCalculateIsMajor) {
                    $pricePerGram = $baseFlavor->brain_major_price / 1000;
                } else {
                    $pricePerGram = $baseFlavor->brain_price / 1000;
                }
            }

            $currentCellPrice = $pricePerGram * $allWeights[$chocolateData['weight_id']];

            if ($uploadStatus == 1 && array_key_exists('picture',$chocolateData) && !is_null($chocolateData['picture'])) {
                $currentCellPrice = $currentCellPrice * $setting->custom_order_picture_coefficient;
            }

            $cellsPrice += $currentCellPrice;
        }

        // sum cellsPrice with layoutPrice
        $layoutPrice = $priceCalculateIsMajor ? $layout->major_price : $layout->price;
        $singleProductPrice = $cellsPrice + $layoutPrice;

        // multiple singleProductPrice with amount
        $amount = $layout->amounts->where('id',session('customOrderData.amount_id'))->first();

        if (!is_null($amount)) {
            $singleProductPrice = $singleProductPrice * $amount->pivot->coefficient;
        }

        $package = null;
        $packagePriceWithoutPicture = 0;
        $packageTotalPrice = 0;

        // calculate and add package price to singleProductPrice
        if ($layout->select_package == 1) {
            $package = $layout->packages()->active()->findOrFail(session('customOrderData.package_id'));

            if ($priceCalculateIsMajor) {
                $packagePriceWithoutPicture += $package->major_price;
                $packageTotalPrice += $package->major_price;
                $singleProductPrice += $package->major_price;
            } else {
                $packagePriceWithoutPicture += $package->price;
                $packageTotalPrice += $package->price;
                $singleProductPrice += $package->price;
            }

            if ($package->upload_picture == 1 && !is_null(session('customOrderData.package_picture'))) {
                $packageTotalPrice += $package->upload_picture_price;
                $singleProductPrice += $package->upload_picture_price;
            }

        } else {
            if ($priceCalculateIsMajor) {
                $packagePriceWithoutPicture += $layout->package_major_price;
                $packageTotalPrice += $layout->package_major_price;
                $singleProductPrice += $layout->package_major_price;
            } else {
                $packagePriceWithoutPicture += $layout->package_price;
                $packageTotalPrice += $layout->package_price;
                $singleProductPrice += $layout->package_price;
            }
        }

        $totalPrice = $singleProductPrice * session('customOrderData.order_count');

        return compact(
            'layoutPrice',
            'singleProductPrice',
            'totalPrice',
            'package',
            'packagePriceWithoutPicture',
            'packageTotalPrice',
            'amount'
        );
    }
}
