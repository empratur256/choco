<?php
return [
    'customorder' => [
        'order'   => 100,
        'title'   => 'سفارش اختصاصی',
        'icon'    => 'fa fa-paint-brush',
        'badge'   => function () {
            //return 1;
        },
        'submenu' => [
            'Material'      => [
                'title'  => 'نوع محصول',
                'action' => 'CustomOrder\Controllers\Admin\MaterialController@index',
            ],
            'Type'      => [
                'title'  => 'دسته بندی محصول',
                'action' => 'CustomOrder\Controllers\Admin\TypeController@index',
            ],
            'Flavor'    => [
                'title'  => 'طعم مکمل',
                'action' => 'CustomOrder\Controllers\Admin\FlavorController@index',
            ],
            'Layout'    => [
                'title'  => 'چینش',
                'action' => 'CustomOrder\Controllers\Admin\LayoutController@index',
            ],
            'Routine'   => [
                'title'  => 'روال',
                'action' => 'CustomOrder\Controllers\Admin\RoutineController@index',
            ],
            'Shape'     => [
                'title'  => 'شکل ها',
                'action' => 'CustomOrder\Controllers\Admin\ShapeController@index',
            ],
            'Portfolio' => [
                'title'  => 'نمونه های اجرا شده',
                'action' => 'CustomOrder\Controllers\Admin\PortfolioController@index',
            ],
            'Weight'    => [
                'title'  => 'وزن ها',
                'action' => 'CustomOrder\Controllers\Admin\WeightController@index',
            ],
            'Amount'    => [
                'title'  => 'سینی ها',
                'action' => 'CustomOrder\Controllers\Admin\AmountController@index',
            ],
            'Brain'     => [
                'title'  => 'مغز ها',
                'action' => 'CustomOrder\Controllers\Admin\BrainController@index',
            ],
            'Package'   => [
                'title'  => 'بسته بندی ها',
                'action' => 'CustomOrder\Controllers\Admin\PackageController@index',
            ],
            'Setting'   => [
                'title'  => 'تنظیمات',
                'action' => 'CustomOrder\Controllers\Admin\CustomOrderController@setting',
            ],
        ],
    ],
];
