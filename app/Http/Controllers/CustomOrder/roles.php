<?php
return [
    'customorder' => [
        'title'  => 'سفارش اختصاصی',
        'access' => [
            'CustomOrderController' => [
                'تنظیمات' => [
                    'setting',
                    'saveSetting',
                ],
            ],

            'PortfolioController'       => [
                'لیست نمونه ها'   => 'index',
                'ایجاد نمونه ها'  => [
                    'create',
                    'store',
                ],
                'ویرایش نمونه ها' => [
                    'edit',
                    'update',
                ],
                'حذف نمونه ها'    => 'destroy',
            ],
            'MaterialController'    => [
                'لیست نوع محصول'   => 'index',
                'ایجاد نوع محصول'  => [
                    'create',
                    'store',
                ],
                'ویرایش نوع محصول' => [
                    'edit',
                    'update',
                ],
                'حذف نوع محصول'    => 'destroy',
            ],
            'TypeController'    => [
                'لیست دسته بندی محصول'   => 'index',
                'ایجاد دسته بندی محصول'  => [
                    'create',
                    'store',
                ],
                'ویرایش دسته بندی محصول' => [
                    'edit',
                    'update',
                ],
                'حذف دسته بندی محصول'    => 'destroy',
            ],
            'LayoutController'          => [
                'لیست نوع جعبه'   => 'index',
                'ایجاد نوع جعبه'  => [
                    'create',
                    'store',
                ],
                'ویرایش نوع جعبه' => [
                    'edit',
                    'update',
                ],
                'حذف نوع جعبه'    => 'destroy',
            ],
            'CellController'            => [
                'مدیریت خانه های چینش' => [
                    'index',
                    'create',
                    'store',
                    'edit',
                    'update',
                    'destroy',
                ],


            ],
            'FlavorController'          => [
                'لیست طعم مکمل'   => 'index',
                'ایجاد طعم مکمل'  => [
                    'create',
                    'store',
                ],
                'ویرایش طعم مکمل' => [
                    'edit',
                    'update',
                ],
                'حذف طعم مکمل'    => 'destroy',
            ],
            'BaseFlavorController'      => [
                'لیست طعم پایه'     => 'index',
                'ایجاد طعم پایه'    => [
                    'create',
                    'store',
                ],
                'ویرایش طعم پایه'   => [
                    'edit',
                    'update',
                ],
                'قیمت طعم های مکمل' => [
                    'flavor',
                    'storeFlavor',
                ],
                'حذف طعم پایه'      => 'destroy',
            ],
            'ShapeController'           => [
                'لیست شکل ها' => 'index',
                'ایجاد شکل '  => [
                    'create',
                    'store',
                ],
                'ویرایش شکل ' => [
                    'edit',
                    'update',
                ],
                'حذف شکل '    => 'destroy',
            ],
            'WeightController'          => [
                'لیست وزن ها' => 'index',
                'ایجاد وزن '  => [
                    'create',
                    'store',
                ],
                'ویرایش وزن ' => [
                    'edit',
                    'update',
                ],
                'حذف وزن '    => 'destroy',
            ],
            'AmountController'          => [
                'لیست سینی ها' => 'index',
                'ایجاد سینی '  => [
                    'create',
                    'store',
                ],
                'ویرایش سینی ' => [
                    'edit',
                    'update',
                ],
                'حذف سینی '    => 'destroy',
            ],
            'BrainController'           => [
                'لیست مغز ها' => 'index',
                'ایجاد مغز'   => [
                    'create',
                    'store',
                ],
                'ویرایش مغز'  => [
                    'edit',
                    'update',
                ],
                'حذف مغز'     => 'destroy',
            ],
            'PackageCategoryController' => [
                'لیست نوع بسته بندی ها' => 'index',
                'ایجاد نوع بسته بندی'   => [
                    'create',
                    'store',
                ],
                'ویرایش نوع بسته بندی'  => [
                    'edit',
                    'update',
                ],
                'حذف نوع بسته بندی'     => 'destroy',
            ],
            'PackageController'         => [
                'لیست بسته بندی ها' => 'index',
                'ایجاد بسته بندی'   => [
                    'create',
                    'store',
                ],
                'ویرایش بسته بندی'  => [
                    'edit',
                    'update',
                ],
                'حذف بسته بندی'     => 'destroy',
            ],
            'RoutineController'         => [
                'لیست روال ها' => 'index',
                'ایجاد روال'   => [
                    'create',
                    'store',
                ],
                'ویرایش روال'  => [
                    'edit',
                    'update',
                ],
                'حذف روال'     => 'destroy',
            ],
        ],
    ],
];
