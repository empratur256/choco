<?php

namespace Sedehi\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Image;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function createImages($realPath, $fileName, $watermark = false, $path = null)
    {
        if(is_null($path)){
            $path = $this->uploadPath;
        }
        
        foreach($this->imageSize as $size){
            $prefix = null;
            $obj = Image::make($realPath);
            if(!array_key_exists('width', $size) && array_key_exists('height', $size)){
                $obj->resize(null, $size['height'], function($constraint){
                    $constraint->aspectRatio();
                });
                $prefix = 'autox'.$size['height'].'-';
            }elseif(array_key_exists('width', $size) && !array_key_exists('height', $size)){
                $obj->resize($size['width'], null, function($constraint){
                    $constraint->aspectRatio();
                });
                $prefix = $size['width'].'xauto-';
            }elseif(array_key_exists('width', $size) && array_key_exists('height', $size)){
                $obj->resize($size['width'], $size['height']);
                $prefix = $size['width'].'x'.$size['height'].'-';
            }
            if(!is_null(config('site.image.watermark')) && $watermark){
                $obj->insert(public_path(config('site.image.watermark')), config('site.image.watermark_position'));
            }
            $obj->save(public_path($path.$prefix.$fileName));
        }
    }

}
