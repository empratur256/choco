<?php

namespace Sedehi\Http\Controllers\Factory\Controllers\Admin;

use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Factory\Models\Factory;
use Sedehi\Http\Controllers\Factory\Requests\Admin\FactoryRequest;
use Illuminate\Http\Request;

class FactoryController extends Controller
{

    public function index()
    {
        $items = Factory::filter()->latest()->paginate(20);

        return view('Factory.views.admin.factory.index', compact('items'));
    }

    public function create()
    {
        return view('Factory.views.admin.factory.add');
    }

    public function store(FactoryRequest $request)
    {
        $item          = new Factory();
        $item->title   = $request->get('title');
        $item->address = $request->get('address');
        $item->save();

        return redirect()->action('Factory\Controllers\Admin\FactoryController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function edit($id)
    {
        $item = Factory::findOrFail($id);

        return view('Factory.views.admin.factory.edit', compact('item'));
    }

    public function update(FactoryRequest $request, $id)
    {
        $item          = Factory::findOrFail($id);
        $item->title   = $request->get('title');
        $item->address = $request->get('address');
        $item->save();

        return redirect()->action('Factory\Controllers\Admin\FactoryController@index')
                         ->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function destroy(FactoryRequest $request)
    {
        Factory::whereIn('id', $request->get('deleteId'))->delete();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }
}
