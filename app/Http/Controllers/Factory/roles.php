<?php
return [
    'factory' => [
        'title'  => 'کارخانه ها',
        'access' => [
            'FactoryController' => [
                'لیست'   => 'index',
                'ایجاد'  => [
                    'create',
                    'store',
                ],
                'ویرایش' => [
                    'edit',
                    'update',
                ],
                'حذف'    => 'destroy',
            ],
        ],
    ],
];
