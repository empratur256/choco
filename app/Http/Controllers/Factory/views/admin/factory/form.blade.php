<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        {!! Form::label('title','عنوان') !!}
        {!! Form::text('title',null,['class'=>'md-input']) !!}
    </div>
    <div class="uk-width-medium-1-1">
        {!! Form::label('address','آدرس') !!}
        {!! Form::text('address',null,['class'=>'md-input']) !!}
    </div>
</div>
