<?php
return [
    'factory' => [
        'title'   => 'کارخانه ها',
        'icon'    => 'fa fa-industry',
        'order'   => 21,
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست',
                'action'     => 'Factory\Controllers\Admin\FactoryController@index',
                'parameters' => [],
            ],
            'Add'   => [
                'title'      => 'ایجاد',
                'action'     => 'Factory\Controllers\Admin\FactoryController@create',
                'parameters' => [],
            ],
        ],
    ],
];
