<?php
Route::group(['prefix'     => config('site.admin'),
              'middleware' => ['admin'],
              'namespace'  => 'Factory\Controllers\Admin',
             ], function(){
    Route::resource('factory', 'FactoryController', ['except' => ['show']]);
});
