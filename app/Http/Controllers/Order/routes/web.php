<?php
Route::group([
                 'prefix'     => config('site.admin'),
                 'middleware' => ['admin'],
                 'namespace'  => 'Order\Controllers\Admin',
             ], function(){
    Route::get('order/download-cell-picture/{orderId}/{cellNumber}', 'OrderController@downloadCellPicture');
    Route::get('order/download-package-picture/{orderId}', 'OrderController@downloadPackagePicture');
    Route::post('order/restore', 'OrderController@restore');
    Route::post('order/track/{id}/{address}', 'OrderController@track');
    Route::post('order/status/{id}', 'OrderController@status');
    Route::resource('order', 'OrderController', [
        'only' => [
            'show',
            'index',
            'destroy',
        ],
    ]);
});
Route::group([
                 'prefix'     => config('site.dashboard'),
                 'middleware' => 'dashboard',
             ], function(){
    Route::get('orders', 'Order\Controllers\Site\OrderController@index');
    Route::get('orders/{id}/payment', 'Order\Controllers\Site\OrderController@payment');
});

