<?php

namespace Sedehi\Http\Controllers\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\CustomOrder\Models\BaseFlavor;
use Sedehi\Http\Controllers\CustomOrder\Models\Brain;
use Sedehi\Http\Controllers\CustomOrder\Models\Flavor;
use Sedehi\Http\Controllers\CustomOrder\Models\Shape;
use Sedehi\Http\Controllers\CustomOrder\Models\Weight;

class Cells extends Model
{

    use  Filterable, SoftDeletes;

    protected $table = 'custom_order_cells';
    public $timestamps = true;

    protected $filterable = [
        'title' => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created'
            ]
        ],
    ];

    public function flavor()
    {
        return $this->hasOne(Flavor::class,'id' ,'flavor_id');
    }

    public function brain()
    {
        return $this->hasOne(Brain::class,'id' ,'brain_id');
    }

    public function baseFlavor()
    {
        return $this->hasOne(BaseFlavor::class ,'id' , 'base_flavor_id');
    }

    public function weight()
    {
        return $this->hasOne(Weight::class ,'id' , 'weight_id');
    }

    public function shape()
    {
        return $this->hasOne(Shape::class ,'id' , 'shape_id');
    }
}
