<?php

namespace Sedehi\Http\Controllers\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Coupon\Jobs\MakeCouponForUser;
use Sedehi\Http\Controllers\Coupon\Models\Code;
use Sedehi\Http\Controllers\Coupon\Models\PresentedCouponCode;
use Sedehi\Http\Controllers\CustomOrder\Models\Amount;
use Sedehi\Http\Controllers\CustomOrder\Models\Layout;
use Sedehi\Http\Controllers\CustomOrder\Models\Material;
use Sedehi\Http\Controllers\CustomOrder\Models\Package;
use Sedehi\Http\Controllers\CustomOrder\Models\Routine;
use Sedehi\Http\Controllers\CustomOrder\Models\Type;
use Sedehi\Http\Controllers\Payment\Models\Payment;
use Sedehi\Http\Controllers\Payment\Models\Settlement;
use Sedehi\Http\Controllers\Setting\Models\Setting;
use Sedehi\Http\Controllers\User\Models\User;

class Order extends Model
{

    use  Filterable, SoftDeletes;

    protected $table      = 'order';
    public    $timestamps = true;

    protected $filterable = [
        'id',
        'total_price',
        'payment_type',
        'confirm',
        'paid',
        'type',
        'processing_stock',
        'ready_to_send',
        'delivered',
        'ready_for_production',
        'in_production_procedure',
        'user_id',
        'need_to_produce',
        'status'        => [
            'scope' => 'status',
        ],
        'custom_single' => [
            'scope' => 'CustomSingle',
        ],
        'created_at'    => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public static function boot()
    {
        parent::boot();
        static::updated(function($order){
            if($order->isDirty('paid')){
                if($order->paid == 1){
                    $setting = Setting::first();
                    if($setting->ref_coupon){
                        $registrationOfOrder = User::find($order->user_id);
                        if(!is_null($registrationOfOrder) && !is_null($registrationOfOrder->presented_by)){
                            $receivingCoupon = User::find($registrationOfOrder->presented_by);
                            dispatch(new MakeCouponForUser($registrationOfOrder, $receivingCoupon, $setting));
                        }
                    }
                }
            }
        });
    }

    public function getFullPriceAttribute()
    {
        return intval($this->total_price + $this->shipping_price + $this->tax_price);
    }

    public function scopeStatus($query)
    {
        $column = null;
        switch(request('status')){
            case 1:
                $column = 'confirm';
            break;
            case 2:
                $column = 'paid';
            break;
            case 3:
                $column = 'processing_stock';
            break;
            case 4:
                $column = 'ready_to_send';
            break;
            case 5:
                $column = 'delivered';
            break;
            case 6:
                $column = 'ready_for_production';
            break;
            case 7:
                $column = 'in_production_procedure';
            break;
        }
        if(!is_null($column)){
            return $query->where($column, 1);
        }
    }

    public function scopeCustomSingle($query)
    {
        return $query->where('type', 'custom')->whereHas('items', function($query){
            $query->where('quantity', 1);
        });
    }
    public function scopeValid($query)
    {
        return $query->where('valid', 1);
    }

    public function scopeCustom($query)
    {
        return $query->where('type', 'custom');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }

    public function couponCode()
    {
        return $this->belongsTo(Code::class, 'id', 'order_id');
    }

    public function couponPresented()
    {
        return $this->belongsTo(PresentedCouponCode::class, 'id', 'order_id');
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function settlements()
    {
        return $this->hasMany(Settlement::class);
    }

    public function statusLog()
    {
        return $this->hasMany(StatusLog::class);
    }

    public function producer()
    {
        return $this->hasMany(OrderFactory::class);
    }

    public function warehouse()
    {
        return $this->hasMany(OrderWarehouse::class);
    }

    public function cells()
    {
        return $this->hasMany(Cells::class);
    }

    public function layout()
    {
        return $this->belongsTo(Layout::class);
    }

    public function amount()
    {
        return $this->belongsTo(Amount::class);
    }

    public function routine()
    {
        return $this->belongsTo(Routine::class);
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function customType()
    {
        return $this->belongsTo(Type::class,'custom_product_type_id');
    }

    public function customMaterial()
    {
        return $this->belongsTo(Material::class,'custom_product_material_id');
    }
}
