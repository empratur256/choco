<?php

namespace Sedehi\Http\Controllers\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\Product\Models\Product;
use Sedehi\Http\Controllers\Warehouse\Models\Warehouse;

class OrderWarehouse extends Model
{

    use  Filterable;

    protected $table      = 'order_product_warehouse';
    public    $timestamps = false;
    public    $dates      = ['date'];
    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
