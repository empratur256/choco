<?php

namespace Sedehi\Http\Controllers\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;
use Sedehi\Http\Controllers\User\Models\User;

class StatusLog extends Model
{

    use  Filterable;

    protected $table      = 'order_change_status_log';
    public    $timestamps = true;

    protected $filterable = [
        'order_id',
        'user_id',
        'status',
        'checked',
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
