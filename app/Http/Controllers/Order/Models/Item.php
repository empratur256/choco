<?php

namespace Sedehi\Http\Controllers\Order\Models;

use Illuminate\Database\Eloquent\Model;
use Sedehi\Filterable\Filterable;


use Sedehi\Http\Controllers\Product\Models\Product;


class Item extends Model
{

    use  Filterable;

    protected $table      = 'order_item';
    public    $timestamps = true;

    protected $filterable = [
        'title'      => [
            'operator' => 'Like',
        ],
        'created_at' => [
            'between' => [
                'start_created',
                'end_created',
            ],
        ],
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
