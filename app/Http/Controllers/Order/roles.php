<?php
return [
    'order' => [
        'title'  => 'سفارش ها',
        'access' => [
            'OrderController' => [
                'لیست'                      => 'index',
                'جزئیات'                    => [
                    'show',
                    'track',
                    'status',
                    'downloadCellPicture',
                    'downloadPackagePicture',
                ],
                'حذف'                       => [
                    'destroy',
                    'restore',
                ],
                'طراحی های اختصاصی'         => 'custom',
                'سفارش های عادی'            => 'order',
                'ویرایش وضعیت پرداخت'       => 'change_paid',
                'ویرایش وضعیت درحال تولید'  => 'change_in_production_procedure',
                'ویرایش وضعیت پردازش انبار' => 'change_processing_stock',
                'ویرایش وضعیت درحال ارسال'  => 'change_ready_to_send',
                'ویرایش وضعیت تحویل شده'    => 'change_delivered',
            ],
        ],
    ],
];
