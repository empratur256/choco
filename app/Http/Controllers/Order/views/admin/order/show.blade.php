@extends('views.layouts.admin.master')
@section('content')
    @if(!is_null($item->deleted_at))
        {!! Form::open(['action'=>['Order\Controllers\Admin\OrderController@restore',1]]) !!}
    @else
        {!! Form::open(['action'=>['Order\Controllers\Admin\OrderController@destroy',1],'method' =>'DELETE']) !!}
    @endif
    {!! Form::hidden('deleteId[]',$item->id) !!}
    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('Order.OrderController.destroy'))
                                @if(!is_null($item->deleted_at))
                                    <button type="submit"
                                            class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect"
                                            style="margin-left: 15px !important;">بازیابی
                                    </button>
                                @else
                                    <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید ؟');"
                                            class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect"
                                            style="margin-left: 15px !important;">حذف
                                    </button>
                                @endif
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            {!! Form::open(['action'=>['Order\Controllers\Admin\OrderController@status',$item->id],'class' => 'statusForm']) !!}
            {!! Form::hidden('',null,['id' => 'data']) !!}
            {!! Form::close() !!}
            <span class="icheck-inline disabled">
                {!! Form::checkbox('confirm',1,$item->confirm,['id' => 'confirm','disabled' => 'disabled','class' => 'large-check submit']) !!}
                {!! Form::label('confirm','ثبت شده',['class' => 'inline-label']) !!}
            </span>
            <span class="icheck-inline">
                {!! Form::checkbox('paid',1,$item->paid,['id' => 'paid','class' => 'large-check submit'] + $paidDisabled) !!}
                {!! Form::label('paid','پرداخت شده',['class' => 'inline-label']) !!}
            </span>
            @if($item->need_to_produce)
                {{--  <span class="icheck-inline">
                  {!! Form::checkbox('ready_for_production',1,$item->ready_for_production,['id' => 'ready_for_production','class' => 'large-check submit']) !!}
                      {!! Form::label('ready_for_production','اماده براي توليد',['class' => 'inline-label']) !!}
              </span>--}}
                <span class="icheck-inline">
                {!! Form::checkbox('in_production_procedure',1,$item->in_production_procedure,['id' => 'in_production_procedure','class' => 'large-check submit'] + $inProductionProcedureDisabled) !!}
                    {!! Form::label('in_production_procedure','در حال توليد',['class' => 'inline-label']) !!}
            </span>

            @endif
            <span class="icheck-inline">
                {!! Form::checkbox('processing_stock',1,$item->processing_stock,['id' => 'processing_stock','class' => 'large-check submit'] + $processingStockDisabled) !!}
                {!! Form::label('processing_stock','پردازش انبار',['class' => 'inline-label']) !!}
            </span>
            <span class="icheck-inline">
                {!! Form::checkbox('ready_to_send',1,$item->ready_to_send,['id' => 'ready_to_send','class' => 'large-check submit'] + $readyToSendDisabled) !!}
                {!! Form::label('ready_to_send','در حال ارسال',['class' => 'inline-label']) !!}
            </span>
            <span class="icheck-inline">
                {!! Form::checkbox('delivered',1,$item->delivered,['id' => 'delivered','class' => 'large-check submit'] + $deliveredDisabled) !!}
                {!! Form::label('delivered','تحویل شده',['class' => 'inline-label']) !!}
            </span>
            @if(!is_null($item->deleted_at))
                <span class="uk-badge uk-badge-danger">حذف شده</span>
            @endif
            <br>
            فاکتور :‌ @if($item->invoice_request) ارسال شود @else ارسال نشود @endif
        </div>
        <div id="page_content_inner">

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                @if($item->in_production_procedure && count($item->producer))
                    <div class="uk-width-xLarge-10-10 uk-width-large-10-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"
                                    style="font: 500 14px/50px Roboto,sans-serif !important;">اطلاعات کارخانه های تولید
                                    کننده</h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <table class="uk-table">
                                            <thead>
                                            <tr>
                                                <th class="uk-width-1-4">کالا</th>
                                                <th class="uk-width-1-4">کارخانه تولید کننده</th>
                                                <th class="uk-width-1-4">شماره</th>
                                                <th class="uk-width-1-4">تاریخ</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($item->producer as $producer)
                                                <tr>
                                                    <td>
                                                        @if(!is_null($producer->product_id))
                                                            {{$producer->product->title}}
                                                        @else

                                                            سفارش اختصاصی
                                                        @endif
                                                    </td>

                                                    <td>{{ $producer->factory->title }}</td>
                                                    <td>{{ $producer->number }}</td>
                                                    <td>@if(!is_null($producer->date)) {{ jdate('Y-m-d',$producer->date->timestamp) }} @endif</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if($item->processing_stock && count($item->warehouse))
                    <div class="uk-width-xLarge-10-10 uk-width-large-10-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"
                                    style="font: 500 14px/50px Roboto,sans-serif !important;">اطلاعات پردازش انبار</h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <table class="uk-table">
                                            <thead>
                                            <tr>
                                                <th class="uk-width-1-4">کالا</th>
                                                <th class="uk-width-1-4">انبار</th>
                                                <th class="uk-width-1-4">شماره</th>
                                                <th class="uk-width-1-4">تاریخ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($item->warehouse as $warehouse)
                                                <tr>
                                                    <td>
                                                        @if(!is_null($warehouse->product_id))
                                                            {{$warehouse->product->title}}
                                                        @else
                                                            سفارش اختصاصی
                                                        @endif
                                                    </td>
                                                    <td>{{ $warehouse->warehouse->title }}</td>
                                                    <td>{{ $warehouse->number }}</td>
                                                    <td>@if(!is_null($warehouse->date)) {{ jdate('Y-m-d',$warehouse->date->timestamp) }} @endif</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text"
                                style="font: 500 14px/50px Roboto,sans-serif !important;">اطلاعات سفارش</h3>
                        </div>
                        <div class="md-card-content">
                            <ul class="md-list">
                                <li>
                                    <div class="md-list-content">
                                        <span class="uk-text-small uk-text-muted uk-display-block">کاربر</span>
                                        <span class="md-list-heading uk-text-large uk-text-success"><a target="_blank"
                                                                                                       href="{!! action('User\Controllers\Admin\UserController@showInfo',$item->user->id) !!}">{{ $item->user->name.' '.$item->user->family }}</a> </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="uk-text-small uk-text-muted uk-display-block">تاریخ ثبت سفارش</span>
                                        <span class="md-list-heading uk-text-large"><span data-uk-tooltip
                                                                                          title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span></span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="uk-text-small uk-text-muted uk-display-block">مالیات</span>
                                        <span class="md-list-heading uk-text-large">{{$item->tax}}
                                            % ({{number_format($item->tax_price)}} تومان) </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="uk-text-small uk-text-muted uk-display-block">تخفیف</span>
                                        <span class="md-list-heading uk-text-large">{{ number_format($item->discount) }}
                                            تومان</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="uk-text-small uk-text-muted uk-display-block">مبلغ کل </span>
                                        <span class="md-list-heading uk-text-large">{{ number_format($item->full_price) }}
                                            تومان</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="uk-text-small uk-text-muted uk-display-block">هزینه حمل و نقل</span>
                                        <span class="md-list-heading uk-text-large">{{ number_format($item->shipping_price) }}
                                            تومان</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="uk-text-small uk-text-muted uk-display-block">روش پرداخت</span>
                                        <span class="md-list-heading uk-text-large">
                                            @if($item->payment_type == 'cash')
                                                نقدی
                                            @elseif($item->payment_type == 'online')
                                                آنلاین
                                            @elseif($item->payment_type == 'card')
                                                کارت به کارت
                                            @elseif($item->payment_type == 'account')
                                                واریز به حساب
                                            @endif</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text"
                                style="font: 500 14px/50px Roboto,sans-serif !important;">
                                @if($item->type == 'custom')
                                    <span class="uk-badge uk-badge-success">سفارش اختصاصی</span>
                                @else
                                    <span>کالا ها</span>
                                @endif
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid" data-uk-grid-margin>
                                <div class="uk-width-medium-1-1">
                                    <table class="uk-table">
                                        <thead>
                                            @if($item->type == 'order')
                                                <tr>
                                                    <th class="uk-width-1-3">کالا</th>
                                                    <th class="uk-width-1-3">کد</th>
                                                    <th class="uk-width-1-3">قیمت</th>
                                                    <th class="uk-width-1-3">تعداد</th>
                                                    <th class="uk-width-1-3">جمع</th>
                                                </tr>
                                            @else
                                                <tr>
                                                    <th class="uk-width-1-3"></th>
                                                    <th class="uk-width-1-3"></th>
                                                    <th class="uk-width-1-3"></th>
                                                    <th class="uk-width-1-3"></th>
                                                </tr>
                                            @endif
                                        </thead>
                                        <tbody>
                                        @php $sumPrice = 0 @endphp
                                        @php $totalPrice = 0 @endphp
                                        @foreach($item->items as $order)
                                            @if(!is_null($order->product_id))
                                                @php $sumPrice += $order->quantity * $order->price @endphp
                                                <tr>
                                                    <td>
                                                        <a target="_blank"
                                                           href="{!! action('Product\Controllers\Admin\ProductController@show',$order->product_id) !!}">{{$order->product->title}}</a>
                                                        <br>

                                                    </td>
                                                    <td>{{$order->product->code}}</td>
                                                    @if($item->type == 'order')
                                                        <td>{{number_format($order->price)}} تومان</td>
                                                    @endif
                                                    <td>{{ $item->type == 'order' ? number_format($order->quantity) : number_format($item->custom_order_count)}}</td>
                                                    <td>{{ number_format($order->quantity * $order->price) }} تومان</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                        <tfoot>

                                        @php $totalPrice +=  $sumPrice @endphp
                                        @if($item->discount > 0)
                                            @php $totalPrice -= $item->discount @endphp
                                            <tr>
                                                <td>تخفیف</td>
                                                @if($item->type == 'order')
                                                    <td></td>
                                                @endif
                                                <td></td>
                                                <td></td>
                                                <td>{{ number_format($item->discount) }} تومان</td>
                                            </tr>
                                        @endif
                                        @if($item->tax > 0 && $item->tax_price > 0)
                                            @php $totalPrice += $item->tax_price @endphp
                                            <tr>
                                                <td style="color: black">{{$item->tax}}% مالیات</td>
                                                @if($item->type == 'order')
                                                    <td></td>
                                                @endif
                                                <td></td>
                                                <td></td>
                                                <td>{{ number_format($item->tax_price) }} تومان</td>
                                            </tr>
                                        @endif
                                        @if($item->shipping_price > 0)
                                            @php $totalPrice += $item->shipping_price @endphp
                                            <tr>
                                                <td style="color: black">هزینه حمل و نقل</td>
                                                @if($item->type == 'order')
                                                    <td></td>
                                                @endif
                                                <td></td>
                                                <td></td>
                                                <td>{{ number_format($item->shipping_price) }} تومان</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td style="color: black">مبلغ قابل پرداخت</td>
                                            @if($item->type == 'order')
                                                <td></td>
                                            @endif
                                            <td></td>
                                            <td></td>
                                            @if($item->type == 'order')
                                                <td style="color: black">{{ number_format($totalPrice) }} تومان</td>
                                            @else
                                                <td style="color: black">{{ number_format($item->full_price) }} تومان</td>
                                            @endif
                                        </tr>
                                        @if($item->type == 'custom')
                                            <tr>
                                                <td style="color: black">نوع محصول</td>
                                                <td></td>
                                                <td></td>
                                                <td>{{ $item->customMaterial->title }}</td>
                                            </tr>
                                            <tr>
                                                <td style="color: black">دسته بندی محصول</td>
                                                <td></td>
                                                <td></td>
                                                <td>{{ $item->customType->title }}</td>
                                            </tr>
                                            <tr>
                                                <td style="color: black">سلول ها</td>
                                                <td></td>
                                                <td></td>
                                                <td style="color: black">
                                                    <button class="uk-button" data-uk-modal="{target:'#my-id'}">مشاهده</button>
                                                    <div id="my-id" class="uk-modal">
                                                        <div class="uk-modal-dialog uk-modal-dialog-large">
                                                            <a class="uk-modal-close uk-close"></a>
                                                            <div class="uk-overflow-container">
                                                                <div class="uk-grid">
                                                                    @foreach($item->cells as $cell)
                                                                    <div class="uk-width-medium-1-5 uk-width-1-3" style="margin: 1% 0 ;">

                                                                        <div class="uk-panel uk-panel-box" style="text-align: center;">
                                                                            <div class="uk-panel-teaser" style="overflow: hidden;border: 1px solid lightgrey;">
                                                                                <img src="@if(!is_null($cell->picture)) {{ asset('uploads/order/'.$item->id.'/'.$cell->picture_thumb) }} @else {{ asset('assets/site/img/nimg.jpg') }} @endif">
                                                                            </div>

                                                                            <p><span>شماره سلول : </span><span>{{$cell->cell_number}}</span></p>
                                                                            <p><span>طعم پایه : </span><span>{{$cell->baseFlavor->title}}</span></p>
                                                                            <p><span>مکمل : </span><span>{{ is_null($cell->flavor) ? '--' : $cell->flavor->title }}</span></p>
                                                                            <p><span>مغز : </span><span>{{ is_null($cell->brain) ? '--' : $cell->brain->title }}</span></p>
                                                                            <p><span>وزن : </span><span>{{ is_null($cell->weight) ? '--' : $cell->weight->title }}</span></p>
                                                                            <p><span>شکل : </span><span>{{ is_null($cell->shape) ? '--' : $cell->shape->title }}</span></p>

                                                                            @if(!is_null($cell->picture))
                                                                                <p><a href="{{ action('Order\Controllers\Admin\OrderController@downloadCellPicture',[$item->id,$cell->cell_number]) }}" class="uk-button uk-button-success"> دانلود تصویر سلول </a></p>
                                                                            @endif
                                                                        </div>

                                                                    </div>

                                                                    @endforeach

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            @if(!is_null($item->amount))
                                                <tr>
                                                    <td style="color: black">سینی</td>
                                                    <td>{{  $item->amount->title }}</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            @endif

                                            @if(!is_null($item->package_id))
                                                <tr>
                                                    <td style="color: black">بسته بندی</td>
                                                    <td>
                                                        <a href="{{action('CustomOrder\Controllers\Admin\PackageController@edit',$item->package->id) }}"> {{$item->package->admin_title}} </a>
                                                    </td>
                                                    <td>
                                                        @if(!is_null($item->package_picture))
                                                            <div class="uk-panel uk-panel-box" style="text-align: center;">
                                                                <div class="uk-panel-teaser" style="overflow: hidden;">
                                                                    <img src="{{ asset('uploads/order/'.$item->id.'/'.str_replace('package','package_thumb',$item->package_picture)) }}">
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td style="color: black">
                                                        @if(!is_null($item->package_picture))
                                                            <a href="{{ action('Order\Controllers\Admin\OrderController@downloadPackagePicture',[$item->id]) }}" class="uk-button uk-button-success"> دانلود تصویر </a>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endif

                                            <tr>
                                                <td style="color: black">چینش</td>
                                                <td><a href="{{action('CustomOrder\Controllers\Admin\LayoutController@edit',$item->layout->id) }}"> {{$item->layout->title}} </a></td>
                                                <td>
                                                    <div class="uk-panel uk-panel-box" style="text-align: center; margin: 0 !important; padding: 0 !important;">
                                                        <div class="uk-panel-teaser" style="overflow: hidden; margin: 0 !important; padding: 0 !important;">
                                                            <img src="{!! asset('uploads/layout/'.'240xauto-'.$item->layout->picture) !!}">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="color: black">
                                                    @if($item->layout_price > 0)
                                                        <span>{{ number_format($item->layout_price) }} تومان</span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="color: black">روال</td>
                                                <td><a href="{{action('CustomOrder\Controllers\Admin\RoutineController@edit',$item->routine->id) }}"> {{$item->routine->title}} </a></td>
                                                <td>
                                                    زمان تحویل {{ $item->routine->preparation_time }} روز
                                                </td>
                                                <td style="color: black">
                                                   {{ $item->routine->major == 0 ? 'قیمت معمولی' : 'قیمت عمده' }}
                                                </td>
                                            </tr>
                                        @endif
                                        </tfoot>
                                    </table>
                                </div>


                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                    @if(!is_null($item->couponCode))

                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"
                                    style="font: 500 14px/50px Roboto,sans-serif !important;">اطلاعات کد تخفیف</h3>
                            </div>
                            <div class="md-card-content">
                                <ul class="md-list">
                                    <li>
                                        <div class="md-list-content">
                                            <span class="uk-text-small uk-text-muted uk-display-block">عنوان تخفیف</span>
                                            <span class="md-list-heading uk-text-large uk-text-success">{{ $item->couponCode->coupon->title }}</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="uk-text-small uk-text-muted uk-display-block">کد کوپن</span>
                                            <span class="md-list-heading uk-text-large">{{ $item->couponCode->code }}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endif
                    @if(!is_null($item->couponPresented))

                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"
                                    style="font: 500 14px/50px Roboto,sans-serif !important;">اطلاعات کد تخفیف</h3>
                            </div>
                            <div class="md-card-content">
                                <ul class="md-list">
                                    <li>
                                        <div class="md-list-content">
                                            <span class="uk-text-small uk-text-muted uk-display-block">عنوان تخفیف</span>
                                            <span class="md-list-heading uk-text-large uk-text-success">معرفی کاربر</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="uk-text-small uk-text-muted uk-display-block">کد کوپن</span>
                                            <span class="md-list-heading uk-text-large">{{ $item->couponPresented->code }}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text"
                                style="font: 500 14px/50px Roboto,sans-serif !important;">تغییرات</h3>
                        </div>
                        <div class="md-card-content">
                            <table class="uk-table">
                                <thead>
                                <tr>
                                    <th class="uk-width-1-3">توسط</th>
                                    <th class="uk-width-1-3">قسمت</th>
                                    <th class="uk-width-1-3">وضعیت</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $sections = ['confirm' => 'ثبت شده','paid' => 'پرداخت شده','processing_stock' => 'پردازش انبار','ready_to_send' => 'در حال ارسال','delivered' => 'تحویل شده','ready_for_production' => 'اماده براي توليد','in_production_procedure'=>'در حال توليد'] @endphp
                                @foreach($item->statusLog as $log)
                                    <tr>
                                        <td>{{ $log->user->name.' '.$log->user->family }}</td>
                                        <td>
                                            {{ $sections[$log->status] }}
                                        </td>
                                        <td>@if($log->checked == 1) تایید شده  @elseif($log->checked == 0) لغو
                                            تایید  @elseif($log->checked == 2)  نیاز به بررسی @endif</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text"
                                style="font: 500 14px/50px Roboto,sans-serif !important;">
                                اطلاعات دریافت کننده
                            </h3>
                        </div>
                        <div class="md-card-content large-padding">
                            <div class="uk-grid uk-grid-divider uk-grid-medium">
                                <div class="uk-width-large-1-2">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">حمل و نقل</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-middle">{{$item->address->shipping->title}}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">استان / شهر</span>
                                        </div>
                                        <div class="uk-width-large-2-3">
                                            <span class="uk-text-middle">{{ $item->address->province->name }}
                                                / {{ $item->address->city->name }}</span>
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">نام دریافت کننده</span>
                                        </div>
                                        <div class="uk-width-large-2-3">{{$item->address->name_family}}
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">موبایل / تلفن ثابت</span>
                                        </div>
                                        <div class="uk-width-large-2-3">{{ $item->address->mobile }}
                                            / {{$item->address->tel}}
                                        </div>
                                    </div>
                                    <hr class="uk-grid-divider">
                                    <div class="uk-grid uk-grid-small">
                                        <div class="uk-width-large-1-3">
                                            <span class="uk-text-muted uk-text-small">کد پستی</span>
                                        </div>
                                        <div class="uk-width-large-2-3">{{ $item->address->postal_code }}</div>
                                    </div>
                                    <hr class="uk-grid-divider uk-hidden-large">
                                </div>
                                <div class="uk-width-large-1-2">
                                    {!! Form::open(['action'=>['Order\Controllers\Admin\OrderController@track',$item->id,$item->address->id]]) !!}

                                    <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                                        <div class="uk-width-medium-2-3">
                                            {!! Form::label('track_id','کد مرسوله') !!}
                                            {!! Form::text('track_id',$item->address->track_id,['class'=>'md-input']) !!}
                                        </div>
                                        <div class="uk-width-medium-1-3">
                                            <button type="submit"
                                                    class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect">
                                                دخیره کد
                                            </button>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                    <hr class="uk-grid-divider">
                                    <p>
                                        <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">آدرس</span>
                                        {{$item->address->address}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(count($item->payments))
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"
                                    style="font: 500 14px/50px Roboto,sans-serif !important;">تراکنش موفق آنلاین</h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <table class="uk-table">
                                            <thead>
                                            <tr>
                                                <th class="uk-width-1-3">کد پیگیری</th>
                                                <th class="uk-width-1-3">مبلغ</th>
                                                <th class="uk-width-1-3">درگاه</th>
                                                <th class="uk-width-1-3">تاریخ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($item->payments as $payment)
                                                <tr>

                                                    <td>{{$payment->reference}}</td>
                                                    <td>{{number_format($payment->amount)}} تومان</td>
                                                    <td>
                                                        @if($payment->provider == 'zarinpal')
                                                            زرین پال
                                                        @elseif($payment->provider == 'mellat')
                                                            ملت
                                                        @endif

                                                    </td>
                                                    <td>{{jdate('Y/m/d H:i',$payment->created_at->timestamp)}}</td>
                                                </tr>

                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    @endif
                    @if(count($item->settlements))
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text"
                                    style="font: 500 14px/50px Roboto,sans-serif !important;">تراکنش </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1">
                                        <table class="uk-table">
                                            <thead>
                                            <tr>
                                                <th class="uk-width-1-3">کد پیگیری</th>
                                                <th class="uk-width-1-3">مبلغ</th>
                                                <th class="uk-width-1-3">حساب</th>
                                                <th class="uk-width-1-3">تاریخ</th>
                                                <th class="uk-width-1-3">وضعیت</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($item->settlements as $settlement)
                                                <tr>

                                                    <td>{{$settlement->reference}}</td>
                                                    <td>{{number_format($settlement->amount)}} تومان</td>
                                                    <td>{{$settlement->account->title}}</td>
                                                    <td>{{jdate('Y/m/d H:i',$settlement->created_at->timestamp)}}</td>
                                                    <td>
                                                        @if($settlement->confirm == 0 && $settlement->reject == 0)
                                                            <span class="uk-badge uk-badge-info">درحال بررسی</span>
                                                        @elseif($settlement->confirm == 1)
                                                            <span class="uk-badge uk-badge-success">تایید شده</span>
                                                        @elseif($settlement->reject == 1)
                                                            <span class="uk-badge uk-badge-danger">رد شده</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    @endif
                </div>


            </div>

        </div>
    </div>
    <div class="uk-modal" id="in_production_procedure_modal">
        <div class="uk-modal-dialog uk-modal-dialog-large">
            <button type="button" class="uk-modal-close uk-close"></button>
            {!! Form::open(['action'=>['Order\Controllers\Admin\OrderController@status',$item->id],'class' => 'in_production_procedure_form']) !!}
            {!! Form::hidden('in_production_procedure',null,['id' => 'in_production_procedure_data']) !!}
            <h2 style="color:green">انتخاب کارخانه</h2>
            <div class="uk-grid" data-uk-grid-margin>
                @foreach($item->items->where('need_to_produce',1) as $order)
                    <div class="uk-width-medium-1-4">
                        <p style="line-height: 65px !important;">
                            @if(is_null($order->product_id))
                                سفارش اختصاصی :‌
                            @else
                                {{ $order->product->title }}
                            @endif
                        </p>
                    </div>
                    <div class="uk-width-medium-1-4">
                        @php $product_id = 0 @endphp
                        @if(!is_null($order->product_id))
                            @php $product_id = $order->product_id @endphp
                        @endif
                        {!! Form::select('factory['.$product_id.'][id]',$factories,null,['class'=>'md-input']) !!}
                    </div>
                    <div class="uk-width-medium-1-4">
                        {!! Form::label('factory_number','شماره') !!}
                        {!! Form::text('factory['.$product_id.'][number]',null,['class'=>'md-input']) !!}
                    </div>
                    <div class="uk-width-medium-1-4">
                        {!! Form::label('factory_date','تاریخ') !!}
                        {!! Form::text('factory['.$product_id.'][date]',null,['class'=>'md-input date']) !!}
                    </div>
                @endforeach

            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="submit" class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect"
                        style="font-size:16px !important;">ذخیره
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="uk-modal" id="processing_stock_modal">
        <div class="uk-modal-dialog uk-modal-dialog-large">
            <button type="button" class="uk-modal-close uk-close"></button>
            {!! Form::open(['action'=>['Order\Controllers\Admin\OrderController@status',$item->id],'class' => 'processing_stock_form']) !!}
            {!! Form::hidden('processing_stock',null,['id' => 'processing_stock_data']) !!}
            <h2 style="color:green">انتخاب انبار</h2>
            <div class="uk-grid" data-uk-grid-margin>
                @foreach($item->items as $order)
                    <div class="uk-width-medium-1-4">
                        <p style="line-height: 65px !important;">
                            @if(is_null($order->product_id))
                                سفارش اختصاصی :‌
                            @else
                                {{ $order->product->title }}
                            @endif
                        </p>
                    </div>
                    <div class="uk-width-medium-1-4">
                        @php $product_id = 0 @endphp
                        @if(!is_null($order->product_id))
                            @php $product_id = $order->product_id @endphp
                        @endif
                        {!! Form::select('warehouse['.$product_id.'][id]',$warehouses,null,['class'=>'md-input']) !!}
                    </div>
                    <div class="uk-width-medium-1-4">
                        {!! Form::label('warehouse_number','شماره') !!}
                        {!! Form::text('warehouse['.$product_id.'][number]',null,['class'=>'md-input']) !!}
                    </div>
                    <div class="uk-width-medium-1-4">
                        {!! Form::label('warehouse_date','تاریخ') !!}
                        {!! Form::text('warehouse['.$product_id.'][date]',null,['class'=>'md-input date']) !!}
                    </div>
                @endforeach
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="submit" class="md-btn md-btn-success md-btn-small md-btn-wave-light waves-effect"
                        style="font-size:16px !important;">ذخیره
                </button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@endsection
@push('js')
    <script>
        $(document).ready(function () {
                    @if(session()->has('processing_stock_modal'))
            var modal = UIkit.modal("#processing_stock_modal");
            modal.show();
            modal.on({
                'hide.uk.modal': function () {
                    $('#processing_stock').prop("checked", false);
                }
            });
                    @endif

                    @if(session()->has('in_production_procedure_modal'))
            var modal = UIkit.modal("#in_production_procedure_modal");
            modal.show();
            modal.on({
                'hide.uk.modal': function () {
                    $('#in_production_procedure').prop("checked", false);
                }
            });
            @endif
            $('.submit').on('change', function (event) {
                var old = !$(this).is(':checked');
                var id = $(this).attr('id');
                if (id == 'in_production_procedure' || id == 'processing_stock') {

                    if (id == 'in_production_procedure') {
                        if ($(this).is(':checked')) {
                            $('#in_production_procedure_data').val(1);
                        } else {
                            $('#in_production_procedure_data').val(0);
                            if (confirm("آیا از انجام این عملیات اطمینان دارید؟")) {
                                $('.in_production_procedure_form').submit();
                            }
                        }

                        if (old) {
                            $(this).prop("checked", true);
                        } else {
                            var modal = UIkit.modal("#in_production_procedure_modal");
                            modal.show();
                            $(this).prop("checked", false);
                        }
                    }
                    else if (id == 'processing_stock') {
                        if ($(this).is(':checked')) {
                            $('#processing_stock_data').val(1);
                        } else {
                            $('#processing_stock_data').val(0);
                            if (confirm("آیا از انجام این عملیات اطمینان دارید؟")) {
                                $('.processing_stock_form').submit();
                            }
                        }

                        if (old) {
                            $(this).prop("checked", true);
                        } else {
                            var modal = UIkit.modal("#processing_stock_modal");
                            modal.show();
                            $(this).prop("checked", false);
                        }
                    }

                } else {
                    if (!$(this).is(':disabled')) {

                        if (confirm("آیا از انجام این عملیات اطمینان دارید؟")) {
                            var name = $(this).attr('name');
                            $('#data').attr('name', name);
                            if ($(this).is(':checked')) {
                                $('#data').val(1);
                            } else {
                                $('#data').val(0);
                            }
                            $('.statusForm').submit();
                        } else {
                            if (old) {
                                $(this).prop("checked", true);
                            } else {
                                $(this).prop("checked", false);

                            }
                        }
                    }

                }

            });

            $('#in_production_procedure').on('change', function (e) {
                e.defaultPrevented();

            });

        });
    </script>
@endpush
@push('css')
    <style>
        input:disabled {
            color: inherit;
        }

        .disabled + .inline-label {
            color: inherit;
        }

        .icheckbox_md.disabled.checked {
            background: #009688;
            border-color: #009688 !important;
        }

        .icheckbox_md.disabled.checked {
            background: #009688 !important;
            border-color: #009688 !important;
        }

        .large-check {
            width: 20px;
            height: 20px;
        }
    </style>

@endpush