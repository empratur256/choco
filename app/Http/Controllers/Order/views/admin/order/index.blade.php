@extends('views.layouts.admin.master')
@section('content')
    @include('Order.views.admin.order.search')
    {!! Form::open(['action'=>['Order\Controllers\Admin\OrderController@destroy',1],'method' =>'DELETE']) !!}

    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-1-1 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-right">
                        <div class="md-btn-group">
                            @if(permission('Order.OrderController.destroy'))
                                <button type="submit" onclick="return confirm('آیا از حذف اطلاعات مطمئن هستید ؟');" class="md-btn md-btn-danger md-btn-small md-btn-wave-light waves-effect" style="margin-left: 15px !important;">حذف</button>
                            @endif
                            <a class="md-btn md-btn-primary md-btn-small md-btn-wave-light waves-effect" href="#" style="margin-left: 15px !important;" id="sidebar_secondary_toggle">جستجو</a>
                            @if(count(request()->except(['page'])))
                                <a class="md-btn md-btn-warning md-btn-small md-btn-wave-light waves-effect" href="{!! action('Order\Controllers\Admin\OrderController@index') !!}" style="margin-left: 15px !important;">تمامی اطلاعات</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table class="uk-table uk-table-nowrap table_check">
                        <thead>
                        <tr>
                            <th class="uk-width-1-10  uk-text-center small_col">
                                {!! Form::checkbox('',1,null,['class' => 'check_all','data-md-icheck']) !!}
                            </th>
                            <th class="uk-width-2-10 uk-text-center">شماره</th>
                            <th class="uk-width-2-10 uk-text-center">کاربر</th>
                            <th class="uk-width-2-10 uk-text-center">شماره کاربر</th>
                            <th class="uk-width-2-10 uk-text-center">مبلغ کل</th>
                            <th class="uk-width-2-10 uk-text-center">روش پرداخت</th>
                            <th class="uk-width-2-10 uk-text-center">وضعیت</th>
                            <th class="uk-width-2-10 uk-text-center">نوع</th>
                            <th class="uk-width-2-10 uk-text-center">تاریخ</th>
                            <th class="uk-width-2-10 uk-text-center">.....</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($items as $item)
                            <tr class="uk-text-center">
                                <td class="uk-text-center uk-table-middle small_col">
                                    {!! Form::checkbox('deleteId[]',$item->id,null,['class' => 'check_row','data-md-icheck']) !!}
                                </td>
                                <td>
                                    {{$item->id}}
                                </td>
                                <td>
                                    <a target="_blank" href="{{action('Order\Controllers\Admin\OrderController@index',['user_id'=>$item->user_id])}}">{{ $item->user->name.' '.$item->user->family }}</a>
                                </td>
                                <td>{{ $item->user_id }}</td>
                                <td>
                                    {{ number_format($item->full_price) }}
                                </td>
                                <td>
                                    @if($item->payment_type == 'cash')
                                        نقدی
                                    @elseif($item->payment_type == 'online')
                                        آنلاین
                                    @elseif($item->payment_type == 'card')
                                        کارت به کارت
                                    @elseif($item->payment_type == 'account')
                                        واریز به حساب
                                    @endif
                                </td>
                                <td>
                                    <span class="uk-badge uk-badge-info">
                                    @if($item->delivered)
                                            @lang('site.orders.delivered')
                                        @elseif($item->ready_to_send)
                                            @lang('site.orders.ready_to_send')
                                        @elseif($item->processing_stock)
                                            @lang('site.orders.processing_stock')
                                        @elseif($item->in_production_procedure)
                                            @lang('site.orders.in_production_procedure')
                                        @elseif($item->ready_for_production)
                                            @lang('site.orders.ready_for_production')
                                        @elseif($item->paid)
                                            @lang('site.orders.paid')
                                        @elseif($item->confirm)
                                            @lang('site.orders.confirm')
                                        @endif

                                    </span>

                                </td>
                                <td>
                                    @if($item->type == 'custom')
                                        سفارش اختصاصی
                                    @elseif($item->type == 'order')
                                        سفارش معمولی
                                    @endif
                                </td>
                                <td>
                                    <span data-uk-tooltip title="{{ $item->created_at->diffForHumans() }}">{{ jdate('H:i - Y/m/d',$item->created_at->timestamp) }}</span>
                                </td>
                                <td class="uk-text-center">
                                    @if(permission('Order.OrderController.show'))
                                        <a href="{!! action('Order\Controllers\Admin\OrderController@show',$item->id) !!}"><i class="md-icon material-icons">visibility</i></a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="20" class="uk-text-center">اطلاعاتی برای نمایش وجود ندارد</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    {!! $items->appends(Request::except('page'))->render('views.vendor.pagination.uikit') !!}
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

@endsection