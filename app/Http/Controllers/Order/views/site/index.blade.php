@extends('views.layouts.site.master')
@section('title', trans('site.orders.orders'))
@section('content')
    <div class="red_box">
        <div class="container">
            <h1> @lang('site.orders.orders')  </h1>
            <div class="divider-title"></div>
            <p> @lang('site.orders.top_text') </p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container bread-crumbs">
                <span> @lang('site.home') </span>
                @if(App::isLocale('en'))
                    <span> <i class="flaticon-right-arrow"></i></span>
                @else
                    <span> <i class="flaticon-left-arrow-1"></i> </span>
                @endif
                <span> @lang('site.orders.orders') </span>
            </div>
        </div>

        <div class="profile-page">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 left-panel">
                        <div class="row">
                            @include('views.errors.errors')
                            <div class="col-xs-12 last-orders with-border">
                                <div class="tables-container border-bottom">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr class="grayish-text text-center">
                                            <td>@lang('site.orders.number')</td>
                                            <td>@lang('site.orders.discount') (@lang('site.currency'))</td>
                                            <td>@lang('site.carts.total_to_pay') (@lang('site.currency'))</td>
                                            <td>@lang('site.date')</td>
                                            <td>@lang('site.status')</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($orders as $order)
                                            <tr class="text-center">
                                                <td>{{ $order->id }}</td>
                                                <td>@if($order->discount > 0){{number_format($order->discount)}} @else @lang('site.not') @endif</td>
                                                <td>{{number_format($order->full_price)}}</td>
                                                <td>{{jdate('Y/m/d',$order->created_at->timestamp,'','','en')}}</td>
                                                <td>
                                                    @if($order->delivered)
                                                        <i class="fa fa-circle" aria-hidden="true"
                                                           style="padding-left: 0.5em; color:darkgreen">@lang('site.orders.delivered')</i>
                                                    @elseif($order->ready_to_send)
                                                        <i class="fa fa-circle" aria-hidden="true"
                                                           style="padding-left: 0.5em; color:darkgreen">  @lang('site.orders.ready_to_send')</i>
                                                    @elseif($order->processing_stock)
                                                        <i class="fa fa-circle" aria-hidden="true"
                                                           style="padding-left: 0.5em; color:darkgreen">  @lang('site.orders.processing_stock')</i>
                                                    @elseif($order->in_production_procedure)
                                                        <i class="fa fa-circle" aria-hidden="true"
                                                           style="padding-left: 0.5em; color:darkgreen"> @lang('site.orders.in_production_procedure')</i>
                                                    @elseif($order->paid)
                                                        <i class="fa fa-circle" aria-hidden="true"
                                                           style="padding-left: 0.5em; color:darkgreen">  @lang('site.orders.paid')</i>
                                                    @elseif($order->confirm)
                                                        <i class="fa fa-circle" aria-hidden="true"
                                                           style="padding-left: 0.5em; color:darkgreen">   @lang('site.orders.confirm')</i>
                                                    @endif
                                                    @if($order->paid == 0)
                                                        <a class="btn grayish-btn red_box"
                                                           href="{!! action('Order\Controllers\Site\OrderController@payment',[$order->id,$order->payment_type]) !!}">@lang('site.pay')</a>
                                                    @endif
                                                    @if($order->paid == 1 && $order->type == 'custom')
                                                        <a class="btn grayish-btn red_box"
                                                           href="{!! action('CustomOrder\Controllers\Site\OrderController@reorder',[$order->id]) !!}">@lang('site.custom_again')</a>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="" class="open-info">
                                                        <i class="fa fa-angle-up fa-2x "
                                                           aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr class="product-info @if(request('order') == $order->id) show-me @endif"
                                                style="display: none;">

                                                <td colspan="5">
                                                    <table class="table">
                                                        <thead>
                                                        <tr class="grayish-text text-center">
                                                            <td>@lang('site.carts.product_description')</td>
                                                            <td>@lang('site.quantity')</td>
                                                            @if($order->type == 'order')
                                                                <td>@lang('site.carts.unit_price')
                                                                    (@lang('site.currency'))
                                                                </td>
                                                            @endif
                                                            <td>@lang('site.carts.total_price') (@lang('site.currency')
                                                                )
                                                            </td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($order->items as $item)
                                                            @if(request('order') == $order->id)
                                                                @php
                                                                    if(!is_null($item->picture)){
                                                                        if (File::exists(public_path('uploads/tmp/'.$item->picture))) {
                                                                            File::move(public_path('uploads/tmp/'.$item->picture),public_path('uploads/order/'.$item->picture));
                                                                        }
                                                                    }
                                                                @endphp
                                                            @endif
                                                            <tr>
                                                                <td>
                                                                    @if($order->type == 'order')
                                                                        {{$item->product->title}}
                                                                    @elseif($order->type == 'custom')
                                                                        @lang('site.custom_order.custom_order')
                                                                    @endif
                                                                </td>
                                                                <td class="text-center">
                                                                    @if($order->type == 'order')
                                                                        {{ $item->quantity }}
                                                                    @elseif($order->type == 'custom')
                                                                        {{ $order->custom_order_count }}
                                                                    @endif
                                                                </td>
                                                                @if($order->type == 'order')
                                                                    <td class="text-center">{{ number_format($item->price) }}</td>
                                                                @endif
                                                                <td class="text-center">
                                                                    @if($order->type == 'order')
                                                                        {{ number_format($item->price * $item->quantity) }}
                                                                    @elseif($order->type == 'custom')
                                                                        {{ number_format($order->total_price) }}
                                                                    @endif
                                                                </td>
                                                            </tr>

                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div class="bought-progress">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-centered text-center">
                                                                <div class="bought-progress border-bottom" dir="rtl">
                                                                    <div class="progres">
                                                                        <div class="circle @if($order->confirm) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                            <span class="title">@lang('site.orders.confirm')</span>
                                                                        </div>
                                                                        <span class="bar @if($order->paid) done @endif">
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                        <div class="circle @if($order->paid) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                            <span class="title">@lang('site.orders.paid')</span>
                                                                        </div>
                                                                        @if($order->need_to_produce)
                                                                            <span class="bar @if($order->in_production_procedure) done @endif">
                                                                         <i class="fa fa-circle" aria-hidden="true"
                                                                            @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    </span>
                                                                            <div class="circle @if($order->in_production_procedure) done @endif">
                                                                        <span class="label"><span
                                                                                    class="fa fa-check"></span></span>
                                                                                <span class="title">@lang('site.orders.in_production_procedure')</span>
                                                                            </div>
                                                                        @endif
                                                                        <span class="bar @if($order->processing_stock) done @endif">
                                                                     <i class="fa fa-circle" aria-hidden="true"
                                                                        @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                        <div class="circle @if($order->processing_stock) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                            <span class="title">@lang('site.orders.processing_stock')</span>
                                                                        </div>
                                                                        <span class="bar @if($order->ready_to_send) done @endif">
                                                                     <i class="fa fa-circle" aria-hidden="true"
                                                                        @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                        <div class="circle @if($order->ready_to_send) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                            <span class="title">@lang('site.orders.ready_to_send')</span>
                                                                        </div>
                                                                        <span class="bar @if($order->delivered) done @endif">
                                                                     <i class="fa fa-circle" aria-hidden="true"
                                                                        @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                    <i class="fa fa-circle" aria-hidden="true"
                                                                       @if($order->paid) done @endif></i>
                                                                </span>
                                                                        <div class="circle @if($order->delivered) done @endif">
                                                                    <span class="label"><span
                                                                                class="fa fa-check"></span></span>
                                                                            <span class="title">@lang('site.orders.delivered')</span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="address-info ">
                                                        <div>
                                                            <span class="red_text">@lang('site.carts.shipping_type')
                                                                : </span>
                                                            <span>{{$order->address->shipping->title}}
                                                                ( @if($order->shipping_price > 0) {{number_format($order->shipping_price)}} @lang('site.currency') @else @lang('site.free') @endif )</span>
                                                        </div>
                                                        @if($order->tax_price > 0 && $order->tax > 0)
                                                            <div>
                                                                <span class="red_text"> @lang('site.carts.tax'): </span>
                                                                <span>{{ number_format($order->tax_price) }} @lang('site.currency')</span>
                                                            </div>
                                                        @endif
                                                        <div>
                                                            <span class="red_text">@lang('site.orders.get_address')
                                                                : </span>
                                                            <span>{{ $order->address->province->name.' - '.$order->address->city->name.' - '.$order->address->address.' - '.trans('site.postal_code').':'.$order->address->postal_code }}</span>
                                                        </div>
                                                        <div>
                                                            <span class="red_text">@lang('site.orders.transferee')
                                                                : </span>
                                                            <span>{{ $order->address->name_family }}</span>
                                                        </div>
                                                        @if(strlen($order->address->track_id) > 0)
                                                            <div>
                                                                <span class="red_text">@lang('site.orders.tracking_code')
                                                                    : </span>
                                                                <span>{{$order->address->track_id}}</span>
                                                            </div>
                                                        @endif
                                                        <div>
                                                            <span class="red_text">@lang('site.telephone'): </span>
                                                            <span>{{ $order->address->mobile.' - '.$order->address->tel }}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4" class="text-center">@lang('site.orders.is_empty')</td>
                                            </tr>
                                        @endforelse

                                        </tbody>
                                    </table>
                                </div>
                                <div class="paginate" style="text-align: center;">
                                    {!! $orders->links() !!}
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/order.css') }}
@endpush

@push('js')
    <script>
        $(document).ready(function () {
            $('.open-info').on('click', openNext);
        });

        function openNext(e) {
            e.preventDefault();
            $(this).find('.fa-angle-up').toggleClass('icon-rotate-180');
            $(e.target).closest('tr').next().slideToggle("slow");
            $(".test").slideToggle("slow");
        }
    </script>
@endpush

