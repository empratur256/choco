<?php
return [
    'order' => [
        'order'   => 20,
        'title'   => 'سفارش ها',
        'icon'    => 'fa fa-shopping-basket',
        'badge'   => function(){
            //return 1;
        },
        'submenu' => [
            'Index' => [
                'title'      => 'لیست کلی',
                'action'     => 'Order\Controllers\Admin\OrderController@index',
                'parameters' => [],
            ],
            'UnPai'                   => [
                'title'      => 'پرداخت نشده',
                'action'     => 'Order\Controllers\Admin\OrderController@index',
                'parameters' => [
                    'paid'                    => 0,
                    'processing_stock'        => 0,
                    'ready_to_send'           => 0,
                    'delivered'               => 0,
                    'in_production_procedure' => 0,
                ],
            ],
            'ready_for_production'    => [
                'title'      => 'پرداخت شده',
                'action'     => 'Order\Controllers\Admin\OrderController@index',
                'parameters' => [
                    'paid'                    => 1,
                    'processing_stock'        => 0,
                    'ready_to_send'           => 0,
                    'delivered'               => 0,
                    'in_production_procedure' => 0,
                ],
            ],
            'in_production_procedure' => [
                'title'      => 'در حال توليد',
                'action'     => 'Order\Controllers\Admin\OrderController@index',
                'parameters' => [
                    'paid'                    => 1,
                    'in_production_procedure' => 1,
                    'processing_stock'        => 0,
                    'ready_to_send'           => 0,
                    'delivered'               => 0,
                    'need_to_produce'         => 1,
                ],
            ],
            'processing_stock'        => [
                'title'      => 'پردازش انبار',
                'action'     => 'Order\Controllers\Admin\OrderController@index',
                'parameters' => [
                    'paid'             => 1,
                    'processing_stock' => 1,
                    'ready_to_send'    => 0,
                    'delivered'        => 0,
                ],
            ],
            'ready_to_send'           => [
                'title'      => 'در حال ارسال',
                'action'     => 'Order\Controllers\Admin\OrderController@index',
                'parameters' => [
                    'paid'             => 1,
                    'processing_stock' => 1,
                    'ready_to_send'    => 1,
                    'delivered'        => 0,
                ],
            ],
            'delivered'               => [
                'title'      => 'تحویل شده',
                'action'     => 'Order\Controllers\Admin\OrderController@index',
                'parameters' => [
                    'paid'             => 1,
                    'processing_stock' => 1,
                    'ready_to_send'    => 1,
                    'delivered'        => 1,
                ],
            ],
        ],
    ],
];
