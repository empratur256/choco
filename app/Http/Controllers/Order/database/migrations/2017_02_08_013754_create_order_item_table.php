<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function(Blueprint $table){
            $table->increments('id');
            $table->integer('order_id')->unsigned()->index();
            $table->integer('product_id')->nullable()->default(null)->unsigned()->index();
            $table->integer('size_id')->nullable()->default(null)->unsigned()->index();
            $table->integer('price')->default(0)->unsigned()->index();
            $table->integer('discount')->unsigned()->default(0);
            $table->integer('quantity')->unsigned()->index();
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('order')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('size_id')->references('id')->on('size')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
