<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductFactory extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('order_product_factory', function(Blueprint $table){
            $table->increments('id');
            $table->integer('factory_id')->unsigned()->index();
            $table->integer('order_id')->unsigned()->index();
            $table->integer('product_id')->nullable()->unsigned()->index();
            $table->foreign('order_id')->references('id')->on('order')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('factory_id')->references('id')->on('factory')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('product')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_factory');
    }
}
