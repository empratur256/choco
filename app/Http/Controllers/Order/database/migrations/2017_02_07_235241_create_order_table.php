<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('order', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('tax', 5)->default(0);
            $table->integer('tax_price')->unsigned()->default(0)->index();
            $table->integer('discount')->unsigned()->default(0);
            $table->integer('total_price')->unsigned()->default(0);
            $table->integer('shipping_price')->unsigned()->default(0);
            $table->enum('payment_type', [
                'cash',
                'online',
                'card',
                'account',
            ])->index();
            $table->tinyInteger('confirm')->default(1)->index();
            $table->tinyInteger('invoice_request')->default(0)->index();
            $table->tinyInteger('paid')->default(0)->index();
            $table->tinyInteger('processing_stock')->default(0)->index();
            $table->tinyInteger('ready_to_send')->default(0)->index();
            $table->tinyInteger('delivered')->default(0)->index();
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('deleted_at');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
