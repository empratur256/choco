<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreCustomOrderFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->unsignedTinyInteger('valid')->default(0);
            $table->unsignedInteger('layout_id')->nullable()->default(null);
            $table->unsignedInteger('custom_order_count')->nullable()->default(null);
            $table->unsignedInteger('amount_id')->nullable()->default(null);
            $table->unsignedInteger('routine_id')->nullable()->default(null);
            $table->unsignedInteger('package_id')->nullable()->default(null);
            $table->string('package_picture')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('valid');
            $table->dropColumn('layout_id');
            $table->dropColumn('custom_order_count');
            $table->dropColumn('package_picture');
            $table->dropColumn('amount_id');
            $table->dropColumn('routine_id');
            $table->dropColumn('package_id');
        });
    }
}
