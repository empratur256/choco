<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNeedToProduceFieldsOrder extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('order', function(Blueprint $table){
            $table->tinyInteger('ready_for_production')->default(0)->index();
            $table->tinyInteger('in_production_procedure')->default(0)->index();
            $table->tinyInteger('need_to_produce')->default(0)->index();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
