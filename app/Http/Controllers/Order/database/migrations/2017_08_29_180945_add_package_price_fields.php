<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPackagePriceFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->unsignedInteger('package_price_without_picture')->nullable()->default(null);
            $table->unsignedInteger('package_total_price')->nullable()->default(null);
            $table->unsignedTinyInteger('package_custom_design')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('package_price_without_picture');
            $table->dropColumn('package_total_price');
            $table->dropColumn('package_custom_design');
        });
    }
}
