<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomOrderItemFields extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('order_item', function(Blueprint $table){
            $table->float('width', 8, 0)->nullable()->default(null)->unsigned()->index();
            $table->float('height', 8, 0)->nullable()->default(null)->unsigned()->index();
            $table->string('picture')->nullable()->default(null);
            $table->integer('shoulder_id')->nullable()->default(null)->unsigned()->index();
            $table->foreign('shoulder_id')->references('id')->on('shoulder')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
