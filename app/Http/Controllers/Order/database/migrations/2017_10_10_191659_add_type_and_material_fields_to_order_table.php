<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeAndMaterialFieldsToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->unsignedInteger('custom_product_type_id')->nullable()->defualt(null);
            $table->unsignedInteger('custom_product_material_id')->nullable()->defualt(null);
            $table->unsignedTinyInteger('custom_product_has_brain')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('custom_product_type_id');
            $table->dropColumn('custom_product_material_id');
            $table->dropColumn('custom_product_has_brain');
        });
    }
}
