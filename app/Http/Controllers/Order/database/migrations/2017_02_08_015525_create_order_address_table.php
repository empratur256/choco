<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAddressTable extends Migration
{

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('order_address', function(Blueprint $table){
            $table->increments('id');
            $table->integer('order_id')->unsigned()->index();
            $table->integer('shipping_id')->unsigned()->index();
            $table->integer('province_id')->unsigned()->index();
            $table->integer('city_id')->unsigned()->index();
            $table->string('track_id')->nullable()->default(null)->index();
            $table->string('postal_code')->index();
            $table->string('name_family');
            $table->string('mobile');
            $table->string('tel');
            $table->text('address');
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('order')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('shipping_id')->references('id')->on('transport')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('province_id')->references('id')->on('city')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('city')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_address');
    }
}
