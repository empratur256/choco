<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomOrderCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_order_cells', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cell_number')->nullable()->default(null);
            $table->unsignedInteger('order_id')->index();
            $table->unsignedInteger('base_flavor_id')->nullable()->default(null);
            $table->unsignedInteger('flavor_id')->nullable()->default(null);
            $table->unsignedInteger('brain_id')->nullable()->default(null);
            $table->unsignedInteger('weight_id')->nullable()->default(null);
            $table->unsignedInteger('shape_id')->nullable()->default(null);
            $table->string('picture')->nullable()->default(null);
            $table->string('picture_thumb')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_order_cells');
    }
}
