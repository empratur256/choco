<?php

namespace Sedehi\Http\Controllers\Order\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Sedehi\Http\Controllers\Order\Models\Order;

class OrderRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        /*
        if (auth()->user()->hasPermission('order.onlybyuser')) {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch ($action) {
            case 'destroy':
                return Order::where('author_id',auth()->user()->id)->whereIn('id', $this->request->get('deleteId'))->count();
                break;
            case 'update':
                return Order::where('author_id',auth()->user()->id)->find($this->route()->parameter('order'));
                break;
        }
        }
        */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $action = explode('@', $this->route()->getActionName());
        $action = end($action);
        switch($action){
            case 'destroy':
            case 'restore':
                return [
                    'deleteId' => 'required|array',
                ];
            break;
            case 'store':
                return [
                    'title' => 'required',
                ];
            break;
            case 'update':
                return [
                    'title' => 'required',
                ];
            break;
            case 'track':
                return [
                    'track_id' => 'required',
                ];
            break;
        }

        return [];
    }

}
