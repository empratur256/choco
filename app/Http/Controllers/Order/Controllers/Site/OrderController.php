<?php

namespace Sedehi\Http\Controllers\Order\Controllers\Site;

use Illuminate\Http\Request;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Payment\Models\BankAccount;
use Sedehi\Payment\Facades\Payment as BankPayment;

class OrderController extends Controller
{

    public function index()
    {
        $orders = auth()->user()->orders()->with([
            'address' => function ($query) {
                $query->with([
                    'shipping' => function ($query) {
                        $query->withTrashed();
                    },
                    'city' => function ($query) {
                        $query->withTrashed();
                    },
                    'province' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'items' => function ($query) {
                $query->with([
                    'product' => function ($query) {
                        $query->withTrashed();
                    }
                ]);
            },
        ])->latest()->paginate(10);

        return view('Order.views.site.index', compact('orders'));
    }

    public function payment($id)
    {
        $order = auth()->user()->orders()->findOrFail($id);
        if ($order->paid == 1) {
            return redirect()->back()->with('info', trans('site.success_payment'));
        }
        if (request()->exists('online')) {
            return BankPayment::zarinpal()->callBackUrl(action('Payment\Controllers\Site\PaymentController@verify'))
                ->amount($order->full_price)->data([
                    'user_id' => auth()->user()->id,
                    'order_id' => $order->id,
                ])->description(@trans('site.orders.paid') . $order->id)->request();
        }
        $accounts = BankAccount::language()->get();

        return view('Order.views.site.credit', compact('accounts', 'order'));
    }

}
