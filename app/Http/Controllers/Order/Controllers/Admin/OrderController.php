<?php

namespace Sedehi\Http\Controllers\Order\Controllers\Admin;

use Carbon\Carbon;
use DB;
use Exception;
use Log;
use Sedehi\Http\Controllers\Coupon\Models\Code;
use Sedehi\Http\Controllers\Coupon\Models\PresentedCouponCode;
use Sedehi\Http\Controllers\Factory\Models\Factory;
use Sedehi\Http\Controllers\Order\Models\Address;
use Sedehi\Http\Controllers\Order\Models\OrderFactory;
use Sedehi\Http\Controllers\Order\Models\OrderWarehouse;
use Sedehi\Http\Controllers\Order\Models\StatusLog;
use Sedehi\Http\Controllers\Warehouse\Models\Warehouse;
use Sedehi\Http\Requests;
use Sedehi\Http\Controllers\Controller;
use Sedehi\Http\Controllers\Order\Models\Order;
use Sedehi\Http\Controllers\Order\Requests\Admin\OrderRequest;
use Illuminate\Http\Request;
use Validator;
use Sedehi\Http\Controllers\Order\Models\Cells;

class OrderController extends Controller
{

    public function index()
    {
        $orderTypes = [];
        if (permission('order.OrderController.custom')) {
            $orderTypes[] = 'custom';
        }
        if (permission('order.OrderController.order')) {
            $orderTypes[] = 'order';
        }
        $items = Order::whereIn('type', $orderTypes)->with([
            'user' => function ($query) {
                $query->withTrashed();
            },
        ])->filter()->valid()->latest()->paginate(20);

        return view('Order.views.admin.order.index', compact('items'));
    }

    public function show($id)
    {
        $item = Order::withTrashed()->with([
            'cells' => function ($query) {
                $query->withTrashed()->with([
                    'flavor' => function ($query) {
                        $query->withTrashed();
                    },
                    'brain' => function ($query) {
                        $query->withTrashed();
                    },
                    'baseFlavor' => function ($query) {
                        $query->withTrashed();
                    },
                    'weight' => function ($query) {
                        $query->withTrashed();
                    },
                    'shape' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'couponCode' => function ($query) {
                $query->with([
                    'coupon' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'layout' => function ($query) {
                $query->withTrashed();
            },
            'amount' => function ($query) {
                $query->withTrashed();
            },
            'routine' => function ($query) {
                $query->withTrashed()->with([
                    'types' => function ($query) {
                        $query->withTrashed();
                    },
                    'materials' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'package' => function ($query) {
                $query->withTrashed()->with([
                    'category' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'settlements' => function ($query) {
                $query->with([
                    'account' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'payments' => function ($query) {
                $query->where('status', 1);
            },
            'items' => function ($query) {
                $query->with([
                    'product' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'user' => function ($query) {
                $query->withTrashed();
            },
            'address' => function ($query) {
                $query->with([
                    'province' => function ($query) {
                        $query->withTrashed();
                    },
                    'city' => function ($query) {
                        $query->withTrashed();
                    },
                    'shipping' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'statusLog' => function ($query) {
                $query->with([
                    'user' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'producer' => function ($query) {
                $query->with([
                    'product' => function ($query) {
                        $query->withTrashed();
                    },
                    'factory' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'warehouse' => function ($query) {
                $query->with([
                    'product' => function ($query) {
                        $query->withTrashed();
                    },
                    'warehouse' => function ($query) {
                        $query->withTrashed();
                    },
                ]);
            },
            'customType' => function ($query) {
                $query->withTrashed();
            },
            'customMaterial' => function ($query) {
                $query->withTrashed();
            }
        ])->findOrFail($id);
        $paidDisabled = ['disabled' => 'disabled'];
        $inProductionProcedureDisabled = ['disabled' => 'disabled'];
        $processingStockDisabled = ['disabled' => 'disabled'];
        $readyToSendDisabled = ['disabled' => 'disabled'];
        $deliveredDisabled = ['disabled' => 'disabled'];
        if (permission('order.OrderController.change_paid')) {
            $paidDisabled = [];
        }
        if ($item->in_production_procedure == 0 || permission('order.OrderController.change_in_production_procedure')) {
            $inProductionProcedureDisabled = [];
        }
        if ($item->processing_stock == 0 || permission('order.OrderController.change_processing_stock')) {
            $processingStockDisabled = [];
        }
        if ($item->ready_to_send == 0 || permission('order.OrderController.change_ready_to_send')) {
            $readyToSendDisabled = [];
        }
        if ($item->delivered == 0 || permission('order.OrderController.change_delivered')) {
            $deliveredDisabled = [];
        }
        $factories = Factory::pluck('title', 'id');
        $warehouses = Warehouse::pluck('title', 'id');

        return view('Order.views.admin.order.show', compact('item', 'paidDisabled', 'inProductionProcedureDisabled', 'readyToSendDisabled', 'processingStockDisabled', 'deliveredDisabled', 'factories', 'warehouses'));
    }

    public function destroy(OrderRequest $request)
    {
        DB::beginTransaction();
        try {
            $orders = Order::whereIn('id', $request->get('deleteId'))->get();
            foreach ($orders as $order) {
                foreach ($order->items as $item) {
                    $product = $item->product;
                    if (is_null($item->size_id)) {
                        if (!is_null($product)) {
                            $product->quantity += $item->quantity;
                            if ($product->order_count > 0) {
                                $product->order_count -= 1;
                            } else {
                                $product->order_count = 0;
                            }
                            $product->save();
                        }
                    } else {
                        if (!is_null($product)) {
                            $product->quantity += $item->quantity;
                            if ($product->order_count > 0) {
                                $product->order_count -= 1;
                            } else {
                                $product->order_count = 0;
                            }
                            $product->save();
                            $price = $product->prices()->first();
                            if (!is_null($price)) {
                                $price->quantity += $item->quantity;
                                $price->save();
                            }
                        }
                    }
                }
            }
            PresentedCouponCode::whereIn('order_id', $request->get('deleteId'))->update([
                'used_at' => null,
                'user_used_id' => null,
                'order_id' => null,
            ]);
            Code::whereIn('order_id', $request->get('deleteId'))->update([
                'used_at' => null,
                'user_id' => null,
                'order_id' => null,
            ]);
            Order::whereIn('id', $request->get('deleteId'))->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar delete sefaresh ' . $e);

            return redirect()->back()->with('error', 'مشکلی در حذف سفارش');
        }

        return redirect()->back()->with('success', 'اطلاعات با موفقیت حذف شد');
    }

    public function restore(OrderRequest $request)
    {
        DB::beginTransaction();
        try {
            $orders = Order::withTrashed()->whereIn('id', $request->get('deleteId'))->get();
            foreach ($orders as $order) {
                if (!is_null($order->deleted_at)) {
                    foreach ($order->items as $item) {
                        $product = $item->product;
                        if (is_null($product)) {
                            return redirect()->back()
                                ->with('error', 'محصول مورد سفارش کاربر در سایت وجود ندارد و امکان بازیابی سفارش نیست');
                        }
                        if (is_null($item->size_id)) {
                            if ($product->quantity < $item->quantity) {
                                return redirect()->back()
                                    ->with('error', 'محصول ' . $product->title . ' موجودی ندارد و امکان بازیابی سفارش نمی باشد');
                            }
                            $product->quantity -= $item->quantity;
                            $product->order_count += 1;
                            $product->save();
                        } else {
                            if ($product->quantity < $item->quantity) {
                                return redirect()->back()
                                    ->with('error', 'محصول ' . $product->title . ' موجودی ندارد و امکان بازیابی سفارش نمی باشد');
                            }
                            $product->quantity -= $item->quantity;
                            $product->order_count += 1;
                            $product->save();
                            $price = $product->prices()->first();
                            if (!is_null($price)) {
                                if ($price->quantity < $item->quantity) {
                                    return redirect()->back()
                                        ->with('error', 'محصول ' . $product->title . ' موجودی ندارد و امکان بازیابی سفارش نمی باشد');
                                }
                                $price->quantity -= $item->quantity;
                                $price->save();
                            }
                        }
                    }
                }
            }
            Order::withTrashed()->whereIn('id', $request->get('deleteId'))->restore();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar restore mahsol ' . $e);

            return redirect()->back()->with('error', 'مشکلی در بازیابی سفارش');
        }

        return redirect()->back()->with('success', 'اطلاعات با موفقیت بازیابی شد');
    }

    public function track(OrderRequest $request, $orderId, $id)
    {
        $address = Address::where('order_id', $orderId)->findOrFail($id);
        $address->track_id = $request->get('track_id');
        $address->save();

        return redirect()->back()->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function status(OrderRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $status = '';
            $checked = 0;
            $item = Order::withTrashed()->findOrFail($id);
            if ($request->has('paid')) {
                if (permission('order.OrderController.change_paid')) {
                    $item->paid = ($request->get('paid')) ? 1 : 0;
                }
            } elseif ($request->has('processing_stock')) {
                $item->processing_stock = ($request->get('processing_stock')) ? 1 : 0;
            } elseif ($request->has('ready_to_send')) {
                $item->ready_to_send = ($request->get('ready_to_send')) ? 1 : 0;
            } elseif ($request->has('delivered')) {
                $item->delivered = ($request->get('delivered')) ? 1 : 0;
            } elseif ($request->has('ready_for_production')) {
                $item->ready_for_production = ($request->get('ready_for_production')) ? 1 : 0;
            } elseif ($request->has('in_production_procedure')) {
                $item->in_production_procedure = ($request->get('in_production_procedure')) ? 1 : 0;
            }
            if ($item->isDirty('paid')) {
                $status = 'paid';
                $checked = ($request->get('paid')) ? 1 : 0;
            } elseif ($item->isDirty('processing_stock')) {
                $status = 'processing_stock';
                $checked = ($request->get('processing_stock')) ? 1 : 0;
            } elseif ($item->isDirty('ready_to_send')) {
                $status = 'ready_to_send';
                $checked = ($request->get('ready_to_send')) ? 1 : 0;
            } elseif ($item->isDirty('delivered')) {
                $status = 'delivered';
                $checked = ($request->get('delivered')) ? 1 : 0;
            } elseif ($item->isDirty('ready_for_production')) {
                $status = 'ready_for_production';
                $checked = ($request->get('ready_for_production')) ? 1 : 0;
            } elseif ($item->isDirty('in_production_procedure')) {
                $status = 'in_production_procedure';
                $checked = ($request->get('in_production_procedure')) ? 1 : 0;
            }
            $item->save();
            if ($request->has('in_production_procedure')) {
                if ($request->get('in_production_procedure') == 1) {
                    $inProductionProcedureValidator = Validator::make(request()->all(), [
                        'factory.*.id' => 'required',
                        'factory.*.number' => 'required',
                        'factory.*.date' => 'required',
                    ]);
                    if ($inProductionProcedureValidator->fails()) {
                        $request->merge(['confirm' => 1]);
                        if ($item->paid) {
                            $request->merge(['paid' => 1]);
                        }
                        if ($item->ready_to_send) {
                            $request->merge(['ready_to_send' => 1]);
                        }
                        if ($item->delivered) {
                            $request->merge(['delivered' => 1]);
                        }

                        return redirect()->back()->withInput($request->all())
                            ->with('in_production_procedure_modal', true)
                            ->with('error', 'لطفا اطلاعات در حال تولید را تکمیل کنید');
                    }
                    OrderFactory::where('order_id', $item->id)->delete();
                    foreach ($request->get('factory') as $product_id => $factoryForm) {
                        $factory = new OrderFactory();
                        $factory->order_id = $item->id;
                        if ($product_id > 0) {
                            $factory->product_id = $product_id;
                        } else {
                            $factory->product_id = null;
                        }
                        $factory->factory_id = $factoryForm['id'];
                        $factory->number = $factoryForm['number'];
                        $date = explode('-', $factoryForm['date']);
                        if (count($date) != 3) {
                            $request->merge(['confirm' => 1]);
                            if ($item->paid) {
                                $request->merge(['paid' => 1]);
                            }
                            if ($item->ready_to_send) {
                                $request->merge(['ready_to_send' => 1]);
                            }
                            if ($item->delivered) {
                                $request->merge(['delivered' => 1]);
                            }

                            return redirect()->back()->withInput($request->all())
                                ->with('in_production_procedure_modal', true)
                                ->with('error', 'لطفا تاریخ اطلاعات درحال تولید را تکمیل کنید');
                        }
                        $date = jmktime(0, 0, 0, $date[1], $date[2], $date[0]);
                        $factory->date = Carbon::createFromTimestamp($date);
                        $factory->save();
                    }
                } elseif ($request->get('in_production_procedure') == 0) {
                    if (permission('order.OrderController.change_in_production_procedure')) {
                        OrderFactory::where('order_id', $item->id)->delete();
                    }
                }
            }
            if ($request->has('processing_stock')) {
                if ($request->get('processing_stock') == 1) {
                    $processingStockValidator = Validator::make(request()->all(), [
                        'warehouse.*.id' => 'required',
                        'warehouse.*.number' => 'required',
                        'warehouse.*.date' => 'required',
                    ]);
                    if ($processingStockValidator->fails()) {
                        $request->merge(['confirm' => 1]);
                        if ($item->paid) {
                            $request->merge(['paid' => 1]);
                        }
                        if ($item->ready_to_send) {
                            $request->merge(['ready_to_send' => 1]);
                        }
                        if ($item->delivered) {
                            $request->merge(['delivered' => 1]);
                        }
                        if ($item->in_production_procedure) {
                            $request->merge(['in_production_procedure' => 1]);
                        }

                        return redirect()->back()->withInput($request->all())->with('processing_stock_modal', true)
                            ->with('error', 'لطفا اطلاعات پردازش انبار را تکمیل کنید');
                    }
                    OrderWarehouse::where('order_id', $item->id)->delete();
                    foreach ($request->get('warehouse') as $product_id => $warehouseForm) {
                        $warehouse = new OrderWarehouse();
                        $warehouse->order_id = $item->id;
                        if ($product_id > 0) {
                            $warehouse->product_id = $product_id;
                        } else {
                            $warehouse->product_id = null;
                        }
                        $warehouse->warehouse_id = $warehouseForm['id'];
                        $warehouse->number = $warehouseForm['number'];
                        $date = explode('-', $warehouseForm['date']);
                        if (count($date) != 3) {
                            $request->merge(['confirm' => 1]);
                            if ($item->paid) {
                                $request->merge(['paid' => 1]);
                            }
                            if ($item->ready_to_send) {
                                $request->merge(['ready_to_send' => 1]);
                            }
                            if ($item->delivered) {
                                $request->merge(['delivered' => 1]);
                            }
                            if ($item->in_production_procedure) {
                                $request->merge(['in_production_procedure' => 1]);
                            }

                            return redirect()->back()->withInput($request->all())->with('processing_stock_modal', true)
                                ->with('error', 'لطفا تاریخ اطلاعات پردازش انبار را تکمیل کنید');
                        }
                        $date = jmktime(0, 0, 0, $date[1], $date[2], $date[0]);
                        $warehouse->date = Carbon::createFromTimestamp($date);
                        $warehouse->save();
                    }
                } elseif ($request->get('processing_stock') == 0) {
                    if (permission('order.OrderController.change_processing_stock')) {
                        OrderWarehouse::where('order_id', $item->id)->delete();
                    }
                }
            }
            if (strlen($status) > 2) {
                $log = new StatusLog();
                $log->order_id = $item->id;
                $log->user_id = auth()->user()->id;
                $log->status = $status;
                $log->checked = $checked;
                $log->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::alert('moshkel dar change status order ' . $e);

            return redirect()->back()->with('error', 'مشکل در تغییر وضعیت');
        }

        return redirect()->back()->with('success', 'اطلاعات با موفقیت ذخیره شد');
    }

    public function downloadCellPicture($orderId, $cellNumber)
    {
        $order = Order::with('cells')->withTrashed()->where('type', 'custom')->findOrFail($orderId);

        $cell = $order->cells->where('cell_number',$cellNumber)->first();

        return response()->download(public_path('uploads/order/'.$order->id.'/'.$cell->picture));
    }

    public function downloadPackagePicture($orderId)
    {
        $order = Order::withTrashed()->where('type', 'custom')->findOrFail($orderId);

        return response()->download(public_path('uploads/order/'.$order->id.'/'.$order->package_picture));
    }
}
