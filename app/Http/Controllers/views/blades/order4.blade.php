@extends('views.layouts.site.master')
@section('title','لیست شکلات ها')
@section('content')
    <div class="red_box">
        <div class="container">
            <span> شکلات بار  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
    <div class="bg-color">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div style="padding-top: 1em;padding-bottom: 1em">
                        <div class="container">
                            <span> خانه </span>
                            <span> &gt; </span>
                            <span> سبد خرید </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center"
                     style="padding-top: 7em;padding-bottom: 3em">
                    <div class="row order-status">
                        <div class="col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">اطلاعات پرداخت</p>
                                </div>
                                <div class="col-xs-4 padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">تایید سفارش</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">پردازش انبار</p>
                                </div>
                                <div class="col-sm-4 col-xs-12  padding-top-2 padding-lr-0">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-12">

                            <div class="row">

                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">پرداخت</p>
                                </div>
                                <div class="col-xs-4  padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                    <p class="green_text">تایید سفارش</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 bg-white">
                            <div class="col-xs-12 padding-top-3 padding-bottom-3">
                                <img class="status-pic center-block img-responsive"
                                     src="../assets/site/img/order-step1.png">
                            </div>
                            <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3">
                                <p class="text-default" style="font-weight:bolder;font-size: 15px;">لورم ایپسوم متن
                                    ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                                    و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                                    تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. </p>
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-push-3 col-xs-12"
                                         style="border: 1px solid #ebebeb;padding: 1em;">
                                        <div class="row">
                                            <div class="col-xs-6 pull-right">
                                                <p>لطفا تغداد سینی رو انتخاب کنید</p>
                                            </div>
                                            <div class="col-xs-6" style="text-align: left">
                                                <label class="checkbox-inline">
                                                    <label>
                                                        <input style="margin-left: 1em" type="radio" name="optradio">
                                                        <text class="red_text">تک سینی</text>
                                                    </label>
                                                </label>
                                                <label class="checkbox-inline">
                                                    <label>
                                                        <input style="margin-left: 1em" type="radio" name="optradio">
                                                        <span class="red_text">دو سینی</span>
                                                    </label>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 space-2-50">
                                <div class="row">
                                    <div class="col-lg-12 product-tabs">
                                        <!-- Nav tabs -->
                                        <div class="card">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#home"
                                                                                          aria-controls="home"
                                                                                          role="tab" data-toggle="tab">پرطرفدار</a>
                                                </li>
                                                <li role="presentation"><a href="#profile" aria-controls="profile"
                                                                           role="tab" data-toggle="tab">ویژه</a></li>
                                                <li role="presentation"><a href="#messages" aria-controls="messages"
                                                                           role="tab" data-toggle="tab">لاکچری</a></li>

                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="home">می باشد.
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="profile">می باشد.
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                                    <div class="product-chooser ">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">

                                                                <div class="product-chooser-item">
                                                                    <img src="../assets/site/img/order-step2.png"
                                                                         class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12"
                                                                         alt="Mobile">
                                                                    <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                                                        <span class="title">عنوان لایه</span>
                                                                        <span class="description">توضیحات لایه</span>
                                                                        <input type="radio" name="product"
                                                                              class="option-input radio" value="mobile">
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/orders.css') }}
    {{ Html::style('assets/site/css/order4.css') }}
@endpush

@push('js')
    <script>
        $(function () {
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
                $(this).parent().find('div.product-chooser-item').removeClass('selected');
                $(this).addClass('selected');
                $(this).find('input[type="radio"]').prop("checked", true);

            });
        });
    </script>

@endpush
@push("css")
    <style>
        .custom-radios div {
            display: inline-block;
        }
        .custom-radios input[type="radio"] {
            display: none;
        }
        .custom-radios input[type="radio"] + label {
            color: #333;
            font-family: Arial, sans-serif;
            font-size: 14px;
        }
        .custom-radios input[type="radio"] + label span {
            display: inline-block;
            width: 40px;
            height: 40px;
            margin: -1px 4px 0 0;
            vertical-align: middle;
            cursor: pointer;
            border-radius: 50%;
            border: 2px solid #FFFFFF;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
            background-repeat: no-repeat;
            background-position: center;
            text-align: center;
            line-height: 44px;
        }
        .custom-radios input[type="radio"] + label span img {
            opacity: 0;
            transition: all .3s ease;
        }
        .custom-radios input[type="radio"]#color-1 + label span {
            background-color: #2ecc71;
        }
        .custom-radios input[type="radio"]#color-2 + label span {
            background-color: #3498db;
        }
        .custom-radios input[type="radio"]#color-3 + label span {
            background-color: #f1c40f;
        }
        .custom-radios input[type="radio"]#color-4 + label span {
            background-color: #e74c3c;
        }
        .custom-radios input[type="radio"]:checked + label span img {
            opacity: 1;
        }


    </style>
@endpush