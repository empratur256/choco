@extends('views.layouts.site.master')
@section('title','لیست شکلات ها')
@section('content')

    <div class="red_box">
        <div class="container">
            <span> شکلات بار  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
    <div class="bg-color">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div style="padding-top: 1em;padding-bottom: 1em">
                        <div class="container">
                            <span> خانه </span>
                            <span> &gt; </span>
                            <span> سبد خرید </span>
                        </div>
                    </div>
                </div>
                <img src="" class="imagepreview222222 test" style="width: 100%;">

                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center"
                     style="padding-top: 2em;padding-bottom: 2em">
                    <div class="row order-status">
                        <div class="col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">اطلاعات پرداخت</p>
                                </div>
                                <div class="col-xs-4 padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">تایید سفارش</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">پردازش انبار</p>
                                </div>
                                <div class="col-sm-4 col-xs-12  padding-top-2 padding-lr-0">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-12">

                            <div class="row">

                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                    <p class="">پرداخت</p>
                                </div>
                                <div class="col-xs-4  padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                    <p class="green_text">تایید سفارش</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 bg-white">
                    <div class="row">
                        <div class="col-xs-12 bg-white">
                            <div class="col-xs-12 padding-top-3 padding-bottom-3">
                                <img class="status-pic center-block img-responsive"
                                     src="../assets/site/img/order-step2.png">
                            </div>
                            <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3">
                                <p class="text-default" style="font-weight:bolder;font-size: 15px;">لورم ایپسوم متن
                                    ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                                    و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                                    تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                </p>
                            </div>


                        </div><!-- end inner row -->

                        <div class="row product-chooser">
                            <div class="col-md-12">
                                <div class="col-md-12 space-2-50">
                                    <h3 class="text-center">لایه انتخاب شده</h3>
                                </div>
                                {!! Form::open(['url' => '']) !!}
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="product-chooser-item selected">
                                        <img src="../assets/site/img/order-step2.png" class="img-rounded col-xs-4 col-sm-6 col-md-6 col-lg-6"
                                             alt="Mobile and Desktop">
                                        <div class="col-xs-8 col-sm-8 col-md-6 col-lg-6">
                                            <span class="title">عنوان لایه</span>
                                            <span class="description">توضیحات لایه</span>
                                            <input type="radio" name="product" value="mobile_desktop">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 space-2-50">
                                    <h3 class="text-center">انتخاب لایه</h3>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-2 col-md-offset-1 col-lg-2">
                                    <div class="product-chooser-item">
                                        <img src=""
                                             class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12"
                                             alt="Mobile and Desktop">
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <span class="title">عنوان لایه</span>
                                            <span class="description">توضیحات لایه</span>
                                            <input type="radio" name="product" value="mobile_desktop">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="product-chooser-item">
                                        <img src="../assets/site/img/order-step2.png"
                                             class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Desktop">
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <span class="title">عنوان لایه</span>
                                            <span class="description">توضیحات لایه</span>
                                            <input type="radio" name="product" value="desktop">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="product-chooser-item">
                                        <img src="../assets/site/img/order-step2.png"
                                             class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile">
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <span class="title">عنوان لایه</span>
                                            <span class="description">توضیحات لایه</span>
                                            <input type="radio" name="product" value="mobile">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                    <div class="product-chooser-item ">
                                        <img src="../assets/site/img/order-step2.png"
                                             class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile">
                                        <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                            <span class="title">عنوان لایه</span>
                                            <span class="description">توضیحات لایه</span>
                                            <input type="radio" name="product" value="mobile">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <ul class="pagination pagination-split pagination-sm ">
                                            <li class="disabled"><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a>
                                            </li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12 space-2-50">
                                    <button type="submit" class="btn submit pull-right"> <i class="fa fa-arrow-right"> </i> مرحله قبلی </button>
                                    <button type="submit" class="btn submit pull-left" >  مرحله بعدی <i class="fa fa-arrow-left"> </i></button>

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                ​<!-- end outer row -->
            </div>
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>







@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/orders.css') }}
    {{ Html::style('assets/site/css/order2.css') }}
    {{ Html::style('assets/site/css/component.css') }}
@endpush

@push('js')
    <script type="text/javascript">
        $(function () {
            $('.full-sise').on('click', function () {
                EmptyImgClass = $(this.classList)[2];
                EmptyImgClass = "" + "." + EmptyImgClass;
                EmptyImg = ($(EmptyImgClass).find('.test')).prevObject[1];
                console.log(($(this).nextAll()).find('.t'));
                //console.log($(this.classList)[2]);
                //console.log(t);
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                $('#imagemodal').modal('show');
            });
        });
        $(function () {
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
                $(this).parent().parent().find('div.product-chooser-item').removeClass('selected');
                $(this).addClass('selected');
                $(this).find('input[type="radio"]').prop("checked", true);

            });
        });
    </script>
@endpush