@extends('CustomOrder.views.site.container')
@section('title','لیست شکلات ها')
@section('content')

    <div class="red_box">
        <div class="container">
            <span> شکلات بار  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
    <div class="bg-color">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div style="padding-top: 1em;padding-bottom: 1em">
                        <div class="container">
                            <span> خانه </span>
                            <span> &gt; </span>
                            <span> سبد خرید </span>
                        </div>
                    </div>
                </div>
                <img src="" class="imagepreview222222 test" style="width: 100%;">

                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center"
                     style="padding-top: 2em;padding-bottom: 2em">
                    <div class="row order-status">
                        <div class="col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">اطلاعات پرداخت</p>
                                </div>
                                <div class="col-xs-4 padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">تایید سفارش</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-xs-4 padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                    <img src="../assets/site/img/circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                    <p class="">پردازش انبار</p>
                                </div>
                                <div class="col-sm-4 col-xs-12  padding-top-2 padding-lr-0">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                    <img class="hidden-xs" src="../assets/site/img/circle.png">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-12">

                            <div class="row">

                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                    <p class="">پرداخت</p>
                                </div>
                                <div class="col-xs-4  padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                </div>
                                <div class="col-xs-4 padding-lr-0">
                                    <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                    <p class="green_text">تایید سفارش</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 bg-white">
                    <div class="row">
                        <div class="container">
                            <div class="col-xs-12 bg-white">
                                <div class="col-xs-12 padding-top-3 padding-bottom-3">
                                    <img class="status-pic center-block img-responsive"
                                         src="../assets/site/img/order-step2.png">
                                </div>
                                <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3">
                                    <p class="text-default" style="font-weight:bolder;font-size: 15px;">لورم ایپسوم متن
                                        ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.
                                        چاپگرها
                                        و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی
                                        تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                    </p>
                                </div>
                                <div class="space-2-30"></div>
                                <div class="divider divider-light space-2-30"></div>



                            </div><!-- end inner row -->

                            <div class="row ">

                                <div class="col-md-12 space-2-30">
                                    <h3 class="text-dark padded-bottom"><i class="fa fa-arrow-circle-o-left"></i> تعداد سفارش </h3>
                                    <div class="divider divider-light"></div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="col-md-6 pull-right">
                                        <div class="cells">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12 pull-right"><span class="text-dark"> تعداد سینی:</span></div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pull-right"><span class="text-danger"> 1 عدد</span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pull-right">
                                        <div class="cells">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-12 pull-right"><span class="text-dark"> تعداد سینی:</span></div>
                                                <div class="col-md-6 col-sm-6 col-xs-12 pull-right"><span class="text-danger"> 1 عدد</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-md-12 space-2-50">
                                        <h3 class="text-dark padded-bottom"><i class="fa fa-arrow-circle-o-left"></i> وضعیت سلول ها </h3>
                                        <div class="divider divider-light"></div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-3 pull-right">
                                            <div class="cells">
                                                <div class="row">
                                                <div class="col-md-12">
                                                    <div class="container">
                                                        <span class="text-danger"> خانه شماره ۱</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                class="text-dark"> پر بودن لایه:</span></div>

                                                    <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> پرباشد</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                class="text-dark"> طعم پایه: </span></div>

                                                    <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> تلخ ۱۰ درصد</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                class="text-dark"> طعم مکمل:</span></div>

                                                    <div class=" col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> لیمویی</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                class="text-dark"> شکل:</span></div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger">مربع</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                class="text-dark"> وزن:</span></div>

                                                    <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> ۳ گرم</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 padded-vertical">
                                                    <div class="col-md-12">
                                                        <span class="dark-text"> تصویر آپلود شده:</span>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="img-uploaded">
                                                            <img src="../assets/site/img/order-step2.png" alt=""
                                                                 class="img-responsive">
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 pull-right">
                                            <div class="cells">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="container">
                                                            <span class="text-danger"> خانه شماره ۱</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> پر بودن لایه:</span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> پرباشد</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> طعم پایه: </span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> تلخ ۱۰ درصد</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> طعم مکمل:</span></div>

                                                        <div class=" col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> لیمویی</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> شکل:</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger">مربع</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> وزن:</span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> ۳ گرم</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-12">
                                                            <span class="dark-text"> تصویر آپلود شده:</span>
                                                        </div>
                                                        <div class="col-md-12 padded-vertical">
                                                            <div class="img-uploaded">
                                                                <img src="../assets/site/img/order-step2.png" alt=""
                                                                     class="img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 pull-right">
                                            <div class="cells">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="container">
                                                            <span class="text-danger"> خانه شماره ۱</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> پر بودن لایه:</span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> پرباشد</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> طعم پایه: </span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> تلخ ۱۰ درصد</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> طعم مکمل:</span></div>

                                                        <div class=" col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> لیمویی</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> شکل:</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger">مربع</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> وزن:</span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> ۳ گرم</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-12">
                                                            <span class="dark-text"> تصویر آپلود شده:</span>
                                                        </div>
                                                        <div class="col-md-12 padded-vertical">
                                                            <div class="img-uploaded">
                                                                <img src="../assets/site/img/order-step2.png" alt=""
                                                                     class="img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 pull-right">
                                            <div class="cells">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="container">
                                                            <span class="text-danger"> خانه شماره ۱</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> پر بودن لایه:</span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> پرباشد</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> طعم پایه: </span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> تلخ ۱۰ درصد</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> طعم مکمل:</span></div>

                                                        <div class=" col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> لیمویی</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> شکل:</span></div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger">مربع</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-6 col-sm-6 col-xs-6 pull-right"><span
                                                                    class="text-dark"> وزن:</span></div>

                                                        <div class="col-md-6 col-sm-6 col-xs-6"><span class="text-danger"> ۳ گرم</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 padded-vertical">
                                                        <div class="col-md-12">
                                                            <span class="dark-text"> تصویر آپلود شده:</span>
                                                        </div>
                                                        <div class="col-md-12 padded-vertical">
                                                            <div class="img-uploaded">
                                                                <img src="../assets/site/img/order-step2.png" alt=""
                                                                     class="img-responsive">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-12 space-2-50">
                                    <div class="col-md-6 detail-pack  pull-right">
                                        <div class="col-md-12 space-2-30">
                                            <h4 class="text-dark padded-bottom"><i class="fa fa-arrow-circle-o-left"></i> جزییات بسته بندی </h4>
                                            <div class="divider divider-light"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="cells">
                                                <div class="row">
                                                    <div class="col-md-5 pull-right">
                                                        <div class="col-md-12 pack-name ">
                                                            <div class="img-pack">
                                                                <img src="{{ asset('../assets/site/img/order-step2.png') }}" alt="" class="img-responsive">
                                                            </div>
                                                            <div class="title text-center padded-vertical">
                                                                <h4 class="text-danger"> عنوان بسته بندی</h4>
                                                            </div>
                                                            <div class="description text-center padded-vertical">
                                                                <p class="text-dark"> توضیحات محصول</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="text-dark padded-vertical">
                                                            <p> تصویر آپلود شده:</p>
                                                        </div>
                                                        <div class="img-uploaded padded-vertical">
                                                            <p class="btn btn-default btn-block text-danger text-center"> تصویری هنوز آپلود نشده است</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pull-right">
                                        <div class="col-md-12 space-2-30">
                                            <h4 class="text-dark padded-bottom"><i class="fa fa-arrow-circle-o-left"></i> جزییات سفارش </h4>
                                            <div class="divider divider-light"></div>
                                        </div>
                                        <div class="col-md-12 factor-items">
                                            <div class="cells">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <div class="col-md-12 price-item padded-vertical">
                                                        <div class="col-md-6 pull-right">
                                                            <p class="text-right"> قیمت محصول</p>
                                                        </div>
                                                        <div class="col-md-6 pull-right">
                                                            <p class="text-left"> ۵۰ هزار تومان</p>
                                                        </div>
                                                    </div>
                                                    <div class="divider divider-light"></div>
                                                    <div class="col-md-12 price-item padded-vertical">
                                                        <div class="col-md-6 pull-right ">
                                                            <p class="text-right"> قیمت محصول</p>
                                                        </div>
                                                        <div class="col-md-6 pull-right">
                                                            <p class="text-left"> ۵۰ هزار تومان</p>
                                                        </div>

                                                    </div>
                                                    <div class="divider divider-light"></div>
                                                    <div class="col-md-12 price-item padded-vertical">
                                                        <div class="col-md-6 pull-right">
                                                            <p class="text-right"> قیمت محصول</p>
                                                        </div>
                                                        <div class="col-md-6 pull-right">
                                                            <p class="text-left"> ۵۰ هزار تومان</p>
                                                        </div>

                                                    </div>
                                                    <div class="divider divider-light"></div>
                                                    <div class="col-md-12 price-item padded-vertical">
                                                        <div class="col-md-6 pull-right">
                                                            <p class="text-right text-success"> جمع کل </p>
                                                        </div>
                                                        <div class="col-md-6 pull-right">
                                                            <p class="text-left text-success"> ۵۰ هزار تومان</p>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-md-12 space-2-30">
                                        <button type="submit" class="btn submit pull-right"> مرحله قبلی
                                        </button>
                                        <button type="submit" class="btn submit pull-left"> پرداخت سفارش </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    ​<!-- end outer row -->
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>

@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
    {{ Html::style('assets/site/css/orders.css') }}
    {{ Html::style('assets/site/css/order2.css') }}
    {{ Html::style('assets/site/css/checkout-step.css') }}
    {{ Html::style('assets/site/css/component.css') }}
@endpush

@push('js')

@endpush