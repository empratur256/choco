@extends('views.layouts.site.master')
@section('title','لیست شکلات ها')
@section('content')

    <div class="red_box">
        <div class="container">
            <span> شکلات بار  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
        <div class="bg-color">
        	<div class="container">
        	<div class="row">
        		<div class="col-xs-12">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container">
                <span> خانه </span>
                <span> &gt; </span>
                <span> سبد خرید </span>
            </div>
        </div>
    </div>
<div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center" style="padding-top: 7em;padding-bottom: 3em">
                                <div class="row order-status">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                                <p class="">اطلاعات پرداخت</p>
                                            </div>
                                            <div class="col-xs-4 padding-top-2 padding-lr-0">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                            </div>
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                                <p class="">تایید سفارش</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-4 padding-top-2 padding-lr-0">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                            </div>
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                                <p class="">پردازش انبار</p>
                                            </div>
                                            <div class="col-sm-4 col-xs-12  padding-top-2 padding-lr-0">
                                                <img class="hidden-xs" src="../assets/site/img/circle.png">
                                                <img class="hidden-xs" src="../assets/site/img/circle.png">
                                                <img class="hidden-xs" src="../assets/site/img/circle.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 col-xs-12">

                                        <div class="row">

                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/not-ok-circle.png">
                                                <p class="">پرداخت</p>
                                            </div>
                                            <div class="col-xs-4  padding-top-2 padding-lr-0">
                                                <img src="../assets/site/img/green-circle.png">
                                                <img src="../assets/site/img/green-circle.png">
                                                <img src="../assets/site/img/green-circle.png">
                                            </div>
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                                <p class="green_text">تایید سفارش</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                            	<div class="row">
                            	<div class="col-xs-12 bg-white">
                            	  <div class="col-xs-12 padding-top-3 padding-bottom-3">
                            	  	  <img class="status-pic center-block img-responsive" src="../assets/site/img/order-step1.png">
                            	  </div>
                            	  <div class="col-sm-8 col-sm-push-2 col-xs-12 text-center padding-bottom-3">
                            	  	 <p class="text-default" style="font-weight:bolder;font-size: 15px;">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. </p>
                            	  </div>
                            	  <div class="col-md-6 col-xs-12 col-md-push-3 order-type">
                            	  	<div class="row">
                            	  	 <div class="col-xs-12" style="padding: 1.5em; border:1px solid #ebebeb">
                            	  	 	<input class="pull-left form-control" type="" name="">
                            	  	 	<p class="pull-right" >تعداد تعداد کل سفارش را وارد کنید</p>
                            	  	 </div>
                            	  	 <div class="col-xs-12" style="padding: 1.5em; border:1px solid #ebebeb;margin-top: 7em">
 <select class="pull-left" style="direction: rtl;font-size: 1em;width: 90px;padding: 0.5em;">
  <option value="volvo">شکلاتی</option>
  <option value="saab">شکلاتی</option>
  <option value="vw">شکلاتی</option>
  <option value="audi" selected>شکلاتی</option>
</select>
                            	  	 	<p class="pull-right" >تعداد تعداد کل سفارش را وارد کنید</p>
                            	  	 </div>
                            	  	 <div class="col-xs-12" style="padding: 1.5em; border:1px solid #ebebeb;margin-top: 7em">
 <select class="pull-left" style="direction: rtl;font-size: 1em;width: 90px;padding: 0.5em;">
  <option value="volvo">شکلاتی</option>
  <option value="saab">شکلاتی</option>
  <option value="vw">شکلاتی</option>
  <option value="audi" selected>شکلاتی</option>
</select>
                            	  	 	<p class="pull-right" >تعداد تعداد کل سفارش را وارد کنید</p>
                            	  	 </div>
                            	  	 <div class="col-xs-12" style="padding: 1.5em; border:1px solid #ebebeb;margin-top: 7em">
 <select class="pull-left" style="direction: rtl;font-size: 1em;width: 90px;padding: 0.5em;">
  <option value="volvo">شکلاتی</option>
  <option value="saab">شکلاتی</option>
  <option value="vw">شکلاتی</option>
  <option value="audi" selected>شکلاتی</option>
</select>
                            	  	 	<p class="pull-right" >تعداد تعداد کل سفارش را وارد کنید</p>
                            	  	 </div>
                            	  	 <div class="col-xs-12" style="padding: 1.5em; border:1px solid #ebebeb;margin-top: 7em">
 <select class="pull-left" style="direction: rtl;font-size: 1em;width: 90px;padding: 0.5em;">
  <option value="volvo">شکلاتی</option>
  <option value="saab">شکلاتی</option>
  <option value="vw">شکلاتی</option>
  <option value="audi" selected>شکلاتی</option>
</select>
                            	  	 	<p class="pull-right" >تعداد تعداد کل سفارش را وارد کنید</p>
                            	  	 </div>

                            	  </div>
                            	</div>
                            	<div class="col-xs-12" style="margin-top: 5em;margin-bottom: 5em">
                            		<button type="button" class="btn pull-left"> مرحله بعد </button>
                            	</div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
</div>

@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/orders.css') }}
@endpush

@push('js')


@endpush