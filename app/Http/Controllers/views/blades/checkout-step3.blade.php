@extends('views.layouts.site.master')
@section('title','سبد خرید')

@section('content')
<div class="red_box">
    <div class="container">
        <span> سبد خرید  </span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>
<div class="bg-color">
    <div style="padding-top: 1em;padding-bottom: 1em">
        <div class="container">
            <span> خانه </span>
            <span> &gt; </span>
            <span> سبد خرید </span>
        </div>
    </div>
    <div class="buy-status">
        <div class="container">
            <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                <div class="row">
                    <div class="col-sm-5 col-xs-12 padding-lr-0">
                        <div class="row">
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/info-pay.png">
                                <p class="">اطلاعات پرداخت</p>
                            </div>
                            <div class="col-xs-2 padding-top-2 padding-lr-0">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                            </div>
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/bag-circle.png">
                                <p class="">بازبینی سفارش</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                    </div>
                    <div class="col-sm-5 col-xs-12 padding-lr-0">
                        <div class="row">
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/ok-circle.png">
                                <p class="">اطلاعات ارسال سفارش</p>
                            </div>
                            <div class="col-xs-2  padding-top-2 padding-lr-0">
                                <img src="../assets/site/img/green-circle.png">
                                <img src="../assets/site/img/green-circle.png">
                                <img src="../assets/site/img/green-circle.png">
                            </div>
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/ok-circle.png">
                                <p class="green_text">ورود به فروشگاه</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="form" class="container">
        <div class="row" style="">
          <div class="col-sm-12" style="padding-bottom: 4em;">
                <table class="table table-hover" style="border-bottom: 1px solid #ebebeb">
                    <thead style="color:#bbbbbb">
                        <tr>
                            <th class="text-right">عنوان محصول</th>
                            <th class="text-center">قیمت واحد</th>
                            <th class="text-center">تعداد</th>
                            <th class="text-center">جمع کل</th>

                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i< 3; $i++)
                         <tr class="product">
                            <td class="col-lg-8 col-sm-9 col-md-9">
                                
                                <a class="pull-right" style="padding-left:1em" class="" href="#">
                                 <img src="../assets/site/img/buy-pic.png">
                                </a>
                                <p class="pull-right comment-buy head-text" style="color:#666666">شکلات صبحانه نوتلا 750 گرمی المان</p>
                                <br>
                                <span class="pull-right" style="color:#9f9f9f;padding-top: 1em">لورم ایپسوم متنی ساختگی با استفاده از طراحان گرافیک است</span>
                            </td>

                            <td class="col-sm-1 col-md-1 head-text" style="text-align: center;padding-top: 20px">
                                <p> 1500 تومان </p>
                            </td>
                            <td class="count-product col-sm-2 col-md-2 text-center head-text" style="text-align: center;padding-top: 20px">
                                <p><span>1</span> <span>عدد</span></p>
                            </td>

                            <td class="col-lg-2 col-sm-1 col-md-1 head-text" style="padding-top: 20px">
                                <p> 15000 تومان </p>
                            </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
                </div>
                <div class="col-sm-6 col-xs-12 col-sm-push-6">
                <div class="row">
                  <div class= "col-xs-12" style="">
                    <div style="padding-bottom:1em; border-bottom:1px solid #ebebeb">
                    <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                    <span class="bolder">خلاصه صورت حساب شما</span>
                  </div>
                  </div>
                  <div class="col-xs-12" style="padding-top: 2em; padding-bottom: 2em">
                  <div style="float: right;border:1px solid #ebebeb " >
                  <div class="col-xs-12"  style="padding-bottom: 1em; padding-top:1em;border-bottom: 1px solid #ebebeb ">
                    <span class="pull-right">جمع کل خرید شما</span>
                    <span class="pull-left">52000تومان</span>
                  </div>
                  <div class="col-xs-12" style="padding-bottom: 1em; padding-top:1em;border-bottom: 1px solid #ebebeb ">
                    <span class="pull-right">هزینه ارسال،بیمه و بسته بندی سفارش</span>
                    <span class="pull-left">20000تومان</span>
                  </div>
                  <div class="col-xs-12" style="padding-bottom: 1em; padding-top:1em;border-bottom: 1px solid #ebebeb ">
                    <span class="pull-right">جمع کل تخفیف</span>
                    <span class="pull-left">0تومان</span>
                  </div>
                  <div class="col-xs-12 green_text" style="padding-bottom: 1em; padding-top:1em">
                    <span class="pull-right">جمع کل قابل پرداخت</span>
                    <span class="pull-left">720000تومان</span>
                  </div>
                  </div>
                  </div>
                </div>  
                </div>

                <div class="col-sm-6 col-sm-pull-6">
                <div class="row">
                  <div class="col-xs-12">
                  <div style="padding-bottom:1em; border-bottom:1px solid #ebebeb">
                    <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                    <span class="bolder">اطلاعات ارسال سفارش</span>
                  </div>
                  </div>
                  <div class="col-xs-12" style="padding-top: 2em">
                    <div style="border:1px solid #ebebeb; padding:15px" >
                  	<p style="padding-bottom: 0.5em">
                  		این سفارش به <span class="green_text"> رضا رضایی </span>به آدرس <span class="red_text"> مشهد هاشمیه </span> و شماره تماس <span class="red_text">09371552960</span> تحویل میگردد
                  	</p>
                  	<p>
                  		این سفارش از طریق<span class="red_text"> پست پیشتاز </span> با <span class="red_text">هزینه 7200 تومان</span> به شما تحویل داده خواهد شد
                    </p>
                  
                  </div>
                </div>

                </div>
            </div>
<div class="col-xs-12 button">
             <button type="submit" class="btn btn-default pull-right">بازگشت به سبد خرید</button>
             <button type="submit" class="btn btn-default pull-left">تایید و انتخاب شیوه پرداخت</button>
            </div>
                </div>
                </div>
</div>
@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/checkout-step.css') }}
{{ Html::style('assets/site/css/checkout-step3.css') }}
@endpush

@push('js')
@endpush