@extends('views.layouts.site.master')
@section('title', trans('site.carts.payments.submit_invoice'))
@section('content')

    <div class="top-section" id="background-pattern">
        <div class="container">
            <div class="title">@lang('site.carts.payments.submit_invoice')</div>
            <div class="bread">
                <span class="place">@lang('site.home')</span>
                <span class="fa fa-angle-double-left divider"></span>
                <span class="place">@lang('site.carts.payments.submit_invoice')</span>
            </div>
        </div>
    </div>

    <div class="credit-page">
        <div class="container panel">

            <div class="row with-border item-introduction">
                <div class="col-xs-3" id="tabs">
                    <ul class="nav nav-pills nav-stacked" role="tablist">
                        <li @if(request()->exists('account')) class="active" @endif>
                            <a href="{!! action('Order\Controllers\Site\OrderController@payment',[$order->id,'account']) !!}">@lang('site.carts.payments.account')</a>
                        </li>

                        <li @if(request()->exists('card')) class="active" @endif>
                            <a href="{!! action('Order\Controllers\Site\OrderController@payment',[$order->id,'card']) !!}">@lang('site.carts.payments.card')</a>
                        </li>
                    </ul>
                </div>

                <div class="col-xs-9" id="tab-panels">
                    @include('views.errors.errors')
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="payment">
                            {{ Form::open(['action' => 'Payment\Controllers\Site\PaymentController@submit']) }}
                            {!! Form::hidden('order',$order->id) !!}
                            <div class="row">
                                <div class="col-sm-6">
                                    @if(request()->exists('card'))
                                        @foreach($accounts as $account)
                                            <div class="col-xs-12 checkbox-row border-round">
                                                <label class="checkbox-container col-xs-2">
                                                    {{ Form::radio('account', $account->id) }}
                                                </label>
                                                <div class="checkbox-info col-xs-10">
                                                    <div class="name"> @lang('site.carts.payments.card_number'): {{ $account->card_number }}</div>
                                                    <div class="address name"> {{$account->bank_name}}- @lang('site.carts.payments.to_name') {{$account->account_name}}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @elseif(!request()->exists('card'))
                                        @foreach($accounts as $account)
                                            <div class="col-xs-12 checkbox-row border-round">
                                                <label class="checkbox-container col-xs-2">
                                                    {{ Form::radio('account', $account->id) }}
                                                </label>
                                                <div class="checkbox-info col-xs-10">
                                                    <div class="name"> @lang('site.carts.payments.account_number'): {{ $account->account_number }}</div>
                                                    <div class="name">@lang('site.carts.payments.sheba') :‌ <br><span style="font-size: 13px">{{$account->sheba}}</span></div>
                                                    <div class="address name"> {{$account->bank_name}}- @lang('site.carts.payments.to_name') {{$account->account_name}}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif

                                </div>

                                <div class="col-sm-6 form-container with-border">
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            @if(request()->exists('card'))
                                                {{ Form::label('from', trans('site.carts.payments.card_number'), ['class' => 'col-sm-12 col-md-3']) }}
                                            @else
                                                {{ Form::label('from', trans('site.carts.payments.account_number'), ['class' => 'col-sm-12 col-md-3']) }}
                                            @endif
                                            {{ Form::text('from', null, ['class' => 'col-sm-12 col-md-9']) }}
                                        </div>
                                        <div class="form-group col-xs-12">
                                            {{ Form::label('reference', trans('site.carts.payments.reference'), ['class' => 'col-sm-12 col-md-3']) }}
                                            {{ Form::text('reference', null, ['class' => 'col-sm-12 col-md-9']) }}
                                        </div>
                                        <div class="form-group col-xs-12">
                                            {{ Form::label('amount', trans('site.carts.payments.amount'), ['class' => 'col-sm-12 col-md-3']) }}
                                            <div class="input-group col-sm-12 col-md-9">
                                                {{ Form::text('amount', null, ['class' => ' form-control', 'aria-describedby' => 'basic-addon1']) }}
                                                <span class="input-group-addon">@lang('site.currency')</span>
                                            </div>
                                        </div>

                                        <div class="form-group col-xs-12">
                                            {{ Form::label('date', trans('site.date'), ['class' => 'col-sm-12 col-md-3 ']) }}
                                            {{ Form::text('date', null, ['class' => 'col-sm-12 col-md-9 datepicker']) }}
                                        </div>
                                        <div class="form-group col-xs-12">
                                            <button class="btn dark-blue-btn pull-left" type="submit">@lang('site.save')</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {{ Form::close() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@push('css')
{{ Html::style('assets/site/plugins/datepicker/css/bootstrap-datepicker.min.css') }}
@endpush

@push('js')
{{ Html::script('assets/site/plugins/datepicker/js/bootstrap-datepicker.min.js') }}
@if(App::isLocale('fa'))
{{ Html::script('assets/site/plugins/datepicker/js/bootstrap-datepicker.fa.min.js') }}
@endif
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            dateFormat: 'yy/mm/dd',
        });
    })
</script>
@endpush