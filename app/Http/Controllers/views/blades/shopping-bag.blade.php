@extends('views.layouts.site.master')
@section('title','سبد خرید')
@section('content')
<div class="red_box">
    <div class="container">
        <span>سبد خرید</span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>
<div class="shopping-bag" style="background-color:#f1f1f1">
    <div class="container">
        <div class="row" style="background-color: white">
            <div class="col-sm-12" style="overflow-x:auto;">
                <h5 style="padding-top: 2em;color:#727272;font-weight: bolder"> مشاهده سبد خرید </h5>

                <table class="table table-hover">
                    <thead style="color:#bbbbbb">
                        <tr>
                            <th class="text-right">عنوان محصول</th>
                            <th class="text-center">قیمت واحد</th>
                            <th class="text-center">تعداد</th>
                            <th class="text-center">جمع کل</th>

                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 0; $i
                        < 3; $i++) <tr class="product">
                            <td class="col-lg-8 col-sm-9 col-md-9">
                                <a class="pull-right remove" href=""><img src="../assets/site/img/remove-product.png"></a>
                                <a class="pull-right" style="padding-left:1em" class="" href="#">
                                 <img src="../assets/site/img/buy-pic.png">
                                </a>
                                <p class="pull-right comment-buy" style="color:#666666">شکلات صبحانه نوتلا 750 گرمی المان</p>
                                <br>
                                <span class="pull-right" style="color:#9f9f9f;padding-top: 1em">لورم ایپسوم متنی ساختگی با استفاده از طراحان گرافیک است</span>
                            </td>

                            <td class="col-sm-1 col-md-1" style="text-align: center;padding-top: 20px">
                                <p> 1500 تومان </p>
                            </td>
                            <td class="count-product col-sm-2 col-md-2 text-center">
                                <form method="POST" action="">
                                    <input type='submit' value='+' class='qtyplus inc-count qty' field='quantity'>

                                    <input type='text' name='quantity' value='1' class='int counter'><text style="color:#666666">عدد </text>
                                <input type='submit' value='-' class='qtyminus dec-count qty' field='quantity'>

                                </form>
                            </td>

                            <td class="col-lg-2 col-sm-1 col-md-1" style="padding-top: 20px">
                                <p> 15000 تومان </p>
                            </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
            </div>
            <div class="col-sm-12">



               
            <div class="col-xs-12" style="padding-bottom: 20px; border-bottom:1px solid #ddd;padding-left: 8px;padding-right: 8px" >
                    
                <div class="pull-left update-bag">
                <button type="button" class="btn btn-default">بروزرسانی سبد خرید
                        </button>
                    </div>
                    <div class="pull-left">
                       <button type="button" class="btn btn-default">خالی کردن سبد خرید</button>
                    </div>

                    <div class="pull-right continue-buy col-lg-2">
                    <button type="button" class="btn btn-default">ادامه خرید</button>
                    </div>
            </div>




                <div class="col-lg-6 col-md-7 col-sm-8 col-xs-12" style="padding:1em 0 0 0">
                    
                        <div class="col-lg-8 col-md-8 col-sm-8 total-buy" style="">
                            <span class="pull-right"> جمع کل خرید شما </span>
                            <span class="pull-left">5200 هزار تومان</span>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 total-pay" style="">
                            <span class="pull-right"> جمع کل قابل پرداخت </span>
                            <span class="pull-left">5200 هزار تومان</span>
                        </div>
                   
                </div>

                <div class="col-lg-6 col-md-5 col-sm-4 col-xs-12" style="padding:2em 8px 0 0;">
                    <p> بر اساس مکان تحویل سفارش امکان افزوده شدن هزینه وجود دارد
                    </p>
                </div>

                <div class="col-xs-12 send-manner" style="padding-bottom: 2em;padding-top: 2em">
                    <button type="button" class="btn pull-left"> انتخاب شیوه ارسال </button>
                </div>
            </div>
        </div>
    </div>

</div>



       
@endsection
@push('css')
   {{ Html::style('assets/site/css/shopping-bag.css') }}
@endpush
@push('js')
{{ Html::script('assets/site/js/shopping-bag.js') }}
@endpush