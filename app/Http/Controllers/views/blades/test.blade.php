
<!DOCTYPE html>
<html>
    <head>
        <!-- Latest compiled and minified CSS -->

{{ Html::style('assets/site/css/bootstrap.min.css') }}
{{ Html::style('assets/site/plugins/bootstrap-slider/css/bootstrap-slider.min.css') }}
  {{ Html::style('assets/site/css/font-awesome.min.css') }}
{{ Html::style('assets/site/css/chocolate-list.css') }}  
   {{ Html::style('assets/site/css/navbar.css') }}
   {{Html::style('assets/site/plugins/bootstrap-select/bootstrap-select.min.css')}}
   {{ Html::style('assets/site/css/ion.rangeSlider.css') }}
   {{ Html::style('assets/site/css/ion.rangeSlider.skinFlat.css') }}

</head>
<body>

<div class="red_box">
    <div class="container">
        <span> شکلات بار  </span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>
<div id="choco-list">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div style="padding-top: 1em;padding-bottom: 0.5em">
                    <span> خانه </span>
                    <span> > </span>
                    <span> شکلات بار </span>
                </div>
            </div>

            <div class="list col-lg-3 col-md-3 col-sm-3 col-xs-3 col-xxs-12 pull-right">
                <div class="hidden-xxs">
                    <h5 class="border" style="margin-bottom: 1em"><span> <img src="../assets/site/img/red-circle.png"> </span> دسته بندی ها  </h5>
                    <div class="padding-bottom"><a href=""><span> <img src="../assets/site/img/red-circle2.png"> </span>شکلات بار <span>(7)</span></a></div>
                    <div class="padding-bottom"><a href=""><span> <img src="../assets/site/img/red-circle2.png"> </span>تابلت<span>(7)</span> </a></div>
                    <div class="padding-bottom"><a href=""><span> <img src="../assets/site/img/red-circle2.png"> </span>کادویی و پذیرایی<span>(7)</span></a></div>
                    <div class="padding-bottom"><a href=""><span> <img src="../assets/site/img/red-circle2.png"> </span>شکلات فانتزی<span>(7)</span> </a></div>
                    <div class="padding-bottom"><a href=""><span> <img src="../assets/site/img/red-circle2.png"> </span>دراژه<span>(7)</span></a></div>
                    <div class="padding-bottom"><a href=""><span> <img src="../assets/site/img/red-circle2.png"> </span>آبنبات<span>(0)</span></a></div>
                    <div class="padding-bottom"><a href=""><span> <img src="../assets/site/img/red-circle2.png"> </span>تافی<span>(0)</span></a></div>

                </div>
                <div class="list-search" style="padding-bottom: 1em">
                    <p class="border"> <span><img src="../assets/site/img/red-circle.png"></span> جستجو </p>
                    <form>
                        <div id="custom-search-input">
                            <div class="input-group">
                                <input type="text" class="  search-query form-control" placeholder="جستجو" />
                                <span class="input-group-btn">
                                    <button class="btn btn-danger" type="submit">
                                        <span class=" glyphicon glyphicon-search"></span>
                                </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
              <!--  
                <div class="range-slider price-range">
                    <p class="border"> <span><img src="../assets/site/img/red-circle.png"></span> رنج قیمت به (تومان) </p>
                    <div class="test" style="padding-top: 1em">
                        <input type="text" class="js-range-slider" value="" />
                    </div>
                </div>
         -->       
 <div class="range-slider price-range">
   <p class="border"> <span><img src="../assets/site/img/red-circle.png"></span> رنج قیمت به (تومان) </p>
<div style="padding-top: 3em">
<input id="price-range" type="text" value="100,10000" data-slider-min="100" data-slider-max="10000" data-slider-step="10" data-slider-value="[100,10000]"  data-value="100,10000" style="">
</div>
</div>



{!! Form::open(['method' => 'GET','class' => 'search']) !!}
{!! Form::hidden('price_from', null, ['id' => 'from']) !!}
{!! Form::hidden('price_to', null, ['id' => 'to']) !!}
{{ Form::hidden('sort',request('sort'), ['id' => 'sort']) }}
            </div>

            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-xxs-12 products">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 col-xxs-12 text-left" style="padding-left: 3px; padding-bottom: 2.5em;">
                  <!--
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">جدیدترین ها
                                <span class="caret"></span></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                 <li role="presentation"><a role="menuitem" href="#"> جدیدترین ها</a></li>
                                <li role="presentation"><a role="menuitem" href="#">شکلات بار </a></li>
                                <li role="presentation"><a role="menuitem" href="#">تابلت</a></li>
                                <li role="presentation"><a role="menuitem" href="#">دراژه</a></li>
                            </ul>
                        </div>
                        -->
                     <select class="selectpicker" id="sort-select" name="sort-select" tabindex="-98">
                      <option value="1">جدید ترین</option>
                      <option value="2">پرفروش ترین ها</option>
                      <option value="3">ارزان ترین</option>
                      <option value="4">گران ترین</option>
                    </select>
                    </div>




                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8 col-xxs-12" style="padding-right:5px; padding-bottom: 2.5em; ">
                        <h5 style="color:#ff5e5e;font-weight: bold"> نمایش 15 محصول از 40 محصول یافت شده </h5>
                    </div>
                    <div class="product-list">
                        @for ($i = 0; $i
                        < 15; $i++) <div class="product product-padding col-lg-2 col-md-2 col-sm-3 col-xs-4" style='float: right;'>
                            <div class="thumbnail">
                                <a href="" target="_blank">
                                    <img src="../assets/site/img/choco-select.png" alt="Lights" class="img-responsive" style="width:100%">
                                    <div class="caption" style="font-weight: 700">
                                        <p style="color:#989898"> شکلات بالوتین </p>
                                        <p style="color:#0ba521"> <span> 25 </span> تومان </p>
                                    </div>
                                </a>
                            </div>
                            <div class="overlay">
                            </div>
                            <div class="overlay-content">
                                <a href="">
                                    <img src="../assets/site/img/shopingbag.png">
                                    <p> افزودن به سبد خرید </p>
                                </a>
                            </div>
                    </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>
</div>
</div>



 {{ Html::script('assets/site/js/jquery-3.2.1.slim.js') }}
{{ Html::script('assets/site/js/bootstrap.min.js') }}
{{ Html::script('assets/site/plugins/bootstrap-slider/js/bootstrap-slider.min.js') }}
{{ Html::script('assets/site/js/shopping-bag.js') }}
{{Html::script('assets/site/plugins/bootstrap-select/bootstrap-select.min.js')}}
{{ Html::script('assets/site/plugins/bootstrap-slider/js/bootstrap-slider.min.js') }}
{{Html::script('assets/site/js/ion.rangeSlider.js')}}
{{Html::script('assets/site/js/list.js')}}  

<script>
 
    $(document).ready(function () {
 
        var mySlider = new Slider("#price-range", 
            {tooltip:'always',tooltip_split:true});
         

        $(".min-slider-handle, .max-slider-handle").on("mouseup", function (slideEvt) {
            var values = $('#price-range').val().split(',');
            $('#from').val(values[0]);
            $('#to').val(values[1]);
            if ($('#sort').val() ==""){
                $('#sort').val('1');
            }
            $('.search').submit();
        });

        $('#from').on('change', function (e) {
            var value = mySlider.getValue();
            value[0] = parseInt($(this).val());
            mySlider.setValue(value);
            if ($('#sort').val() ==""){
                $('#sort').val('1');
            }
            $('.search').submit();
        });

        $('#to').on('change', function (e) {
            var value = mySlider.getValue();
            value[1] = parseInt($(this).val());
            mySlider.setValue(value);
              if ($('#sort').val() ==""){
                $('#sort').val('1');
            }
            $('.search').submit();
        });

     
        $('#list-view').on('click', function (e) {
            $(this).toggleClass('active');
            $('#large-view').toggleClass('active');

        });

        $('#large-view').on('click', function (e) {
            $(this).toggleClass('active');
            $('#list-view').toggleClass('active');
            $('#profile').tab('show');
        });

        $('select[name="sort-select"],.brands').on('change', function () {
            $('#sort').val($(this).val());
            var values = $('#price-range').val().split(',');
            $('#from').val(values[0]);
            $('#to').val(values[1]);
            $('.search').submit();
        });
        /*
         $('.dropdown-menu a').click(function(){
            $('#sort').val($(this).text());
            var values = $('#price-range').val().split(',');
            $('#from').val(values[0]);
            $('#to').val(values[1]);
            $('.search').submit();

  });
     */    
        /*

        $('#see-more').on('click', function (e) {
            e.preventDefault();
            $('.list-group').toggleClass('show-full');
        });

        $("#to, #from").on("keypress", function (event) {

            var textValue = $(this).val();

            var ew = event.which;
            if (ew == 32 || ew == 64 || ew == 45 || ew == 95 || ew == 33 || ew == 8 || ew == 0 || ew == 13)
                return true;
            if (48 <= ew && ew <= 57)
                return true;
            if (65 <= ew && ew <= 90)
                return true;
            if (97 <= ew && ew <= 122)
                return true;

            var c = String.fromCharCode(ew)

            var test = {
                '۱': '1',
                '۲': '2',
                '۳': '3',
                '۴': '4',
                '۵': '5',
                '۶': '6',
                '۷': '7',
                '۸': '8',
                '۹': '9',
                '۰': '0',
            };

            $.each(test, function (index, value) {
                if (c == index) {
                    textValue += value;
                }
            });

            $(this).val(textValue);

            return false;

        });
        //milad for push

        */

    });

</script>
</body>
</html>