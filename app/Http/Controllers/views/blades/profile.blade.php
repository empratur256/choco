@extends('views.layouts.site.master')
@section('title','پروفایل کاربری')
@section('content')
<div class="red_box">
    <div class="container">
        <span> پروفایل کاربری  </span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>

<div class="bg-color">
    <div style="padding-top: 1em;padding-bottom: 1em">
        <div class="container">
            <span> خانه </span>
            <span> &gt; </span>
            <span> پروفایل کاربری </span>
        </div>
    </div>
    <div class="container">
        <div class="col-sm-3 col-sm-push-9 profile text-center" style="">
            <div class="row bg-white">

                <img class="m-profile" src="../assets/site/img/profile-m.png">
                <p class="padding-top-down-1">رضا رضایی</p>
                <p class="padding-top-down-1" style="width: 90%;margin: auto;border-bottom: 1px solid #ebebeb;"><i class="fa fa-usd dollar" aria-hidden="true" style=""></i> شما 12 بن تخفیف دارید</p>
                <div class="col-xs-12">
                    <p class="padding-top-down-1 padding-top-3 defult-text text-right">آدرس ایمیل</p>
                    <p class="padding-top-down-1 text-right black-text bolder">reza@yahoo.com</p>
                    <p class="padding-top-down-1 text-right defult-text">شماره تماس</p>
                    <p class="padding-top-down-1 text-right black-text bolder">09371552862</p>
                    <p class="padding-top-down-1 text-right defult-text">آدرس</p>
                    <p class="padding-top-down-1 text-right black-text bolder" style="padding-bottom: 2.5em">مشهد-بلوارسپهر-شقایق5-پلاک83</p>
                </div>

            </div>
        </div>
        <div class="col-sm-9 col-sm-pull-3">
            <div class="product-tabs">
                <!-- Nav tabs -->
                <div class="card">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">آخرین سفارشات</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">محصولات مورد علاقه</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="table-responsive" style="">
                                <table class="table table-bordred table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-right defult-text" style="">شماره سفارش</th>

                                            <th class="text-right defult-text" style="padding-right:1.2em">قیمت کل</th>
                                            
                                            <th class="text-right defult-text" style="padding-right:1.2em">تاریخ</th>
                                            
                                            <th class="text-right defult-text" style="padding-right:1.2em">وضعیت</th>
                                            

                                        </tr>
                                    </thead>
                                    <tbody>
                                      @for ($i = 0; $i< 5; $i++) 
                                          <tr>
                                            <td>#25623</td>
                                            <td>1500 تومان</td>
                                            <td>96/5/14</td>
                                            <td>
                                            <a href="" class="open-info">
                                                <i class="fa fa-circle" aria-hidden="true" style="padding-left: 0.5em; color:aqua">
                                                  </i> ارسال شده
                                                   <i class="fa fa-angle-up fa-2x" aria-hidden="true"></i>
                                             </a>
                                              </td>
                                            </tr>
                                            
                                            <tr style="display: none;height: 400px">
                                               <td style="background-color: white" colspan="5">
                                            <table class="table table-bordred table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-right defult-text" style="">کالا</th>
                                            <th class="text-right defult-text" style="padding-right:1.2em">تعداد</th>
                                            <th class="text-right defult-text" style="padding-right:1.2em">قیمت واحد</th>
                                            <th class="text-right defult-text" style="padding-right:1.2em">قیمت کل</th>
                                            <th class="text-right defult-text" style="padding-right:1.2em">تخفیف</th>
                                            <th class="text-right defult-text" style="padding-right:1.2em">مبلغ کل</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <tr style="background-color: white">
                                            <td class="bolder">نوتلا 350 گرمی</td>
                                            <td class="bolder">1 عدد</td>
                                            <td class="bolder">1400 تومان</td>
                                            <td class="bolder">1400 تومان</td>
                                            <td class="bolder">0</td>
                                            <td class="bolder">1400 تومان</td>
                                        </tr>

                                    </tbody>
                                </table>
                             <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                                <div class="row">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/info-pay.png">
                                                <p class="">اطلاعات پرداخت</p>
                                            </div>
                                            <div class="col-xs-4 padding-top-2 padding-lr-0">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                            </div>
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                                <p class="green_text">تایید سفارش</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="row">
                                            <div class="col-xs-4 padding-top-2 padding-lr-0">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                                <img src="../assets/site/img/circle.png">
                                            </div>
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                                <p class="green_text">پردازش انبار</p>
                                            </div>
                                            <div class="col-sm-4 col-xs-12  padding-top-2 padding-lr-0">
                                                <img class="hidden-xs" src="../assets/site/img/circle.png">
                                                <img class="hidden-xs" src="../assets/site/img/circle.png">
                                                <img class="hidden-xs" src="../assets/site/img/circle.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 col-xs-12">

                                        <div class="row">

                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                                <p class="green_text">پرداخت</p>
                                            </div>
                                            <div class="col-xs-4  padding-top-2 padding-lr-0">
                                                <img src="../assets/site/img/green-circle.png">
                                                <img src="../assets/site/img/green-circle.png">
                                                <img src="../assets/site/img/green-circle.png">
                                            </div>
                                            <div class="col-xs-4 padding-lr-0">
                                                <img class="status-pic" src="../assets/site/img/ok-circle.png">
                                                <p class="green_text">تایید سفارش</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="padding-top-2">
                              <div style="padding: 1em; border:1px solid #ebebeb ">
                                    <p class="padding-top-1"><span class="red_text bolder">روش ارسال :</span> پیشتاز(4500تومان)</p>
                                    <p class="padding-top-1"><span class="red_text bolder">آدرس تحویل :</span> مشهد</p>
                                    <p class="padding-top-1"><span class="red_text bolder">تحویل گیرنده :</span> رضا رضایی</p>
                                    <p class="padding-top-1"><span class="red_text bolder">کد مرسوله :</span> 45236987456</p>
                                    <p class="padding-top-1"><span class="red_text bolder">شماره تماس :</span> 32659874563</p>
                                </div>
                            </div>

                                               </td>
                                            </tr>

                                        @endfor

                                    </tbody>
                                </table>
                            </div>

     

                        </div>
                        <div role="tabpanel" class="tab-pane" id="profile">لورم ایپسوم متن ساختگی با تولید سادگی 22نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 border">
                <h5 class="pull-right bolder">
              	درخواست های پشتیبانی
              </h5>
                <h5 class="pull-left">
              <a class="red_text bolder hover-hand">+ مشاهده همه</a>
              </h5>
            </div>
           <div class="col-xs-12 bg-white">
            <div class="table-responsive clear_both suport-request padding-bottom-2" style="">
                <table class="table table-bordred table-striped bg-white">
                    <thead>
                        <tr>
                            <th class="text-right defult-text" style="">عنوان درخواست</th>
                            <th class="text-right defult-text" style="padding-right:1.2em">وضعیت</th>
                            <th class="text-right defult-text" style="padding-right:1.2em">تاریخ</th>
                            <th></th>
                        </tr>

                    </thead>
                    <tbody>
                    @for ($i = 0; $i< 5; $i++)
                        <tr>
                            <td class="bolder">پیگیری وضعیت سفارش</td>
                            <td class="bolder"><i class="fa fa-circle" aria-hidden="true" style="padding-left: 0.5em; color:aqua">
                              </i> در انتظار تایید </td>
                            <td class="bolder">12/11/98</td>
                            <td class="bolder red_text text-center">مشاهده</td>
                        </tr>
                      @endfor
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
    
</div>
@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/profile.css') }}
@endpush

@push('js')
<script>
    $(document).ready(function () {
        $('.open-info').on('click', openNext);
    });

    function openNext(e) {
        e.preventDefault();
        $(this).find('.fa-angle-up').toggleClass('icon-rotate-180');
        $(e.target).closest('tr').next().slideToggle("slow");
        $(".test").slideToggle("slow");
    }
</script>
@endpush