@extends('views.layouts.site.master')
@section('title','سبد خرید')
@section('content')
    <div class="red_box">
        <div class="container">
            <span> سبد خرید  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
  <div class="container" style="padding-top: 5em;padding-bottom: 5em;">
    <div class="col-xs-12">
  <div class="your text-center" style="direction: initial;text-align: -webkit-center;" >
  <div><img class="img-responsive" src="../assets/site/img/single-product.png"></div>
  <div><img class="img-responsive" src="../assets/site/img/single-product.png"></div>
  <div><img class="img-responsive" src="../assets/site/img/single-product.png"></div>
  <div><img class="img-responsive" src="../assets/site/img/single-product.png"></div>
  <div><img class="img-responsive" src="../assets/site/img/single-product.png"></div>
  <div><img class="img-responsive" src="../assets/site/img/single-product.png"></div>
</div>
</div>
</div>

@endsection
@push('css')
{{ Html::style('assets/site/plugins/slick/slick/slick.css') }}
{{ Html::style('assets/site/plugins/slick/slick/slick-theme.css') }}

{{ Html::style('assets/site/css/portfolio.css')}}
{{ Html::style('assets/site/css/public.css')}}
@endpush

@push('js')

{{ Html::script('assets/site/plugins/slick/slick/slick.min.js') }}
<script>
	$('.your').slick({
  dots: true,
  infinite: false,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
</script>
@endpush