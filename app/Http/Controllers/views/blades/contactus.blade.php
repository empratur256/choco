@extends('views.layouts.site.master')
@section('title','test page')

@section('content')
<div class="red_box">
   <div class="container">
      <span> ارتباط با ما   </span>
      <p> لورم ایپسوم متنی ساختگی با استفاده از طراحان گرافیک است </p>
   </div>
</div>
<div class="contactus">
   <div class="container">
   <div class="row">
      <div class="form col-lg-8 col-md-8 col-sm-8 col-xs-6 col-xxs-12 pull-right">
         <div style="padding-bottom: 2em">
            <h4 style=""> ارتباط با ما </h4>
            <p > متنی است بی مفهوم که تشکیل شده از کلمات معنی دار یا بی معنی کنار هم. کاربر با دیدن متن لورم ایپسوم تصور میکند متنی که در صفحه مشاهده میکند این متن واقعی و مربوط به توضیحات صفحه مورد نظر است واقعی است. حالا سوال اینجاست که این متن " لورم ایپسوم " به چه دردی میخورد و اساسا مواجه بودند
            </p>
         </div>
         <form>
            <div class="row">
               <div class="col-lg-5 col-md-6 col-sm-8 col-xs-11 form-group pull-right ">
                  <label for="name">نام شما</label>
                  {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
                  <span class="glyphicon glyphicon-user"></span>
               </div>
               <div class="col-lg-5 col-md-6 col-sm-8 col-xs-11 form-group pull-right">
                  <label for="email">ایمیل شما</label>
                  {{ Form::text('email', null, ['class' => 'form-control']) }}
                  <span class="glyphicon glyphicon-envelope"></span>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-5 col-md-6 col-sm-8 col-xs-11 form-group pull-right ">
                  <label for="titleMessage">عنوان پیام</label>
                  {{ Form::text('titleMessage', null, ['class' => 'form-control']) }}
                  
               </div>
               <div class="col-lg-5 col-md-6 col-sm-8 col-xs-11 form-group pull-right">
                  <label for="companyName">نام شرکت</label>
                  {{ Form::text('companyName', null, ['class' => 'form-control']) }}
               </div>
            </div>
            <div class="row">
               <div class="form-group col-lg-10 col-md-12 col-sm-8 col-xs-11 pull-right">
                  <label for ="description"> متن پیام</label>
                  <textarea  class="form-control" id="description" placeholder=""></textarea>
               </div>
            </div>
            <div class="captcha" style="padding-bottom: 2.5em" >
                 {!! captcha_img() !!}
                 {{ Form::text('captcha', null, ['class' => 'form-control']) }}
            </div>

            <div class="row submit">
                <div class="col-lg-3 pull-right">
                   <button type="submit" class="btn btn-default"> ثبت نظر </button>
                </div>
            </div>
         </form>
      </div>

<div style="position: relative;" class="img col-lg-4 col-md-4 col-sm-4 col-xs-6">
   <img id="address-img" src="../assets/site/img/contactus2.png" class="img-responsive">
   <div id="address"  style="">
      <h5> اطلاعات تماس </h5>
      <div style="padding-top: 2em">
         <p style="padding-bottom: 1em"> <i class="glyphicon glyphicon-phone-alt"></i> شماره تماس : 3455679-021  </p>
         <p style="padding-bottom: 1em"> <i class="fa fa-fax fa" aria-hidden="true"></i> فکس 3456790-021 </p>
         <p> <span class="glyphicon glyphicon-home"></span> تهران-خیابان ابکوه-نبش ابکوه 23 </p>
      </div>
   </div>
</div>


   </div>
</div>
</div>
<div>
</div>
@endsection

@push('css')
   {{ Html::style('assets/site/css/contactus.css') }}
@endpush

@push('js')

@endpush
