@extends('views.layouts.site.master')
@section('title','سبد خرید')
@section('content')
    <div class="red_box">
        <div class="container">
            <span> پروفایل کاربری  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
        <div class="bg-color">
          <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container">
                <span> خانه </span>
                <span> &gt; </span>
                <span > پروفایل کاربری </span>
            </div>
          </div>
        
        <div class="container">
                    <div class="col-sm-3 col-sm-push-9 profile text-center" style="">
              <div class="bg-white">
              <img class="m-profile" src="../assets/site/img/profile-m.png">
              <p class="padding-top-down-1">رضا رضایی</p>
              <p class="padding-top-down-1">reza@yahoo.com</p>
              <p class="padding-top-down-1">09371552862</p>
              <p class="padding-top-down-1" style="padding-bottom: 2.5em">مشهد-بلوارسپهر-شقایق5-پلاک83</p>
            </div>
            </div>
            <div class="col-sm-9 col-sm-pull-3 bg-white ticket">
              <div class="row">
                <div class="col-lg-11 col-sm-10 col-xs-12" style="padding-bottom: 2em">
                 <div class="form-group title-request">
                  <input type="text" class="form-control" placeholder="عنوان درخواست">
                 </div>
                 <div class="pull-right">
                 <p style="padding-bottom: 0.5em">بخش</p>
                 <select class="selectpicker" id="sort-select" name="sort-select" tabindex="-98">
                                <option value="1">حسابداری</option>
                                <option value="2">پرفروش ترین ها</option>
                                <option value="3">ارزان ترین</option>
                                <option value="4">گران ترین</option>
                  </select>
                </div>
                <div class="pull-right margin-right-1">
                 <p style="padding-bottom: 0.5em">اولویت</p>
                 <select class="selectpicker" id="sort-select" name="sort-select" tabindex="-98">
                                <option value="1">فوری</option>
                                <option value="2">پرفروش ترین ها</option>
                                <option value="3">ارزان ترین</option>
                                <option value="4">گران ترین</option>
                  </select>
                </div>
               <div class="form-group col-xs-12 description">
                  <textarea class="form-control" id="description" placeholder="متن درخواست"></textarea>
               </div>
               <div class="col-lg-12" style="padding-bottom: 4em;padding-top: 0.5em">
                 <div class="pull-right">
                 	captche
                 </div>
                 <div class="pull-left">
                   <button type="submit" class="btn btn-default submit-button"> ارسال نظر </button>
                 </div>
               </div>
                </div>
                <div class="col-lg-1 col-sm-2 s-profile hidden-xs">
                  <img class="" src="../assets/site/img/s-profile.png">
                </div>
              </div>
            </div>

        </div>
        </div>
@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/send-ticket.css') }}

@endpush

@push('js')


@endpush