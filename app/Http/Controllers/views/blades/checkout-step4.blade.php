@extends('views.layouts.site.master')
@section('title','سبد خرید')

@section('content')
    <div class="red_box">
        <div class="container">
            <span> سبد خرید  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
    <div class="bg-color">
        <div style="padding-top: 1em;padding-bottom: 1em">
            <div class="container">
                <span> خانه </span>
                <span> &gt; </span>
                <span> سبد خرید </span>
            </div>
        </div>
        <div class="buy-status">
            <div class="container">
                <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                    <div class="row">
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="../assets/site/img/info-pay.png">
                                    <p class="">اطلاعات پرداخت</p>
                                </div>
                                <div class="col-xs-2 padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="../assets/site/img/ok-circle.png">
                                    <p class="green_text">بازبینی سفارش</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                            <img class="hidden-xs" src="../assets/site/img/green-circle.png">
                            <img class="hidden-xs" src="../assets/site/img/green-circle.png">
                            <img class="hidden-xs" src="../assets/site/img/green-circle.png">
                        </div>
                        <div class="col-sm-5 col-xs-12 padding-lr-0">
                            <div class="row">
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="../assets/site/img/ok-circle.png">
                                    <p class="green_text">اطلاعات ارسال سفارش</p>
                                </div>
                                <div class="col-xs-2  padding-top-2 padding-lr-0">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                    <img src="../assets/site/img/green-circle.png">
                                </div>
                                <div class="col-xs-5 padding-lr-0">
                                    <img src="../assets/site/img/ok-circle.png">
                                    <p class="green_text">ورود به فروشگاه</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="form" class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6">
                            <div class="total-price">
                                <span> جمع کل قابل پرداخت </span>
                                <span class="pull-left">7200 تومان </span>
                            </div>
                        </div>
                        <div class="col-sm-6 col-sm-pull-6">
                            <div class="off">
                                <form>
                                    <div class="input-group">
                                <span class="input-group-btn">
        <button class="btn btn-default" type="submit">اعمال تخفیف</button>
      </span>
                                        <input type="text" class="form-control" placeholder="">
                                    </div>
                                    <!-- /input-group -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">
                    <div style="border-bottom :1px solid #ebebeb;padding-bottom: 1em">
                        <img src="../assets/site/img/left-side.png"><span> شیوه پرداخت </span>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="row">

                        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 pull-right">

                            <div class="radio" style="text-align: -webkit-right;">
                                <label>
                                    <input type="radio" name="radio-address" value="" style="float: right;">

                                    <span class="cr" style="float: initial;margin-left: 1em;"><i
                                                class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                                    <p class="red_text bolder padding-bottom-05">پرداخت اینترنتی</p>
                                    <p class="defult-text padding-bottom-02" style="font-size: 11px;">درگاه پرداخت
                                        اینترنتی بانک سامان</p>
                                </label>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 pull-right">

                            <div class="radio" style="text-align: -webkit-right;">
                                <label>
                                    <input type="radio" name="radio-address" checked value="" style="float: right;">

                                    <span class="cr" style="float: initial;margin-left: 1em;"><i
                                                class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                                    <p class="red_text bolder padding-bottom-05">کارت به کارت</p>
                                    <p class="defult-text padding-bottom-02" style="font-size: 11px;">میتوانید وجه سفارش
                                        خود را به شکل انتقال وجه پرداخت کنید</p>

                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 button">
                    <button type="submit" class="btn btn-default pull-right">بازگشت به سبد خرید</button>
                    <button type="submit" class="btn btn-default pull-left">نهایی کردن خرید</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/checkout-step.css') }}
{{ Html::style('assets/site/css/checkout-step4.css') }}
@endpush

@push('js')
<script>
    $(document).ready(function () {
        $('#atm-pay').click(function () {
            $(this).prop('checked', true);
            $('#net-pay').prop('checked', false);
        });
        $('#net-pay').click(function () {
            $(this).prop('checked', true);
            $('#atm-pay').prop('checked', false);
        })
    });

</script>

@endpush