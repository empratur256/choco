@extends('views.layouts.site.master')
@section('title','مشخصات محصول')
@section('content')
<div class="red_box">
    <div class="container">
        <span> پروفایل کاربری  </span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>
<div class="bg-color">
    <div style="padding-top: 1em;padding-bottom: 1em">
        <div class="container">
            <span> خانه </span>
            <span> &gt; </span>
            <span> پروفایل کاربری </span>
        </div>
    </div>

    <div class="container">
        <div class="col-sm-3 col-sm-push-9 profile text-center" style="">
            <div class="bg-white">
                <img class="m-profile" src="../assets/site/img/profile-m.png">
                <p class="padding-top-down-1">رضا رضایی</p>
                <p class="padding-top-down-1">reza@yahoo.com</p>
                <p class="padding-top-down-1">09371552862</p>
                <p class="padding-top-down-1" style="padding-bottom: 2.5em">مشهد-بلوارسپهر-شقایق5-پلاک83</p>
            </div>
        </div>
        <div class="col-sm-9 col-sm-pull-3 bg-white ticket">
            <div class="row">
                <div class="col-xs-12">
                    <h5 class="bolder">درخواست بازگشت وجه</h5>
                </div>
               <div class="col-xs-12">
               <div class="people-row">
                    <div class="row">
                        <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                            <span><span class="bolder">بخش : </span>واحد حسابداری</span>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                            <span><span class="bolder">ارسال شده : </span>1396/1/15</span>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6 pull-right">
                            <span><span class="bolder">اولویت : </span>متوسط</span>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-xs-12">
                  <div class="people-row">
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <img class="s-profile" style="margin-top: 0;padding-left: 1em" src="../assets/site/img/s-profile.png">
                        </div>
                        <div class="col-lg-11">
                            <h5 class="bolder pull-right">کارشناس پشتیبانی</h5>
                            <h6 class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 1395/1/16</h6>
                            <p class="pull-right defult-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-xs-12" style="">
                  <div class="people-row">
                    <div class="row">
                        <div class="col-lg-1 pull-right">
                            <img class="s-profile" style="margin-top: 0;padding-left: 1em" src="../assets/site/img/s-profile.png">
                        </div>
                        <div class="col-lg-11">
                            <h5 class="bolder pull-right">کارشناس پشتیبانی</h5>
                            <h6 class="pull-left"><i class="fa fa-calendar" aria-hidden="true"></i> 1395/1/16</h6>
                            <p class="pull-right defult-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                        </div>
                    </div>
                    </div>
                </div>
                            <div class="form-group col-xs-12 description" style="width:98%">
                  <textarea class="form-control" id="description" placeholder="متن درخواست"></textarea>
               </div>
               <div class="col-xs-12" style="padding-bottom: 2em">
                   <button type="submit" class="btn btn-default submit-button pull-left"> ارسال نظر </button>
                 </div>
            </div>

        </div>
    </div>
</div>

@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/send-ticket.css') }}
@endpush
@push('js')

@endpush