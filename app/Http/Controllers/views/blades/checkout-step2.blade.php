@extends('views.layouts.site.master')
@section('title','سبد خرید')

@section('content')
<div class="red_box">
    <div class="container">
        <span> سبد خرید  </span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>
<div class="bg-color">
    <div style="padding-top: 1em;padding-bottom: 1em">
        <div class="container">
            <span> خانه </span>
            <span> &gt; </span>
            <span> سبد خرید </span>
        </div>
    </div>
    <div class="buy-status">
        <div class="container">
            <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                <div class="row">
                    <div class="col-sm-5 col-xs-12 padding-lr-0">
                        <div class="row">
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/info-pay.png">
                                <p class="">اطلاعات پرداخت</p>
                            </div>
                            <div class="col-xs-2 padding-top-2 padding-lr-0">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                            </div>
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/bag-circle.png">
                                <p class="">بازبینی سفارش</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                    </div>
                    <div class="col-sm-5 col-xs-12 padding-lr-0">
                        <div class="row">
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/info-product.png">
                                <p class="">اطلاعات ارسال سفارش</p>
                            </div>
                            <div class="col-xs-2  padding-top-2 padding-lr-0">
                                <img src="../assets/site/img/green-circle.png">
                                <img src="../assets/site/img/green-circle.png">
                                <img src="../assets/site/img/green-circle.png">
                            </div>
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/ok-circle.png">
                                <p class="green_text">ورود به فروشگاه</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="form" class="container">
        <div class="row">
            <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">
                <div style="border-bottom :1px solid #ebebeb;padding-bottom: 1em">
                    <img src="../assets/site/img/left-side.png"><span style="font-weight: bolder;"> انتخاب ادرس </span>
                    <a class="pull-left" href=""><img src="../assets/site/img/green-plus.png"><span class="green_text"> افزودن ادرس جدید </span> </a>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    @for ($i = 0; $i
                    < 3; $i++) <div class="col-sm-4 col-xs-12 pull-right">

                        <div class="radio" style="text-align: -webkit-right;">
                            <label>
                                <input type="radio" name="radio-address" value="" style="float: right;">

                                <span class="cr" style="float: initial;margin-left: 1em;"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                                <P class="red_text bolder padding-bottom-05">رضا رضایی</P>
                                <p class="defult-text padding-bottom-02">خراسان رضوی مشهد بلوار هاشمیه</p>
                                <p class="defult-text padding-bottom-02">کد پستی:4444444444</p>
                                <p class="defult-text">شماره تماس:5555555555</p>
                            </label>
                        </div>
                        <!--
          <div class="radio radio-primary">
            <input type="radio" name="radio1" id="radio1" value="option1">
            <label for="radio1">
             <P class="red_text bolder padding-bottom-05">رضا رضایی</P>
             <p class="defult-text padding-bottom-02">خراسان رضوی مشهد بلوار هاشمیه</p>
             <p class="defult-text padding-bottom-02">کد پستی:4444444444</p>
             <p class="defult-text">شماره تماس:5555555555</p>
            </label>
           </div>
           -->
                </div>
                @endfor
            </div>
        </div>
        <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">
            <div style="border-bottom :1px solid #ebebeb;padding-bottom: 1em">
                <img src="../assets/site/img/left-side.png"><span class="bolder"> شیوه ارسال </span>
            </div>
        </div>

        <div class="col-xs-12">
            <div class="row">

                <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                    <div class="radio" style="text-align: -webkit-right;">
                        <label>
                            <input type="radio" name="radio-pay" value="" style="float: right;">

                            <span class="cr" style="float: initial;margin-left: 1em;"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <P class="red_text bolder padding-bottom-05">پست پیشتاز</P>
                            <p class="defult-text">هزینه ارسال 7200</p>
                        </label>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                    <div class="radio" style="text-align: -webkit-right;">
                        <label>
                            <input type="radio" name="radio-pay" value="" style="float: right;">

                            <span class="cr" style="float: initial;margin-left: 1em;"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <P class="red_text bolder padding-bottom-05">پست پیشتاز</P>

                            <p class="defult-text">هزینه ارسال 7200</p>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">
            <div style="border-bottom :1px solid #ebebeb;padding-bottom: 1em">
                <img style="padding-left: 3px" src="../assets/site/img/left-side.png"><span class="bolder">شخص حقوقی هستید؟</span>
            </div>
        </div>
        <div class="col-xs-12 text-right padding-top-2 padding-bottom-1">
            <div style="border:1px solid #ebebeb;background-color: #fcfcfc">
                <div style="padding: 1em">
                    <i class="fa fa-pencil-square-o red_text" aria-hidden="true"></i>
                    <span class="defult-text">چنانچه تمایل دارید فاکتور این سفارش به نام شخص حقوقی ثبت شود کافی است <a href="" class="red_text">اطلاعات حقوقی خود را تکمیل کنید</a></span>
                </div>
            </div>
            <div class="factor">
                
                <span class="pull-right"><i class="fa fa-angle-double-left" aria-hidden="true"></i> ایا مایل هستید به همراه این سفارش فاکتور ارسال شود ؟</span>
                <div class="radio2-res pull-right">
                     <div class="radio2 pull-right" style="text-align: -webkit-right;">
                        <label>
                            <input type="radio" name="radio-pay" value="" style="float: right;">

                            <span class="cr" style="float: initial;margin-left: 1em;"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <span style="position: relative;top: -5px;"> بله </span>
                        </label>
                    </div>
                    <div class="radio2 pull-right" style="text-align: -webkit-right;">
                        <label>
                            <input type="radio" name="radio-pay" value="" style="float: right;">

                            <span class="cr" style="float: initial;margin-left: 1em;"><i class="cr-icon glyphicon glyphicon-ok-sign"></i></span>
                            <span style="position: relative;top: -5px;"> خیر </span>
                        </label>
                    </div>
                    </div>
                
            </div>
        </div>
        <div class="col-xs-12 button">
            <button type="submit" class="btn btn-default right pull-right col-sm-2 col-xs-7 col-sm-pull-0 col-xs-pull-2">بازگشت به سبد خرید</button>
            <button type="submit" class="btn btn-default left pull-left col-md-2 col-sm-3 col-xs-7 col-sm-push-0 col-xs-push-3">ثبت اطلاعات و ادامه خرید</button>
        </div>
    </div>
</div>
</div>
@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/checkout-step.css') }}
{{ Html::style('assets/site/css/checkout-step2.css') }}
@endpush

@push('js')
@endpush