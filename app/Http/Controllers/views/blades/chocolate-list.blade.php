@extends('views.layouts.site.master')
@section('title','لیست شکلات ها')

@section('content')

    <div class="red_box">
        <div class="container">
            <span> شکلات بار  </span>
            <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
        </div>
    </div>
    <div id="choco-list">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div style="padding-top: 1em;padding-bottom: 0.5em">
                        <span> خانه </span>
                        <span> > </span>
                        <span> شکلات بار </span>
                    </div>
                </div>

                <div class="list col-lg-3 col-md-3 col-sm-3 col-xs-3 col-xxs-12 pull-right">
                    <div class="hidden-xxs">
                        <h5 class="border" style="margin-bottom: 1em"><span> <img src="../assets/site/img/red-circle.png"> </span> برندها                        
                        </h5>
                          <div class="padding-bottom">     
                                <label for="brands_2">
                                    <input id="brands_2" class="brands_2" name="brand[2]" type="checkbox" value="2">   
                                    <span style="cursor:pointer;">ساوین</span>
                                </label>
                            </div>                          
                        <div class="padding-bottom">
                          <label for="brand_4">
                            <input id="brand_4" class="brands_2" name="brand[4]" type="checkbox" value="4">        
                            <span style="cursor:pointer;">پاتریس</span>
                          </label>
                        </div>
                    </div>
                    <div class="hidden-xxs">
                        <h5 class="border" style="margin-bottom: 1em"><span> <img
                                        src="../assets/site/img/red-circle.png"> </span> دسته بندی ها </h5>
                        <div class="padding-bottom category"><a data-value="1"><img
                                            src="../assets/site/img/red-circle2.png"> 
                                            شکلات بار <span>(7)</span></a>
                        </div>
                        <div class="padding-bottom category"><a  data-value="2"><span> <img
                                            src="../assets/site/img/red-circle2.png"> </span>تابلت<span>(7)</span> </a>
                        </div>
                        <div class="padding-bottom category"><a  data-value="3"><span> <img
                                            src="../assets/site/img/red-circle2.png"> </span>کادویی و
                                پذیرایی<span>(7)</span></a></div>
                        <div class="padding-bottom category"><a  data-value="4"><span> <img
                                            src="../assets/site/img/red-circle2.png"> </span>شکلات
                                فانتزی<span>(7)</span> </a></div>
                        <div class="padding-bottom category"><a  data-value="5"><span> <img
                                            src="../assets/site/img/red-circle2.png"> </span>دراژه<span>(7)</span></a>
                        </div>
                        <div class="padding-bottom category"><a  data-value="6"><span> <img
                                            src="../assets/site/img/red-circle2.png"> </span>آبنبات<span>(0)</span></a>
                        </div>
                        <div class="padding-bottom category"><a  data-value="7"><span> <img
                                            src="../assets/site/img/red-circle2.png"> </span>تافی<span>(0)</span></a>
                        </div>

                    </div>
                    <div class="list-search" style="padding-bottom: 1em">
                        <p class="border align-right"><span><img src="../assets/site/img/red-circle.png"></span> جستجو
                        </p>
                        <form>
                            <div id="custom-search-input" method="get">
                                <div class="input-group">
                                    <input type="text" name="search" class="  search-query form-control"
                                           placeholder="جستجو"/>
                                    <span class="input-group-btn">
                                    <button class="btn btn-danger" type="submit">
                                        <span class=" glyphicon glyphicon-search"></span>
                                </button>
                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
    
                    <div class="range-slider price-range">
                        <p class="border align-right"><span><img src="../assets/site/img/red-circle.png"></span> رنج
                            قیمت به (تومان) </p>
                        <div style="padding-top: 3em;width: 90%;margin: auto">
                            <input id="price-range" type="text" value="100,10000" data-slider-min="100"
                                   data-slider-max="10000" data-slider-step="10" data-slider-value="[100,10000]"
                                   data-value="100,10000" style="">
                        </div>
                    </div>


                    {!! Form::open(['method' => 'GET','class' => 'search']) !!}
                    {!! Form::hidden('price_from', null, ['id' => 'from']) !!}
                    {!! Form::hidden('price_to', null, ['id' => 'to']) !!}
                    {{ Form::hidden('sort',request('sort'), ['id' => 'sort']) }}
                    {{ Form::hidden('status',request('status'), ['id' => 'status']) }}
                    {{ Form::hidden('category',request('category'), ['id' => 'category']) }}
                    {{ Form::hidden('brand',request('brand'), ['id' => 'brand']) }}
                </div>

                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 col-xxs-12 products">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12 text-left"
                             style="padding-left: 3px; padding-bottom: 2.5em;">
          
                            <select class="selectpicker" id="sort-select" name="sort-select" tabindex="-98">
                                <option value="1">جدید ترین</option>
                                <option value="2">پرفروش ترین ها</option>
                                <option value="3">ارزان ترین</option>
                                <option value="4">گران ترین</option>
                            </select>
                        </div>


                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12"
                             style="padding-right:5px; padding-bottom: 2.5em; ">
                            <h5 style="color:#ff5e5e;font-weight: bold"> نمایش 15 محصول از 40 محصول یافت شده </h5>
                        </div>
                        <div class="product-list">
                            @for ($i = 0; $i
                            < 15; $i++)
                                <div class="product product-padding col-lg-2 col-md-2 col-sm-3 col-xs-4"
                                     style='float: right;'>
                                    <div class="thumbnail">
                                        <a href="" target="_blank">
                                            <img src="../assets/site/img/choco-select.png" alt="Lights"
                                                 class="img-responsive" style="width:100%">
                                            <div class="caption" style="font-weight: 700">
                                                <p style="color:#989898"> شکلات بالوتین </p>
                                                <p style="color:#0ba521"><span> 25 </span> تومان </p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="overlay">
                                    </div>
                                    <div class="overlay-content">
                                        <a href="">
                                            <img src="../assets/site/img/shopingbag.png">
                                            <p> افزودن به سبد خرید </p>
                                        </a>
                                    </div>
                                </div>
                            @endfor
                        </div>
                        <div class="col-xs-12" style="padding:2em 5px 2em 5px">
                            <div class="text-left col-sm-6 col-xs-12 col-lg-push-0 col-md-push-1 pages-number">
                                list of page
                            </div>
                            <div class="col-sm-6 col-xs-12 check-sort">
                                <span> نمایش بر اساس </span>
                                <input class="status" type="checkbox" checked>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('css')
{{ Html::style('assets/site/plugins/bootstrap-slider/css/bootstrap-slider.min.css') }}
{{Html::style('assets/site/plugins/bootstrap-select/bootstrap-select.min.css')}}

{{ Html::style('assets/site/css/chocolate-list.css') }}
@endpush

@push('js')
{{Html::script('assets/site/plugins/bootstrap-select/bootstrap-select.min.js')}}
{{ Html::script('assets/site/plugins/bootstrap-slider/js/bootstrap-slider.min.js') }}
{{Html::script('assets/site/plugins/bootstrap-checkbox/bootstrap-checkbox.min.js')}}


<script>

    $(document).ready(function () {
        $('.brands_2').on('change',function(){
            var values = $('#price-range').val().split(',');
            $('#from').val(values[0]);
            $('#to').val(values[1]);
            if ($('#sort').val() == "") {
                $('#sort').val('1');
            }
            if ($('#status').val() == "") {
                $('#status').val('all');
            }
          if ($(this).is(':checked')) {
            $('#brand').val($(this).val());

          }
          $('.search').submit();
        })

        $('.status').checkboxpicker({
            offLabel: 'موجود',
            onLabel: 'همه',
            onActiveCls: 'btn-danger',
            toggleKeyCodes: [13, 32]
        });
        $('.status').checkboxpicker().on('change', function () {

            var url = $(this)[0]['baseURI'];
            var values = $('#price-range').val().split(',');
            $('#from').val(values[0]);
            $('#to').val(values[1]);
            if ($('#sort').val() == "") {
                $('#sort').val('1');
            }

            if ($(this)[0]['baseURI'].indexOf('status=available') > 0) {
                $('#status').val('all');
                $('.search').submit();
            }
            else {
                $('#status').val('available');
                console.log($(this)[0]['baseURI'].split('&'));
                $('.search').submit();
            }

        });


        var mySlider = new Slider("#price-range",
            {tooltip: 'always', tooltip_split: true});


        $(".min-slider-handle, .max-slider-handle").on("mouseup", function (slideEvt) {
            var values = $('#price-range').val().split(',');
            $('#from').val(values[0]);
            $('#to').val(values[1]);
            if ($('#sort').val() == "") {
                $('#sort').val('1');
            }
            if ($('#status').val() == "") {
                $('#status').val('all');
            }
            $('.search').submit();
        });

        $('#from').on('change', function (e) {
            var value = mySlider.getValue();
            value[0] = parseInt($(this).val());
            mySlider.setValue(value);
            if ($('#sort').val() == "") {
                $('#sort').val('1');
            }
            if ($('#status').val() == "") {
                $('#status').val('all');
            }
            $('.search').submit();
        });

        $('#to').on('change', function (e) {
            var value = mySlider.getValue();
            value[1] = parseInt($(this).val());
            mySlider.setValue(value);
            if ($('#sort').val() == "") {
                $('#sort').val('1');
            }
            if ($('#status').val() == "") {
                $('#status').val('all');
            }
            $('.search').submit();
        });


        $('#list-view').on('click', function (e) {
            $(this).toggleClass('active');
            $('#large-view').toggleClass('active');

        });

        $('#large-view').on('click', function (e) {
            $(this).toggleClass('active');
            $('#list-view').toggleClass('active');
            $('#profile').tab('show');
        });

        $('select[name="sort-select"],.brands').on('change', function () {
            $('#sort').val($(this).val());
            var values = $('#price-range').val().split(',');
            if ($('#status').val() == "") {
                $('#status').val('all');
            }
            $('#from').val(values[0]);
            $('#to').val(values[1]);
            $('.search').submit();
        });

        $('.category a').click(function(){
            if ($('#sort').val() == "") {
                $('#sort').val('1');
            }
            var values = $('#price-range').val().split(',');
          $('#category').val(($(this).data('value')));
          if ($('#status').val() == "") {
                $('#status').val('all');
            }
            $('#from').val(values[0]);
            $('#to').val(values[1]);
          $('.search').submit();

        });

    });

</script>
@endpush
