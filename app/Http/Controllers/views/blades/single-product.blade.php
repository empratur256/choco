@extends('views.layouts.site.master')
@section('title','مشخصات محصول')
@section('content')
<div class="red_box">
    <div class="container">
        <span>نوتلا 350 گرمی لهستان</span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>
<div class="single-product">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="padding-top: 1em;padding-bottom: 1em">
                <span> خانه </span>
                <span> &gt; </span>
                <span> شکلات بار </span>
                <span> &gt; </span>
                <span> شکلات صبحانه </span>
            </div>
            <div class="col-lg-push-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 product-img text-center">
                <div>
                    <img class="img-responsive" src="../assets/site/img/single-product.png">
                </div>
            </div>
            <div class="col-lg-pull-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 product-details">
                <div class="details pull-right">
                    <div style="padding-top: 2em;padding-bottom: 14.2em">
                        <h4 style="padding-bottom: 1em;font-weight: bold;color: #727272;">نوتلا 350 گرمی لهستان</h4>
                        <div style="padding-bottom: 1em">
                            <i style="color:gold" class="fa fa-star" aria-hidden="true"></i>
                            <i style="color:gold" class="fa fa-star" aria-hidden="true"></i>
                            <i style="color:gold" class="fa fa-star" aria-hidden="true"></i>
                            <i style="color:#e1e1e1" class="fa fa-star" aria-hidden="true"></i>
                            <i style="color:#e1e1e1" class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <p style="padding-bottom: 1em"> لطفا برای ثبت امتیاز، <a style="color:rgb(255, 82, 83);" href=""> وارد حساب کاربری خود شوید </a> </p>
                        <p style="padding-bottom: 1em;color:#727272;font-weight: bolder;">وضعیت:<span style="color:#40ab34">موجود</span></p>
                        <P style="padding-bottom: 1em;color:#40ab34">8000 تومان</P>
                        <h5 style="padding-bottom: 0.5em; font-weight: bolder; color: #727272;"> معرفی کوتاه </h5>
                        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.</p>
                    </div>
                    <div class="pull-right" style="padding-bottom: 1em">
                        <div class="pull-right number-buy">
                            <span style=""> تعداد </span>
                            <select class="" id="number-select" name="number-select" tabindex="-98">
                                <option value="1">1عدد</option>
                                <option value="2">2عدد</option>
                                <option value="3">3عدد</option>
                                <option value="4">4عدد</option>
                            </select>
                        </div>
                        <div class="pull-right button-buy">
                            <button type="button" class="btn btn-default">افزودن به سبد خرید</button>
                        </div>
                        <div class="like pull-right">
                            <a href="">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        </div>

                        <div class="share pull-right">
                            <a href="">
                                <i class="fa fa-share-alt" aria-hidden="true"></i>
                            </a>
                        </div>
                        <div class="pull-right button-buy2">
                            <button type="button" class="btn btn-default">افزودن به سبد خرید</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-9 product-tabs">
            <!-- Nav tabs --><div class="card">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">درباره محصول</a></li>
                                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">جدول مشخصات</a></li>
                                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">دیدگاه های کاربران</a></li>
                                
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="home">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="profile">لورم ایپسوم متن ساختگی با تولید سادگی 22نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد.
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="messages">
                                        <div class="col-xs-12">
                                        <input type="radio" id="comment5" name="rate" value="5">
                                        <label for="comment5" title="5 ستاره">5 ستاره</label>
                                        <input type="radio" id="comment5" name="rate" value="4">
                                        <label for="comment5" title="5 ستاره">4 ستاره</label>
                                        <input type="radio" id="comment5" name="rate" value="3">
                                        <label for="comment5" title="5 ستاره">3 ستاره</label>
                                        <input type="radio" id="comment5" name="rate" value="2">
                                        <label for="comment5" title="5 ستاره">2 ستاره</label>
                                        <input type="radio" id="comment5" name="rate" value="1">
                                        <label for="comment5" title="5 ستاره">1 ستاره</label>
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div id="#similar" class="similar-products">
                                      <h5 class="title"> محصولات مشابه </h5>
                                       <div class="col-lg-12 products text-center">
                                         @for ($i = 0; $i< 4; $i++)
                                            <div class="col-lg-3 product">
                                              <div class="border">
                                              </div>
                                              <div class="overlay">
                                              </div>
                                              <div class="overlay-content">
                                        <a href="">
                                            <img src="../assets/site/img/shopingbag.png">
                                            <p> افزودن به سبد خرید </p>
                                        </a>
                                    </div>
                                              <img class="img-responsive" src="../assets/site/img/similar-product.png">
                                              <p style="padding-top: 2em"> کورن فلکس کوکو پارس</p>
                                              <p style="padding-top: 1em"> 800 تومان </p>
                                            </div>
                                         @endfor
                                       </div>
                                    </div>
            </div>
            <div class="col-lg-3 other-products" style="">

                <h5 class="title"> محصولات دیگر این برند </h5>
                <div class="pull-right" style="background-color: white">
                @for ($i = 0; $i< 5; $i++)
                <div class="pull-right product" style="">
                    <a class="pull-right" style="padding-left:1em" href="#">
                        <img class="pull-right" src="../assets/site/img/buy-pic.png">
                        <p class="pull-right comment-buy" style="color:#666666">شکلات صبحانه نوتلا 750 گرمی المان</p>
                    <br>
                    <span class="pull-right" style="color:#40ab34;padding-top: 1em">800 تومان</span>
                    </a>
                    
                    
                </div>
                 @endfor
                 </div>
            </div>

        </div>
    </div>
</div>
@endsection
@push('css')
   {{Html::style('assets/site/plugins/bootstrap-select/bootstrap-select.min.css')}}
   {{ Html::style('assets/site/css/single-product.css') }}

@endpush
@push('js')
{{Html::script('assets/site/plugins/bootstrap-select/bootstrap-select.min.js')}}
@endpush