@extends('views.layouts.site.master')
@section('title','سبد خرید')

@section('content')
<div class="red_box">
    <div class="container">
        <span> سبد خرید  </span>
        <p> در این بخش میتوانید محصولات شکلات بار رو ببینید </p>
    </div>
</div>
<div class="bg-color">
    <div style="padding-top: 1em;padding-bottom: 1em">
        <div class="container">
            <span> خانه </span>
            <span> &gt; </span>
            <span> سبد خرید </span>
        </div>
    </div>
    <div class="buy-status">
        <div class="container">
            <div class="col-lg-8 col-md-9 col-sm-10 col-xs-12 col-centered text-center">
                <div class="row">
                    <div class="col-sm-5 col-xs-12 padding-lr-0">
                        <div class="row">
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/info-pay.png">
                                <p class="">اطلاعات پرداخت</p>
                            </div>
                            <div class="col-xs-2 padding-top-2 padding-lr-0">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                            </div>
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/bag-circle.png">
                                <p class="">بازبینی سفارش</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-12  padding-top-2 padding-lr-0">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                        <img class="hidden-xs" src="../assets/site/img/circle.png">
                    </div>
                    <div class="col-sm-5 col-xs-12 padding-lr-0">
                        <div class="row">
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/info-product.png">
                                <p class="">اطلاعات ارسال سفارش</p>
                            </div>
                            <div class="col-xs-2  padding-top-2 padding-lr-0">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                                <img src="../assets/site/img/circle.png">
                            </div>
                            <div class="col-xs-5 padding-lr-0">
                                <img src="../assets/site/img/user.png">
                                <p class="">ورود به فروشگاه</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="login-form" class="container login-form">
    <div class="col-lg-7 col-md-8 col-sm-8 col-xs-12 col-sm-push-2 ">
    <div  style="background-color: white;float:right">
      <div class="col-xs-12">
        <h5 class="text-center bolder" style="
    padding-top: 2em;
    padding-bottom: 2em;
">ورود به حساب کاربری</h5>
      </div>
      <form>
         <div class="col-lg-6 col-sm-8 col-xs-10 col-lg-push-3 col-sm-push-2 col-xs-push-1 form-group">
                                <label for="email">ایمیل شما</label>
                                <input class="form-control" name="email" type="text">
                                <span class="glyphicon glyphicon-envelope defult-icon-color"></span>
                            </div>
                            <div class="col-lg-6 col-sm-8 col-xs-10 col-lg-push-3 col-sm-push-2 col-xs-push-1 form-group">
                                <label for="telephone">رمز عبور</label>
                                <input class="form-control" name="password" type="password" value="">
                                <span class="glyphicon glyphicon-lock defult-icon-color"></span>
                            </div>
                           <div class="col-xs-12 text-center" style="margin-top: 2em;margin-bottom: 2em;">
                            <button type="submit" class="btn btn-default">ورود</button>
                            <p style="padding-top: 2em;" ><a class="red_text" style="padding-bottom: 0.5em;border-bottom:1px solid #ebebeb" href="">رمز عبور خود را گم یا فراموش کرده اید؟</a></p>
                            </div>
                            </form>

    </div>
                                <div class="col-xs-12 text-center register" style="">
                              <span>عضو نیستید؟</span><a style="color:#33beff" href="">عضو شوید</a>
                            </div>
    </div>
    </div>
</div>
@endsection
@push('css')
{{ Html::style('assets/site/css/public.css') }}
{{ Html::style('assets/site/css/checkout-step.css') }}
{{ Html::style('assets/site/css/checkout-step1.css') }}
@endpush
@push('js')
@endpush