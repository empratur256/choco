@if(!(request()->is(config('site.admin').'/*') ||  request()->is(config('site.admin'))))
    @if($errors->any())
        {{ Html::alerts($errors) }}
    @else
        {{ Html::alerts() }}
    @endif
@endif