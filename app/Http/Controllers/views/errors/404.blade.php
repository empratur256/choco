@extends('views.layouts.site.master')
@section('title', trans('site.404.home'))
@section('content')

    <div class="red_box">
        <div class="container">
            <h1> @lang('site.404.home')  </h1>
            <div class="divider-title"></div>
            <p> @lang('site.404.not_fuond') </p>
        </div>
    </div>
    <div style="padding-top: 1em;padding-bottom: 1em">


        <div class="shop-basket-page">
            <div class="container padding-top-2">

                <div class="row">
                    <div class="col-xs-12 text-center">
                        <div class="body-panel text-center">
                            <img class="img-responsive" style="margin:auto"
                                 src="{{ asset('assets/site/img/404vector.png') }}" alt="404">
                            <p class="defult-text"
                               style="margin: 25px 0;font-size: 18px;font-weight: bold; padding-top:2em">
                                @lang('site.404.not_fuond')
                            </p>
                            <a href="{{action('HomeController@index')}}" class="btn btn-danger">
                               @lang('site.404.return')
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('css')
    {{ Html::style('assets/site/css/public.css') }}
@endpush