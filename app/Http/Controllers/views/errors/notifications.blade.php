@if($errors->any())
    {{ Html::notifications($errors) }}
@else
    {{ Html::notifications() }}
@endif