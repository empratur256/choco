@extends('views.layouts.site.master')
@section('title',$setting->title)
@section('index-page',true)
@section('content')
    @if($routine->count() != 0)
        <div class="BGC_ga customChoco" id="second-section">
            <div class="ch_special container padding_special">
                <div class="row">
                <div class="align_right m-b-20 flex border">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <img class="img-responsive icon-img" src="{{asset('assets/site/img/fancy-mark2.png')}}">
                        <div class="desc-section">
                        <h1> @lang('site.index.chocolat') <span class="hot">@lang('site.index.customer') </span></h1>
                        <p> @lang('site.index.chocolat_top') </p>
                        </div>
                    </div>
                </div>
                <div>
                    <ul style="padding: 0 ; width: 100%; border: 0;" class="nav nav-tabs" role="tablist">

                        @foreach($routine as $item)
                            <li role="presentation" class="pull-right {{$loop->first ? 'active' : '' }}">
                                <a href="#{{$item->id}}"
                                   aria-controls="home"
                                   role="tab"
                                   data-toggle="tab">{{$item->title}}</a>
                            </li>
                        @endforeach
                        <a class="btn btn-danger pull-left"
                           href="{{action('CustomOrder\Controllers\Site\OrderController@portfolio')}}"
                           type="button"> @lang('site.index.cast') </a>
                    </ul>
                    <div class="tab-content">
                        @foreach($routine as $item)
                            <div role="tabpanel" class="tab-pane fade in {{ $loop->first ? 'active' : '' }}"
                                 id="{{ $item->id }}">
                                <div class="space-2-30 row">
                                    <div class='container'>
                                        <div class="col-md-4">
                                            <img class="img-responsive one"
                                                 src="{{asset('uploads/routine/350xauto-'.$item->picture)}}">
                                        </div>
                                        <div class="col-md-8 description-order">
                                            <p>{!! $item->description !!}</p>
                                            <a class="btn btn-danger space-2-20"
                                               href="{{action('CustomOrder\Controllers\Site\OrderController@setRoutine',$item->id)}}"
                                               type="button"> @lang('site.index.custom') </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                </div>
            </div>
        </div>
    @endif
    @if($discounts->count() != 0)
        <div class="fancy_off container">
            <section class="row padding_special">
                <div class="col-md-12 flex border">


                    <div class="col-md-12">
                        <img class="icon-img img-responsive inline" src="{{asset('assets/site/img/fancy-mark2.png')}}">
                        <div class="desc-section">
                        <h1> @lang('site.index.discounts') <span class="hot"> @lang('site.fancy') </span></h1>
                        <p> @lang('site.index.discounts_text') </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 products padding_top_3 align_center">
                    <div class="row owl-carousel offProductsCarousel">
                    @foreach($discounts as $item)
                        <div id="one" class="product owl-item one" data-countdown="{{ $item->discount_time_start }}">
                            <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}">
                            <section class="card">
                                    <p class="discountPercent"> {{$item->discount.($item->discount_type == 'percent' ? '%' : trans('site.cur'))}} </p>
                                    <img src="{{ asset('uploads/product/'.$item->id.'/175x175-'.$item->picture) }}"
                                             class="card-img-top">
                                  <div class="card-block">
                                      <h4 class="card-title text-muted">{{$item->title}}</h4>
                                      {{--<p class="card-text"><strike>{{number_format($item->price)}} @lang('site.currency')</strike></p>--}}
                                      <p class="card-text text-success">{{number_format(finalPrice($item))}} @lang('site.currency')</p>
                                      <div class="off_counter">
                                          <svg class="secounds" width="33" height="35" xmlns="http://www.w3.org/2000/svg">
                                              <g>
                                                  <title>@lang('site.secend')</title>
                                                  <circle id="circle" r="13" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                                  <circle id="circle" class="circle_animation" r="13" cy="18" cx="18"
                                                          stroke-width="2" stroke="rgb(255, 82, 83)" fill="none"/>
                                                  <circle id="circle" r="12" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                                  <text id="secounds_str" class="secounds_str"
                                                        data-countdown="{{ $item->discount_time_end }}"
                                                        x="50%" y="50%" text-anchor="middle" transform="rotate(90,17,19)"
                                                        stroke="#51c5cf" stroke-width="0.5px" dy=".3em">0
                                                  </text>
                                              </g>
                                          </svg>
                                          <svg class="minutes" width="33" height="35" xmlns="http://www.w3.org/2000/svg">
                                              <g>
                                                  <title>@lang('site.min')</title>
                                                  <circle id="circle" r="13" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                                  <circle id="circle" class="circle_animation" r="13" cy="18" cx="18"
                                                          stroke-width="2" stroke="rgb(255, 82, 83)" fill="none"/>
                                                  <circle id="circle" r="12" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                                  <text id="minutes_str" class="minutes_str"
                                                        data-countdown="{{ $item->discount_time_end }}"
                                                        x="50%" y="50%" text-anchor="middle" transform="rotate(90,17,19)"
                                                        stroke="#51c5cf" stroke-width="0.5px" dy=".3em">0
                                                  </text>
                                              </g>
                                          </svg>
                                          <svg class="hours" width="33" height="35" xmlns="http://www.w3.org/2000/svg">
                                              <g>
                                                  <title>@lang('site.houer')</title>
                                                  <circle id="circle" r="13" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                                  <circle id="circle" class="circle_animation" r="13" cy="18" cx="18"
                                                          stroke-width="2" stroke="rgb(255, 82, 83)" fill="none"/>
                                                  <circle id="circle" r="12" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                                  <text id="hours_str" class="hours_str"
                                                        data-countdown="{{ $item->discount_time_end }}" x="50%"
                                                        y="50%" text-anchor="middle" transform="rotate(90,17,19)"
                                                        stroke="#51c5cf" stroke-width="0.5px" dy=".3em">0
                                                  </text>
                                              </g>
                                          </svg>
                                          <svg class="days" width="33" height="35" xmlns="http://www.w3.org/2000/svg">
                                              <g>
                                                  <title>  @lang('site.day')</title>
                                                  <circle id="circle" r="13" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                                  <text id="days_str" data-countdown="{{ $item->discount_time_end }}"
                                                        class="days_str" x="50%"
                                                        y="50%" text-anchor="middle" transform="rotate(90,17,19)"
                                                        stroke="#51c5cf" stroke-width="0.5px" dy=".3em">0
                                                  </text>
                                                  <circle id="circle" class="circle_animation" r="12" cy="18" cx="18"
                                                          stroke-width="2" stroke="rgb(255, 82, 83)" fill="none"/>
                                                  <circle id="circle" r="12" cy="18" cx="18" stroke-width="1"
                                                          stroke="rgba(128, 128, 128, 0.43)" fill="none"/>
                                              </g>
                                          </svg>




                                      </div>

                                      <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}"
                                             class="buy_for_off btn btn-success"> <i class="flaticon-shopping-cart" style="font-size: 16px"></i> @lang('site.products.add_to_cart')</a>
                                  </div>
                            </section>
                            </a>
                        </div>
                    @endforeach
                    </div>
                </div>
            </section>
        </div>
    @endif
    @if($newest->count() != 0)
        <div class="newFancyCategory">
            <div class=" container">
                <section class="row padding_special">
                    <div class="newProducts col-md-10 col-xs-12 pull-right">
                        <div class="align_right flex border">
                            <div class="col-md-12">
                                <img class="img-responsive icon-img" src="{{asset('assets/site/img/fancy-mark2.png')}}">
                                <div class="desc-section">
                                <h1> @lang('site.index.news') <span class="hot"> @lang('site.fancy')  </span></h1>
                                <p> @lang('site.index.news_top') </p>
                                </div>
                            </div>
                        </div>
                        <div class="newProductsCarousel padding_top_3 " style="opacity: 1; display: block;">
                            <div class="owl-carousel">
                            @foreach($newest as $item)
                                <div class="product">
                                    <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}">
                                        <section class="card">
                                            <img src="{{ asset('uploads/product/'.$item->id.'/175x175-'.$item->picture) }}"
                                                 class="card-img-top">
                                            <div class="card-block">
                                                <h3 class="card-title text-muted">{{$item->title}}</h3>
                                                <p class="card-text text-success">{{number_format(finalPrice($item))}} @lang('site.currency') </p>
                                                {{--<span  class="buy_for_off btn btn-success">@lang('site.products.add_to_cart')</span>--}}
                                            </div>
                                        </section>
                                    </a>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-12 pull-left ">
                        <p><i class="flaticon-left-arrow-1"
                                                          style="font-size: 12px;"></i> @lang('site.index.categories')
 <span class="text-danger"> @lang('site.index.chocolat') </span></p>
                        <ul>
                            <div class="li_circle">
                                @foreach($categories as $category)
                                    <li class="categoryItem"><a href="{{action('Product\Controllers\Site\ProductController@index',[$category->id, utf8_slug($category->name)])}}">{{ $category->name_fa }}</a></li>
                                @endforeach
                            </div>
                            <div class="categoryItem"><a class="seeAll text-danger"
                                                         href="{{action('Product\Controllers\Site\ProductController@index')}}">
                                    <i class="fa fa-plus"></i> @lang('site.index.show_all') </a></div>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    @endif
    @if($bestsellers->count() != 0)
        <div class="container">
            <section class="row padding_special">
                <div class="col-md-10 col-sm-9 favoriteProducts">
                    <div class="align_right flex border">
                        <div class="col-md-12 col-sm-1 col-xs-12">
                            <img class="img-responsive icon-img" src="{{asset('assets/site/img/fancy-mark2.png')}}">
                            <div class="desc-section">

                            <h1> @lang('site.index.bestsellers') <span class="hot"> @lang('site.fancy') </span></h1>
                            <p> @lang('site.index.bestsellers_top') </p>
                            </div>
                        </div>

                    </div>
                    <div class="padding_top_3 bestSellerCarousel align_center">
                        <div class="owl-carousel">
                        @foreach($bestsellers as $item)
                            <div class="product">
                                <a href="{!! action('Product\Controllers\Site\ProductController@show',[$item->id,utf8_slug($item->title)]) !!}">
                                    <section class="card">
                                        <img src="{{ asset('uploads/product/'.$item->id.'/175x175-'.$item->picture) }}"
                                             class="card-img-top ">
                                        <div class="card-block">
                                            <h3 class="card-title text-muted">{{$item->title}}</h3>
                                            <p class="card-text text-success">{{number_format(finalPrice($item))}} @lang('site.currency') </p>
                                            {{--<span  class="buy_for_off btn btn-success">@lang('site.products.add_to_cart')</span>--}}
                                        </div>
                                    </section>
                                </a>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-3">
                    <img src="{{asset('uploads/home_picture.png')}}" class="img-responsive favoriteBanner">
                </div>
            </section>
        </div>
    @endif
@endsection

@push('css')
    {{ Html::style('assets/site/css/main.css') }}
    {{ Html::style('assets/site/css/public.css') }}
    <style type="text/css">
        .start {
            background-image: url("{{ asset('uploads/'.$setting->main_header_picture) }}");
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            height: 100%;
        }
    </style>
@endpush
@push('js')
    {{ Html::script('assets/site/js/countdown/jquery.countdown.js') }}
    {{ Html::script('assets/site/js/nav.js') }}
    {{ Html::script('assets/site/js/main.js') }}
    {{Html::script('assets/site/plugins/bootstrap-select/bootstrap-select.min.js')}}
    <script>

        $(document).ready(function() {
            $('.offProductsCarousel.owl-carousel').owlCarousel({
                rtl: true,
                loop: false,
                dots: true,
                margin: 10,
                nav: false,
                items: 6,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 6
                    }
                }
            });
            $('.newProductsCarousel .owl-carousel').owlCarousel({
                rtl: true,
                loop: false,
                dots: true,
                margin: 10,
                nav:false,
                items: 5,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            })
            $('.bestSellerCarousel .owl-carousel').owlCarousel({
                rtl: true,
                loop: false,
                margin: 10,
                items: 5,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                }
            });
            $(function(){
//                $(".dropdown").hover(
//                    function() {
//                        $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
//                        $(this).toggleClass('open');
//
//                    },
//                    function() {
//                        $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
//                        $(this).toggleClass('open');
//
//                    });
            });
        });




    </script>
@endpush