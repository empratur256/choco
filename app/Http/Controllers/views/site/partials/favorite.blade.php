<div class="favorites with-border" style="background-color: white">
    <div class="header">
        @if(count($favorites) > 2)
            <a href="{!! action('User\Controllers\Site\DashboardController@favorite') !!}"
               class="grayish-text pull-left">@lang('site.products.more')</a>
            @php $favorites->pop() @endphp
        @endif
    </div>
    <div class="table-responsive" style="">
        <table class="table table-bordred table-striped">
            <thead>
            <tr>
                <th class="text-right defult-text" style=""></th>
                <th class="pull-right text-right defult-text"
                    style="padding-right:1.2em">@lang('site.products.title')
                </th>
                <th class="text-right defult-text"
                    style="padding-right:1.2em">@lang('site.orders.final_price')
                    (@lang('site.currency'))
                </th>
                <th class="text-right defult-text"
                    style="padding-right:1.2em">@lang('site.status')</th>
                <th class="text-right defult-text"
                    style="padding-right:1.2em"></th>
            </tr>
            </thead>
            <tbody>
            @forelse($favorites as $favorite)

                <tr class="item @if(!$loop->last) border-bottom @endif">
                    <td>
                        <a href="{!! action('Product\Controllers\Site\ProductController@show',[$favorite->id,utf8_slug($favorite->title)]) !!}"><img style="width: 20%;" src="{{ asset('uploads/product/'.$favorite->id.'/111x111-'.$favorite->picture) }}"
                             alt="item picture"></a>
                    </td>
                    <td class="pull-right">
                        {{str_limit($favorite->title,60)}}
                    </td>
                    <td>
                        @if(finalPrice($favorite) > 0) {{ number_format(finalPrice($favorite)) }} @lang('site.currency') @endif
                    </td>
                    <td>
                        @if($favorite->quantity > 0) @lang('site.products.available') @else @lang('site.products.unavailable') @endif
                    </td>
                    <td>
                        {!! Form::open(['action' => 'Product\Controllers\Site\ProductController@favorite']) !!}
                        {!! Form::hidden('id',$favorite->id) !!}
                        <button type="submit" class="btn btn-danger">
                            <span class="fa fa-times"></span>
                        </button>
                        {!! Form::close() !!}
                    </td>
                </tr>

            @empty
                <tr>
                    <td colspan="6" class="text-center">@lang('site.orders.is_empty')</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>