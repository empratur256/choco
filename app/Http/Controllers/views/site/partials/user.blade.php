<div class="row bg-white">
    <a class="edit-link-profile col-md-12" href="{!! action('User\Controllers\Site\DashboardController@edit') !!}"
       class="pull-right">
        @lang('site.user.edit_profile')
    </a>
    <img class="m-profile" src="{{asset('assets/site/img/profile-m.png')}}">
    <p class="padding-top-down-1">{{ auth()->user()->name.' '.auth()->user()->family }}</p>
    <a href="{!! action('Coupon\Controllers\Site\CouponController@index') !!}">
        <p class="padding-top-down-1" style="width: 90%;margin: auto;border-bottom: 1px solid #ebebeb;">
            <i class="fa fa-usd dollar" aria-hidden="true" style=""></i>   @lang('site.my_coupon') </p>
    </a>
    <div class="col-xs-12">
        <p class="padding-top-down-1 padding-top-3 defult-text text-right">@lang('site.contact.email')</p>
        <p class="padding-top-down-1 text-right black-text bolder">{{ auth()->user()->email }}</p>
        <p class="padding-top-down-1 text-right defult-text">@lang('site.contact.phone_no')</p>
        <p class="padding-top-down-1 text-right black-text bolder">{{ auth()->user()->mobile }}</p>
        @if(!is_null($address))
            <p class="padding-top-down-1 text-right defult-text">@lang('site.address')</p>
            <p class="padding-top-down-1 text-right black-text bolder"
               style="padding-bottom: 2.5em">{{ $address->address }}</p>
        @endif
        <p class="padding-top-down-1 text-right defult-text">@lang('site.user.link')</p>
        <p class="padding-top-down-1 text-right black-text bolder" style="padding-bottom: 2.5em">
            <span class="pull-right">
                        <a href="{!! remove_http(action('User\Controllers\Site\UserController@presented',[auth()->user()->coupon_rand,auth()->user()->id])) !!}">
{!! remove_http(action('User\Controllers\Site\UserController@presented',[auth()->user()->coupon_rand,auth()->user()->id])) !!}
                        </a>
            </span>
        </p>
    </div>

</div>