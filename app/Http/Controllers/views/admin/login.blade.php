<!DOCTYPE html>
<!--[if lte IE 9]>
<html class="lte-ie9 app_theme_d" lang="fa" dir="rtl"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="fa" dir="rtl" class="app_theme_d"><!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>مدیریت</title>
    <meta name="robots" content="noindex">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>

    {!! Html::style('assets/admin/css/uikit.rtl.css') !!}
    {!! Html::style('assets/admin/css/login_page.min.css') !!}
    {!! Html::style('assets/admin/css/sedehi.css') !!}

</head>
<body class="login_page">

<div class="login_page_wrapper">
    <div class="md-card" id="login_card">
        <div class="md-card-content large-padding" id="login_form">
            <div class="login_heading">
                <div class="user_avatar"></div>
            </div>
            {!! Form::open() !!}
            <div class="uk-form-row">
                {!! Form::label('email','ایمیل') !!}
                {!! Form::text('email',null,['class' => 'md-input','id' => 'email']) !!}
            </div>
            <div class="uk-form-row">
                {!! Form::label('password','رمزعبور') !!}
                {!! Form::text('password',null,['class' => 'md-input','id' => 'password']) !!}
            </div>

            <div class="uk-margin-medium-top">
                <a href="index-2.html" class="md-btn md-btn-primary md-btn-block md-btn-large">ورود به مدیریت</a>
            </div>

            <div class="uk-margin-top">
                <a href="#" id="password_reset_show">رمزعبور را فراموش کردم!</a>
            </div>
            </form>
        </div>

        <div class="md-card-content large-padding" id="login_password_reset" style="display: none">
            <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
            <h2 class="heading_a uk-margin-large-bottom">بازیابی رمز عبور</h2>
            {!! Form::open() !!}
            <div class="uk-form-row">
                {!! Form::label('email','ایمیل') !!}
                {!! Form::text('email',null,['class' => 'md-input','id' => 'email']) !!}
            </div>
            <div class="uk-margin-medium-top">
                <button type="submit" class="md-btn md-btn-primary md-btn-block">بازیابی رمز عبور</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

</div>

{!! Html::script('assets/admin/js/common.min.js') !!}
{!! Html::script('assets/admin/js/uikit_custom.min.js') !!}
{!! Html::script('assets/admin/js/altair_admin_common.min.js') !!}
{!! Html::script('assets/admin/js/pages/login.min.js') !!}
</body>

</html>