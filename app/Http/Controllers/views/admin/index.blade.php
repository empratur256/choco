@extends('views.layouts.admin.master')
@section('title','مدیریت سایت')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium  hierarchical_show" data-uk-sortable data-uk-grid-margin>
                @if(permission('Comments.CommentsController.index'))
                    <a href="{!! action('Comments\Controllers\Admin\CommentsController@index',['confirm' => 0,'reject' => 0]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-comment uk-text-primary fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">نظر های نیاز به بررسی</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($commentCount) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
                @if(permission('Payment.PaymentController.index'))
                    <a href="{!! action('Payment\Controllers\Admin\PaymentController@index',['status' => 1]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-dollar uk-text-success fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">تراکنش های پرداخت شده</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($paymentCount) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif

                @if(permission('Contact.ContactController.index'))
                    <a href="{!! action('Contact\Controllers\Admin\ContactController@index',['unreaded' => 1]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-comments uk-text-danger fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">پیام های تماس با جدید</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($unreadedContact) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
                @if(permission('Ticket.TicketController.index'))
                    <a href="{!! action('Ticket\Controllers\Admin\TicketController@index',['answered' => 0]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-ticket uk-text fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">تیکت های بی پاسخ</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($ticketCount) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
                @if(permission('Order.OrderController.index'))
                    <a href="{!! action('Order\Controllers\Admin\OrderController@index',['confirm' => 1,'paid' => 0,'processing_stock' => 0,'ready_to_send' => 0,'delivered' => 0]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-shopping-basket uk-text-primary fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">سفارش جدید ثبت شده</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($unConfirmOrder) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
                @if(permission('Order.OrderController.index'))
                    <a href="{!! action('Order\Controllers\Admin\OrderController@index',['confirm' => 1,'paid' => 0]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-shopping-basket uk-text-success fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">سفارش پرداخت نشده</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($unPaiOrder) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
                @if(permission('Order.OrderController.index'))
                    <a href="{!! action('Order\Controllers\Admin\OrderController@index',['confirm' => 1,'processing_stock' => 1]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-shopping-basket uk-text-danger fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">سفارش پردازش انبار</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($processingStock) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
                @if(permission('Order.OrderController.index'))
                    <a href="{!! action('Order\Controllers\Admin\OrderController@index',['confirm' => 1,'ready_to_send' => 1]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-shopping-basket uk-text fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">سفارش در حال ارسال</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($readyToSend) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif

                @if(permission('Payment.SettlementController.index'))
                    <a href="{!! action('Payment\Controllers\Admin\SettlementController@index',['confirm' => 0,'reject' => 0]) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-dollar uk-text fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">فیش های بانکی نیاز به تایید</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($settlementNeedToConfirm) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
                @if(permission('Order.OrderController.index'))
                    <a href="{!! action('Order\Controllers\Admin\OrderController@index',['confirm' => 1,'paid' => 0,'processing_stock' => 0,'ready_to_send' => 0,'delivered' => 0,'type' => 'custom']) !!}">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-float-right uk-margin-top uk-margin-small-right">
                                    <i class="md-icon fa fa-shopping-basket uk-text-success fa-3x"></i>
                                </div>
                                <span class="uk-text-muted">سفارش های اختصاصی جدید</span>
                                <h2 class="uk-margin-remove">
                                    <span class="countUpMe">{{ number_format($customOrderNeedCheck) }}</span>
                                </h2>
                            </div>
                        </div>
                    </a>
                @endif
            </div>
        </div>
    </div>
@endsection
