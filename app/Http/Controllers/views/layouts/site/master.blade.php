<!DOCTYPE HTML>
<html>
@include('views.layouts.site.partials.head')
{{ Html::style('assets/site/css/toastr/toastr.min.css') }}
<body>
@stack('modal')
@if (trim($__env->yieldContent('index-page')))
    @include('views.layouts.site.partials.index-header')
@else
    @include('views.layouts.site.partials.header')
@endif

@yield('content')

@include('views.layouts.site.partials.footer')


{{ Html::script('assets/site/js/bootstrap.min.js') }}
{{ Html::script('assets/site/css/toastr/toastr.min.js') }}
@stack('js')
<script>
    toastr.options = {
        "timeOut": "15000"
    };

    @if(session()->has('popup-error'))
        toastr["error"]("{{ session()->get('popup-error') }}");
    @endif

            @if(session()->has('popup-success'))
        toastr["success"]("{{ session()->get('popup-success') }}");
    @endif

            @if(session()->has('popup-info'))
        toastr["info"]("{{ session()->get('popup-info') }}");
    @endif
</script>
</body>
</html>