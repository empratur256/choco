<nav class="navbar navbar-inverse">
    <div class="container">
        @if(auth()->user())
            <div id="log-reg" class="container-fluid hidden-xs log-reg">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ action('User\Controllers\Site\DashboardController@index') }}"><span
                                    class=""></span><span style="color:#5fbf63"> @lang('site.header.profile') </span></a>
                    </li>
                    <li><a href="#"><span class="">/</span></a></li>
                    <li><a href="{{ action('Auth\Controllers\Site\AuthController@logout') }}"><span
                                    class="glyphicon glyphicon-log-in"></span> @lang('site.logout') </a></li>
                </ul>
            </div>
        @else
            <div id="log-reg" class="container-fluid hidden-xs log-reg">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ action('Auth\Controllers\Site\AuthController@showSignupForm') }}"><span
                                    class=""></span><span style="color:#5fbf63"> @lang('site.membership') </span></a>
                    </li>
                    <li><a href="#"><span class="">/</span></a></li>
                    <li><a href="{{ action('Auth\Controllers\Site\AuthController@showLoginForm') }}"><span
                                    class="glyphicon glyphicon-log-in"></span> @lang('site.user.login') </a></li>
                </ul>
            </div>
        @endif
        <div class=" container-fluid main-nav">

            <div class="navbar-header my-navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{action('HomeController@index')}}"><img class="img-responsive"
                                                      src="{{ asset('assets/site/img/fancy-mark.png') }}"></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class=" nav navbar-nav mynav">
                    <li><a href="{!! action('HomeController@index') !!}">@lang('site.header.homepage')</a></li>
                    @foreach($categories as $node)
                        {!! renderNode($node) !!}
                    @endforeach
                    <li><a href="{!! action('Contact\Controllers\Site\ContactController@index') !!}">@lang('site.contact.home')</a></li>
                    <li class="visible-xs"><a
                                href="{!! action('Cart\Controllers\Site\CartController@index') !!}">@lang('site.cart')</a>
                    </li>
                    <li class="visible-xs"><a
                                href="{{ action('Auth\Controllers\Site\AuthController@showLoginForm') }}">@lang('site.user.login')</a>
                    </li>
                    <li class="visible-xs"><a
                                href="{{ action('Auth\Controllers\Site\AuthController@showSignupForm') }}">@lang('site.membership')</a>
                    </li>
                    <li class="visible-xs search">
                        {{ Form::open(['method' => 'GET','action' => 'Product\Controllers\Site\ProductController@index']) }}
                        <input style="direction:rtl" type="text" name="search" placeholder=""
                               onfocus="this.placeholder='@lang('site.header.search')'" onblur="this.placeholder=''">
                        {{ Form::close() }}
                    </li>
                </ul>
                <ul style="direction:ltr" class="nav navbar-nav navbar-right hidden-xs">
                    @php $cartCount = count(Cart::content()) @endphp
                    @if($cartCount == 0)
                        <li><a href="{!! action('Cart\Controllers\Site\CartController@index') !!}"><span
                                        class="flaticon-shopping-cart"></span><span
                                        class="order-count">0</span></a></li>
                    @else
                        <li><a href="{!! action('Cart\Controllers\Site\CartController@index') !!}"><span
                                        class="flaticon-shopping-cart"></span><span
                                        class="order-count">{{$cartCount}}</span></a></li>
                    @endif
                    <li class="search">
                        {{ Form::open(['method' => 'GET','action' => 'Product\Controllers\Site\ProductController@index']) }}
                        <input id="float-search" style="direction:rtl" type="text" name="search" placeholder=""
                               onfocus="this.placeholder='@lang('site.header.search')'" onblur="this.placeholder=''">
                        {{ Form::close() }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<script>
    $(document).ready(function () {
        $('.navbar a.dropdown-toggle').click( function (e) {
            var $el = $(this);
            var $parent = $(this).offsetParent(".dropdown-menu");
            $(this).parent("li").toggleClass('open');
            $('.dropdown-menu').css("left", "156");
            if (!$parent.parent().hasClass('nav')) {
                $el.next().css({"top": $el[0].offsetTop -8, "left": $parent.outerWidth() - 315});
            }

            $('.nav li.open').not($(this).parents("li")).removeClass("open");

            return false;
        });
    });
</script>