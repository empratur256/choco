<div class="brands-in-products" style="background-color:rgba(128, 128, 128, 0.12)">
    <div class="brands">
        <div class="container">


            <div class="row  center-block">
                    @foreach($photoBrands as $photoBrand)
                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6   thumb"
                             style="margin:auto !important;text-align: center;">
                            <img class="img-thumbnail center-block"
                                 src="{{ asset('uploads/brand/128x86-'.$photoBrand->photo) }}">
                        </div>
                    @endforeach
            </div>
        </div>
    </div>
</div>

<div class="about">
    <div class="container">
        <div class="row padding_bottom_2">
            <div class="trust col-lg-4 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-right padding-bottom-2">
                        <h4><span class="hot">@lang('site.buy_confidently')</span></h4>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <img class="img-responsive" src="{{ asset('assets/site/img/samandehi.png') }}">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <img class="img-responsive" src="{{ asset('assets/site/img/etemad.png') }}">
                    </div>
                </div>
            </div>
            <div class="useful_links col-lg-4 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                <div class="row">
                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 align-right padding-bottom-2">
                        <h4><span class="hot">@lang('site.footer.links')</span></h4>
                    </div>
                    @foreach($footerPages as $footerPage)
                        @if($loop->first)
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
                                <ul>
                                    @endif
                                    @if((($loop->index)) == 5)
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pull-right">
                                            <ul>
                                                @endif
                                                <li class="padding-bottom">
                                                    <a href="{{ action('Page\Controllers\Site\PageController@show',[$footerPage->id,utf8_slug($footerPage->title)]) }}">
                                                        {{ $footerPage->title }}
                                                    </a>
                                                </li>
                                                @if((($loop->index+1)) == 5)
                                            </ul>
                                        </div>
                                    @endif
                                    @if($loop->last )
                                </ul>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="about_fancy col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                <div class="padding-bottom-2">
                    <h4><span class="hot">@lang('site.about_fancy')</span></h4>
                </div>
                <p style="white-space: pre-line; text-align: justify">{{ str_limit(strip_tags($aboutUsPage->text),190) }}</p>
                <br>
                <a href="{{ action('Page\Controllers\Site\PageController@show',3) }}"
                   class="btn btn-default">@lang('site.read_more')</a>
            </div>
        </div>
        <div class="row footer">
            <div class="social col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                <a target="_blank" href="{{ $setting->twitter }}">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a target="_blank" href="{{ $setting->instagram }}">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a target="_blank" href="{{ $setting->facebook }}">
                    <span class="fa-stack fa-lg" style="color:#356299">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <a target="_blank" href="{{ $setting->telegram }}">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-telegram fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
            </div>
            <div class="col-lg-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <span>@lang('site.footer.copyright')</span>
            </div>

        </div>
    </div>
</div>