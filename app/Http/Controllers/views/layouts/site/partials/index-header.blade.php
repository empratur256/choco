

<div class="start">
    <section>
        <nav class="navbar">
            <div class="container">
                @if(auth()->user())
                    <div id="log-reg" class="container-fluid hidden-xs log-reg">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ action('User\Controllers\Site\DashboardController@index') }}"><span
                                            class=""></span><span style="color:#5fbf63"> @lang('site.header.profile') </span></a>
                            </li>
                            <li><a href="#"><span class="">/</span></a></li>
                            <li><a href="{{ action('Auth\Controllers\Site\AuthController@logout') }}"><span
                                            class="glyphicon glyphicon-log-in"></span> @lang('site.logout') </a></li>
                        </ul>
                    </div>
                @else
                    <div id="log-reg" class="container-fluid hidden-xs log-reg">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{ action('Auth\Controllers\Site\AuthController@showSignupForm') }}"><span
                                            class=""></span><span style="color:#6cd070"> @lang('site.membership') </span></a>
                            </li>
                            <li><a href="#"><span class="">/</span></a></li>
                            <li><a href="{{ action('Auth\Controllers\Site\AuthController@showLoginForm') }}"><span
                                            class="glyphicon glyphicon-log-in"></span> @lang('site.user.login') </a></li>
                        </ul>
                    </div>
                @endif
                <div class=" container-fluid main-nav">

                    <div class="navbar-header my-navbar-header">
                        <button type="button" class="navbar-toggle navbar-toggle plus collapsed" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{action('HomeController@index')}}"><img class="img-responsive"
                                                                                               src="{{ asset('assets/site/img/fancy-mark.png') }}"></a>
                    </div>
                    <div class="collapse navbar-collapse"  id="myNavbar">
                        <ul class=" nav navbar-nav mynav">
                            <li><a href="{!! action('HomeController@index') !!}">@lang('site.header.homepage')</a></li>
                            <li><a href="#second-section">@lang('site.custom_order_menu')</a></li>
                            <li><a href="{{ action('Product\Controllers\Site\ProductController@index') }}">@lang('site.other_chocolates_menu')</a></li>
                            <li><a href="{!! action('Contact\Controllers\Site\ContactController@index') !!}">@lang('site.contact.home')</a></li>
                            <li class="visible-xs"><a
                                        href="{!! action('Cart\Controllers\Site\CartController@index') !!}">@lang('site.cart')</a>
                            </li>
                            <li class="visible-xs"><a
                                        href="{{ action('Auth\Controllers\Site\AuthController@showLoginForm') }}">@lang('site.user.login')</a>
                            </li>
                            <li class="visible-xs"><a
                                        href="{{ action('Auth\Controllers\Site\AuthController@showSignupForm') }}">@lang('site.membership')</a>
                            </li>
                            <li class="visible-xs search">
                                {{ Form::open(['method' => 'GET','action' => 'Product\Controllers\Site\ProductController@index']) }}
                                <input style="direction:rtl" type="text" name="search" placeholder=""
                                       onfocus="this.placeholder='@lang('site.header.search')'" onblur="this.placeholder=''">
                                {{ Form::close() }}
                            </li>
                        </ul>
                        <ul style="direction:ltr" class="nav navbar-nav navbar-right hidden-xs">
                            @php $cartCount = count(Cart::content()) @endphp
                            @if($cartCount == 0)
                                <li><a href="{!! action('Cart\Controllers\Site\CartController@index') !!}"><span
                                                class="flaticon-shopping-cart"></span><span
                                                class="order-count">0</span></a></li>
                            @else
                                <li><a href="{!! action('Cart\Controllers\Site\CartController@index') !!}"><span
                                                class="flaticon-shopping-cart "></span><span
                                                class="order-count">{{$cartCount}}</span></a></li>
                            @endif
                            <li class="search">
                                {{ Form::open(['method' => 'GET','action' => 'Product\Controllers\Site\ProductController@index']) }}
                                <input id="float-search" type="text" name="search" placeholder=""
                                       onfocus="this.placeholder='@lang('site.header.search')'" onblur="this.placeholder=''">
                                {{ Form::close() }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="summary">
            <h1> {{$setting->trial_title}} </h1>
            <p>{!! $setting->trial_text !!}</p>
        </div>
        <div class="col-lg-7 col-md-8 col-sm-11 col-xs-11 search-hero margin_auto">
            {{ Form::open(['method' => 'GET','action' => 'Product\Controllers\Site\ProductController@index']) }}
            <div class="search-inner">
                <div class="col-md-2 pull-left search button" style="padding-right: 0 !important;">
                    <button type="submit" class="btn btn-danegr btn-block"  type="button">@lang('site.header.search')</button>
                </div>
                <div class="col-md-10 search input">
                    <input name="search" type="text" class="form-control pull-right" placeholder="@lang('site.header.big_search')">
                </div>
            </div><!-- /input-group -->
            {{ Form::close() }}
        </div><!-- /.col-lg-6 -->
        <a class="next-section text-light" href="#second-section"><img src="{{ asset('assets/site/img/mouse.png') }}" alt=""></a>

    </section>

</div>
<style>
    .nav>li>a:focus, .nav>li>a:hover ,.nav .open>a, .nav .open>a:focus, .nav .open>a:hover{
        background: transparent;
    }
    .navbar-toggle{
        background: #4CAF50 !important;
        border:none;
    }
    .navbar-toggle:hover{
        background: #8BC34A !important;
    }
    .icon-bar{
        background: #ffffff !important;
    }
    div.product {
        width: 100%;
    }
</style>
<script>
    $(document).ready(function () {
        $('[data-toggle="slide-collapse"]').on('click', function() {
            $navMenuCont = $($(this).data('target'));
            $navMenuCont.animate({
                'width': 'toggle'
            }, 350);
            $(".menu-overlay").fadeIn(500);

        });
        $(".menu-overlay").click(function(event) {
            $(".navbar-toggle").trigger("click");
            $(".menu-overlay").fadeOut(500);
        });

        $('.navbar a.dropdown-toggle').click( function (e) {
            var $el = $(this);
            var $parent = $(this).offsetParent(".dropdown-menu");
            $(this).parent("li").toggleClass('open');
            $('.dropdown-menu').css("left", "156");
            if (!$parent.parent().hasClass('nav')) {
                $el.next().css({"top": $el[0].offsetTop -8, "left": $parent.outerWidth() - 315});
            }

            $('.nav li.open').not($(this).parents("li")).removeClass("open");

            return false;
        });

    });
</script>