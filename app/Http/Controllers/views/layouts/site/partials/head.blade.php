<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title')
    </title>

    {{ Html::style('assets/site/css/dropzone.css') }}
    {{ Html::style('assets/site/css/bootstrap.min.css') }}

    {{ Html::style('assets/site/css/font-awesome.min.css') }}

    @if (trim($__env->yieldContent('index-page')))

    @else
        {{ Html::style('assets/site/css/navbar.css') }}
    @endif
    {{ Html::style('assets/site/css/footer.css') }}
    {{ Html::style('assets/site/css/responsive.css') }}
    {{ Html::style('assets/site/css/component.css') }}
    {{ Html::style('assets/site/css/owl.theme.default.min.css') }}
    {{ Html::style('assets/site/css/owl.carousel.min.css') }}
    @stack('css')

    {{--<script src="https://code.jquery.com/jquery-3.1.0.js"></script>--}}
    {{ Html::script('assets/site/js/jquery.min.js') }}
    {{ Html::script('assets/site/js/owl.carousel.min.js') }}
    {{ Html::script('assets/site/js/dropzone.js') }}

</head>
