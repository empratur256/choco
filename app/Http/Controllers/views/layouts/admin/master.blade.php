<!DOCTYPE html>
<!--[if lte IE 9]>
<html class="lte-ie9 app_theme_d" lang="fa" dir="rtl"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="fa" dir="rtl" class="app_theme_d"><!--<![endif]-->
@include('views.layouts.admin.partials.head')
<body class="sidebar_main_open sidebar_main_swipe @yield('bodyClass')">
@include('views.layouts.admin.partials.header')
@include('views.layouts.admin.partials.sidebar-menu')
<style>
    .scrollbar-inner > .scroll-element.scroll-y .scroll-bar {
        width: 10px !important;
        left: -15px !important;
    }

    #sidebar_main .menu_section > ul > li > a {
        font: 500 15px/25px Tahoma, Arial;
    }

    #sidebar_main .menu_section > ul > li ul a {
        font: 400 14px/18px Tahoma, Arial;
    }
</style>

@yield('content')

@include('views.layouts.admin.partials.footer')

</body>
</html>