<head>
    <meta charset="utf-8">
    <title>@yield('title',$adminTitle)</title>
    <meta name="robots" content="noindex">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <meta name="adminBreadcrumb" content="{{ $adminBreadcrumb }}">
    <meta name="adminSection" content="{{ $section }}">
    @stack('head')
    {!! Html::style('assets/admin/css/uikit.rtl.css') !!}
    {!! Html::style('assets/admin/icons/flags/flags.min.css') !!}
    {!! Html::style('assets/admin/css/main.min.css') !!}
    {!! Html::style('assets/admin/css/themes/themes_combined.min.css') !!}
    {!! Html::style('assets/admin/bower_components/datepicker/bootstrap-datetimepicker.min.css') !!}
    <!--[if lte IE 9]>
    {!! Html::script('assets/admin/bower_components/matchMedia/matchMedia.js') !!}
    {!! Html::script('assets/admin/bower_components/matchMedia/matchMedia.addListener.js') !!}
    {!! Html::style('assets/admin/css/ie.css') !!}
    <![endif]-->
    {!! Html::style('assets/admin/css/font-awesome.min.css') !!}
    {!! Html::style('assets/admin/skins/dropify/css/dropify.css') !!}
    {!! Html::style('assets/admin/colorbox/colorbox.css') !!}
    {!! Html::style('assets/admin/css/sedehi.css') !!}
    {!! HTML::script('assets/admin/editor/ckeditor.js') !!}
    @stack('css')
</head>