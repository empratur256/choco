{!! Html::script('assets/admin/js/common.min.js') !!}
{!! Html::script('assets/admin/js/uikit_custom.min.js') !!}
{!! Html::script('assets/admin/js/altair_admin_common.min.js') !!}
{!! Html::script('assets/admin/bower_components/datepicker/moment.js') !!}
{!! Html::script('assets/admin/bower_components/datepicker/bootstrap-persian-datetimepicker.min.js') !!}
{!! Html::script('assets/admin/js/custom/dropify/dist/js/dropify.min.js') !!}
{!! Html::script('assets/admin/bower_components/jquery-idletimer/dist/idle-timer.min.js') !!}
{!! Html::script('assets/admin/colorbox/jquery.colorbox-min.js') !!}
{!! Html::script('packages/barryvdh/elfinder/js/standalonepopup.js') !!}
{!! Html::script('assets/admin/js/sedehi.js') !!}
<script>
    $(function () {
        if (isHighDensity) {
            // enable hires images
            altair_helpers.retina_images();
        }
        if (Modernizr.touch) {
            // fastClick (touch devices)
            FastClick.attach(document.body);
        }
    });
    $window.load(function () {
        // ie fixes
        altair_helpers.ie_fix();
    });
    $(function () {
        $body.append('<div class="uk-modal" id="modal_idle"><div class="uk-modal-dialog"><div class="uk-modal-header"><h3 class="uk-modal-title">شما در حال خروج از مدیریت هستید!</h3></div><p>شما برای مدتی غیر فعال بودید. برای امنیت بیشتر شما را به طور خودکار از مدیریت خارج می کنیم.</p><p>برای ماندن در مدیریت بر روی دکمه خارج نشو کلیک کنید</p><p>شما تا <span class="uk-text-bold md-color-red-500" id="sessionSecondsRemaining"></span> ثانیه دیگر از مدیریت خارج می شود.</p><div class="uk-modal-footer uk-text-left"><button id="staySigned" type="button" class="md-btn md-btn-flat uk-modal-close">خارج نشو</button><button type="button" class="md-btn md-btn-flat md-btn-flat-primary" id="logoutSession">خروج</button></div></div></div>');
        var o = UIkit.modal("#modal_idle", {bgclose: !1}), n = {
            inactiveTimeout: {!! config('site.inactiveTimeout') !!},
            warningTimeout: 30000,
            minWarning: 5000,
            warningStart: null,
            warningTimer: null,
            autologout: {logouturl: "{!! action('Auth\Controllers\Admin\AuthController@logout') !!}"},
            logout: function () {
                window.location = n.autologout.logouturl
            }
        }, t = $("#sessionSecondsRemaining").html(n.warningTimeout / 1e3);
        $(document).on("idle.idleTimer", function (i, e, a) {
            var l = +new Date - a.lastActive - a.timeout, u = +new Date - l;
            l >= n.warningTimeout || u <= n.minWarning ? o.hide() : (t.html(Math.round((n.warningTimeout - l) / 1e3)), o.show(), n.warningStart = +new Date - l, n.warningTimer = setInterval(function () {
                var o = Math.round(n.warningTimeout / 1e3 - (+new Date - n.warningStart) / 1e3);
                o >= 0 ? t.html(o) : n.logout()
            }, 1e3))
        }), $body.on("click", "#staySigned", function () {
            clearTimeout(n.warningTimer), o.hide()
        }).on("click", "#logoutSession", function () {
            n.logout()
        }), $(document).idleTimer(n.inactiveTimeout)
    });
</script>
@stack('js')
@include('views.errors.notifications')
