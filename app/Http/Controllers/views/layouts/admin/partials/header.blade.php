<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                <span class="sSwitchIcon"></span>
            </a>
            @include('views.layouts.admin.partials.navbar-shortcuts')
            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <li>
                        <a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a>
                    </li>
                    {{--
                                        @include('views.layouts.admin.partials.notification')
                    --}}
                    @include('views.layouts.admin.partials.user')
                </ul>
            </div>
        </nav>
    </div>
</header>
