<li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
    <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">16</span></a>
    <div class="uk-dropdown uk-dropdown-xlarge">
        <div class="md-card-content">
            <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                <li class="uk-width-1-2 uk-active">
                    <a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                <li class="uk-width-1-2">
                    <a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
            </ul>
            <ul id="header_alerts" class="uk-switcher uk-margin">
                <li>
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <span class="md-user-letters md-bg-cyan">vn</span>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading"><a href="pages_mailbox.html">Laudantium aut.</a></span>
                                <span class="uk-text-small uk-text-muted">Officia cupiditate nostrum eum corrupti voluptas aut deleniti inventore.</span>
                            </div>
                        </li>


                    </ul>
                    <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                        <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                    </div>
                </li>
                <li>
                    <ul class="md-list md-list-addon">
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">Id qui temporibus.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Labore culpa quisquam inventore laboriosam.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">Molestiae ut.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Omnis non eligendi perspiciatis id.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">Est et enim.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Consequatur fuga et cum iure commodi dolor.</span>
                            </div>
                        </li>
                        <li>
                            <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                            </div>
                            <div class="md-list-content">
                                <span class="md-list-heading">Illo et.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Sit qui adipisci magni deleniti.</span>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</li>
