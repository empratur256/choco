<aside id="sidebar_main">
    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="{!! action('HomeController@admin') !!}" class="sSidebar_hide sidebar_logo_large">
            </a>
            <a href="{!! action('HomeController@admin') !!}" class="sSidebar_show sidebar_logo_small">
            </a>
        </div>
    </div>
    <div class="menu_section">
        <ul>
            <li>
                <a target="_blank" href="{!! action('HomeController@index') !!}">
                    <span class="menu_icon"><i class="fa fa-globe" style="font-size: 20px"></i></span>
                    <span class="menu_title">نمایش وب سایت</span>
                </a>
            </li>
            <li>
                <a href="{!! action('HomeController@admin') !!}">
                    <span class="menu_icon"><i class="fa fa-home" style="font-size: 20px"></i></span>
                    <span class="menu_title">صفحه نخست</span>
                </a>
            </li>

            @foreach(adminMenu() as $keyMenu=>$valueMenu)

                @if(permission($keyMenu))
                    @php
                        $parameters = [];
                    @endphp
                    @if(isset($valueMenu['parameters']))
                        @php $parameters = $valueMenu['parameters'] @endphp
                    @endif


                    <li @if(isset($valueMenu['action'])) class="{{ Html::activeAdmin(action($valueMenu['action']),true) }}" @endif id="admin_menu_{{$keyMenu}}" title="{{$valueMenu['title']}}">
                        <a href="@if(isset($valueMenu['action'])){!! action($valueMenu['action'],$parameters) !!}@else#@endif">
                            <span class="menu_icon"><i class="{{$valueMenu['icon']}}" style="font-size: 20px"></i></span>
                            <span class="menu_title">{{$valueMenu['title']}}</span>
                        </a>
                        @if(isset($valueMenu['submenu']))
                            <ul>
                                @foreach($valueMenu['submenu'] as $keyLevelTwo=>$valueLevelTwo)
                                    @if(isset($valueLevelTwo['action']))
                                        @php
                                            $subMenuAction = explode('\\', strtolower($valueLevelTwo['action']));
                                            $submenuController = explode('@', end($subMenuAction));
                                        @endphp
                                        @if(permission($keyMenu.'.'.$submenuController[0].'.'.$submenuController[1]))
                                            <li class="{{ Html::activeAdmin(action($valueLevelTwo['action'])) }}">
                                                @php
                                                    $parameters = [];
                                                @endphp
                                                @if(isset($valueLevelTwo['parameters']))
                                                    @php $parameters = $valueLevelTwo['parameters'] @endphp
                                                @endif

                                                <a href="@if(isset($valueLevelTwo['action'])) {!! action($valueLevelTwo['action'],$parameters) !!} @else#@endif">
                                                    {{ $valueLevelTwo['title'] }}
                                                </a>
                                            </li>
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>

                @endif
                @if(isset($valueMenu['order']))
                    @if($valueMenu['order'] == 150)
                        @if(permission('filemanager'))
                            <li>
                                <a href="" class="popup_selector" data-inputid="feature_image" data-url="{!! url(config('elfinder.route.prefix').'/popup/') !!}/">
                                    <span class="menu_icon"><i class="fa fa-file" style="font-size: 20px"></i></span>
                                    <span class="menu_title">مدیریت فایل</span>
                                </a>
                            </li>
                        @endif
                    @endif
                @endif
            @endforeach
            @if(auth()->user()->programmer)
                <li>
                    <a target="_blank" href="{!! URL::to('sedehi/log-viewer') !!}">
                        <span class="menu_icon"><i class="fa fa-tasks"></i></span>
                        <span class="menu_title">لاگ ها</span>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</aside>

@push('js')
<script>
    $(document).ready(function () {
        if ($('#admin_menu_' + $("meta[name=adminSection]").attr("content")).find('.act_item').length == 0) {
            $('#admin_menu_' + $("meta[name=adminSection]").attr("content")).addClass('act_section current_section');
            $('#admin_menu_' + $("meta[name=adminSection]").attr("content")).find('a').trigger('click');
        }
    });
</script>
@endpush