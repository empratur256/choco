<li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
    <a href="#"><i class="material-icons md-24 md-light">settings</i></a>
    <div class="uk-dropdown uk-dropdown-small">
        <ul class="uk-nav js-uk-prevent">
            <li><a href="{{ action('User\Controllers\Admin\UserController@showProfile') }}">پروفایل</a></li>
            @if(permission('setting.SettingController.index'))
                <li><a href="{{ action('Setting\Controllers\Admin\SettingController@index') }}">تنظیمات</a></li>
            @endif
            <li><a href="{!! action('Auth\Controllers\Admin\AuthController@logout') !!}">خروج</a></li>
        </ul>
    </div>
</li>
