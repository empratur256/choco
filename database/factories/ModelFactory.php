<?php

foreach (File::directories(app_path('Http/Controllers')) as $directory) {
    if (File::exists($directory.'/database/factory.php')) {
        include_once $directory.'/database/factory.php';
    }
}