<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{

    use \Illuminate\Console\AppNamespaceDetectorTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = array_sort_recursive(File::directories(app_path('Http/Controllers')));
        foreach ($files as $directory) {
            if (File::isDirectory($directory.'/database/seeds/')) {
                foreach (File::files($directory.'/database/seeds') as $seed) {
                    $this->call($this->getAppNamespace().'Http\Controllers\\'.File::name($directory).'\database\seeds\\'.File::name($seed));
                }
            }
        }
    }
}
