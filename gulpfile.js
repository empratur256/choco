const elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('app.scss' , 'resources/assets/css')
        .copy("node_modules/jquery/dist/jquery.js" , "resources/assets/js/jquery.js")
        .copy("node_modules/bootstrap-sass/assets/javascripts/bootstrap.js" , "resources/assets/js/bootstrap.js")
        .copy("node_modules/trianglify/dist/trianglify.min.js" , "resources/assets/js/trianglify.min.js")
        .styles([
            'bootstrap-select.css',
            'app.css',
        ], 'public/assets/site/css/all.css')
        .scripts([
            'jquery.js',
            'throttle.min.js',
            'bootstrap.js',
            'bootstrap-select.js',
            'trianglify.min.js',
            'app.js'
        ], 'public/assets/site/js/all.js')
    // .version(['css/all.css' , 'js/all.js'])
    ;

    mix.sass('ltr.scss', 'resources/assets/css')
        .styles('ltr.css', 'public/assets/site/css/ltr.css');
});