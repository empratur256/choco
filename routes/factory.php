<?php

Route::group(['prefix' => 'factory'], function () {
    set_time_limit(0);

    Route::get('brand', function () {
        factory(\Sedehi\Http\Controllers\Brand\Models\Brand::class, 10)->create();
    });
    Route::get('category', function () {
        factory(\Sedehi\Http\Controllers\Category\Models\Category::class, 10)->create();
    });
    Route::get('size', function () {
        factory(\Sedehi\Http\Controllers\Size\Models\Size::class, 6)->create();
    });
    Route::get('comment', function () {
        factory(\Sedehi\Http\Controllers\Comments\Models\Comments::class, 100)->create();
    });
    Route::get('frame', function () {
        factory(\Sedehi\Http\Controllers\Frame\Models\Frame::class, 40)->create();
    });
    Route::get('gallery', function () {
        factory(\Sedehi\Http\Controllers\Gallery\Models\Gallery::class, 20)->create();
    });

    Route::get('product', function () {
        factory(\Sedehi\Http\Controllers\Product\Models\Product::class, 20)->create()->each(function ($product) {

            $file = File::files(public_path('samples/product'))[array_rand(File::files(public_path('samples/product')))];

            $fileName = time().md5_file($file).'.'.File::extension($file);
            $path     = app(\Sedehi\Http\Controllers\Product\Controllers\Admin\ProductController::class)->uploadPath;
            $path     = $path.$product->id.'/';
            if (!File::isDirectory(public_path($path))) {
                File::makeDirectory(public_path($path));
            }
            Image::make($file)->widen(1920, function ($constraint) {
                $constraint->upsize();
            })->save($path.$fileName, 90);

            app(\Sedehi\Http\Controllers\Product\Controllers\Admin\ProductController::class)->createImages($file,
                                                                                                           $fileName,
                                                                                                           false,
                                                                                                           $path);

            $product->picture = $fileName;


            if ($product->frame_type == 'custom') {
                $product->frames()->attach(\Sedehi\Http\Controllers\Frame\Models\Frame::inRandomOrder()
                                                                                      ->limit(rand(2, 30))
                                                                                      ->get());
            }


            if (rand(0, 1)) {
                $iRand = rand(1, 7);
                for ($i = $iRand; $i > 0; $i--) {

                    $file     = File::files(public_path('samples/product'))[array_rand(File::files(public_path('samples/product')))];
                    $fileName = time().md5_file($file).'.'.File::extension($file);

                    app(\Sedehi\Http\Controllers\Product\Controllers\Admin\ProductController::class)->createImages($file,
                                                                                                                   $fileName,
                                                                                                                   false,
                                                                                                                   $path);

                    $data             = new \Sedehi\Http\Controllers\Product\Models\Picture();
                    $data->picture    = $fileName;
                    $data->product_id = $product->id;
                    $data->save();
                    Image::make($file)->widen(1920, function ($constraint) {
                        $constraint->upsize();
                    })->save($path.$fileName, 90);
                    $images[] = $fileName;
                }
            }

            if ($product->price_type == 'dynamic') {
                $iRand = rand(1, 7);

                for ($i = $iRand; $i > 0; $i--) {
                    $size = \Sedehi\Http\Controllers\Size\Models\Size::inRandomOrder()->first()->id;

                    if (!\Sedehi\Http\Controllers\Product\Models\Price::where('product_id', $product->id)
                                                                      ->where('size_id', $size)
                                                                      ->count()
                    ) {

                        $price             = new \Sedehi\Http\Controllers\Product\Models\Price();
                        $price->product_id = $product->id;
                        $price->size_id    = $size;
                        $price->price      = rand(100000, 10000000);
                        $price->quantity   = rand(0, 70);
                        $price->save();
                    }
                }

                $minPrice           = \Sedehi\Http\Controllers\Product\Models\Price::where('product_id', $product->id)
                                                                                   ->min('price');
                $maxPrice           = \Sedehi\Http\Controllers\Product\Models\Price::where('product_id', $product->id)
                                                                                   ->max('price');
                $quantity           = \Sedehi\Http\Controllers\Product\Models\Price::where('product_id', $product->id)
                                                                                   ->sum('quantity');
                $product->price     = $minPrice;
                $product->min_price = $minPrice;
                $product->max_price = $maxPrice;
                $product->quantity  = $quantity;
            }

            $product->save();
        });
    });
});