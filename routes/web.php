<?php
use Sedehi\Http\Controllers\Product\Models\Product;

Route::get('/', 'HomeController@index');

Route::get('lang/{lang}', function ($lang) {
    $cookie = cookie()->forever('lang', $lang);

    return redirect()->action('HomeController@index')->withCookie($cookie);
});

Route::group(['prefix' => config('site.admin'), 'middleware' => ['admin']], function () {
    Route::get('/', 'HomeController@admin');
});

foreach (File::directories(app_path('Http/Controllers')) as $directory) {
    if (File::exists($directory.'/routes/web.php')) {
        include_once $directory.'/routes/web.php';
    }
}

if (config('app.env') == 'local') {
    include_once base_path('routes/factory.php');
}

Route::get('sitemap.xml', function () {

    $sitemap = App::make("sitemap");

    if (!$sitemap->isCached()) {
        $sitemap->add(URL::to('/'), '2012-08-25T20:10:00+02:00', '1.0', 'daily');

        $posts = Product::active()->get();

        foreach ($posts as $post) {
            $sitemap->add(action('Product\Controllers\Site\ProductController@show',
                                 [$post->id, utf8_slug($post->title)]), $post->updated_at->toAtomString(), '0.5',
                          'weekly');
        }
    }

    return $sitemap->render('xml');
});