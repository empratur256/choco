<?php

foreach (File::directories(app_path('Http/Controllers')) as $directory) {
    if (File::exists($directory.'/routes/api.php')) {
        include_once $directory.'/routes/api.php';
    }
}
