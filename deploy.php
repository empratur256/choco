<?php

namespace Deployer;

require 'recipe/laravel.php';
set('ssh_type', 'native');
set('keep_releases', 1);
set('ssh_multiplexing', true);
set('repository', 'git@gitlab.com:sedehi/Chocofancy.git');
add('shared_dirs', ['storage', 'public/uploads']);
add('writable_dirs', ['storage', 'public/uploads']);
host('parsedesign.ir')->port(3390)->user('parse')->set('deploy_path', '/home/parse/public_html/chocofancy');
after('artisan:optimize', 'artisan:section:migrate');
desc('Execute section migrate');
task('artisan:section:migrate', function(){

    $tt = run('{{bin/php}} {{release_path}}/artisan section:migrate');
    writeln($tt);
});
after('deploy:failed', 'deploy:unlock');
