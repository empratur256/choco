<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted'             => ':attribute باید پذیرفته شده باشد.',
    'active_url'           => 'آدرس :attribute معتبر نیست',
    'after'                => ':attribute باید تاریخی بعد از :date باشد.',
    'alpha'                => ':attribute باید شامل حروف الفبا باشد.',
    'alpha_dash'           => ':attribute باید شامل حروف الفبا و عدد و خظ تیره(-) باشد.',
    'alpha_num'            => ':attribute باید شامل حروف الفبا و عدد باشد.',
    'array'                => ':attribute باید شامل آرایه باشد.',
    'before'               => ':attribute باید تاریخی قبل از :date باشد.',
    'between'              => [
        'numeric' => ':attribute باید بین :min و :max باشد.',
        'file'    => ':attribute باید بین :min و :max کیلوبایت باشد.',
        'string'  => ':attribute باید بین :min و :max کاراکتر باشد.',
        'array'   => ':attribute باید بین :min و :max آیتم باشد.',
    ],
    'boolean'              => 'فیلد :attribute فقط میتواند صحیح و یا غلط باشد',
    'confirmed'            => ':attribute با تاییدیه مطابقت ندارد.',
    'date'                 => ':attribute یک تاریخ معتبر نیست.',
    'date_format'          => ':attribute با الگوی :format مطاقبت ندارد.',
    'different'            => ':attribute و :other باید متفاوت باشند.',
    'digits'               => ':attribute باید :digits رقم باشد.',
    'digits_between'       => ':attribute باید بین :min و :max رقم باشد.',
    'dimensions'           => ':attribute دارای ابعاد تصویر نامعتبر می‌باشد.',
    'distinct'             => 'فیلد :attribute دارای یک مقدار تکراری می‌باشد.',
    'email'                => 'فرمت :attribute معتبر نیست.',
    'exists'               => ':attribute انتخاب شده، معتبر نیست.',
    'file'                 => ':attribute باید یک فایل باشد',
    'filled'               => 'فیلد :attribute الزامی است',
    'image'                => ':attribute باید تصویر باشد.',
    'in'                   => ':attribute انتخاب شده، معتبر نیست.',
    'in_array'             => 'فیلد :attribute در :other وجود ندارد.',
    'integer'              => ':attribute باید نوع داده ای عددی (integer) باشد.',
    'ip'                   => ':attribute باید IP آدرس معتبر باشد.',
    'json'                 => 'فیلد :attribute باید یک رشته از نوع JSON باشد.',
    'max'                  => [
        'numeric' => ':attribute نباید بزرگتر از :max باشد.',
        'file'    => ':attribute نباید بزرگتر از :max کیلوبایت باشد.',
        'string'  => ':attribute نباید بیشتر از :max کاراکتر باشد.',
        'array'   => ':attribute نباید بیشتر از :max آیتم باشد.',
    ],
    'mimes'                => ':attribute باید یکی از فرمت های :values باشد.',
    'mimetypes'            => ':attribute باید یکی از فرمت های :values باشد.',
    'min'                  => [
        'numeric' => ':attribute نباید کوچکتر از :min باشد.',
        'file'    => ':attribute نباید کوچکتر از :min کیلوبایت باشد.',
        'string'  => ':attribute نباید کمتر از :min کاراکتر باشد.',
        'array'   => ':attribute نباید کمتر از :min آیتم باشد.',
    ],
    'not_in'               => ':attribute انتخاب شده، معتبر نیست.',
    'numeric'              => ':attribute باید شامل عدد باشد.',
    'present'              => 'فیلد :attribute باید در پارامترهای ارسالی وجود داشته باشد.',
    'regex'                => ':attribute یک فرمت معتبر نیست',
    'required'             => 'فیلد :attribute الزامی است',
    'required_if'          => 'فیلد :attribute هنگامی که :other برابر با :value است، الزامیست.',
    'required_unless'      => 'فیلد :attribute ضروری است، مگر آنکه :other در :values وجود داشته باشد.',
    'required_with'        => ':attribute الزامی است زمانی که :values موجود است.',
    'required_with_all'    => ':attribute الزامی است زمانی که :values موجود است.',
    'required_without'     => ':attribute الزامی است زمانی که :values موجود نیست.',
    'required_without_all' => ':attribute الزامی است زمانی که :values موجود نیست.',
    'same'                 => ':attribute و :other باید مانند هم باشند.',
    'size'                 => [
        'numeric' => ':attribute باید برابر با :size باشد.',
        'file'    => ':attribute باید برابر با :size کیلوبایت باشد.',
        'string'  => ':attribute باید برابر با :size کاراکتر باشد.',
        'array'   => ':attribute باسد شامل :size آیتم باشد.',
    ],
    'string'               => 'فیلد :attribute باید یک String باشد.',
    'timezone'             => 'فیلد :attribute باید یک منطقه صحیح باشد.',
    'unique'               => ':attribute قبلا انتخاب شده است.',
    'uploaded'             => 'فایل :attribute با موفقیت آپلود نشد.',
    'url'                  => 'فرمت آدرس :attribute اشتباه است.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom'               => [
        'captcha'  => [
            'required' => 'عبارت امنیتی الزامی است',
            'captcha'  => 'عبارت امنیتی صحیح نمی باشد',
        ],
        'deleteId' => [
            'required' => 'لطفا یک گزینه برای حذف انتخاب کنید',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes'           => [
        'name'                             => 'نام',
        'family'                           => 'نام خانوادگی',
        'username'                         => 'نام کاربری',
        'email'                            => 'پست الکترونیکی',
        'first_name'                       => 'نام',
        'last_name'                        => 'نام خانوادگی',
        'password'                         => 'رمز عبور',
        'password_confirmation'            => 'تاییدیه ی رمز عبور',
        'city'                             => 'شهر',
        'country'                          => 'کشور',
        'address'                          => 'آدرس',
        'phone'                            => 'تلفن',
        'mobile'                           => 'تلفن همراه',
        'age'                              => 'سن',
        'sex'                              => 'جنسیت',
        'gender'                           => 'جنسیت',
        'day'                              => 'روز',
        'month'                            => 'ماه',
        'year'                             => 'سال',
        'hour'                             => 'ساعت',
        'minute'                           => 'دقیقه',
        'second'                           => 'ثانیه',
        'title'                            => 'عنوان',
        'text'                             => 'متن',
        'content'                          => 'محتوا',
        'description'                      => 'توضیحات',
        'excerpt'                          => 'گلچین کردن',
        'date'                             => 'تاریخ',
        'time'                             => 'زمان',
        'available'                        => 'موجود',
        'size'                             => 'سایز',
        'terms'                            => 'شرایط',
        'captcha'                          => 'عبارت امنیتی',
        'file'                             => 'فایل',
        'link'                             => 'لینک',
        'answer'                           => 'پاسخ',
        'picture'                          => 'تصویر',
        'price'                            => 'قیمت',
        'discount'                         => 'مقدار تخفیف',
        'ref_coupon_discount'              => 'مقدار تخفیف',
        'category_id'                      => 'دسته بندی',
        'code'                             => 'کد محصول',
        'discount_type'                    => 'نوع تخفیف',
        'count'                            => 'تعداد',
        'agreement'                        => 'قوانین و مقررات',
        'province_id'                      => 'استان',
        'postal_code'                      => 'کد پستی',
        'name_family'                      => 'نام و نام خانوادگی',
        'track_id'                         => 'کد مرسوله',
        'quantity'                         => 'تعداد',
        'city_id'                          => 'شهر',
        'tel'                              => 'تلفن ثابت',
        'ship_address'                     => 'آدرس سفارش',
        'ship'                             => 'شیوه ارسال',
        'invoice_request'                  => 'درخواست فاکتور',
        'coupon'                           => 'کد تخفیف',
        'payment_type'                     => 'شیوه پرداخت',
        'subject'                          => 'عنوان',
        'old_password'                     => 'رمز عبور قدیم',
        'short_title'                      => 'عنوان کوتاه',
        'sheba'                            => 'شماره شبا',
        'account_number'                   => 'شماره حساب',
        'account_name'                     => 'نام مالک حساب',
        'card_number'                      => 'شماره کارت',
        'bank_name'                        => 'نام بانک',
        'height'                           => 'طول',
        'width'                            => 'عرض',
        'posted_picture'                   => 'تصویر ارسالی کاربر',
        'performed_picture'                => 'تصویر اجرا شده',
        'custom_order_home_picture'        => 'تصویر صفحه اول',
        'ref_coupon_day_expire'            => 'تعداد روز معتبر بودن کوپن',
        'trial_url'                        => 'لینک قسمت توضیحات آبی',
        'shoulder'                         => 'شانه',
        'meter_price'                      => 'قیمت هر متر',
        'border_left'                      => 'حاشیه قاب از چپ',
        'border_top'                       => 'حاشیه قاب از بالا',
        'type'                             => 'نوع',
        'title_fa'                         => 'عنوان فارسی',
        'title_en'                         => 'عنوان انگلیسی',
        'name_en'                          => 'نام انگلیسی',
        'name_fa'                          => 'نام فارسی',
        'phone_code'                       => 'کد شهر',
        'weight'                           => 'وزن',
        'upload_capacity'                  => 'تعداد تصویر آپلودی',
        'capacity'                         => 'ظرفیت',
        'order_count'                      => 'تعداد سفارش',
        'material_id'                      => 'نوع محصول',
        'type_id'                          => 'دسته بندی محصول',
        'has_brain'                        => 'نوع حجمدار/فلت',
        'base_flavor_id'                   => 'طعم پایه',
        'flavor_id'                        => 'طعم مکمل',
        'brain_id'                         => 'مغز',
        'shape_id'                         => 'شکل',
        'weight_id'                        => 'وزن',
        'layout_id'                        => 'لایه',
        'major_price'                      => 'قیمت عمده',
        'admin_title'                      => 'عنوان در مدیریت',
        'cell'                             => 'شماره خانه',
        'preparation_time'                 => 'زمان آماده شدن سفارش',
        'min_order'                        => 'حداقل تعداد سفارش',
        'max_order'                        => 'حداکثر تعداد سفارش',
        'custom_order_picture_coefficient' => 'ضریب محاسبه هزینه آپلود عکس',
        'amount_id'                        =>  'میزان سفارش',
        'package_id'                       =>  'بسته بندی',
        'package_price'                    =>  'قیمت بسته بندی',
        'package_major_price'              =>  'قیمت عمده بسته بندی',
        'flavor_price'                      =>  'قیمت طعم مکمل',
        'flavor_major_price'                =>  'قیمت عمده طعم مکمل',
        'brain_price'                      =>  'قیمت مغزدار',
        'brain_major_price'                =>  'قیمت عمده مغزدار',
        'flavor_brain_price'               =>  'قیمت طعم مکمل مغز دار',
        'flavor_brain_major_price'         =>  'قیمت عمده طعم مکمل مغز دار',
    ],
];
