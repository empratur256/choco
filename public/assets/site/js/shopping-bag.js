
    $(document).ready(function () {
        $('.counter').on('change', function (e) {
            if ($(this).val() < 0) {
                $(this).val(0);
            }
        });

        $('.inc-count').on('click', function (e) {
            e.preventDefault();
            $(this).next().val(parseInt($(this).next().val()) + 1);
            
        });

        $('.dec-count').on('click', function (e) {
            e.preventDefault();
            var value = parseInt($(this).prev().prev('input').val());
            if (value <= 1) {
                return;
            }
            else
            $(this).prev().prev('input').val(value-1);
        });

        $('.submit').on('click', function () {
            $('.update-form').submit();
        });
    })
