$(document).ready(function () {

    $('#adminBreadcrumb').html($("meta[name=adminBreadcrumb]").attr("content"));
    window.ODate = Date;
    window.Date = pDate;

    $('.dateTime').datetimepicker({
        format: 'YYYY-MM-DD LT',
        locale: 'fa',
        sideBySide: true,
        showClose: true,
        showClear: true,
        showTodayButton: true,

    });

    $('.dateTime').on('dp.change', function (e) {
        $($(this).attr('timestamp')).val(e.date.unix());
    });

    $('.date').datetimepicker({
        format: 'YYYY-MM-DD',
        locale: 'fa',
        showClose: true,
        showClear: true,
        showTodayButton: true,
    });

    $('.date').on('dp.change', function (e) {
        $($(this).attr('timestamp')).val(e.date.unix());
    });

    $('.searchPanel').css('min-height', $('.scrollbar-inner').height());


    var drEvent = $('.dropify').dropify({
        messages: {
            'default': 'برای آپلود کلیک کنید',
            'replace': 'برای تغییر کلیک کنید',
            'remove': 'حذف',
            'error': 'مشکلی در آپلود به وجود آمده است.'
        }
    });


    drEvent.on('dropify.afterClear', function (event, element) {
        var name = $(this).attr('remove');
        $('#'+name).val(1);
    });


});
