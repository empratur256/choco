<?php
return [
    'admin'               => 'manager',
    'dashboard'           => 'dashboard',
    'asset_version'       => '1.1',
    // order - alphabet
    'menu_sorting'        => 'order',
    'image'               => [
        // path watermark
        'watermark'          => null,
        'watermark_position' => 'bottom-right',
        //cache time
        'lifetime'           => 60 * 24 * 7,
    ],
    'inactiveTimeout'     => 1000 * 60 * 60,
    'query_time_to_log'   => '1000',
    'day_for_get_coupon'  => 30 * 3,

    // time to remove temp files in hours
    'time_to_remove_temp' => 2,

    // time to remove invalid orders in hours
    'time_to_remove_invalid_order' => 2,

    'domain_fa'           => 'chocofancy.dev',
    'domain_en'           => 'chocofancy-en.dev',
];